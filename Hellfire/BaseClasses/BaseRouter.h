//
//  BaseRouter.h
//  GmIosClient
//
//  Created by Алексей Молокович on 24.03.16.
//  Copyright © 2016 Danil Nurgaliev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GeoCatalogAssembly.h"
#import "GeoCatalogModuleInput.h"
#import "HotseatPlayersListAssembly.h"
#import "HotseatPlayersListModuleInput.h"
@class CDPlayer;
@protocol RamblerViperModuleTransitionHandlerProtocol;
@interface BaseRouter : NSObject

@property (strong, nonatomic) RamblerViperModuleFactory *factory;
@property (strong, nonatomic) GeoCatalogAssembly *geoCatalogAssembly;
@property (nonatomic) HotseatPlayersListAssembly *hotseatPlayersListAssembly;

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

- (void)pushModuleUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                 withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock;

- (void)presentModuleUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                 withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock;

- (void)presentModuleWithNavigationControllerUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                      withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock;
    
- (void)presentModuleUsingFactoryNoAnimation:(RamblerViperModuleFactory *)moduleFactory
                               withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock;

- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player;
- (void)presentRegistration;
- (void)closeCurrentModule;
@end
