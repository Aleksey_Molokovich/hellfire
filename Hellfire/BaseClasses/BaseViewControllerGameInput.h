//
//  BaseViewControllerGameInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 03.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef BaseViewControllerGameInput_h
#define BaseViewControllerGameInput_h

@protocol BaseViewControllerGameInput<NSObject>
- (void)showAlertAfterDisconnected;
- (void)updateBLEStatusView;
- (void)goOut;
- (void)gameOver;
- (void)continueGame;
@end
#endif /* BaseViewControllerGameInput_h */
