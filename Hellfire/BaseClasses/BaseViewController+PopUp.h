//
//  BaseViewController+PopUp.h
//  Hellfire
//
//  Created by Алексей Молокович on 26.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseViewController.h"
#import "PopUpTableView.h"

@interface BaseViewController (BaseViewController_PopUp) <UIGestureRecognizerDelegate>

- (void)configurePopUpWithData:(NSArray *)data output:(id<PopUpTableViewProtocol>)output;
- (void)hidePopUp;
@end
