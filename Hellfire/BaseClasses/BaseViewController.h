//
//  BaseViewController.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewControllerProtocol.h"
#import "LGAlertView.h"

typedef NS_ENUM(NSInteger,BackgroundColorType){
    BackgroundColorClear,
    BackgroundColorGreen,
    BackgroundColorBlue,
    BackgroundColorOrange,
    BackgroundColorGray,
    BackgroundColorWhite
};

static NSString *const HFBackgroundColorClear = @"bg_clear";
static NSString *const HFBackgroundColorGreen = @"bg_green";
static NSString *const HFBackgroundColorGreenHotseat = @"bg_green_hotseat";
static NSString *const HFBackgroundColorBlue = @"bg_blue";
static NSString *const HFBackgroundColorOrange = @"bg_orange";
static NSString *const HFBackgroundColorOrangeHotseat = @"bg_orange_hotseat";
static NSString *const HFBackgroundColorGray = @"bg_gray";
static NSString *const HFBackgroundColorWhite = @"bg_white";


@interface BaseViewController : UIViewController<BaseViewControllerProtocol, LGAlertViewDelegate>
@property(strong, nonatomic) UIView *viewBg;
@property (strong, nonatomic) LGAlertView *alert;
//- (void)showNavigationBarWithColor:(UIColor*)color;
//- (void)showNavigationBarWithImageType:(BackgroundColorType)type;
- (void)prepareNavigationBarAndStatusBar;
- (void)prepareStatusBar;
- (void)prepareStyleWithHFBGColorType:(NSString*)hfColor;
- (void)prepareStatusBarWithHFBGColorType:(NSString*)hfColor;
- (void)prepareNavigationBarWithHFBGColorType:(NSString *)hfColor;
- (void)prepareBackgroundWithHFBGColorType:(NSString *)hfColor;

- (NSInteger)getHeightNavigationBarAndSatusBar;

- (void)hideBackButton;
- (void)setBGImg:(BackgroundColorType)type;

- (void)showShareWithUrl:(NSString*)url text:(NSString*)text;
- (void)showShareWithUrl:(NSString*)url text:(NSString*)text image:(UIImage*)image;
@end
