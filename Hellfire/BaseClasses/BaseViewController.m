//
//  BaseViewController.m
//  Hellfire
//
//  Created by Алексей Молокович on 21.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseViewController.h"
#import "UIColor+HF.h"
#import <LGHelper/UIImage+LGHelper.h>
#import "BaseViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface BaseViewController ()

@property(strong, nonatomic) UIImageView *imageViewBg;
@property(strong, nonatomic) UIImageView *statusBGView;
@property(strong, nonatomic) UIImageView *navigationBGView;
@property(strong, nonatomic) NSString *bgImageName;
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideProgress];
}
- (void)prepareNavigationBarAndStatusBar{
    [self prepareStatusBar];
    [self showNavigationBar];
    [self addViewBG];
}

- (void)prepareStatusBar{
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor clearColor];
    }
}

- (void)prepareStyleWithHFBGColorType:(NSString *)hfColor
{
    [self prepareStatusBarWithHFBGColorType:hfColor];
    [self prepareNavigationBarWithHFBGColorType:hfColor];
    [self prepareBackgroundWithHFBGColorType:hfColor];
}

- (void)prepareStatusBarWithHFBGColorType:(NSString *)hfColor
{
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor clearColor];
    }
    
    NSString *sbImageName = [hfColor stringByAppendingString:@"_sb"];
    NSInteger heightSB=[UIApplication sharedApplication].statusBarFrame.size.height;

    CGRect frame=CGRectMake(0, 0, self.view.frame.size.width, heightSB);
    
    if (!_statusBGView) {
        _statusBGView = [UIImageView new];
        _statusBGView.frame = frame;
        _statusBGView.clipsToBounds=YES;
        _statusBGView.image = [UIImage imageNamed:sbImageName];
        _statusBGView.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:_statusBGView];
    }

    
}

- (void)prepareBackgroundWithHFBGColorType:(NSString *)hfColor
{
    if (hfColor && !_imageViewBg) {
        _imageViewBg=[UIImageView new] ;
        [self.view addSubview:_imageViewBg];
        UIView *lastview=[self.view.subviews lastObject];
        UIView *firstView=[self.view.subviews firstObject];
        [self.view insertSubview:lastview
                    belowSubview:firstView];
        [firstView  sendSubviewToBack:lastview];
    }
    _imageViewBg.image = [UIImage imageNamed:hfColor];
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    _imageViewBg.frame=CGRectMake(0, -65, frame.size.width, frame.size.height +65 );
}

- (void)prepareNavigationBarWithHFBGColorType:(NSString *)hfColor
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.hidden=NO;
    
    NSString *sbImageName = [hfColor stringByAppendingString:@"_nb"];
    NSInteger heightNB=self.navigationController.navigationBar.frame.size.height;
    if (self.navigationController.navigationBar.hidden)
    {
        heightNB=0;
        _navigationBGView = nil;
        return;
    }
    NSInteger heightSB=[UIApplication sharedApplication].statusBarFrame.size.height;

    CGRect frame = CGRectMake(0, heightSB, self.navigationController.navigationBar.frame.size.width, heightNB);


    if (!_navigationBGView) {
        _navigationBGView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:sbImageName]];
        _navigationBGView.clipsToBounds=YES;
        _navigationBGView.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:_navigationBGView];
        _navigationBGView.frame =frame;
        _navigationBGView.layer.zPosition=1;
    }
    _navigationBGView.image = [UIImage imageNamed:sbImageName];
}


-(NSInteger)getHeightNavigationBarAndSatusBar{
    NSInteger heightNB=self.navigationController.navigationBar.frame.size.height;
    if (self.navigationController.navigationBar.hidden) {
        heightNB=0;
    }
    
    NSInteger heightSB=[UIApplication sharedApplication].statusBarFrame.size.height;
    return heightNB+heightSB;
}
#pragma mark ConfigureStatus



#pragma mark ConfigureNavigation
- (void)showNavigationBarWithImageType:(BackgroundColorType)type
{
    
}

- (void)showNavigationBarWithColor:(UIColor*)color
{
    
}

- (void)showNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    self.navigationController.navigationBar.hidden=NO;
    [self updateViwBG];
}

- (void)hideNaviagtionBar{
    [_navigationBGView removeFromSuperview];
    _navigationBGView=nil;
    self.navigationController.navigationBar.hidden=YES;
    [self updateViwBG];
}

- (void)hideBackButton{
    self.navigationItem.hidesBackButton = YES;
}

#pragma mark ConfigureNavigationBG

- (void)setBGImg:(BackgroundColorType)type{
    switch (type) {
        case BackgroundColorBlue:
            _bgImageName=@"bg_blue";
            break;
        case BackgroundColorGreen:
            _bgImageName=@"bg_green";
            break;
        case BackgroundColorOrange:
            _bgImageName=@"bg_orange";
            break;
        case BackgroundColorGray:
            _bgImageName=@"bg_gray";
            break;
        case BackgroundColorClear:
        case BackgroundColorWhite:
            _bgImageName=nil;
            break;
            
        default:
            break;
    }
    if (_bgImageName.length && !_imageViewBg) {
        _imageViewBg=[[UIImageView alloc] initWithImage:[UIImage imageNamed:_bgImageName]];
        [self.view addSubview:_imageViewBg];
        UIView *lastview=[self.view.subviews lastObject];
        UIView *firstView=[self.view.subviews firstObject];
        [self.view insertSubview:lastview
                    belowSubview:firstView];
        [firstView  sendSubviewToBack:lastview];
    }
    CGRect frame = [[[UIApplication sharedApplication] delegate] window].frame;
    _imageViewBg.frame=CGRectMake(0, -65, frame.size.width, frame.size.height +65 );
    
}



- (void)addViewBG{
    if (!_viewBg) {
        _viewBg=[UIView new];
        [self.view addSubview:_viewBg];
        _viewBg.layer.zPosition=1;
        [_viewBg setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:_bgImageName]]];
    }
    
    [self updateViwBG];
}

- (void)updateViwBG{
    CGRect frame=_viewBg.frame;
    frame.size=CGSizeMake(self.view.frame.size.width, [self getHeightNavigationBarAndSatusBar]);
    frame.origin.x=0;
    frame.origin.y=0;
    _viewBg.frame=frame;
}


- (void)showBottomBar{
    self.tabBarController.tabBar.hidden=NO;
}

- (void)hideBottomBar{
    self.tabBarController.tabBar.hidden=YES;
}


- (void)showProgressWithTitle:(NSString*)title message:(NSString*)message{
    _alert=[LGAlertView alertViewWithActivityIndicatorAndTitle:title
                                                      message:message
                                                        style:LGAlertViewStyleAlert
                                            progressLabelText:nil
                                                 buttonTitles:nil
                                            cancelButtonTitle:@"Отмена"
                                        destructiveButtonTitle:nil];
    [_alert showAnimated];
    
}

- (void)hideProgressWithTitleAndMessage{
    [_alert dismissAnimated];
}

- (void)showUploadProgressWithColor:(UIColor*)color
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setForegroundColor:color];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
    
}

- (void)updateUploadProgress:(float)progress
{
    [SVProgressHUD showProgress:progress];
}

- (void)showProgressWithColor:(UIColor*)color
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
        [SVProgressHUD setForegroundColor:color];
        [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        [SVProgressHUD show];
    }]; 
}



- (void)hideProgress
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [SVProgressHUD dismiss];
    }];
    
}


- (void)showAlertWithMessage:(NSString *)message{
    [self showAlertWithMessage:message tag:0];
}

- (void)showAlert2ButtonsWithMessage:(NSString *)message tag:(NSInteger)tag
{
    _alert = [[LGAlertView alloc] initWithTitle:nil
                                        message:message
                                          style:LGAlertViewStyleAlert
                                   buttonTitles:@[@"Пройти",@"Отмена"]
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil] ;
    _alert.delegate = self;
    _alert.tag = tag;
    [_alert showAnimated];
}

- (void)showAlertWithMessage:(NSString *)message tag:(NSInteger)tag
{
    _alert = [[LGAlertView alloc] initWithTitle:nil
                                        message:message
                                          style:LGAlertViewStyleActionSheet
                                   buttonTitles:@[@"OK"]
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil] ;
    _alert.tag = tag;
    [_alert showAnimated];
}


- (void)showShareWithUrl:(NSString*)url text:(NSString*)text{
    
    [self showShareWithUrl:url text:text image:nil];
   
    
}

- (void)showShareWithUrl:(NSString*)url text:(NSString*)text image:(UIImage*)image{
    
    NSMutableArray *objectsToShare = [NSMutableArray new];
    
    
    if(url && [NSURL URLWithString:url])
        [objectsToShare addObject:[NSURL URLWithString:url]];

    if(text)
        [objectsToShare addObject:text];
    
    
    if (image) {
        objectsToShare = [NSMutableArray new];
        [objectsToShare addObject:image];
    }
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    if ( [activityVC respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        
        activityVC.popoverPresentationController.sourceView=self.view;
        
    }
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


@end
