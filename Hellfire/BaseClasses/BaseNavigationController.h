//
//  BaseNavigationController.h
//  Hellfire
//
//  Created by Алексей Молокович on 24.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController

@end
