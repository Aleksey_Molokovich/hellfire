//
//  BaseView.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

-(instancetype)init{
    self = [super init];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (void)setup{
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.view];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    
    self.view.backgroundColor=[UIColor clearColor];
}

-(NSInteger)expectedHeight
{
    return self.view.frame.size.height;
}

@end
