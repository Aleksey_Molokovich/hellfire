//
//  BaseView.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewProtocol.h"

@interface BaseView : UIView<BaseViewProtocol>
@property (strong, nonatomic) IBOutlet UIView *view;
- (void)setup;
- (void)configureWithData:(id)data;

@end
