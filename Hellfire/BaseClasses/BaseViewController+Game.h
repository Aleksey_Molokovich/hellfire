//
//  BaseViewController+Game.h
//  Hellfire
//
//  Created by Алексей Молокович on 24.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseViewController.h"
#import "BottomBluetoothStatusView.h"
#import "BluetoothService.h"
#import "BaseViewControllerGameInput.h"
@interface BaseViewController (BaseViewController_Game)<BaseViewControllerGameInput>

@property (nonatomic, strong) BottomBluetoothStatusView *bottomBLEStatusView;


@end
