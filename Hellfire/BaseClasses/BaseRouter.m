//
//  BaseRouter.m
//  GmIosClient
//
//  Created by Алексей Молокович on 24.03.16.
//  Copyright © 2016 Danil Nurgaliev. All rights reserved.
//

#import "BaseRouter.h"
#import "BaseNavigationController.h"
@implementation BaseRouter
#pragma mark - Transition template

- (void)pushModuleUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                 withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock
{
    [[self.transitionHandler openModuleUsingFactory:moduleFactory
                                withTransitionBlock:^(id <RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler,
                                                      id <RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {

                                    UIViewController *sourceViewController = (id) sourceModuleTransitionHandler;
                                    UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;

                                    
                                      [sourceViewController.navigationController pushViewController:destinationViewController animated:YES];
                                    
                                    
                                    

                                }] thenChainUsingBlock:linkBlock];
}

- (void)presentModuleUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                    withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock
{
    [[self.transitionHandler openModuleUsingFactory:moduleFactory
                                withTransitionBlock:^(id <RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler,
                                                      id <RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {

                                    UIViewController *sourceViewController = (id) sourceModuleTransitionHandler;
                                    UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;

                                    [sourceViewController presentViewController:destinationViewController animated:YES completion:nil];
                                    
                                }] thenChainUsingBlock:linkBlock];
}

- (void)presentModuleWithNavigationControllerUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                    withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock
{
    [[self.transitionHandler openModuleUsingFactory:moduleFactory
                                withTransitionBlock:^(id <RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler,
                                                      id <RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
                                    
                                    UIViewController *sourceViewController = (id) sourceModuleTransitionHandler;
                                    UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;
                                    
                                    BaseNavigationController *nc = [[BaseNavigationController alloc] initWithRootViewController:destinationViewController];
                                    nc.navigationBar.hidden=YES;
                                    [sourceViewController presentViewController:nc animated:YES completion:nil];
                                    
                                }] thenChainUsingBlock:linkBlock];
}

- (void)presentModuleNoAnimationUsingFactory:(RamblerViperModuleFactory *)moduleFactory
                    withLinkBlock:(RamblerViperModuleLinkBlock)linkBlock
{
    [[self.transitionHandler openModuleUsingFactory:moduleFactory
                                withTransitionBlock:^(id <RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler,
                                                      id <RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
                                    
                                    UIViewController *sourceViewController = (id) sourceModuleTransitionHandler;
                                    UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;
                                    
                                    [sourceViewController presentViewController:destinationViewController animated:NO completion:nil];
                                    
                                }] thenChainUsingBlock:linkBlock];
}

- (void)presentRegistration
{
    self.geoCatalogAssembly = [[GeoCatalogAssembly new] activated];
    self.factory = [self.geoCatalogAssembly factoryGeoCatalog];
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<GeoCatalogModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithType:CatalogCountry style:CatalogStyleDark action:CatalogActionRegistration
                                                   country:nil];
                                                  return nil;
                                              }];
}

- (void)closeCurrentModule
{
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player
{
    _hotseatPlayersListAssembly=[[HotseatPlayersListAssembly new] activated];
    self.factory=[_hotseatPlayersListAssembly factoryHotseatPlayersList];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<HotseatPlayersListModuleInput> moduleInput) {
                       [moduleInput configureModuleWithPlayer:player];
                       return nil;
                   }];
}
@end
