//
//  BaseViewController+Game.m
//  Hellfire
//
//  Created by Алексей Молокович on 24.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseViewController+Game.h"
#import <objc/runtime.h>

@implementation BaseViewController (BaseViewController_Game)


- (void)goOut
{
    __weak typeof(self) weakSelf = self;
    
    [[LGAlertView alertViewWithTitle:@"Внимание"
                             message:@"Результаты игры не будут сохранены, Вы хотите покинуть игру?"
                               style:LGAlertViewStyleAlert
                        buttonTitles:@[@"Покинуть"]
                   cancelButtonTitle:@"Отмена"
              destructiveButtonTitle:nil
                       actionHandler:^(LGAlertView * _Nonnull alertView, NSUInteger index, NSString * _Nullable title) {

                           __strong typeof(weakSelf) blockSelf = weakSelf;
if (index == 0) {
                               
                               [blockSelf gameOver];
                           }
                        
                       } cancelHandler:^(LGAlertView * _Nonnull alertView) {
                           __strong typeof(weakSelf) blockSelf = weakSelf;

                           [blockSelf continueGame];
                       }
                  destructiveHandler:nil] showAnimated];
}

- (void)gameOver
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)continueGame
{
    
}

- (void) changeConnectionState:(NSNotification*)notification
{
    [self updateBLEStatusView];
}


- (void)showAlertAfterDisconnected
{
    if (![[self.navigationController.viewControllers lastObject] isEqual:self])
    {
        return;
    }
    
    [[LGAlertView alertViewWithTitle:@"Внимание"
                             message:@"Связь с устройством потерена. Результаты игры не будут сохранены."
                               style:LGAlertViewStyleAlert
                        buttonTitles:@[@"Ok"]
                   cancelButtonTitle:nil
              destructiveButtonTitle:nil
                       actionHandler:^(LGAlertView * _Nonnull alertView, NSUInteger index, NSString * _Nullable title) {
                           if (index == 0) {
                               [self.navigationController popToRootViewControllerAnimated:YES];
                               
                           }
                       } cancelHandler:nil
                  destructiveHandler:nil] showAnimated];
}

- (void)updateBLEStatusView
{
    if (!self.bottomBLEStatusView)
    {
        self.bottomBLEStatusView = [BottomBluetoothStatusView new];
        [self.view addSubview:self.bottomBLEStatusView];
        
    }
    
    
    CGRect frame = self.view.frame;
    frame.origin.y = frame.size.height - 32;
    frame.size.height = 32;
    self.bottomBLEStatusView.frame = frame;
    
    [self.bottomBLEStatusView connectcedToDevice:[[BluetoothService sharedInstance] connectedPeripheral].name];
}

- (BottomBluetoothStatusView *)bottomBLEStatusView {
    return objc_getAssociatedObject(self, @selector(bottomBLEStatusView));
}

- (void)setBottomBLEStatusView:(BottomBluetoothStatusView *)unicorn {
    objc_setAssociatedObject(self, @selector(bottomBLEStatusView), unicorn, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
