//
//  BaseTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCellProtocol.h"
#import "CallBackActionProtocol.h"

@interface BaseTableViewCell : UITableViewCell<BaseTableViewCellProtocol>
@property (strong, nonatomic) NSIndexPath *cellIndex;
- (void)configureWithData:(id)data;
@property (weak, nonatomic) id<CallBackActionProtocol> output;

- (CGFloat)estimateHeight;

@end
