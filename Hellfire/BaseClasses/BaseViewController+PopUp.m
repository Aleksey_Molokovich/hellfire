//
//  BaseViewController+PopUp.m
//  Hellfire
//
//  Created by Алексей Молокович on 26.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseViewController+PopUp.h"
#import "PopUpTableViewProtocol.h"
#import <objc/runtime.h>

@interface BaseViewController ()
@property (strong, nonatomic)  PopUpTableView *popUp;

@end
@implementation BaseViewController (BaseViewController_PopUp)

- (void)configurePopUpWithData:(NSArray *)data output:(id<PopUpTableViewProtocol>)output
{
    UIView *view = [self.view viewWithTag:111];
    if (!view)
    {
        view = [[UIView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:view];
        UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"bg_clear"]];
        [view addSubview:imageView];
        imageView.frame = view.frame;
        view.tag = 111;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopUp)];
        tap.delegate = self;
        [view addGestureRecognizer:tap];
        self.popUp = [[PopUpTableView alloc] init];
        self.popUp.tag = 1111;
        [view addSubview:self.popUp];
        
    }
    NSInteger height = data.count*40;
    self.popUp.frame = CGRectMake(self.view.center.x-100,
                              self.view.center.y-height/2,
                              200, height);
    self.popUp.output = output;
    [self.popUp configureWithData:data];

}

- (void)hidePopUp
{
    if ([self.view viewWithTag:111])
    {
        [[self.view viewWithTag:111] removeFromSuperview];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:[self.view viewWithTag:1111]]) {
        return NO;
    }
    return YES;
}

- (PopUpTableView *)popUp{
    return objc_getAssociatedObject(self, @selector(popUp));
}

- (void)setPopUp:(PopUpTableView *)popUp{
    objc_setAssociatedObject(self, @selector(popUp), popUp, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
