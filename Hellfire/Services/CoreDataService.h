//
//  CoreDataService.h
//  Hellfire
//
//  Created by Алексей Молокович on 14.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataService : NSObject

@property (nonatomic) NSManagedObjectContext *friends;
@property (nonatomic) NSManagedObjectContext *settings;



+ (instancetype)sharedInstance;
@end
