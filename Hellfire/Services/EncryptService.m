
//
//  EncryptService.m
//  Hellfire
//
//  Created by Алексей Молокович on 07.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "EncryptService.h"
#import "NSString+MD5.h"

@implementation EncryptService

+(NSString *)encryptFromDictionary:(NSDictionary *)params
{
    NSDictionary * dic = [params copy];
    NSMutableArray *order = [NSMutableArray new];
    for (id value in [dic allValues]) {
        
        if ([self toString:value])
            [order addObject:[[self toString:value] MD5]];
    
        
        if ([value isKindOfClass:[NSArray class]]) {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:value options:0 error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

            
            [order addObject:[jsonString MD5]];
        }
    }
    

    NSArray *sortedArray = [order sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray* reversedArray = [[sortedArray reverseObjectEnumerator] allObjects];
    
    NSString *result = @"94a08da1fecbb6e8b46990538c7b50b2";
    
    for (NSString *str in reversedArray) {
        result = [result stringByAppendingString:str];
    }
    
//    return @"94a08da1fecbb6e8b46990538c7b50b2";//[result MD5];
    return [result MD5];
}

+ (NSString*)toString:(id)value
{
    NSString *str;
    
    if ([value isKindOfClass:[NSString class]]) {
        str = value;
    }
    
    if ([value isKindOfClass:[NSNumber class]]) {
        str = [value stringValue];
    }
    if (!str) {
        
    }
    return str;
}

@end
