//
//  HTTPService.h
//  DiaryCulture
//
//  Created by Алексей Молокович on 08.07.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <AFSessionOperation/AsynchronousOperation.h>
@class ResponseModel;
typedef void (^ResponseCompletion)(ResponseModel *responseObject, NSError *error);


static NSString *const HTTPKeyUploadData=@"uploadData";

@interface HTTPService : NSObject

+ (void)postRequestWithUrl:(NSString *)url
                                    andParams:(NSDictionary *)params
                                   completion:(ResponseCompletion)completion;

+ (void)getRequestWithUrl:(NSString *)url
                                    andParams:(NSDictionary *)params
                                   completion:(ResponseCompletion)completion;

+(void)uploadImage:(NSData*)data
           withURL:(NSString*)url
          progress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock
        completion:(ResponseCompletion)completion;

+(NSString*)baseURL;
@end
