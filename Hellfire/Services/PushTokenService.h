//
//  PushTokenService.h
//  GmIosClient
//
//  Created by Arthur Bikmullin on 27.01.16.
//  Copyright © 2016 Danil Nurgaliev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushTokenService : NSObject

@property (strong, nonatomic) NSData *deviceToken;

+ (instancetype)sharedInstance;

- (void)sendPushToken;

@end
