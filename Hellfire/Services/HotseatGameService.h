//
//  HotseatGameService.h
//  Hellfire
//
//  Created by Алексей Молокович on 08.04.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;

typedef NS_ENUM(NSInteger, HotseatGameServiceStatus) {
    HotseatGameServiceStatusReadyToCreateGame,
    HotseatGameServiceStatusCreatingGame,
    HotseatGameServiceStatusReadyToPlay,
    HotseatGameServiceStatusReject
};

@interface HotseatGameService : NSObject

+ (instancetype)sharedInstance;
- (BOOL)checkForHotseatGame;
- (void)createHotseatGameWithSelectedPlayer:(CDPlayer*)player completion:(void (^)(BOOL isCreated))completion;

@property (nonatomic, assign) HotseatGameServiceStatus status;
@end
