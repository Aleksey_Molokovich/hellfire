//
//  HotseatGameService.m
//  Hellfire
//
//  Created by Алексей Молокович on 08.04.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatGameService.h"

#import "UserRepository.h"
#import "GameRepository.h"

#import "GameRequest.h"
#import "CDGameResults+CoreDataClass.h"
#import "HFGameSettings.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "BluetoothService.h"

@implementation HotseatGameService

+ (instancetype)sharedInstance {
    static HotseatGameService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}


- (BOOL)checkForHotseatGame
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld", PlayerHotseatStatusAccept, PlayerHotseatStatusAcceptLock];
    
    NSArray *hotseatPlayers = [CDHotseatPlayer MR_findAllWithPredicate:predicate];
    
    if ([hotseatPlayers count]>1) {
        return YES;
    }
    
    return NO;
}

- (void)createHotseatGameWithSelectedPlayer:(CDPlayer*)selectedPlayer completion:(void (^)(BOOL isCreated))completion
{
    if (self.status == HotseatGameServiceStatusCreatingGame ||
        self.status == HotseatGameServiceStatusReadyToPlay) {
        completion(YES);
        return;
    }
    
    NSInteger selectedId = selectedPlayer.id;
    
    if (selectedPlayer) {
        [CDHotseatPlayer MR_truncateAll];
        [CDGameResults MR_truncateAll];
        [HFGameSettings load].gameId = 0;
    }
    
    self.status = HotseatGameServiceStatusCreatingGame;
    
    __weak typeof(self) weakSelf = self;
    
    [UserRepository friendsWithParam:nil
                          completion:^(NSArray *results)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == 1 "];
         NSArray *players = [results filteredArrayUsingPredicate:predicate];
         if ([players count]>0) {
             
             [CDHotseatPlayer MR_truncateAll];
             [CDGameResults MR_truncateAll];
             
             for (CDPlayer *player in players) {
                 CDHotseatPlayer *hPlayer = [CDHotseatPlayer MR_createEntity];
                 hPlayer.id = player.id;
                 hPlayer.avatarPath = player.avatarPath;
                 hPlayer.status = PlayerHotseatStatusNone;
                 if (selectedId == player.id) {
                     hPlayer.status = PlayerHotseatStatusMarked;
                 }
                 hPlayer.hotseatPoints = player.hotseatPoints;
                 hPlayer.soloPoints = player.soloPoints;
                 hPlayer.userName = player.userName;
             }
             
             [blockSelf createHotseatGameCompletion:^(BOOL isCreated) {
                 completion(isCreated);
                 blockSelf.status = HotseatGameServiceStatusReadyToPlay;
             }];
         }
         else
         {
            [self addSelfToPlayer];
             completion(NO);
             blockSelf.status = HotseatGameServiceStatusReadyToCreateGame;
         }
     }];
}

- (void)addSelfToPlayer{
    CDHotseatPlayer *me = [CDHotseatPlayer MR_createEntity];
    me.avatarPath = [CDAccount MR_findFirst].avatarPath;
    me.status = PlayerHotseatStatusAcceptLock;
    me.id = [CDAccount MR_findFirst].user.id;
    me.index = -1;
    me.currentPlayer = YES;
    me.userName = [CDAccount MR_findFirst].user.nickname;
}

- (void)createHotseatGameCompletion:(void (^)(BOOL))completion
{
    [self addSelfToPlayer];
    GameRequest *gameRequest = [GameRequest new];
    gameRequest.hfPipeSerialNumber = [BluetoothService sharedInstance].macAddressPeripheral?[BluetoothService sharedInstance].macAddressPeripheral:@"1";

    [GameRepository createHotseat:gameRequest completion:^(NSNumber *receive) {
        [HFGameSettings load].gameId = [receive intValue];
        if (receive) {
            completion(YES);
        }else{
            completion(NO);
        }
        
    }];
}


@end
