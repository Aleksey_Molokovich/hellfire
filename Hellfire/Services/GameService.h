//
//  GameService.h
//  Hellfire
//
//  Created by Алексей Молокович on 10.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFGameSettings.h"

@protocol GameServiceProtocol <NSObject>

- (void)updateTimerWithSeconds:(NSInteger)seconds;

@end
@class CDHotseatPlayer;
@class CDGameResults;
@class HFSoloGameResult;
@class HFHotseatGameResult;
@class HFHotseatPlayer;
@class CDPlayer;

@protocol GameServiceProtocol;
@interface GameService : NSObject
//+(instancetype)shared;

@property (nonatomic, strong) id<GameServiceProtocol> delegate;



- (void)startWithDuration:(NSInteger)duration;
- (void)stop;
- (NSInteger)currentSeccond;

- (BOOL)isSoloGame;

- (CDHotseatPlayer*)changeCurrentHotseatPlayer;
- (CDHotseatPlayer*)nextHotseatPlayer;
- (CDHotseatPlayer*)currentHotseatPlayer;

- (CDGameResults*)currentGameResults;
- (CDGameResults*)bestQualityResults;
- (CDGameResults*)bestSpeedResults;
- (CDGameResults*)bestPointResults;

- (NSInteger)calculateAllPointsForCurrentPlayer;
- (NSInteger)getCurrentRound;
- (NSInteger)getCountRounds;
- (BOOL)isGameOver;
- (BOOL)nextRound;

- (void)sendCurrentHotseatRoundResultsWithCompletion:(void (^)(HFHotseatGameResult *result))completion;
- (void)sendSoloGameResultsWithCompletion:(void (^)(HFSoloGameResult *))completion;

- (void)shareSoloGame:(NSNumber*)gameId completion:(void (^)(HFSoloGameResult *))completion;

@end
