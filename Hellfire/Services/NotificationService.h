//
//  NotificationService.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HFNotification;
@interface NotificationService : NSObject

@property (nonatomic, assign) BOOL needToSave;
@property (nonatomic) NSDictionary *push;

+ (instancetype)shared;

- (void)openWithPush:(NSDictionary*)push;
- (void)updateWithPush:(NSDictionary*)push;

@end
