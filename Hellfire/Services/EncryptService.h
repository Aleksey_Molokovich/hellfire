//
//  EncryptService.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptService : NSObject

+ (NSString*)encryptFromDictionary:(NSDictionary*)dic;

@end
