//
//  AccountService.h
//  Hellfire
//
//  Created by Алексей Молокович on 11.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HellfireDefines.h"

@class CDUser;
@class CDAccount;
@interface AccountService : NSObject

+(AccountStateType)state;
+(CDUser*)user;
+(CDAccount*)account;
+(BOOL)itMyId:(NSInteger)userId;
+(void)createAvatarWithImage:(UIImage*)image;
+(void)createAvatarWithColor;
+(void)deleteAvatar;
+(void)clearAllEntityWithAccount:(BOOL)withAccount;
+(void)createFilterSettingsForPlayers:(BOOL)forPlayers;
@end
