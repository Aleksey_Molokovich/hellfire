//
//  AccountService.m
//  Hellfire
//
//  Created by Алексей Молокович on 11.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <LGHelper/UIColor+LGHelper.h>

#import "AccountService.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import <LGHelper/UIImage+LGHelper.h>
#import "UIColor+HF.h"
#import "UIFont+HF.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "CDFilterSetting+CoreDataClass.h"
#import "CDSortOfFilter+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
#import "FilterForPlayersAndClansActions.h"
#import "CDUser+CoreDataClass.h"
#import "HFFilterModel.h"
#import "HellfireDefines.h"

@implementation AccountService

+(AccountStateType)state
{
    CDAccount *account = [CDAccount MR_findFirst];
    return account.state;
}

+(CDAccount *)account
{
    return [CDAccount MR_findFirst];
}
+(CDUser*)user
{
    CDAccount *account = [CDAccount MR_findFirst];
    return account.user;
}

+(BOOL)itMyId:(NSInteger)userId
{
    return ([self user].id == userId);
}

+(void)createFilterSettingsForPlayers:(BOOL)forPlayers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"filterForPlayers = %@ and saved = 0", @(forPlayers)];
    CDFilterSetting *filter = [CDFilterSetting MR_findFirstWithPredicate:predicate];
    if (!filter) {
        filter = [self newFilterForPLayers:forPlayers];
        filter.saved = NO;
        filter.includeMy = YES;
    }
    
    if (forPlayers) {
        HFFilterModel *filterSettings =[[HFFilterModel alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:forPlayers?FilterPlayer:FilterClan]
                                                                           error:nil] ;
        filter.city = [CDCatalog MR_createEntity];
        filter.city.id = filterSettings?filterSettings.cityId:[[CDAccount MR_findFirst] city].id;
        filter.city.name = filterSettings?filterSettings.cityName:[[CDAccount MR_findFirst] city].name;
        
        filter.country = [CDCatalog MR_createEntity];
        filter.country.id = filterSettings?filterSettings.countryId:[[CDAccount MR_findFirst] country].id;
        filter.country.name = filterSettings?filterSettings.countryName:[[CDAccount MR_findFirst] country].name;
        
    }
    
    
    
    predicate = [NSPredicate predicateWithFormat:@"filterForPlayers = %@ and saved = 1", @(forPlayers)];
    CDFilterSetting *filterSaved = [CDFilterSetting MR_findFirstWithPredicate:predicate];
    if (!filterSaved) {
        filterSaved = [self newFilterForPLayers:forPlayers];
        filterSaved.saved = YES;
        filterSaved.includeMy = YES;
    }
    
    if (forPlayers) {
        HFFilterModel *filterSettings =[[HFFilterModel alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:forPlayers?FilterPlayer:FilterClan]
                                                                           error:nil] ;
        filterSaved.city = [CDCatalog MR_createEntity];
        filterSaved.city.id = filterSettings?filterSettings.cityId:[[CDAccount MR_findFirst] city].id;
        filterSaved.city.name = filterSettings?filterSettings.cityName:[[CDAccount MR_findFirst] city].name;
        
        filterSaved.country = [CDCatalog MR_createEntity];
        filterSaved.country.id = filterSettings?filterSettings.countryId:[[CDAccount MR_findFirst] country].id;
        filterSaved.country.name = filterSettings?filterSettings.countryName:[[CDAccount MR_findFirst] country].name;
        
    }
    
}

+(CDFilterSetting*)newFilterForPLayers:(BOOL)forPlayers
{
    CDFilterSetting *filter = [CDFilterSetting MR_createEntity];
    filter.filterForPlayers = forPlayers;
    filter.byName = [CDSortOfFilter MR_createEntity];
    filter.byName.title =forPlayers? kKindOfSortTypeByName : kKindOfSortTypeByClan;
    filter.byName.selected = YES;
    filter.byName.ascending = YES;
    filter.bySoloPoints = [CDSortOfFilter MR_createEntity];
    filter.bySoloPoints.title = kKindOfSortTypeBySoloPoints;
    filter.bySoloPoints.selected = NO;
    filter.byHotseatPoints = [CDSortOfFilter MR_createEntity];
    filter.byHotseatPoints.title = kKindOfSortTypeByHotseatPoints;
    if (!forPlayers) {
        filter.byMembers = [CDSortOfFilter MR_createEntity];
        filter.byMembers.title = kKindOfSortClanTypeByAmountOfMembers;
        filter.byMembers.selected = NO;
        
    }
    
    if (forPlayers) {
        HFFilterModel *filterSettings =[[HFFilterModel alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:forPlayers?FilterPlayer:FilterClan]
                                                                           error:nil] ;
        filter.city = [CDCatalog MR_createEntity];
        filter.city.id = filterSettings?filterSettings.cityId:[[CDAccount MR_findFirst] city].id;
        filter.city.name = filterSettings?filterSettings.cityName:[[CDAccount MR_findFirst] city].name;
        
        filter.country = [CDCatalog MR_createEntity];
        filter.country.id = filterSettings?filterSettings.countryId:[[CDAccount MR_findFirst] country].id;
        filter.country.name = filterSettings?filterSettings.countryName:[[CDAccount MR_findFirst] country].name;
        filter.includeMy = YES;
    }
    
    return filter;
}

+(void)createAvatarWithImage:(UIImage*)image
{
    UIImage *smallImage = [image imageScaledToSize:CGSizeMake(40, 40) scalingMode:LGImageScalingModeAspectFill];
    UIImage *circleImage = [self circularScaleAndCropImage:smallImage];
    [self imageByDrawingCircleOnImage:circleImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"avatar.png"];
    
    [UIImagePNGRepresentation(circleImage) writeToFile:filePath atomically:YES];
    
    filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"avatar_big.png"];
    
    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
}

+ (UIImage*)circularScaleAndCropImage:(UIImage*)image  {
    // This function returns a newImage, based on image, that has been:
    // - scaled to fit in (CGRect) rect
    // - and cropped within a circle of radius: rectWidth/2
    
    //Create the bitmap graphics context
    
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGRect rect = CGRectMake(0, 0, 40, 40);
    
    // Create and CLIP to a CIRCULAR Path
    // (This could be replaced with any closed path if you want a different shaped clip)
    CGPathRef path = CGPathCreateWithEllipseInRect(rect, NULL);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGPathRelease(path);
    
    //Set the SCALE factor for the graphics context
    //All future draw calls will be scaled by this factor
    CGContextScaleCTM (context, 1, 1);
    
    // Draw the IMAGE
    [image drawInRect:rect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(void)createAvatarWithColor
{
    UIColor *color = [self randomColor];
    
    CDAccount *account = [CDAccount MR_findFirst];
    account.colorAvatar = [color hex];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
    }];
    
    [self createAvatarWithSize:CGSizeMake(40, 40)
                      fontSize:20
                         color:color
                        circle:YES
                          name:@"avatar.png"];
    
    
    [self createAvatarWithSize:CGSizeMake(1024, 1024)
                      fontSize:300
                         color:color
                        circle:NO
                          name:@"avatar_big.png"];
}


+(void)createAvatarWithSize:(CGSize)size
                   fontSize:(NSInteger)fontSize
                      color:(UIColor*)color
                     circle:(BOOL) isCircle
                       name:(NSString*)name
{
    CGRect rect = CGRectMake(0, 0, 40, 40);
    rect.size = size;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (isCircle) {
        CGPathRef path = CGPathCreateWithEllipseInRect(rect, NULL);
        CGContextAddPath(context, path);
        CGContextClip(context);
        CGPathRelease(path);
    }
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    
    CGContextFillRect(context, rect);

    
    NSDictionary *textAttributes = @{
                                     NSFontAttributeName: [UIFont hfRegularSize:fontSize],
                                     NSForegroundColorAttributeName: [UIColor whiteColor]
                                     };
    
    NSString *text=[self letters];
    
    
    CGSize textSize = [text sizeWithAttributes:textAttributes];
    
    [ text drawInRect:CGRectMake(rect.size.width/2 - textSize.width/2,
                                 rect.size.height/2 - textSize.height/2,
                                 textSize.width,
                                 textSize.height)
       withAttributes:textAttributes];
    
    UIImage *theImage=UIGraphicsGetImageFromCurrentImageContext();   // extract the image
    
    
    
    UIGraphicsEndImageContext();
    
    if (isCircle) {
        [self imageByDrawingCircleOnImage:theImage];
    }
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:name];
    
    // Save image.
    [UIImagePNGRepresentation(theImage) writeToFile:filePath atomically:YES];
}



+ (void)imageByDrawingCircleOnImage:(UIImage *)image
{
    // begin a graphics context of sufficient size
    UIGraphicsBeginImageContext(image.size);
    
    // draw original image into the context
    [image drawAtPoint:CGPointZero];
    
    // get the context for CoreGraphics
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // set stroking color and draw circle
    [[UIColor whiteColor] setStroke];
    
    // make circle rect 5 px from border
    CGRect circleRect = CGRectMake(0, 0,
                                   image.size.width,
                                   image.size.height);
    circleRect = CGRectInset(circleRect, 1, 1);
    
    // draw circle
    CGContextStrokeEllipseInRect(ctx, circleRect);
    
    // make image out of bitmap context
    UIImage *retImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // free the context
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"avatar_selected.png"];
    
    // Save image.
    [UIImagePNGRepresentation(retImage) writeToFile:filePath atomically:YES];
}

+ (NSString*)letters
{
    CDAccount *cdAccount = [CDAccount MR_findFirst];

    NSString *string = @"";
    
    if (cdAccount.user.nickname )
        string = cdAccount.user.nickname;
    
    if (cdAccount.user.fullName )
        string = cdAccount.user.fullName;
    

    
    NSMutableString *displayString = [NSMutableString stringWithString:@""];

    NSMutableArray *words = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    
    if ([words count]) {
        NSString *firstWord = [words firstObject];
        if ([firstWord length]) {
            // Get character range to handle emoji (emojis consist of 2 characters in sequence)
            NSRange firstLetterRange = [firstWord rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
            [displayString appendString:[firstWord substringWithRange:firstLetterRange]];
        }
        
        if ([words count] >= 2) {
            NSString *lastWord = [words lastObject];
            
            while ([lastWord length] == 0 && [words count] >= 2) {
                [words removeLastObject];
                lastWord = [words lastObject];
            }
            
            if ([words count] > 1) {
                // Get character range to handle emoji (emojis consist of 2 characters in sequence)
                NSRange lastLetterRange = [lastWord rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
                [displayString appendString:[lastWord substringWithRange:lastLetterRange]];
            }
        }
    }
    
    return displayString;
}

+ (UIColor *)randomColor {
    
    srand48(arc4random());
    
    float red = 0.0;
    while (red < 0.1 || red > 0.84) {
        red = drand48();
    }
    
    float green = 0.0;
    while (green < 0.1 || green > 0.84) {
        green = drand48();
    }
    
    float blue = 0.0;
    while (blue < 0.1 || blue > 0.84) {
        blue = drand48();
    }
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
}

+(void)deleteFile:(NSString*)name
{
     NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:name];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
}

+(void)deleteAvatar
{
    [self deleteFile:@"avatar_selected.png"];
    [self deleteFile:@"avatar.png"];
    [self deleteFile:@"avatar_big.png"];
}

+(void)clearAllEntityWithAccount:(BOOL)withAccount
{
    [CDPlayer MR_truncateAll] ;
    
    if (withAccount) {
        [CDUser MR_truncateAll];
        [CDAccount MR_truncateAll];
    }
    
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
}
                          
@end
