//
//  PushTokenService.m
//  GmIosClient
//
//  Created by Arthur Bikmullin on 27.01.16.
//  Copyright © 2016 Danil Nurgaliev. All rights reserved.
//

#import "PushTokenService.h"
#import <Crashlytics/Crashlytics.h>
#import "HTTPService.h"

@interface PushTokenService ()

@property (strong, nonatomic) NSString *pushToken;
@property (strong, nonatomic) NSString *unregisteredPushToken;
@end


@implementation PushTokenService

@synthesize pushToken=_pushToken;

+ (instancetype)sharedInstance {
    static PushTokenService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
        [shared setup];
    });
    return shared;
}

- (void)setup
{
    //setup here
}

- (void)setDeviceToken:(NSData *)deviceToken
{
    _deviceToken = deviceToken;
    
    NSString *token = [[self.deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"contsetPushTokenent---%@", token);
    
    self.unregisteredPushToken = token;
    
    [CrashlyticsKit setUserName:self.unregisteredPushToken];
}

- (void)sendPushToken
{
    NSString *token = self.unregisteredPushToken ? : self.pushToken;
    
    if (!token)
    {
        return;
    }
}


#pragma mark - property

- (NSString *)getPushToken
{
    
    return _pushToken;
}

- (void)setPushToken:(NSString *)pushToken
{
    _pushToken = pushToken;
    
}

@end
