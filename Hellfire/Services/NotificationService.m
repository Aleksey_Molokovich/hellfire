//
//  NotificationService.m
//  Hellfire
//
//  Created by Алексей Молокович on 27.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "NotificationService.h"
#import "HellfireDefines.h"
#import "CDPlayer+CoreDataClass.h"
#import "HFNotification.h"

static NSString *const kPushHotseatRoundResult = @"7";
static NSString *const kPushHotseatGameFinishResult = @"8";

@implementation NotificationService

+(instancetype)shared
{
    static NotificationService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
        shared.needToSave = YES;
    });
    return shared;
}

- (HFNotification*)prepareWithNotification:(NSDictionary*)push
{
    NSError *error;
    HFNotification *aps= [[HFNotification alloc] initWithDictionary:push[@"aps"] error:&error];

    if (!aps.data)
    {
        return nil;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@ ",aps.data.Id];
    if (![aps.data.Type isEqualToString:@"4"]) {
        CDPlayer *player = [CDPlayer MR_findFirstWithPredicate:predicate];
        
        if (!player)
        {
            player = [CDPlayer MR_createEntity];
            player.id = [aps.data.Id intValue];
        }
        
        aps.data.player = player;
    }
    
    
    return aps;
    
}

- (void)updateWithPush:(NSDictionary*)push
{
    [self postNotification:push fromBackground:NO];
}

- (void)openWithPush:(NSDictionary*)push
{
    if (_needToSave) {
        _push = push;
    }
    
    [self postNotification:push fromBackground:YES];
}

- (void)postNotification:(NSDictionary*)push fromBackground:(BOOL)isBG
{
    if (push)
    {
        HFNotification *aps = [self prepareWithNotification:push];
        if ([aps.data.Type isEqualToString:@"1"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationNewFriendRequest object:aps];
        }else if ([aps.data.Type isEqualToString:@"4"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationInviteToHotseat object:aps];
        }
        else if ([aps.data.Type isEqualToString:@"5"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationInviteToHotseatAccept object:aps];
        }
        else if ([aps.data.Type isEqualToString:@"6"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationInviteToHotseatReject object:aps];
        }
        else if ([aps.data.Type isEqualToString:kPushHotseatRoundResult])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationHotseatRoundResult object:aps];
        }
        else if ([aps.data.Type isEqualToString:kPushHotseatGameFinishResult])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationHotseatGameResult object:aps];
        }
        else
        {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:HFUpdatePlayersList object:aps];
            if (isBG) {
                [[NSNotificationCenter defaultCenter] postNotificationName:HFLaunchAppByPushNotification object:[self prepareWithNotification:push]];
            }
        }
    }
}

@end
