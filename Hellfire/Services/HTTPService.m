//
//  HTTPService.m
//  DiaryCulture
//
//  Created by Алексей Молокович on 08.07.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HTTPService.h"
#import <AFSessionOperation/AFHTTPSessionOperation.h>
#import <AFSessionOperation/AFURLSessionOperation.h>
#import <AFNetworking/AFNetworking.h>
#import "ResponseModel.h"
#import "HellfireDefines.h"
#import "EncryptService.h"

@implementation HTTPService
+(void)uploadImage:(NSData*)data
           withURL:(NSString*)url
          progress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock
        completion:(ResponseCompletion)completion
{
    [HTTPService abstractRequestWithMethod:@"POST"
                                   withUrl:url
                                  withData:data
                                  progress:uploadProgressBlock
                                completion:completion];
}



+(void)postRequestWithUrl:(NSString *)url andParams:(NSDictionary *)params completion:(ResponseCompletion)completion{
    
    url =[NSString stringWithFormat:@"%@?sign=%@",url,[EncryptService encryptFromDictionary:params]];
    
    [HTTPService abstractRequestWithMethod:@"POST" withUrl:url withParams:params withData:nil completion:completion];
    
}

+(void)getRequestWithUrl:(NSString *)url andParams:(NSDictionary *)dic completion:(ResponseCompletion)completion{
    
    NSMutableDictionary *params = [dic?dic:@{} mutableCopy];
    [params setObject:[EncryptService encryptFromDictionary:params] forKey:@"sign"];
    
    [HTTPService abstractRequestWithMethod:@"GET" withUrl:url withParams:params withData:nil completion:completion];
    
}



+(void)abstractRequestWithMethod:(NSString *)method
                         withUrl:(NSString *)url
                      withParams:(NSDictionary *)params
                        withData:(NSData*)data
                      completion:(ResponseCompletion)completion{
    
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer  = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"image/png", nil];
    
    manager.responseSerializer = responseSerializer;

    if ([url isEqualToString:@"share/solo"]) {
        [manager.requestSerializer setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
    }
    else
    {
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    }
    
    url=[NSString stringWithFormat:@"%@%@",[self baseURL],url];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:SessionToken];
    
    
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Client-Agent"];
    [manager.requestSerializer setValue:@"94a08da1fecbb6e8b46990538c7b50b2" forHTTPHeaderField:@"Sign"];
    [manager.requestSerializer setValue:@"ru_RU" forHTTPHeaderField:@"Locale"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    [manager.requestSerializer setValue:build forHTTPHeaderField:@"Version"];
    
    if (token.length)
        [manager.requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    
    
    [[NSOperationQueue new] addOperation:
     [AFHTTPSessionOperation operationWithManager:manager
                                       HTTPMethod:method
                                        URLString:url
                                       parameters:params
                                   uploadProgress:nil
                                 downloadProgress:nil
                                          success:^(NSURLSessionDataTask *task, id responseObject) {                                              
                                              if(completion){
                                                  NSError *error;
                                                  ResponseModel *response=[[ResponseModel alloc] initWithDictionary:responseObject error:&error];
                                                  
                                                  if (response.error.code == 450) {
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationUpdateApp object:nil];
                                                  }else if (response.error.code == 401) {
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationUnvalidToken object:nil];
                                                  }
                                                  
                                                  completion(response,error);
                                              };
                                          }
                                          failure:^(NSURLSessionDataTask *task, NSError *error) {
                                              
                                              if(completion){
                                                  NSString *mt = [[task response] MIMEType];
                                                  NSString* ErrorResponse;
                                                  if ([mt isEqualToString:@"image/png"]) {
                                                      ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];

                                                  }
                                                  completion(nil,error);
                                              }
                                          }]];

}

+(void)abstractRequestWithMethod:(NSString *)method
                         withUrl:(NSString *)url
                        withData:(NSData*)data
                        progress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock
                      completion:(ResponseCompletion)completion{
    
    url=[NSString stringWithFormat:@"%@%@",[self baseURL],url];

    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data
                                    name:@"file"
                                fileName:@"file.jpg"
                                mimeType:@"image/jpg"];
        
    } error:nil];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:SessionToken];

    [request setValue:@"ios" forHTTPHeaderField:@"Client-Agent"];
    [request setValue:@"94a08da1fecbb6e8b46990538c7b50b2" forHTTPHeaderField:@"Sign"];
    [request setValue:@"ru_RU" forHTTPHeaderField:@"Locale"];
    [request setValue:token forHTTPHeaderField:@"Token"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    [request setValue:build forHTTPHeaderField:@"Version"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if (uploadProgressBlock) {
                              uploadProgressBlock(uploadProgress);
                          }
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      NSError *err;
                      ResponseModel *res=[[ResponseModel alloc] initWithDictionary:responseObject error:&err];
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                      }
                      completion(res, error);
                  }];
    
    [uploadTask resume];
    
}


+(NSString*)baseURL{
#ifdef DEBUG
//    return @"http://api.hellfirebattle.com/api/";
    return @"http://dev.hellfirebattle.com/api/";
#else
    return @"http://api.hellfirebattle.com/api/";
#endif
}
@end
