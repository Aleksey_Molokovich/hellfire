//
//  BluetoothService.m
//  Hellfire
//
//  Created by Алексей Молокович on 24.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothService.h"
#import "CBPeripheralModel.h"
#import "CBUUID+StringExtraction.h"
#import <LGAlertView/LGAlertView.h>



@interface BluetoothService()<CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, strong) NSArray<CBUUID*> *serviceUUIDs;
@property (nonatomic, strong) CBUUID *characteristicUUID;
@property (nonatomic, assign) CBCharacteristicWriteType writeType;
@property (nonatomic, strong) LGAlertView *alert;
@end


@implementation BluetoothService



+(instancetype)sharedInstance
{
    static BluetoothService *shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [BluetoothService new];
        shared.serviceUUIDs = @[[CBUUID UUIDWithString:@"fee7"],[CBUUID UUIDWithString:@"ffe0"]];
        shared.characteristicUUID = [CBUUID UUIDWithString:@"ffe1"];
        shared.peripherals=[NSMutableDictionary new];
        shared.centralManager = [[CBCentralManager alloc] initWithDelegate:shared queue:nil];
        shared.writeType = CBCharacteristicWriteWithoutResponse;

    });
    return shared;
}

-(BOOL)isReady
{
    return (_centralManager.state == CBManagerStatePoweredOn &&
            _connectedPeripheral != nil &&
            _writeCharacteristic != nil);
}

-(BOOL)isScanning
{
    return _centralManager.isScanning;
}

-(BOOL)isPoweredOn
{
    return _centralManager.state == CBManagerStatePoweredOn;
}

/// Start scanning for peripherals
- (void)startScan
{
    if (!self.isPoweredOn)
    {
        return;
    }
    // start scanning for peripherals with correct service UUID
    [_centralManager scanForPeripheralsWithServices:self.serviceUUIDs options:nil];
    
    NSArray *peripherals = [_centralManager retrieveConnectedPeripheralsWithServices:self.serviceUUIDs];
    // retrieve peripherals that are already connected
    // see this stackoverflow question http://stackoverflow.com/questions/13286487
    for (CBPeripheral *peripheral in peripherals)
    {
        if ([_delegate respondsToSelector:@selector(serialDidDiscoverPeripheral:RSSI:)])
        {
            [_delegate serialDidDiscoverPeripheral:peripheral RSSI:nil];
        }
    }
}

- (void)stopScan
{
    [_centralManager stopScan];
}
/// Try to connect to the given peripheral
- (void)connectToPeripheral:(CBPeripheral*)peripheral
{
    _pendingPeripheral = peripheral;
    [_centralManager connectPeripheral:peripheral options:nil];
}

/// Disconnect from the connected peripheral or stop connecting to it
- (void) disconnect
{
    if (_connectedPeripheral)
    {
        [_centralManager cancelPeripheralConnection:_connectedPeripheral];
    }
    else if (_pendingPeripheral)
    {
        [_centralManager cancelPeripheralConnection:_pendingPeripheral];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HFPushNotificationCancelPeripheralConnection object:nil];
}

/// The didReadRSSI delegate function will be called after calling this function
- (void)readRSSI
{
    if (self.isReady)
    {
        [_connectedPeripheral readRSSI];
    }
    else
    {
        return;
    }
}

/// Send a string to the device
- (void)sendMessageToDevice:(NSString *)message
{
    if (self.isReady)
    {
        if (message)
        {
            [_connectedPeripheral writeValue:[[NSData alloc] initWithBase64EncodedString:message options:NSUTF8StringEncoding] forCharacteristic:_writeCharacteristic type:_writeType];
        }
    }
}

- (void) sendBytesToDevice:(UInt8) bytes
{
    if (self.isReady)
    {
        [_connectedPeripheral writeValue:[NSData dataWithBytes:&bytes length:sizeof(bytes)] forCharacteristic:_writeCharacteristic type:_writeType];
    }
}

- (void) sendDataToDevice:(NSData*)data
{
    if (self.isReady)
    {
        [_connectedPeripheral writeValue:data forCharacteristic:_writeCharacteristic type:_writeType];
    }
}

- (void)didChangeConnectionState
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kBluetoothServiceDidChangeConnection object:_connectedPeripheral.name];
}

#pragma mark CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(nonnull CBPeripheral *)peripheral advertisementData:(nonnull NSDictionary<NSString *,id> *)advertisementData RSSI:(nonnull NSNumber *)RSSI
{
    if (_delegate && [_delegate respondsToSelector:@selector(serialDidDiscoverPeripheral:RSSI:)])
    {
        [_delegate serialDidDiscoverPeripheral:peripheral RSSI:RSSI];
    }
}


- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    peripheral.delegate = self;
    _pendingPeripheral = nil;
    _connectedPeripheral = peripheral;
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kBluetoothServiceDidConnected object:peripheral];

    [self didChangeConnectionState];
    
    if (_delegate && [_delegate respondsToSelector:@selector(serialDidConnect:)])
    {
        [_delegate serialDidConnect:peripheral];
    }
    
    
    [peripheral discoverServices:self.serviceUUIDs];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    _pendingPeripheral = nil;


    if (_delegate && [_delegate respondsToSelector:@selector(serialDidFailToConnect:error:)])
    {
        [_delegate serialDidFailToConnect:peripheral error:error];
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    _connectedPeripheral = nil;
    _pendingPeripheral = nil;
    if (_delegate && [_delegate respondsToSelector:@selector(serialDidChangeState)])
    {
        [_delegate serialDidChangeState];
    }
}


- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    peripheral.delegate = nil;
    _pendingPeripheral = nil;
    _connectedPeripheral = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kBluetoothServiceDidDisconnected object:peripheral];
    
    [self didChangeConnectionState];
    if (_delegate && [_delegate respondsToSelector:@selector(serialDidConnect:)])
    {
        [_delegate serialDidConnect:peripheral];
    }
}
#pragma mark CBPeripheralDelegate


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:@[_characteristicUUID,[CBUUID UUIDWithString:@"fec9"]] forService:service];

    }
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(nonnull CBService *)service error:(nullable NSError *)error
{
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        
        NSLog(@"characteristic UUID  %@",[characteristic UUID]);

        if( [[characteristic UUID] isEqual: _characteristicUUID])
        {
            // subscribe to this value (so we'll get notified when there is serial data for us..)
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            
            // keep a reference to this characteristic so we can write to it
            _writeCharacteristic = characteristic;
            
            // find out writeType
            if (characteristic.properties == CBCharacteristicPropertyWrite)
            {
                _writeType = CBCharacteristicWriteWithResponse;
            }
            else
            {
                _writeType = CBCharacteristicWriteWithoutResponse;
            }
            
            // notify the delegate we're ready for communication
            if (_delegate && [_delegate respondsToSelector:@selector(serialIsReady:)])
            {
                [_delegate serialIsReady:peripheral];
            }
        }
        
        if ([[characteristic.UUID representativeString] isEqualToString:@"fec9"]) {
            [service.peripheral readValueForCharacteristic:characteristic];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    NSData *data = characteristic.value;
    
    if ([[characteristic.UUID representativeString] isEqualToString:@"fec9"]) {
        NSString *macAddress = @"";
        for (int i=0;i<[data length];i++) {
            UInt32 lightData = *(UInt32*)([[data subdataWithRange:NSMakeRange(i, 1)] bytes]);
            NSString *s= [NSString stringWithFormat:@"%02X", (unsigned int) lightData];
            if ([macAddress length]>1) {
                macAddress = [macAddress stringByAppendingString:@":"];
            }
            macAddress = [macAddress stringByAppendingString:s];
            NSLog(@"%@",macAddress);
        }
        
        self.macAddressPeripheral = [macAddress copy];
        return;
    }
    
    if (!data)
    {
        return;
    }
    
//    // first the data
//    if (_delegate && [_delegate respondsToSelector:@selector(serialDidReceiveData:)])
//    {
//        [_delegate serialDidReceiveData:data];
//    }
//    // then the string
//    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//
//    if (string)
//    {
//        if (_delegate && [_delegate respondsToSelector:@selector(serialDidReceiveString:)])
//        {
//            [_delegate serialDidReceiveString:string];
//        }
//    }
//    else
//    {
//        NSLog(@"Received an invalid string!");
//    }
    
    // now the bytes array
//    ((UInt8)data)
//    var bytes = [UInt8](repeating: 0, count: data!.count / MemoryLayout<UInt8>.size)
//    (data! as NSData).getBytes[(&bytes, length: data!.count)
    
//     UInt8 bytes = (UInt8)[data bytes];
    
    UInt32 lightData = *(UInt32*)([[data subdataWithRange:NSMakeRange(0, 1)] bytes]);
    UInt32 speedData = *(UInt32*)([[data subdataWithRange:NSMakeRange(1, 1)] bytes]);
//    UInt32 b3 = *(UInt32*)([[data subdataWithRange:NSMakeRange(2, 1)] bytes]);
//    UInt32 b4 = *(UInt32*)([[data subdataWithRange:NSMakeRange(3, 1)] bytes]);
//    
//    NSMutableData *mData1 = [NSMutableData new];
//    
//    [mData1 appendData:[data subdataWithRange:NSMakeRange(4, 4)]];
//
//    UInt32 value = *(UInt32*)([mData1 bytes]);
//
//    NSMutableData *mData2 = [NSMutableData new];
//    
//    [mData2 appendData:[data subdataWithRange:NSMakeRange(8, 4)]];
//    UInt32 value2 = *(UInt32*)([mData2 bytes]);

//    NSLog(@"b1:%u  b2:%u  b3:%u  b4:%u  d1:%u  d2:%u",(unsigned int)lightData,(unsigned int)speedData,(unsigned int)b3,(unsigned int)b4,(unsigned int)value,(unsigned int)value2);
    
    if (_delegate && [_delegate respondsToSelector:@selector(serialDidReceiveQualityData:speedData:)])
    {
        [_delegate serialDidReceiveQualityData:lightData speedData:speedData];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error
{
    NSLog(@"%@", RSSI);
    if (_delegate && [_delegate respondsToSelector:@selector(serialDidReadRSSI:)])
    {
        [_delegate serialDidReadRSSI:RSSI];
    }
}






- (void)showAlertBLENotReadyActionHandler:(BluetoothServiceCompletionHandler)actionHandler 
{
    self.alert = [[LGAlertView alloc] initWithTitle:@"Внимание"
                                            message:@"Игра возможна только с подключенным устройством.\nПодключите HFPipe"
                                              style:LGAlertViewStyleAlert
                                       buttonTitles:@[@"Подключить"]
                                  cancelButtonTitle:@"Закрыть"
                             destructiveButtonTitle:nil actionHandler:^(LGAlertView * _Nonnull alertView, NSUInteger index, NSString * _Nullable title) {
                                 actionHandler();
                             } cancelHandler:nil
                               destructiveHandler:nil] ;
    
    self.alert.buttonsHeight = 44;
    [self.alert showAnimated];
}








@end
