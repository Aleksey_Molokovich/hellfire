//
//  BluetoothService.h
//  Hellfire
//
//  Created by Алексей Молокович on 24.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreBluetooth;

static NSString *const kBluetoothServiceDidChangeConnection =@"bluetoothServiceDidChangeConnection";
static NSString *const kBluetoothServiceDidConnected =@"bluetoothServiceDidConnected";
static NSString *const kBluetoothServiceDidDisconnected =@"bluetoothServiceDidDisconnected";

typedef void (^ _Nullable BluetoothServiceCompletionHandler)();

@protocol BluetoothServiceDelegate<NSObject>

/// Called when de state of the CBCentralManager changes (e.g. when bluetooth is turned on/off)
- (void)serialDidChangeState;

/// Called when a peripheral disconnected
- (void)serialDidDisconnect:(CBPeripheral*)peripheral error:(NSError*)error;


@optional
/// Called when a message is received
- (void) serialDidReceiveString:(NSString*)message;

/// Called when a message is received
- (void) serialDidReceiveBytes:(UInt8)bytes;

/// Called when a message is received
- (void) serialDidReceiveData:(NSData*)data;

/// Called when a message is received
- (void) serialDidReceiveQualityData:(NSInteger)quality speedData:(NSInteger)speed;

/// Called when the RSSI of the connected peripheral is read
- (void) serialDidReadRSSI:(NSNumber*)rssi;

/// Called when a new peripheral is discovered while scanning. Also gives the RSSI (signal strength)
- (void) serialDidDiscoverPeripheral:(CBPeripheral*)peripheral RSSI:(NSNumber*)RSSI;

/// Called when a peripheral is connected (but not yet ready for cummunication)
- (void) serialDidConnect:(CBPeripheral*) peripheral;

/// Called when a pending connection failed
- (void) serialDidFailToConnect:(CBPeripheral*)peripheral error:(NSError*)error;

/// Called when a peripheral is ready for communication
- (void) serialIsReady:(CBPeripheral*)peripheral;


//- (void)updatePeripherals;
//- (void)shouldEnableBluetooth;


@end


@interface BluetoothService : NSObject

@property (weak, nonatomic) id<BluetoothServiceDelegate> delegate;

@property (strong, nonatomic) NSMutableDictionary *peripherals;

/// The CBCentralManager this bluetooth serial handler uses for... well, everything really
@property (strong, nonatomic) CBCentralManager *centralManager;

/// The peripheral we're trying to connect to (nil if none)
@property (strong, nonatomic) CBPeripheral *pendingPeripheral;

/// The connected peripheral (nil if none is connected)
@property (strong, nonatomic) CBPeripheral *connectedPeripheral;

@property (weak, nonatomic) CBCharacteristic *writeCharacteristic;

@property (atomic, assign, readonly) BOOL isScanning;
@property (atomic, assign, readonly) BOOL isReady;
@property (atomic, assign, readonly) BOOL isPoweredOn;

@property (nonatomic, assign) BOOL isSendMetrics;


@property (nonatomic) NSString *macAddressPeripheral;

+ (instancetype)sharedInstance;

- (void)startScan;
- (void)stopScan;
- (void)connectToPeripheral:(CBPeripheral*)peripheral;
- (void)disconnect;

- (void)showAlertBLENotReadyActionHandler:(BluetoothServiceCompletionHandler)actionHandler;
@end
