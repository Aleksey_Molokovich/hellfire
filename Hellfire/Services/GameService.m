//
//  GameService.m
//  Hellfire
//
//  Created by Алексей Молокович on 10.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameService.h"
#import "GameRepository.h"
#import "HFGameSettings.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "CDGameResults+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "GameRequest.h"
#import "HFSoloGameResult.h"
#import "HFHotseatPlayer.h"
#import "BluetoothService.h"


@interface GameService(){
    NSTimer *timer;
    NSInteger count;
    
}

@property (nonatomic,assign) BOOL isRunning;

@end

@implementation GameService


- (void) dealloc
{
    NSLog(@"%@ deallocated",NSStringFromClass(self.class));
}

-(CDHotseatPlayer*)currentHotseatPlayer
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentPlayer = 1"];
    return [CDHotseatPlayer MR_findFirstWithPredicate:predicate];
}

-(CDHotseatPlayer*)nextHotseatPlayer
{
    CDHotseatPlayer *current = [self currentHotseatPlayer];
    NSPredicate *predicateStatus = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld", PlayerHotseatStatusAccept, PlayerHotseatStatusAcceptLock];
    NSPredicate *predicateIndex = [NSPredicate predicateWithFormat:@"index > %ld", current.index];
    
    NSPredicate *allPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateIndex,predicateStatus]];
    CDHotseatPlayer *next = [CDHotseatPlayer MR_findFirstWithPredicate:allPredicate sortedBy:@"index" ascending:YES];
    
    if (!next) {
        next =[CDHotseatPlayer MR_findFirstWithPredicate:predicateStatus sortedBy:@"index" ascending:YES];
    }
    
    return next;
}

-(CDHotseatPlayer*)changeCurrentHotseatPlayer
{
    CDHotseatPlayer *current = [self currentHotseatPlayer];
    current.currentPlayer = NO;
    NSPredicate *predicateStatus = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld", PlayerHotseatStatusAccept, PlayerHotseatStatusAcceptLock];
    NSPredicate *predicateIndex = [NSPredicate predicateWithFormat:@"index > %ld", current.index];
    
    NSPredicate *allPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateIndex,predicateStatus]];
    CDHotseatPlayer *next = [CDHotseatPlayer MR_findFirstWithPredicate:allPredicate sortedBy:@"index" ascending:YES];
    
    if (!next) {
        next =[CDHotseatPlayer MR_findFirstWithPredicate:predicateStatus sortedBy:@"index" ascending:YES];
    }
    next.currentPlayer = YES;
    
    return next;
}

- (NSInteger)calculateAllPointsForCurrentPlayer
{
    

    NSPredicate *predicateUser=nil;
    if ([self isSoloGame]) {
        predicateUser = [NSPredicate predicateWithFormat:@"playerId = %ld", [CDAccount MR_findFirst].user.id];
    }
    else
    {
        predicateUser = [NSPredicate predicateWithFormat:@"playerId = %ld", [self currentHotseatPlayer].id];
    }
    
    NSArray *results = [CDGameResults MR_findAllWithPredicate:predicateUser];
    CGFloat allPoints = 0;
    for (CDGameResults *res in results)
    {
        allPoints +=res.points;
    }
    NSPredicate *predicateRound = [NSPredicate predicateWithFormat:@"currentRound = %ld", [self getCurrentRound]];
    [CDGameResults MR_findFirstWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[predicateUser, predicateRound]]].allGamePoints = (int)allPoints;
    
    return (int)allPoints;
}

- (void)startWithDuration:(NSInteger)duration
{
    count=duration;
    [self changeCounter];
    
}

- (void) changeCounter{
    
    if (count<0 || !_delegate) {
        [self stop];
        return;
    }
    
    NSLog(@"class %@ timer %ld",NSStringFromClass(self.delegate.class),(long)count);
    
    if ([_delegate respondsToSelector:@selector(updateTimerWithSeconds:)]) {
        [_delegate updateTimerWithSeconds:count];
    }
    
    count--;

    timer=[NSTimer scheduledTimerWithTimeInterval:1
                                           target:self
                                         selector:@selector(changeCounter)
                                         userInfo:nil
                                          repeats:NO];
    
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
    
}

- (void)stop
{
    [timer invalidate];
    timer=nil;
    if ([_delegate respondsToSelector:@selector(updateTimerWithSeconds:)]) {
        [_delegate updateTimerWithSeconds:0];
    }
}

-(BOOL)isSoloGame
{
    return [HFGameSettings load].gameId==0?YES:NO;
}

-(CDGameResults *)currentGameResults
{
    if ([self isSoloGame])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", [self getCurrentRound]];
        CDGameResults *result = [CDGameResults MR_findFirstWithPredicate:predicate];
        if (!result) {
            result = [CDGameResults MR_createEntity];
            result.currentRound = [self getCurrentRound];
            result.playerId = [CDAccount MR_findFirst].user.id;
            result.playerAvatar = [CDAccount MR_findFirst].avatarPath;
        }
        
        return result;
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld AND playerId = %ld", [self getCurrentRound],[self currentHotseatPlayer].id];
        CDGameResults *result = [CDGameResults MR_findFirstWithPredicate:predicate];
        if (result.playerId==0) {
            result = [CDGameResults MR_createEntity];
            result.currentRound = [self getCurrentRound];
            result.playerId = [self currentHotseatPlayer].id;
            result.playerAvatar = [self currentHotseatPlayer].avatarPath;
        }
        return result;
    }
}

-(NSInteger)getCurrentRound{
    return [HFGameSettings load].currentRound;
}

-(NSInteger)getCountRounds{
    return [HFGameSettings load].countRounds;
}

- (BOOL)isGameOver{
    HFGameSettings *settings=[HFGameSettings load];
    if (settings.currentRound+1>settings.countRounds) {
        return YES;
    }
    return NO;
}

-(BOOL)nextRound
{
    HFGameSettings *settings=[HFGameSettings load];
    if (settings.currentRound+1>settings.countRounds) {
        return NO;
    }
    settings.currentRound+=1;
    return YES;
}

- (CDGameResults*)bestQualityResults
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", [self getCurrentRound]];
    return [[CDGameResults MR_findAllSortedBy:@"quality" ascending:NO withPredicate:predicate] firstObject];
}

- (CDGameResults*)bestSpeedResults
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", [self getCurrentRound]];

    return [[CDGameResults MR_findAllSortedBy:@"speed" ascending:NO withPredicate:predicate] firstObject];
}

- (CDGameResults*)bestPointResults
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", [self getCurrentRound]];

    return [[CDGameResults MR_findAllSortedBy:@"points" ascending:NO withPredicate:predicate] firstObject];
}



- (void)sendCurrentHotseatRoundResultsWithCompletion:(void (^)(HFHotseatGameResult *result))completion
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", [self getCurrentRound]];
   NSArray *playersResults = [CDGameResults MR_findAllWithPredicate:predicate];
    
    HotseatGameRoundRequest *request = [HotseatGameRoundRequest new];
    request.gameId = @([HFGameSettings load].gameId);
    request.currentRound = @([self getCurrentRound]);
    request.lastRound = [self isGameOver]?@"true":@"false";
    
    NSMutableArray<HotseatGamePLayer> *players = [NSMutableArray new];
    
    for (CDGameResults *result in playersResults) {
        HotseatGamePLayer *player = [HotseatGamePLayer new];
        player.userId = @(result.playerId);
        player.round = [GameRound new];
        player.round.duration = @([HFGameSettings load].durationRound);
        player.round.averageQuality = @((int)result.quality/result.tiksCount);
        player.round.maxQuality = @((int)result.maxQuality);
        player.round.averageSpeed = @((int)result.speed/result.tiksCount);
        player.round.maxSpeed = @((int)result.maxSpeed);
        player.round.points = @((int)result.points);
        player.round.volume = @((int)result.volume);
        player.round.frequency = @50;
        if ([BluetoothService sharedInstance].isSendMetrics && result.metrics) {
            player.round.metrics = [NSJSONSerialization JSONObjectWithData:result.metrics options:0 error:nil] ;
        }else{
            player.round.metrics = @[];
        }
        
        
        [players addObject:player];
    }
    request.players = players;
    
    [GameRepository saveHotseatGameRound:request completion:^(HFHotseatGameResult *result) {
        completion (result);
    }];
}


- (void)sendSoloGameResultsWithCompletion:(void (^)(HFSoloGameResult *))completion
{
    GameRequest *gameRequest = [GameRequest new];

    gameRequest.hfPipeSerialNumber = [BluetoothService sharedInstance].macAddressPeripheral?[BluetoothService sharedInstance].macAddressPeripheral:@"1";
    
    HFGameSettings *settings = [HFGameSettings load];
    
    NSMutableArray *rounds = [NSMutableArray new];
    for (int i=1 ; i<=settings.countRounds; i++) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", i];
        CDGameResults *result = [CDGameResults MR_findFirstWithPredicate:predicate];
        GameRound *round = [GameRound new];
        round.duration = @(settings.durationRound);
        round.maxQuality = @(result.maxQuality);
        round.averageQuality = result.tiksCount?@((int)(result.quality/result.tiksCount)):@0;
        round.maxSpeed = @(result.maxSpeed);
        round.averageSpeed = result.tiksCount?@((int)(result.speed/result.tiksCount)):@0;
        round.points = @(result.points);
        round.frequency = @50;
        round.volume = @(result.volume);
        round.metrics = @[];
//        if ([BluetoothService sharedInstance].isSendMetrics && result.metrics) {
//            round.metrics = [NSJSONSerialization JSONObjectWithData:result.metrics options:0 error:nil] ;
//        }else{
//           round.metrics = @[];
//        }
        
        [rounds addObject: round];
    }
    
    gameRequest.rounds = [rounds copy];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yyyy HH:mm:ss"];
    
    gameRequest.gameDate = [dateFormatter stringFromDate:[NSDate date]];
    [GameRepository createSolo:gameRequest completion:^(HFSoloGameResult *receive)
     {
         completion(receive);
     }];
}

- (void)shareSoloGame:(NSNumber*)gameId completion:(void (^)(HFSoloGameResult *))completion
{
    
    [GameRepository shareSolo:@([CDAccount MR_findFirst].user.id)
                         game:gameId
                      message:@"message"
                   completion:^(NSArray *receive) {
                       
                   }];
}







@end
