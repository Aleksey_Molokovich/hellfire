//
//  SubscribeService.h
//  DiaryCulture
//
//  Created by Алексей Молокович on 28.07.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Notification;


@interface SubscribeService : NSObject
+ (instancetype)shared;

- (void)start;
- (void)stop;
@property (nonatomic,assign) BOOL isExecuting;
@property (strong, nonatomic) Notification *notif;
@end
