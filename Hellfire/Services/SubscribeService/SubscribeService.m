//
//  SubscribeService.m
//  DiaryCulture
//
//  Created by Алексей Молокович on 28.07.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SubscribeService.h"
#import "HTTPService.h"
#import "ResponseModel.h"
#import "CDPlayer+CoreDataClass.h"
#import "HellfireDefines.h"

@interface SubscribeService () {
    NSTimer *subscribeTimer;
    
}

@property(nonatomic, assign) BOOL isRun;

@end

@implementation SubscribeService
+ (instancetype)shared
{
    static SubscribeService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
        [shared setup];
    });
    return shared;
}

- (void)setup
{
#ifndef DEBUG
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didBecomeActive)
     name:NSExtensionHostDidBecomeActiveNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didEnterBacground)
     name:NSExtensionHostDidEnterBackgroundNotification
     object:nil];
#endif
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didBecomeActive
{
    [self scheduledTimerWithInterval:0];
}
- (void)didEnterBacground {
    [self stop];
}

- (void)start
{
    if (_isExecuting)
    {
        return;
    }
    _isExecuting = YES;
    [self startSubscribeMessages];
}

- (void)stop {
    _isExecuting=NO;
    [subscribeTimer invalidate];
}

- (void)startSubscribeMessages
{
    if (_isExecuting) {
        [self scheduledTimerWithInterval:5];
    }
}

- (void)startSubscribeAfterError
{
    if (_isExecuting)
    {
        [self scheduledTimerWithInterval:10.0];
    }
}

- (void)scheduledTimerWithInterval:(double)interval
{
    [subscribeTimer invalidate];
    subscribeTimer =
    [NSTimer scheduledTimerWithTimeInterval:interval
                                     target:self
                                   selector:@selector(subscribeMessages)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)subscribeMessages
{
    
    if (self.isRun)  //исключает повторный запуск
        return;
    
    __weak __typeof(self)weakSelf = self;
    
    self.isRun = YES;
    [HTTPService getRequestWithUrl:@"user/friends"
                         andParams:nil
                        completion:^(ResponseModel *responseObject, NSError *error)
     {
         __strong __typeof(weakSelf)blockSelf = weakSelf;
         blockSelf.isRun = NO;
         if (responseObject.status == 1) {
             NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == 0"];
             NSArray *oldRequest = [CDPlayer MR_findAllWithPredicate:predicate] ;
             [CDPlayer MR_importFromArray:responseObject.data[@"users"]];
             
             
             if ([oldRequest count] < [[CDPlayer MR_findAllWithPredicate:predicate] count])
             {
                 [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationHasNewFriendRequest object:nil];
//                 [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
             }
             
           
         }
           [blockSelf performSelector:@selector(startSubscribeMessages)];
     }];
    
}


@end
