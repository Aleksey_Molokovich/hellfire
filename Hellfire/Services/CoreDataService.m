//
//  CoreDataService.m
//  Hellfire
//
//  Created by Алексей Молокович on 14.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "CoreDataService.h"

@implementation CoreDataService
+ (instancetype)sharedInstance {
    static CoreDataService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
        [shared setup];
    });
    return shared;
}

- (void)setup
{
    _friends = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
    self.settings = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];

}
@end
