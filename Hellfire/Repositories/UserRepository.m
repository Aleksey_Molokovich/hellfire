//
//  UserRepository.m
//  Hellfire
//
//  Created by Алексей Молокович on 23.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserRepository.h"
#import "HTTPService.h"
#import "ResponseModel.h"
#import "CDPlayer+CoreDataClass.h"
#import "PlayersRequest.h"
#import "CDClan+CoreDataClass.h"
#import "ClansRequest.h"
#import "UserProfile.h"
#import "CoreDataService.h"
#import "CoreDataService.h"
#import "ProfileResponse.h"
#import "BluetoothService.h"
@implementation UserRepository

+ (void)userProfileWithCompletion:(void (^)(NSArray *))completion
{
    [HTTPService getRequestWithUrl:@"user/profile"
                          andParams:nil
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             
                             if (completion) {
                                 if (responseObject.status == 1) {
                                     CDAccount *account = [CDAccount MR_findFirst];
                                     
                                     NSError *err;
                                     ProfileResponse *profile = [[ProfileResponse alloc] initWithDictionary:responseObject.data[@"profile"] error:&err];
                                     account = [profile toCoreData];
                                     [BluetoothService sharedInstance].isSendMetrics = profile.isSendMetrics;
//                                     [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
                                     completion(responseObject.data);
                                 }else{
                                     completion(nil);
                                 }
                                 
                             }
                         }];
}

+ (void)updateUserProfile:(UserProfile *)profile completion:(void (^)(NSArray *))completion
{
    [HTTPService postRequestWithUrl:@"user/update_profile"
                         andParams:[profile toDictionary]
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             
                             if (completion) {
                                 if (responseObject.status == 1) {
                                    
                                     completion(responseObject.data);
                                 }else{
                                     completion(nil);
                                 }
                                 
                             }
                         }];
}


+ (void)playersWithParam:(PlayersRequest *)param completion:(void (^)(NSArray *, NSInteger))completion
{
    [HTTPService getRequestWithUrl:@"user/get_users"
                         andParams:[param toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1) {
                                    NSArray *results = [CDPlayer MR_importFromArray:responseObject.data[@"users"]];

                                    completion(results,responseObject.meta.count);
                                }else{
                                    completion(nil,0);
                                }
                                
                            }
                        }];
}

+ (void)friendsWithParam:(PlayersRequest*)param
              completion:(void (^)(NSArray *))completion
{
    [HTTPService getRequestWithUrl:@"user/friends"
                         andParams:@{@"query":param.query?param.query:@""}
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1 || responseObject.status == 2) {
                                    [CDPlayer MR_truncateAllInContext:[CoreDataService sharedInstance].friends];
                                    NSArray *results = [CDPlayer MR_importFromArray:responseObject.data[@"users"] inContext:[CoreDataService sharedInstance].friends];
                                    completion(results);
                                }else{
                                    completion(nil);
                                }
                                
                            }
                        }];
}



+ (void)info:(NSInteger)playerId completion:(void (^)(CDPlayer *, NSArray *))completion
{
    [HTTPService getRequestWithUrl:@"user/user_card"
                         andParams:@{@"userId":@(playerId)}
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1)
                                {
                                    CDPlayer *userInfo = [CDPlayer MR_importFromObject:responseObject.data[@"userInfo"]];
                                    NSArray *clans = [CDClan MR_importFromArray:responseObject.data[@"userClans"]];
                                    completion(userInfo, clans);
                                }else{
                                    completion(nil,nil);
                                }
                            }
                        }];
}

+(void)removeFriend:(CDPlayer *)player completion:(void (^)(BOOL))completion
{
    [self friend:player isAccept:NO method:@"user/remove_friend" completion:completion];
}

+(void)acceptFriend:(CDPlayer *)player completion:(void (^)(BOOL))completion
{
     [self friend:player isAccept:YES method:@"user/accept_friend" completion:completion];
}

+(void)rejectFriend:(CDPlayer *)player completion:(void (^)(BOOL))completion
{
    [self friend:player isAccept:NO method:@"user/accept_friend" completion:completion];
}

+ (void)addFriend:(CDPlayer*)player completion:(void (^)(BOOL response))completion
{
    [self friend:player isAccept:YES method:@"user/add_friend" completion:completion];
}


+ (void)friend:(CDPlayer*)player
      isAccept:(BOOL)isAccept
        method:(NSString*)url
    completion:(void (^)(BOOL response))completion
{
    NSDictionary *params = @{@"userId":@(player.id)};
    if ([url isEqualToString:@"user/accept_friend"] ||
        [url isEqualToString:@"user/remove_friend"])
    {
        params = @{@"isAccept":isAccept?@"true":@"false",
                   @"userId":@(player.id)};
    }
    NSInteger playerId = player.id;
    [HTTPService getRequestWithUrl:url
                         andParams:params
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 2) {
                                    
                                    [self info:playerId completion:^(CDPlayer *user, NSArray *clans)
                                     {
                                         completion(YES);
                                    }];
                                }else{
                                    completion(NO);
                                }
                            }
                        }];
}





@end
