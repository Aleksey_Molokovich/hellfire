//
//  GameRepository.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameRepository.h"
#import "HTTPService.h"
#import "GameRequest.h"
#import "HFSoloGameResult.h"
#import "ResponseModel.h"
#import "HFHotseatGameResult.h"
#import "NSString+URLEncode.h"


@implementation GameRepository
+(void)createSolo:(GameRequest *)game completion:(void (^)(HFSoloGameResult *))completion
{
    [HTTPService postRequestWithUrl:@"game/solo/create"
                          andParams:[game toDictionary]
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             if (completion) {
                                 if (responseObject.status == 1)
                                 {
                                     HFSoloGameResult *data = [[HFSoloGameResult alloc] initWithDictionary:responseObject.data[@"soloSummary"] error:nil];
                                     completion(data);
                                 }
                                 else
                                 {
                                   completion(nil);
                                 }
                             }
                             
                         }];
}

+(void)createHotseat:(GameRequest *)game completion:(void (^)(NSNumber *))completion
{
    [HTTPService getRequestWithUrl:@"game/hotseat/create"
                          andParams:[game toDictionary]
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             if (completion) {
                                 if (responseObject.status == 1)
                                 {
                                     completion(responseObject.data[@"gameId"]);
                                 }
                                 else
                                 {
                                     completion(nil);
                                 }
                             }
                             
                         }];
}

+(void)saveHotseatGameRound:(HotseatGameRoundRequest*)round completion:(void (^)(HFHotseatGameResult *))completion
{
    [HTTPService postRequestWithUrl:@"game/hotseat/save_round"
                          andParams:[round toDictionary]
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             if (completion) {
                                 if (responseObject.status == 1)
                                 {
                                     HFHotseatGameResult *data = [[HFHotseatGameResult alloc] initWithDictionary:responseObject.data[@"hotseatSummary"] error:nil];
                                     completion(data);
                                 }
                                 else
                                 {
                                     completion(nil);
                                 }
                             }
                             
                         }];
}

+(void)invitePlayers:(NSArray *)players game:(NSNumber *)game completion:(void (^)(BOOL))completion
{
    [HTTPService postRequestWithUrl:@"game/hotseat/set_users"
                          andParams:@{@"gameId":game,
                                      @"users":players
                                      } completion:^(ResponseModel *responseObject, NSError *error) {
                                          if (completion) {
                                              completion(error?NO:YES);
                                          }
                                      }];
}

+(void)setSettingsForGame:(NSNumber *)game roundCount:(NSNumber *)count duration:(NSNumber *)duration completion:(void (^)(BOOL))completion
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yyyy HH:mm:ss"];
    
    [HTTPService postRequestWithUrl:@"game/hotseat/set_settings"
                          andParams:@{@"gameId":game,
                                      @"roundCount":count,
                                      @"duration":duration,
                                      @"gameDate":[dateFormatter stringFromDate:[NSDate date]]
                                      }
                         completion:^(ResponseModel *responseObject, NSError *error) {
                                          if (completion) {
                                              completion(error?NO:YES);
                                          }
                                      }];
}



+(void)acceptInviteToGame:(NSNumber *)game
               completion:(void (^)(NSNumber *))completion
{
    [HTTPService getRequestWithUrl:@"game/hotseat/accept"
                          andParams:@{@"gameId":game,
                                      @"isAccept":@"true"
                                      } completion:^(ResponseModel *responseObject, NSError *error) {
                                          
                                      }];
}
+(void)rejectInviteToGame:(NSNumber *)game
               completion:(void (^)(NSNumber *))completion
{
    [HTTPService getRequestWithUrl:@"game/hotseat/accept"
                          andParams:@{@"gameId":game,
                                      @"isAccept":@"false"
                                      } completion:^(ResponseModel *responseObject, NSError *error) {
                                          
                                      }];
}

+(void)checkPlayers:(NSArray *)players game:(NSNumber *)game completion:(void (^)(NSArray*))completion
{
    [HTTPService postRequestWithUrl:@"game/hotseat/check_users"
                          andParams:@{@"gameId":game,
                                      @"users":players
                                      } completion:^(ResponseModel *responseObject, NSError *error) {
                                          if (completion) {
                                              if (responseObject.status == 1) {
                                                  completion(responseObject.data[@"acceptedUsers"]);
                                              }
                                              else
                                              {
                                                  completion(nil);
                                              }
                                              
                                          }
                                      }];
}

+(void)shareSolo:(NSNumber *)playerId game:(NSNumber *)game message:(NSString*)message completion:(void (^)(NSArray*))completion
{

    NSData *nsdata = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSCharacterSet *allowedCharacters = [NSCharacterSet URLFragmentAllowedCharacterSet];
    NSString *percentEncodedString = [base64Encoded stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    if (!game || !playerId || !base64Encoded)
        return;
    
    [HTTPService getRequestWithUrl:@"share/solo"
                          andParams:@{@"gameId":game,
                                      @"userId":playerId,
                                      @"message":base64Encoded
                                      } completion:^(ResponseModel *responseObject, NSError *error) {
                                          if (completion) {
                                              if (responseObject.status == 1) {
                                                  completion(responseObject.data[@"acceptedUsers"]);
                                              }
                                              else
                                              {
                                                  completion(nil);
                                              }
                                              
                                          }
                                      }];
}
@end
