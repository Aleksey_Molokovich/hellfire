//
//  CatalogsRepository.h
//  Hellfire
//
//  Created by Алексей Молокович on 24.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPService.h"
@class CatalogModel;
@class AgreementResponse;

@interface CatalogsRepository : NSObject

+(void) getCountryContain:(NSString *)string completion:(void (^)(NSArray<CatalogModel*> *, NSError *))completion;
+(void) getCityContain:(NSString *)string country:(CatalogModel*)country completion:(void (^)(NSArray<CatalogModel *> *, NSError *))completion;

+(void)agreementWithComplition:(void (^)(AgreementResponse *agreement))completion;
@end
