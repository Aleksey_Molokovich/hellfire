//
//  CatalogsRepository.m
//  Hellfire
//
//  Created by Алексей Молокович on 24.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CatalogsRepository.h"
#import "CatalogModel.h"
#import "ResponseModel.h"
#import "AgreementResponse.h"
@implementation CatalogsRepository

+(void) getCountryContain:(NSString *)string completion:(void (^)(NSArray<CatalogModel*> *, NSError *))completion{
     [HTTPService getRequestWithUrl:[NSString stringWithFormat:@"dictionary/countries"]
                                andParams:@{@"query":string}
                               completion:^(ResponseModel *response, NSError *error) {

                                   if(completion){
                                       if(response.status > 0 ){
                                           NSError *err;
                                           NSArray<CatalogModel*> *countries=[CatalogModel arrayOfModelsFromDictionaries:response.data[@"countries"] error:&err];
                                           
                                           completion(countries,nil);
                                       }
                                   }
                               }];
}


+(void) getCityContain:(NSString *)string country:(CatalogModel*)country completion:(void (^)(NSArray<CatalogModel *> *, NSError *))completion{
     [HTTPService getRequestWithUrl:[NSString stringWithFormat:@"dictionary/cities"]
                                andParams:@{@"query":string,
                                            @"countryId":@(country.id)}
                               completion:^(ResponseModel *response, NSError *error) {
                                   
                                   if(completion){
                                       if(response.status > 0 ){
                                           NSError *err;
                                           NSArray<CatalogModel*> *countries=[CatalogModel arrayOfModelsFromDictionaries:response.data[@"cities"] error:&err];
                                           
                                           completion(countries,nil);
                                       }
                                   }
                               }];
}

+(void)agreementWithComplition:(void (^)(AgreementResponse *))completion
{
    [HTTPService getRequestWithUrl:[NSString stringWithFormat:@"dictionary/terms"]
                         andParams:nil
                        completion:^(ResponseModel *response, NSError *error) {
                            
                            if(completion){
                                if (response.status == 1) {
                                    AgreementResponse *model = [[AgreementResponse alloc] initWithDictionary:response.data error:nil];
                                    completion(model);
                                }
                                else{
                                    completion(nil);
                                }
                                
                            }
                        }];
}
@end
