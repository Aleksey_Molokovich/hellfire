//
//  StatisticsRepository.m
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "StatisticsRepository.h"
#import "HTTPService.h"
#import "ResponseModel.h"
#import "UsersStatisticsModel.h"
#import "StatisticRequestModel.h"

@implementation StatisticsRepository


+(void)statisticsWithCompletion:(void (^)(id))completion
{
    [HTTPService getRequestWithUrl:@"statistics"
                          andParams:nil
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             
                             if (completion) {
                                 if (responseObject.status == 1) {
                                     completion([[UsersStatisticsModel alloc] initWithDictionary:responseObject.data error:nil]);
                                 }else{
                                     completion(nil);
                                 }
                                 
                             }
                         }];
}

+(void)statisticsSoloWithParam:(StatisticRequestModel*)param completion:(void (^)(NSArray *, NSNumber *))completion
{
    NSDictionary *params = @{@"request.from":param.from,
                             @"request.to":param.to
                             };
    
    [HTTPService getRequestWithUrl:@"statistics/history/solo"
                         andParams:params
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1) {
                                    NSError *err;
                                    NSArray *result = [UsersStatisticsSoloHistoryModel arrayOfModelsFromDictionaries:responseObject.data[@"soloHistory"] error:&err];
                                    completion(result,@(responseObject.meta.count));
                                }else{
                                    completion(nil,nil);
                                }
                                
                            }
                        }];
}

+(void)statisticsHotseatWithParam:(StatisticRequestModel*)param completion:(void (^)(NSArray *, NSNumber *))completion
{
    NSDictionary *params = @{@"request.from":param.from,
                             @"request.to":param.to
                             };
    
    [HTTPService getRequestWithUrl:@"statistics/history/hotseat"
                         andParams:params
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1) {
                                    NSError *err;
                                    NSArray *result = [UsersStatisticsHotseatHistoryModel arrayOfModelsFromDictionaries:responseObject.data[@"hotseatHistory"] error:&err];
                                    completion(result,@(responseObject.meta.count));
                                }else{
                                    completion(nil,nil);
                                }
                                
                            }
                        }];
}
@end
