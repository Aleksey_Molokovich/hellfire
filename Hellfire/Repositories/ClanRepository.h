//
//  ClanRepository.h
//  Hellfire
//
//  Created by Алексей Молокович on 20.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AdPostRequest;
@class HFPost;
@class ClansRequest;
@class CDClan;

@interface ClanRepository : NSObject

+ (void) posts:(AdPostRequest*)params completion:(void(^)(NSArray<HFPost*> *results))completion;

+ (void) clansWithRequest:(ClansRequest*)request completion:(void (^)(NSArray * results, NSInteger count))completion;

+ (void)userClansWithParam:(ClansRequest *)param completion:(void (^)(NSArray *))completion;

+ (void) membersWithRequest:(ClansRequest*)request completion:(void (^)(NSArray * results, NSInteger count))completion;

+ (void)info:(ClansRequest *)params completion:(void (^)(CDClan *clan, NSArray* soloLead, NSArray *hotseatLead))completion;

+ (void) joinToClan:(ClansRequest*)request completion:(void (^)(BOOL isSend))completion;

+ (void) leaveClan:(ClansRequest*)request completion:(void (^)(BOOL))completion;
@end
