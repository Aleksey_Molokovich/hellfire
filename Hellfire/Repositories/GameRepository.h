//
//  GameRepository.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameRequest;
@class HFSoloGameResult;
@class HotseatGameRoundRequest;
@class HFHotseatGameResult;
@interface GameRepository : NSObject


+ (void)createSolo:(GameRequest*)game
        completion:(void(^)(HFSoloGameResult *receive))completion;

+ (void)createHotseat:(GameRequest *)game
           completion:(void (^)(NSNumber *))completion;

+ (void)invitePlayers:(NSArray *)players
                 game:(NSNumber*)game
           completion:(void (^)(BOOL))completion;

+ (void)setSettingsForGame:(NSNumber*)game
                roundCount:(NSNumber*)count
                  duration:(NSNumber*)duration
           completion:(void (^)(BOOL))completion;

+ (void)acceptInviteToGame:(NSNumber *)game
               completion:(void (^)(NSNumber *))completion;

+ (void)rejectInviteToGame:(NSNumber *)game
               completion:(void (^)(NSNumber *))completion;

+ (void)checkPlayers:(NSArray *)players
                game:(NSNumber*)game
          completion:(void (^)(NSArray*))completion;

+ (void)saveHotseatGameRound:(HotseatGameRoundRequest*)round
                  completion:(void (^)(HFHotseatGameResult *result))completion;

+(void)shareSolo:(NSNumber *)playerId
            game:(NSNumber *)game
         message:(NSString*)message
      completion:(void (^)(NSArray*))completion;

@end
