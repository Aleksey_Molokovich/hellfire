//
//  StatisticsRepository.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UsersStatisticsModel;
@interface StatisticsRepository : NSObject

+ (void)statisticsWithCompletion:(void(^)(UsersStatisticsModel *receive))completion;
+ (void)statisticsSoloWithParam:(id)param completion:(void(^)(NSArray *receive, NSNumber *count))completion;
+ (void)statisticsHotseatWithParam:(id)param completion:(void(^)(NSArray *receive, NSNumber *count))completion;
@end
