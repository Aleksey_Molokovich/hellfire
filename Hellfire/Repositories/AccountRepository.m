//
//  AccountRepository.m
//  Hellfire
//
//  Created by Алексей Молокович on 21.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AccountRepository.h"
#import <UIKit/UIKit.h>
#import "Account.h"
#import "AccountNew.h"
#import "ResponseModel.h"
#import "HellfireDefines.h"
#import "ProfileResponse.h"
#import "BluetoothService.h"
//@import Firebase;
@implementation AccountRepository

+(void)recoveryPasswordWithEmail:(NSString *)email completion:(void (^)(ResponseModel *))completion
{
    AccountNew *account=[AccountNew new];
    account.email = email;
    [HTTPService getRequestWithUrl:@"account/recovery_password"
                          andParams:[self addInfo:account]
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             if(completion){
                                 completion(responseObject);
                             }
                         }];
}

+(void)changePassword:(NSString*)password newPassword:(NSString *)newPassword completion:(void (^)(ResponseModel *))completion
{
    [HTTPService postRequestWithUrl:@"account/change_password"
                         andParams:@{@"oldPassword":password,
                                     @"newPassword":newPassword,
                                     }
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            if(completion){
                                completion(responseObject);
                            }
                        }];
}

+(void)autoLoginWithCompletion:(void (^)(ResponseModel *response))completion{
    AccountNew *account=[AccountNew new];
    account.token=[[NSUserDefaults standardUserDefaults] objectForKey:SessionToken];

    if (!account.token){
        completion(nil);
        return;
    }
    
    [self registration:account url:@"account/auto_login" completion:completion];
}

+(void)loginWithFacebook:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion{
    account.social=@"facebook";
     [self registration:account url:@"account/login_social" completion:completion];
}

+(void)loginWithVK:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion{
    account.social=@"vk";

     [self registration:account url:@"account/login_social" completion:completion];
}

+(void)registrationWithFacebook:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion{
    
    account.social=@"facebook";
     [self registration:account url:@"account/register_social" completion:completion];
}

+(void)registrationWithVK:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion{
    
    account.social=@"vk";
     [self registration:account url:@"account/register_social" completion:completion];
}

+(void)registration:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion{
     [self registration:account url:@"account/register_auth" completion:completion];
}

+(void)logIn:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion{
        account.country = nil;
        account.city = nil;
        account.cityId = nil;
     [self registration:account url:@"account/auth" completion:completion];
}

+(void)registration:(AccountNew *)account url:(NSString*)url completion:(void (^)(ResponseModel *response))completionn{
    
     [HTTPService postRequestWithUrl:url
                                 andParams:[self addInfo:[account copy]]
                                completion:^(ResponseModel *responseObject, NSError *error) {
    
               
                                    if(completionn){
                                        if (responseObject.status == 1 || responseObject.status == 2)
                                        {
                                            
                                            CDAccount *account = [CDAccount MR_findFirst];

                                            if (responseObject.data && responseObject.data[@"profile"])
                                            {
                                                NSError *err;
                                                ProfileResponse *profile = [[ProfileResponse alloc] initWithDictionary:responseObject.data[@"profile"] error:&err];
                                                [BluetoothService sharedInstance].isSendMetrics = profile.isSendMetrics;
                                                account = [profile toCoreData];
                                            }
                                            
                                            [[NSUserDefaults standardUserDefaults] setObject:responseObject.data[@"token"]
                                                                                      forKey:SessionToken];
                                            [[NSUserDefaults standardUserDefaults] synchronize];
                                            account.state = AccountStateTypeSuccessfulRegister;
                                            
//                                            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
                                            
                                        }
                                        completionn(responseObject);
                                    }
                                }];
}

+(NSDictionary*)addInfo:(AccountNew*)account{
#if TARGET_IPHONE_SIMULATOR
    account.deviceId=@"06E2C358-13A5-4DEF-9D1A-7F9DA4B6BA42";
    account.pushToken=@"d9a8c37aac88056b57cc427dae6db9dd64489c28f1f21455deb50f4e78fdc8aa";
#else
    account.pushToken=[[NSUserDefaults standardUserDefaults] objectForKey:DevicePushToken];
    account.deviceId=[[NSUserDefaults standardUserDefaults] objectForKey:DeviceID];
#endif
    account.city = nil;
    account.country = nil;
    
     return [account toDictionary];
}

+(void)logOut
{
    [HTTPService getRequestWithUrl:@"account/logout" andParams:nil completion:^(ResponseModel *responseObject, NSError *error) {
        
    }];
}
@end
