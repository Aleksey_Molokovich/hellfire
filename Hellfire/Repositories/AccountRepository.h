//
//  AccountRepository.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPService.h"
@class ResponseModel;
@class AccountNew;


@interface AccountRepository : NSObject

+(void)recoveryPasswordWithEmail:(NSString*)email completion:(void (^)(ResponseModel *response))completion;

+(void)changePassword:(NSString*)password newPassword:(NSString *)newPassword completion:(void (^)(ResponseModel *))completion;

+(void)autoLoginWithCompletion:(void (^)(ResponseModel *response))completion;

+(void)loginWithFacebook:(AccountNew *)account completion:(void(^)(ResponseModel *response))completion;

+(void)registrationWithFacebook:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion;


+(void)loginWithVK:(AccountNew *)account completion:(void(^)(ResponseModel *response))completion;

+(void)registrationWithVK:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion;

+(void)registration:(AccountNew*)account completion:(void (^)(ResponseModel *response))completion;

+(void)logIn:(AccountNew *)account completion:(void (^)(ResponseModel *response))completion;

+(void)logOut;


@end
