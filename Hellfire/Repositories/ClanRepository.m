//
//  ClanRepository.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanRepository.h"
#import "AdPostRequest.h"
#import "HTTPService.h"
#import "AdPostResponse.h"
#import "ClansRequest.h"
#import "CDClan+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "HFPost.h"
#import "CoreDataService.h"

@implementation ClanRepository

+(void)info:(ClansRequest *)params completion:(void (^)(CDClan *, NSArray *, NSArray *))completion
{
    
    [HTTPService getRequestWithUrl:@"clan/info"
                         andParams:[params toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1) {
                                    CDClan *result = [CDClan MR_importFromObject:responseObject.data[@"clan"]];
                                    
                                    NSArray *soloLead = [CDPlayer MR_importFromArray:responseObject.data[@"soloLeaders"]];
                                    NSArray *hotseatLead = [CDPlayer MR_importFromArray:responseObject.data[@"hotseatLeaders"]];
                                    completion(result, soloLead, hotseatLead);
                                }else{
                                    completion(nil, nil, nil);
                                }
                                
                            }
                            
                        }];
    
}

+(void)posts:(AdPostRequest *)params completion:(void (^)(NSArray<HFPost *> *))completion{
    
     [HTTPService getRequestWithUrl:@"clan/posts"
                          andParams:[params toDictionary]
                         completion:^(ResponseModel *responseObject, NSError *error) {
                             
                              if (completion) {
                                  if (responseObject.status == 1) {
                                      NSArray *results = [HFPost arrayOfModelsFromDictionaries:responseObject.data[@"adPosts"] error:nil];
                                    
                                      completion(results);
                                  }else{
                                        completion(nil);
                                  }
                                  
                              }
                             
                          }];
    
}

+ (void)membersWithRequest:(ClansRequest *)request completion:(void (^)(NSArray *, NSInteger))completion
{
    [HTTPService getRequestWithUrl:@"clan/members"
                         andParams:[request toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            if (completion)
                            {
                                if (responseObject.status == 1)
                                {
                                    NSArray *results = [CDPlayer MR_importFromArray:responseObject.data[@"users"]];
                                    completion(results,responseObject.meta.count);
                                }else{
                                    completion(nil,0);
                                }
                            }
                        }];
}

+ (void) clansWithRequest:(ClansRequest*)request completion:(void (^)(NSArray *, NSInteger))completion
{
//    request.includeMyClans = @"true";
    
    [HTTPService getRequestWithUrl:@"clan/clans"
                         andParams:[request toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            if (completion)
                            {
                                if (responseObject.status == 1)
                                {
                                    NSArray *results = [CDClan MR_importFromArray:responseObject.data[@"clans"]];
                                    completion(results,responseObject.meta.count);
                                }else{
                                    completion(nil,0);
                                }
                            }
                        }];
}

+ (void)userClansWithParam:(ClansRequest *)param completion:(void (^)(NSArray *))completion
{
    
    [HTTPService getRequestWithUrl:@"user/clans"
                         andParams:[param toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            
                            if (completion) {
                                if (responseObject.status == 1)
                                {
                                    NSArray *results = [CDClan MR_importFromArray:responseObject.data[@"clans"]];
                                    completion(results);
                                }else{
                                    completion(nil);
                                }
                                
                            }
                        }];
}

+ (void) joinToClan:(ClansRequest*)request completion:(void (^)(BOOL))completion
{
    [HTTPService getRequestWithUrl:@"clan/join"
                         andParams:[request toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            if (completion)
                            {
                                if (responseObject.status == 2)
                                {
                                    completion(YES);
                                }else{
                                    completion(NO);
                                }
                            }
                         }];
}

+ (void) leaveClan:(ClansRequest*)request completion:(void (^)(BOOL))completion
{
    [HTTPService getRequestWithUrl:@"clan/leave"
                         andParams:[request toDictionary]
                        completion:^(ResponseModel *responseObject, NSError *error) {
                            if (completion)
                            {
                                if (responseObject.status == 2)
                                {
                                    completion(YES);
                                }else{
                                    completion(NO);
                                }
                            }
                        }];
}

@end
