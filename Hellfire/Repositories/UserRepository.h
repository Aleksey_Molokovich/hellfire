//
//  UserRepository.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UserProfile;
@class PlayersRequest;
@class CDPlayer;
@class ClansRequest;
@interface UserRepository : NSObject

+ (void)userProfileWithCompletion:(void (^)(NSArray *))completion;

+ (void)updateUserProfile:(UserProfile*)profile completion:(void(^)(id receive))completion;

+ (void)playersWithParam:(PlayersRequest*)param
              completion:(void (^)(NSArray * results, NSInteger count))completion;

+ (void)friendsWithParam:(PlayersRequest*)param
              completion:(void (^)(NSArray *))completion;

+ (void)clansWithParam:(ClansRequest*)param
              completion:(void (^)(NSArray *))completion;

+ (void)addFriend:(CDPlayer*)player completion:(void (^)(BOOL response))completion;

+ (void)acceptFriend:(CDPlayer*)player completion:(void (^)(BOOL response))completion;

+ (void)removeFriend:(CDPlayer *)player completion:(void (^)(BOOL))completion;

+ (void)rejectFriend:(CDPlayer *)player completion:(void (^)(BOOL))completion;

+ (void)info:(NSInteger)playerId completion:(void (^)(CDPlayer *user, NSArray *clans))completion;
@end
