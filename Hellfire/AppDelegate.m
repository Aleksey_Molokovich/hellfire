//
//  AppDelegate.m
//  Hellfire
//
//  Created by Алексей Молокович on 19.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//
@import Firebase;
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>
#import <AFNetworkActivityLogger/AFNetworkActivityConsoleLogger.h>
#import "VKSdk.h"
#import "PushTokenService.h"
#import <MagicalRecord/MagicalRecord.h>
#import "HellfireDefines.h"
#import "NotificationService.h"

@import UserNotifications;
@interface AppDelegate ()<UNUserNotificationCenterDelegate>
@property (strong, nonatomic) NotificationService *notificationService;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [FIRApp configure];
//    [FIRMessaging messaging].delegate = self;

    AFNetworkActivityConsoleLogger *loger=[AFNetworkActivityConsoleLogger new];
    [loger setLevel:AFLoggerLevelDebug];
    
    [[AFNetworkActivityLogger sharedLogger] addLogger:loger];
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [Fabric with:@[[Crashlytics class]]];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [application registerForRemoteNotifications];
    
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"HellfireCoreData"];
    [NSManagedObjectContext MR_context];
    
    if (launchOptions && launchOptions[@"UIApplicationLaunchOptionsRemoteNotificationKey"]) {
        [[NotificationService shared] openWithPush:launchOptions[@"UIApplicationLaunchOptionsRemoteNotificationKey"]];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    BOOL fb=[[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    
    BOOL vk=[VKSdk processOpenURL:url fromApplication:sourceApplication];
    return fb || vk;
}


#pragma mark UIUserNotification protocol
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    

    [[NotificationService shared] updateWithPush:userInfo];

    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:token
                                              forKey:DevicePushToken];
    
    [[NSUserDefaults standardUserDefaults] setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString]
                                              forKey:DeviceID];
    
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"appleToken---%@", token);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[NotificationService shared] openWithPush:userInfo];
}



- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"%@",str);
}


@end
