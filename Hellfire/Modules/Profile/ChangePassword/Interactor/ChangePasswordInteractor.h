//
//  ChangePasswordInteractor.h
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ChangePasswordInteractorInput.h"

@protocol ChangePasswordInteractorOutput;

@interface ChangePasswordInteractor : NSObject <ChangePasswordInteractorInput>

@property (nonatomic, weak) id<ChangePasswordInteractorOutput> output;

@end
