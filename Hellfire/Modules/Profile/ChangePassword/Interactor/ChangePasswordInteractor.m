//
//  ChangePasswordInteractor.m
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ChangePasswordInteractor.h"
#import "ChangePasswordInteractorOutput.h"
#import "AccountRepository.h"
#import "ResponseModel.h"
#import "CDAccount+CoreDataClass.h"
@implementation ChangePasswordInteractor

#pragma mark - Методы ChangePasswordInteractorInput


- (void)updatePassword:(NSString*)password newPassword:(NSString *)newPassword
{
    [AccountRepository changePassword:password newPassword:newPassword completion:^(ResponseModel *response) {
                               
                               if (response.status == 2) {
                                   [CDAccount MR_findFirst].password = password;
//                                   [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                               }
                               
                               [self.output succesfulWithMessage:response.error.message];
                           }];
}
@end
