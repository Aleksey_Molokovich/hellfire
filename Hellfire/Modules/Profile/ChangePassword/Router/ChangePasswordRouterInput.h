//
//  ChangePasswordRouterInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChangePasswordRouterInput <NSObject>

- (void)closeCurrentModule;
@end
