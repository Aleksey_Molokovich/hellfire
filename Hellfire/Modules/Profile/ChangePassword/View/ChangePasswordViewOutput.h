//
//  ChangePasswordViewOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChangePasswordViewOutput <NSObject>

/**
 @author Алексей Молокович

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)updatePassword:(NSString*)password newPassword:(NSString *)newPassword;
- (void)close;
@end
