//
//  ChangePasswordViewController.m
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "CDAccount+CoreDataClass.h"
#import "ChangePasswordViewOutput.h"
//#import <LGAlertView/LGAlertView.h>
@implementation ChangePasswordViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}
- (IBAction)goBack:(id)sender {
    [self.output close];
}


- (IBAction)btnOldeye:(id)sender {
    self.btnOldEye.hide =! self.btnOldEye.hide;
    self.tfOldPassword.secureTextEntry = self.btnOldEye.hide;
}
- (IBAction)btnNeweye:(id)sender {
    self.btnNewEye.hide =! self.btnNewEye.hide;
    self.tfNewPassword.secureTextEntry = self.btnNewEye.hide;
}
- (IBAction)btnNew2eye:(id)sender {
    self.btnNew2Eye.hide =! self.btnNew2Eye.hide;
    self.tfConfirmPassword.secureTextEntry = self.btnNew2Eye.hide;
}


- (IBAction)saveChange:(id)sender {
    
    CDAccount *account = [CDAccount MR_findFirst];
    
    
    BOOL isNewTrue = [self.tfNewPassword.text isEqualToString:self.tfConfirmPassword.text];
    
    
    if (!isNewTrue) {
        [[LGAlertView alertViewWithTitle:nil message:@"Новый пароль не совпадает" style:LGAlertViewStyleActionSheet buttonTitles:nil cancelButtonTitle:@"Ok" destructiveButtonTitle:nil] showAnimated];
        return;
    }
    

    
    [self.output updatePassword:self.tfOldPassword.text newPassword:self.tfConfirmPassword.text];
    
}

#pragma mark - Методы ChangePasswordViewInput

- (void)setupInitialState {
	[self prepareStyleWithHFBGColorType:HFBackgroundColorBlue];
    [self hideBottomBar];
    [self prepareBackgroundWithHFBGColorType:HFBackgroundColorWhite];
    
    self.btnOldEye.hide = YES;
    self.btnNewEye.hide = YES;
    self.btnNew2Eye.hide = YES;
    
    [self.btnOldEye setWhite:NO];
    [self.btnNewEye setWhite:NO];
    [self.btnNew2Eye setWhite:NO];
}

@end
