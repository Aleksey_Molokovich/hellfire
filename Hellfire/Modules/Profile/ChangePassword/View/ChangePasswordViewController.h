//
//  ChangePasswordViewController.h
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangePasswordViewInput.h"
#import "BaseViewController.h"
#import "UIButtonEye.h"

@protocol ChangePasswordViewOutput;

@interface ChangePasswordViewController : BaseViewController <ChangePasswordViewInput>

@property (nonatomic, strong) id<ChangePasswordViewOutput> output;
@property (weak, nonatomic) IBOutlet UITextField *tfOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPassword;

@property (weak, nonatomic) IBOutlet UIButtonEye *btnOldEye;
@property (weak, nonatomic) IBOutlet UIButtonEye *btnNewEye;
@property (weak, nonatomic) IBOutlet UIButtonEye *btnNew2Eye;

@end
