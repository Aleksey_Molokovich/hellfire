//
//  ChangePasswordPresenter.m
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ChangePasswordPresenter.h"

#import "ChangePasswordViewInput.h"
#import "ChangePasswordInteractorInput.h"
#import "ChangePasswordRouterInput.h"
#import "UIColor+HF.h"
@implementation ChangePasswordPresenter

#pragma mark - Методы ChangePasswordModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы ChangePasswordViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)updatePassword:(NSString *)password newPassword:(NSString *)newPassword
{
    [self.view showProgressWithColor:[UIColor hfBlue]];
    [self.interactor updatePassword:password newPassword:newPassword];
}

- (void)close
{
    [_view hideProgress];
    [self.router closeCurrentModule];
}
#pragma mark - Методы ChangePasswordInteractorOutput

- (void)succesfulWithMessage:(NSString *)message
{
     [_view hideProgress];
    if (message) {
        [self.view showAlertWithMessage:message];
    }else{
       [self.router closeCurrentModule];
    }
}
@end
