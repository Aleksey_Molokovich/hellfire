//
//  ChangePasswordPresenter.h
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ChangePasswordViewOutput.h"
#import "ChangePasswordInteractorOutput.h"
#import "ChangePasswordModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol ChangePasswordViewInput;
@protocol ChangePasswordInteractorInput;
@protocol ChangePasswordRouterInput;

@interface ChangePasswordPresenter : NSObject <ChangePasswordModuleInput, ChangePasswordViewOutput, ChangePasswordInteractorOutput>

@property (nonatomic, weak) id<ChangePasswordViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<ChangePasswordInteractorInput> interactor;
@property (nonatomic, strong) id<ChangePasswordRouterInput> router;

@end
