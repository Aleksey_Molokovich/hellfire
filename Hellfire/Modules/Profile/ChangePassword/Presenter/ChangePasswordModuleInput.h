//
//  ChangePasswordModuleInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol ChangePasswordModuleInput <RamblerViperModuleInput>

/**
 @author Алексей Молокович

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
