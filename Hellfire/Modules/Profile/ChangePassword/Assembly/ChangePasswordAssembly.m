//
//  ChangePasswordAssembly.m
//  Hellfire
//
//  Created by Алексей Молокович on 09/04/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ChangePasswordAssembly.h"

#import "ChangePasswordViewController.h"
#import "ChangePasswordInteractor.h"
#import "ChangePasswordPresenter.h"
#import "ChangePasswordRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ChangePasswordAssembly

- (ChangePasswordViewController *)viewChangePassword {
    return [TyphoonDefinition withClass:[ChangePasswordViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChangePassword]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterChangePassword]];
                          }];
}

- (ChangePasswordInteractor *)interactorChangePassword {
    return [TyphoonDefinition withClass:[ChangePasswordInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChangePassword]];
                          }];
}

- (ChangePasswordPresenter *)presenterChangePassword{
    return [TyphoonDefinition withClass:[ChangePasswordPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewChangePassword]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorChangePassword]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerChangePassword]];
                          }];
}

- (ChangePasswordRouter *)routerChangePassword{
    return [TyphoonDefinition withClass:[ChangePasswordRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewChangePassword]];
                          }];
}

- (RamblerViperModuleFactory *)factoryChangePassword  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardChangePassword]];
                                                  [initializer injectParameterWith:@"ChangePasswordViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardChangePassword {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"User"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
