//
//  EditTextFieldRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "EditTextFieldRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface EditTextFieldRouter : BaseRouter <EditTextFieldRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
