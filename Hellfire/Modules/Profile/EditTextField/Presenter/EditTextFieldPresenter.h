//
//  EditTextFieldPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "EditTextFieldViewOutput.h"
#import "EditTextFieldInteractorOutput.h"
#import "EditTextFieldModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "EditTextFieldModuleOutput.h"

@protocol EditTextFieldModuleOutput;
@protocol BaseViewControllerProtocol;
@protocol EditTextFieldViewInput;
@protocol EditTextFieldInteractorInput;
@protocol EditTextFieldRouterInput;

@interface EditTextFieldPresenter : NSObject <EditTextFieldModuleInput, EditTextFieldViewOutput, EditTextFieldInteractorOutput>

@property (nonatomic, weak) id<EditTextFieldViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<EditTextFieldInteractorInput> interactor;
@property (nonatomic, strong) id<EditTextFieldRouterInput> router;
@property (nonatomic, strong) id<EditTextFieldModuleOutput> output;

@end
