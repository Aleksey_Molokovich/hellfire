//
//  EditTextFieldPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "EditTextFieldPresenter.h"

#import "EditTextFieldViewInput.h"
#import "EditTextFieldInteractorInput.h"
#import "EditTextFieldRouterInput.h"


@implementation EditTextFieldPresenter

#pragma mark - Методы EditTextFieldModuleInput

- (void)configureModuleWithTitle:(NSString *)title
{
    [_view showTitle:title];
}

- (void)setModuleOutput:(id<EditTextFieldModuleOutput>)moduleOutput
{
    self.output = moduleOutput;
}
#pragma mark - Методы EditTextFieldViewOutput

- (void)didTriggerViewReadyEvent
{
    NSString *text;
    if ([_output respondsToSelector:@selector(text)])
        text = [_output text];
    
	[self.view setupInitialStateWithText:text];
}

- (void)shouldSaveText:(NSString *)text
{
    [_output saveText:text];
    [_view goBack];
}
#pragma mark - Методы EditTextFieldInteractorOutput

@end
