//
//  EditTextFieldModuleOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 22.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef EditTextFieldModuleOutput_h
#define EditTextFieldModuleOutput_h
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol EditTextFieldModuleOutput<RamblerViperModuleOutput>

- (void)saveText:(NSString*)text;
- (NSString *)text;

@end

#endif /* EditTextFieldModuleOutput_h */
