//
//  EditTextFieldInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "EditTextFieldInteractorInput.h"

@protocol EditTextFieldInteractorOutput;

@interface EditTextFieldInteractor : NSObject <EditTextFieldInteractorInput>

@property (nonatomic, weak) id<EditTextFieldInteractorOutput> output;

@end
