//
//  EditTextFieldViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "EditTextFieldViewController.h"

#import "EditTextFieldViewOutput.h"

@implementation EditTextFieldViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (IBAction)goBack:(id)sender
{
    [self goBack];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)save:(id)sender
{
    [_output shouldSaveText:_textField.text];
}

#pragma mark - Методы EditTextFieldViewInput

- (void)setupInitialStateWithText:(NSString *)text
{
    _textField.text = text;
    self.roundedView.layer.borderColor = [UIColor blackColor].CGColor;
    self.roundedView.layer.borderWidth = 1;
    self.roundedView.layer.cornerRadius = self.roundedView.frame.size.height/2;
    self.roundedView.layer.masksToBounds = YES;
    [self prepareStyleWithHFBGColorType:HFBackgroundColorBlue];
    [self hideBottomBar];
    [self prepareBackgroundWithHFBGColorType:HFBackgroundColorWhite];
}


- (void)showTitle:(NSString *)title
{
    self.navigationItem.title = title;
}
@end
