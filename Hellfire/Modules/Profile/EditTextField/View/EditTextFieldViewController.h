//
//  EditTextFieldViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditTextFieldViewInput.h"
#import "BaseViewController.h"

@protocol EditTextFieldViewOutput;

@interface EditTextFieldViewController : BaseViewController <EditTextFieldViewInput>

@property (nonatomic, strong) id<EditTextFieldViewOutput> output;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIView *roundedView;

@end
