//
//  EditTextFieldViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EditTextFieldViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateWithText:(NSString*)text;
- (void)goBack;
- (void)showTitle:(NSString*)title;
@end
