//
//  EditTextFieldAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 EditTextField module
 */
@interface EditTextFieldAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryEditTextField;
@end
