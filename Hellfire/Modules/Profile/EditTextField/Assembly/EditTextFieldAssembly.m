//
//  EditTextFieldAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "EditTextFieldAssembly.h"

#import "EditTextFieldViewController.h"
#import "EditTextFieldInteractor.h"
#import "EditTextFieldPresenter.h"
#import "EditTextFieldRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation EditTextFieldAssembly

- (EditTextFieldViewController *)viewEditTextField {
    return [TyphoonDefinition withClass:[EditTextFieldViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterEditTextField]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterEditTextField]];
                          }];
}

- (EditTextFieldInteractor *)interactorEditTextField {
    return [TyphoonDefinition withClass:[EditTextFieldInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterEditTextField]];
                          }];
}

- (EditTextFieldPresenter *)presenterEditTextField{
    return [TyphoonDefinition withClass:[EditTextFieldPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewEditTextField]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorEditTextField]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerEditTextField]];
                          }];
}

- (EditTextFieldRouter *)routerEditTextField{
    return [TyphoonDefinition withClass:[EditTextFieldRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewEditTextField]];
                          }];
}

- (RamblerViperModuleFactory *)factoryEditTextField  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardEditTextField]];
                                                  [initializer injectParameterWith:@"EditTextFieldViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardEditTextField {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"User"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
