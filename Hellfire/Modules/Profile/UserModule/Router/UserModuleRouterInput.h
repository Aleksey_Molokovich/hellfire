//
//  UserModuleRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol GeoCatalogModuleOutput;
@protocol EditTextFieldModuleOutput;

@protocol UserModuleRouterInput <NSObject>

- (void)closeCurrentModule;
- (void)presentRegistration;
- (void)pushEditTextFieldWithOutput:(id<EditTextFieldModuleOutput>)output title:(NSString*)title;
- (void)pushEditLocationWithOutput:(id<GeoCatalogModuleOutput>)output;
- (void)pushUsersStatisticsWithSolo:(BOOL)isSolo;
- (void)presentAgreement;
- (void)pushEditPasswordWithOutput:(id<EditTextFieldModuleOutput>)output;
@end
