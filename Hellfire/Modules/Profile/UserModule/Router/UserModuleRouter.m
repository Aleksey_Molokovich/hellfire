//
//  UserModuleRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleRouter.h"

#import "EditTextFieldAssembly.h"
#import "EditTextFieldModuleOutput.h"
#import "EditTextFieldModuleInput.h"
#import "UsersStatisticsAssembly.h"
#import "UsersStatisticsModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "AgreementModuleAssembly.h"
#import "AgreementModuleModuleInput.h"
#import "ChangePasswordAssembly.h"
#import "ChangePasswordModuleInput.h"
@interface UserModuleRouter()

@property (strong, nonatomic) EditTextFieldAssembly *editTextFieldAssembly;
@property (strong, nonatomic) UsersStatisticsAssembly *usersStatisticsAssembly;
@property (nonatomic) AgreementModuleAssembly *agreementModuleAssembly;
@property (nonatomic) ChangePasswordAssembly *changePasswordAssembly;

@end


@implementation UserModuleRouter

#pragma mark - Методы UserModuleRouterInput

- (void)pushUsersStatisticsWithSolo:(BOOL)isSolo
{
    _usersStatisticsAssembly = [[UsersStatisticsAssembly new] activated];
    self.factory = [_usersStatisticsAssembly factoryUsersStatistics];
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<UsersStatisticsModuleInput> moduleInput) {
                       [moduleInput configureModuleForSolo:isSolo];
                       return nil;
                   }];
}

- (void)pushEditTextFieldWithOutput:(id<EditTextFieldModuleOutput>)output title:(NSString*)title
{
    _editTextFieldAssembly = [[EditTextFieldAssembly new] activated];
    self.factory = [_editTextFieldAssembly factoryEditTextField];
    [self pushModuleUsingFactory:self.factory
                      withLinkBlock:^id<RamblerViperModuleOutput>(id<EditTextFieldModuleInput> moduleInput) {
                          [moduleInput configureModuleWithTitle:title];
                          return output;
                      }];
}

- (void)pushEditPasswordWithOutput:(id<EditTextFieldModuleOutput>)output
{
    _changePasswordAssembly = [[ChangePasswordAssembly new] activated];
    self.factory = [_changePasswordAssembly factoryChangePassword];
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<EditTextFieldModuleInput> moduleInput) {
                       return output;
                   }];
}

- (void)pushEditLocationWithOutput:(id<EditTextFieldModuleOutput>)output
{
    self.geoCatalogAssembly = [[GeoCatalogAssembly new] activated];
    
    self.factory = [self.geoCatalogAssembly factoryGeoCatalog];
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                      withLinkBlock:^id<RamblerViperModuleOutput>(id<GeoCatalogModuleInput> moduleInput) {
                          [moduleInput configureModuleWithType:CatalogCountry
                                                         style:CatalogStyleLight
                                                        action:CatalogActionEditUserLocation
                                                       country:nil];
                          return output;
                      }];
}

- (void)presentAgreement
{
    _agreementModuleAssembly=[[AgreementModuleAssembly new] activated];
    self.factory=[_agreementModuleAssembly factoryAgreementModule];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<AgreementModuleModuleInput> moduleInput) {
                                                  [moduleInput configureModuleFirstTime:NO];
                                                  return nil;
                                              }];
}
@end
