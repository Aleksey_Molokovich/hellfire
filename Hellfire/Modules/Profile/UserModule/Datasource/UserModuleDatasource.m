//
//  UserModuleDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleDatasource.h"
#import "ObjectTableViewCell.h"
#import "UserModuleDatasourceOutput.h"
#import "CallBackActionProtocol.h"

@interface UserModuleDatasource()<CallBackActionProtocol>

@property (nonatomic, strong) NSMutableArray<ObjectTableViewCell*> *data;

@end

@implementation UserModuleDatasource

#pragma mark - Методы UserModuleDatasourceInput
- (void)prepareDatasourceForNonRegisterUser
{
	_data=[NSMutableArray new];
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"ProfileRegistrationTableViewCell";
    item.height=-1;
    item.viewData.data = nil;
    [_data addObject:item];
}

- (void)prepareDatasourceForRegisterUser
{
    _data=[NSMutableArray new];
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"UserProfileTableViewCell";
    item.height=728;
    item.viewData.data = nil;
    [_data addObject:item];
}

-(NSInteger)count{
    return _data.count;
}

-(CGFloat)heightAtIndex:(NSInteger)index{
    return _data[index].height;
}

-(NSString *)cellIdAtIndex:(NSInteger)index{
    return _data[index].cellId;
}

-(id)dataAtIndex:(NSInteger)index{
    return _data[index].data;
}
-(NSArray*)getCellsId{
    
    NSMutableArray *cellIds = [NSMutableArray new];
    
    for (ObjectTableViewCell *item in _data) {
        [cellIds addObject:item.cellId];
    }
    
    return cellIds;
}
@end
