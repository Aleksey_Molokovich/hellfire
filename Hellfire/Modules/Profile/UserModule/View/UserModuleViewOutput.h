//
//  UserModuleViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"

@protocol UserModuleViewOutput <CallBackActionProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)logOut;
- (void)saveAvatar:(NSData*)data;
- (void)viewWillAppear;
@end
