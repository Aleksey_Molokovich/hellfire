//
//  UserModuleViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserModuleViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateRegister;
- (void)setupInitialStateNonRegister;
- (void)updateCells;
- (void)update;
@end
