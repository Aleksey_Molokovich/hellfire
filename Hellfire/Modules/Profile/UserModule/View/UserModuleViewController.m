//
//  UserModuleViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleViewController.h"
#import "BaseTableViewCell.h"
#import "UserModuleViewOutput.h"
#import "CellManager.h"
#import "CDAccount+CoreDataClass.h"
#import <LGHelper/UIColor+LGHelper.h>
#import "HellfireDefines.h"
#import <LGHelper/UIImage+LGHelper.h>
@implementation UserModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
	[self.output didTriggerViewReadyEvent];
    
    self.offset =CGPointMake(0, 0);
}

- (IBAction)takePhoto:(id)sender {
    
    self.alert = [[LGAlertView alloc] initWithTitle:nil
                                            message:nil
                                              style:LGAlertViewStyleActionSheet
                                       buttonTitles:@[@"Галерея",@"Камера"]
                                  cancelButtonTitle:@"Отмена"
                             destructiveButtonTitle:nil] ;
    self.alert.delegate = self;
    
    self.alert.buttonsHeight = 44;
    [self.alert showAnimated];
    
    
}

- (IBAction)logOut:(id)sender {
    [_output logOut];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.output viewWillAppear];
    
    self.tableView.estimatedRowHeight = self.tableView.frame.size.height;
    [self showBottomBar];
    [self.tableView setContentOffset:self.offset animated:NO];

    [_tableView reloadData];
    
}

#pragma mark - Методы UserModuleViewInput

- (void)setupInitialStateNonRegister {
    [self hideNaviagtionBar];
    [self prepareBackgroundWithHFBGColorType:HFBackgroundColorGray];
}
- (void)setupInitialStateRegister
{
    
    [self prepareNavigationBarWithHFBGColorType:HFBackgroundColorClear];
    [self prepareBackgroundWithHFBGColorType:HFBackgroundColorGray];
    [self hideBackButton];
}



-(void)updateCells
{
    [CellManager registerCellsWithId:[_datasource getCellsId]
                           tableView:_tableView];
}
-(void)update
{
    [self.tableView setContentOffset:self.offset animated:NO];
    __weak typeof(self) weakSelf = self;

    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        [blockSelf.tableView reloadData];
    }];
}

#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource cellIdAtIndex:indexPath.row]];
    [cell configureWithData:[_datasource dataAtIndex:indexPath.row]];
    cell.output = _output;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if ([_datasource heightAtIndex:indexPath.row]<0) {
//        return self.tableView.frame.size.height;
//    }
    return UITableViewAutomaticDimension;
}

#pragma mark UIImagePickerController

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];

    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    cropViewController.aspectRatioPreset = TOCropViewControllerAspectRatioPresetSquare;
    cropViewController.aspectRatioLockEnabled = YES;
    cropViewController.toolbar.rotateClockwiseButtonHidden = YES;
    cropViewController.toolbar.rotateCounterclockwiseButtonHidden = YES;
    cropViewController.toolbar.clampButtonHidden = YES;
    cropViewController.toolbar.resetButton.hidden = YES;
    cropViewController.toolbar.cancelTextButtonTitle = @"Отменить";
    cropViewController.toolbar.doneTextButtonTitle = @"Сохранить";
    [picker dismissViewControllerAnimated:NO completion:nil];
    [self presentViewController:cropViewController animated:YES completion:nil];
    
}



- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if (index == 0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }else
    {
       picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        picker.showsCameraControls = YES;
    }
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
        NSData *data = UIImageJPEGRepresentation([image imageScaledToSize:CGSizeMake(500, 500) scalingMode:LGImageScalingModeAspectFill],0.7);
        [_output saveAvatar:data];
    
    [cropViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UITableView *)scrollView{

    if (scrollView.contentOffset.y<0) {
        return;
    }
    self.offset = scrollView.contentOffset;

}
@end
