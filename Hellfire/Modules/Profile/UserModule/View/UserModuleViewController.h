//
//  UserModuleViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModuleViewInput.h"
#import "BaseViewController.h"
#import "UserModuleDatasourceInput.h"
#import <TOCropViewController/TOCropViewController.h>



@protocol UserModuleViewOutput;
@protocol UserModuleDatasourceInput;

@interface UserModuleViewController : BaseViewController <UserModuleViewInput,UINavigationControllerDelegate, UIImagePickerControllerDelegate, TOCropViewControllerDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) id<UserModuleViewOutput> output;
@property (nonatomic, strong) id<UserModuleDatasourceInput> datasource;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) CGPoint offset;
@end
