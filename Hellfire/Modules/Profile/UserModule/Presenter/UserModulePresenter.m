//
//  UserModulePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModulePresenter.h"

#import "UserModuleViewInput.h"
#import "UserModuleInteractorInput.h"
#import "UserModuleRouterInput.h"
#import "HellfireDefines.h"
#import "UserProfileTableViewCellActions.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "UIColor+HF.h"
#import "Definitions.h"

typedef NS_ENUM(NSInteger, UserModuleActionType)
{
    UserModuleActionEditNickname,
    UserModuleActionEditPassword
};

@interface UserModulePresenter()
@property (nonatomic, assign) UserModuleActionType action;
@end

@implementation UserModulePresenter

#pragma mark - Методы UserModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы UserModuleViewOutput

- (void)didTriggerViewReadyEvent {
   
    [self didChangeStatus];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didChangeStatus)
     name:HFNotificationChangeAccountStatus object:nil];
}

- (void)viewWillAppear
{
    if ([_interactor accountState] == AccountStateTypeSuccessfulRegister) {
        [self.interactor reloadInfo];
    }
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)saveAvatar:(NSData *)data
{
    [_interactor saveAvatar:data];
}

#pragma mark - Методы UserModuleInteractorOutput

- (void)didChangeStatus{
    switch ([_interactor accountState] ) {
        case AccountStateTypeNonRegister:
            [_datasource prepareDatasourceForNonRegisterUser];
            [_view updateCells];
            [self.view setupInitialStateNonRegister];
            break;
        case AccountStateTypeSuccessfulRegister:
            [_datasource prepareDatasourceForRegisterUser];
            [_view updateCells];
            [self.view setupInitialStateRegister];
            break;
        default:
            break;
    }
    
    [_view update];
}

- (void)showUploadProgress:(float)progress
{
    if (progress == 0) {
        [_view showProgressWithColor:[UIColor hfBlue]];
    }else if(progress == 1)
    {
        [_view hideProgress];
    }else
    {
      [_view updateUploadProgress:progress];
    }
    
}

- (void)showProgress
{
    [_view showProgressWithColor:[UIColor hfBlue]];
}

- (void)hideProgress
{
    [_view hideProgress];
}

- (void)logOut
{
    [_interactor logOut];
}
#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    switch ([_interactor accountState]) {
        case AccountStateTypeNonRegister:
            [_router presentRegistration];
            break;
        case AccountStateTypeSuccessfulRegister:
            if ([object.action isEqualToString:HFActionEditUserNickname])
            {
                _action = UserModuleActionEditNickname;
                [_router pushEditTextFieldWithOutput:self title:@"Псевдоним"];
            }
            else if ([object.action isEqualToString:HFActionEditUserLocation])
            {
                [_router pushEditLocationWithOutput:self];
            }
            else if ([object.action isEqualToString:HFActionEditUserPassword])
            {
                _action = UserModuleActionEditPassword;
                [_router pushEditPasswordWithOutput:self];
            }
            else if ([object.action isEqualToString:HFActionShowSoloStatistics])
            {
                [_router pushUsersStatisticsWithSolo:YES];
            }
            else if ([object.action isEqualToString:HFActionShowHotseatStatistics])
            {
                [_router pushUsersStatisticsWithSolo:NO];
            }
            else if ([object.action isEqualToString:kHFActionShowGameRule])
            {
                CDAccount *account = [CDAccount MR_findFirst];
                if (!account.soloRule || !account.hotseatRule) {
                    account.soloRule = YES;
                    account.hotseatRule = YES;
                }else
                {
                    account.soloRule = NO;
                    account.hotseatRule = NO;
                }
//                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                
                [self.view update];
            }
            else if ([object.action isEqualToString:kHFActionShowAgreement])
            {
                [self.router presentAgreement];
            }
            break;
        default:
            break;
    }
    
}

#pragma mark - Методы EditTextField

- (void)saveText:(NSString *)text
{
    if (_action == UserModuleActionEditNickname)
    {
        [_interactor changeNickname:text];
    }
    else if (_action == UserModuleActionEditPassword)
    {
        [_interactor changePassword:text];
    }
    
    [_view update];
}

-(NSString *)text
{
    if (_action == UserModuleActionEditNickname)
    {
       return [CDAccount MR_findFirst].user.nickname;
    }
    else if (_action == UserModuleActionEditPassword)
    {
        return [CDAccount MR_findFirst].password;
    }
    
    return nil;
}

#pragma mark - Методы GeoCatalogModuleOutput

- (void)changeCity:(CatalogModel *)model
{
    [_interactor changeCity:model];
}

- (void)changeCountry:(CatalogModel *)model
{
    [_interactor changeCountry:model];
}


@end
