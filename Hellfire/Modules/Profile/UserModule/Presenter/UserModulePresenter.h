//
//  UserModulePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleViewOutput.h"
#import "UserModuleInteractorOutput.h"
#import "UserModuleModuleInput.h"
#import "UserModuleDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"
#import "EditTextFieldModuleOutput.h"
#import "GeoCatalogModuleOutput.h"

@protocol GeoCatalogModuleOutput;
@protocol BaseViewControllerProtocol;
@protocol UserModuleViewInput;
@protocol UserModuleInteractorInput;
@protocol UserModuleRouterInput;
@protocol UserModuleDatasourceOutput;

@interface UserModulePresenter : NSObject <UserModuleModuleInput, UserModuleViewOutput, UserModuleInteractorOutput, EditTextFieldModuleOutput, GeoCatalogModuleOutput>

@property (nonatomic, weak) id<UserModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<UserModuleInteractorInput> interactor;
@property (nonatomic, strong) id<UserModuleRouterInput> router;
@property (nonatomic, strong) id<UserModuleDatasourceOutput> datasource;


@end
