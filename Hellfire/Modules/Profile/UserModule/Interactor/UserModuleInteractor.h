//
//  UserModuleInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleInteractorInput.h"

@protocol UserModuleInteractorOutput;
@class CatalogModel;

@interface UserModuleInteractor : NSObject <UserModuleInteractorInput>

@property (nonatomic, weak) id<UserModuleInteractorOutput> output;
@property (nonatomic, strong) CatalogModel *changedCountry;
@end
