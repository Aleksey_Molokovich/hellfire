//
//  UserModuleInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleInteractor.h"
#import "AccountService.h"
#import "AccountRepository.h"
#import "UserModuleInteractorOutput.h"
#import "SubscribeService.h"
#import "UserRepository.h"
#import "UserProfile.h"
#import "CatalogModel.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "ResponseModel.h"
#import <SDWebImage/SDWebImageManager.h>

@implementation UserModuleInteractor

#pragma mark - Методы UserModuleInteractorInput
- (void)reloadInfo
{
    __weak typeof(self) weakSelf = self;
    [_output showProgress];
    [UserRepository userProfileWithCompletion:^(NSArray *res) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        [blockSelf.output didChangeStatus];
        [blockSelf.output hideProgress];
    }];
}

-(AccountStateType)accountState
{
    return [AccountService state];
}

- (void)changePassword:(NSString*)text
{
    CDAccount *account = [CDAccount MR_findFirst];
    account.password = text;
    
}

- (void)changeNickname:(NSString*)text
{
    [CDAccount MR_findFirst].user.nickname = text;
    [CDAccount MR_findFirst].user.fullName = text;
    [self updateAccount:[CDAccount MR_findFirst]];
}

- (void)changeCity:(CatalogModel *)model
{
    CDAccount *account = [CDAccount MR_findFirst];
    account.city = [model toCoreData];
    account.country = [_changedCountry toCoreData];
    [self updateAccount:account];
}

- (void)changeCountry:(CatalogModel *)model
{
    _changedCountry = model;
}

- (void)updateAccount:(CDAccount*)account
{
    [_output showProgress];
    __weak typeof(self) weakSelf = self;
    
    [UserRepository updateUserProfile:[UserProfile fromCDAccount:account] completion:^(id receive) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        [blockSelf reloadInfo];
    }];
}

- (void)logOut
{
    [AccountRepository logOut];
    [[SubscribeService shared] stop];
    [AccountService deleteAvatar];
    
    [AccountService clearAllEntityWithAccount:YES];

        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SessionToken];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAccountStatus object:nil];
}

- (void)saveAvatar:(NSData *)data
{
    [_output showUploadProgress:0];
    __weak typeof(self) weakSelf = self;
    
    [HTTPService uploadImage:data
                     withURL:@"user/update_avatar" progress:^(NSProgress *uploadProgress) {
                         [_output showUploadProgress:uploadProgress.completedUnitCount/uploadProgress.totalUnitCount];
                     } completion:^(ResponseModel *responseObject, NSError *error) {
                         __strong typeof(weakSelf) blockSelf = weakSelf;
                         if (!blockSelf) return;
                         
                         if (responseObject.status == 1) {
                             [CDAccount MR_findFirst].avatarPath = responseObject.data[@"avatar"][@"path"];
                             [AccountService createAvatarWithImage:[UIImage imageWithData:data]];
                             [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAvatar object:nil];
                             [blockSelf.output didChangeStatus];
                         }
                         [blockSelf.output showUploadProgress:1];

                     }];
}








@end
