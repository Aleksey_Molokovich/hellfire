//
//  UserModuleInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HellfireDefines.h"
@class CatalogModel;
@protocol UserModuleInteractorInput <NSObject>

- (AccountStateType)accountState;
- (void)reloadInfo;
- (void)changeNickname:(NSString*)text;
- (void)changePassword:(NSString*)text;
- (void)changeCity:(CatalogModel *)model;
- (void)changeCountry:(CatalogModel *)model;
- (void)logOut;
- (void)saveAvatar:(NSData *)data;
@end
