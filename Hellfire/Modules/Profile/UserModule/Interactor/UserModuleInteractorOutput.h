//
//  UserModuleInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserModuleInteractorOutput <NSObject>

- (void)didChangeStatus;
- (void)showUploadProgress:(float)progress;
- (void)showProgress;
- (void)hideProgress;
@end
