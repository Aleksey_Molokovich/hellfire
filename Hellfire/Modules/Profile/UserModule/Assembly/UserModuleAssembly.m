//
//  UserModuleAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 06/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserModuleAssembly.h"

#import "UserModuleViewController.h"
#import "UserModuleInteractor.h"
#import "UserModulePresenter.h"
#import "UserModuleRouter.h"
#import "UserModuleDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation UserModuleAssembly

- (UserModuleViewController *)viewUserModule {
    return [TyphoonDefinition withClass:[UserModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterUserModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterUserModule]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceUserModule]];
                          }];
}

- (UserModuleInteractor *)interactorUserModule {
    return [TyphoonDefinition withClass:[UserModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterUserModule]];
                          }];
}

- (UserModulePresenter *)presenterUserModule{
    return [TyphoonDefinition withClass:[UserModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewUserModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorUserModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerUserModule]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceUserModule]];
                          }];
}

- (UserModuleDatasource *)datasourceUserModule{
    return [TyphoonDefinition withClass:[UserModuleDatasource class]];
}

- (UserModuleRouter *)routerUserModule{
    return [TyphoonDefinition withClass:[UserModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewUserModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryUserModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardUserModule]];
                                                  [initializer injectParameterWith:@"UserModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardUserModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"User"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}


@end
