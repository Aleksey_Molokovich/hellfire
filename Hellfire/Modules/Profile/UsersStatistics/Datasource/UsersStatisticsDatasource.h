//
//  UsersStatisticsDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsDatasourceInput.h"

@protocol UsersStatisticsDatasourceOutput;

@interface UsersStatisticsDatasource : NSObject <UsersStatisticsDatasourceInput ,UsersStatisticsDatasourceOutput>



@end
