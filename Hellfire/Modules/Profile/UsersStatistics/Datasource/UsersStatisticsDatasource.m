//
//  UsersStatisticsDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsDatasource.h"
#import "ObjectTableViewCell.h"
#import "UsersStatisticsDatasourceOutput.h"
#import "MainAchievementModel.h"
#import "UsersStatisticsModel.h"
#import "DateRangeFilterModel.h"
#import "UsersStatisticsModel.h"

@interface UsersStatisticsDatasource()

@property (nonatomic, strong) NSArray<NSArray<ObjectTableViewCell*>*> *data;

@end

@implementation UsersStatisticsDatasource
@synthesize usersStatisticsModel;
@synthesize dateToSolo, dateFromSolo, dateToHotseat, dateFromHotseat, countGamesSolo, countGamesHotseat, soloGames, hotseatGames;
#pragma mark - Методы UsersStatisticsDatasourceInput
- (void)prepareForSolo:(BOOL)isSolo
{
    if (isSolo) {
       [self prepareSolo];
    }
    else
    {
        [self prepareHotseat];
    }
}

- (void)prepareHotseat
{
    NSMutableArray *firstSection=[NSMutableArray new];
    
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    MainAchievementModel *data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypeHotseatScore;
    data.v1 = @(usersStatisticsModel.hotseat.parts.v1);
    data.v2 = @(usersStatisticsModel.hotseat.parts.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypeNumberOfWins;
    data.v1 = @(usersStatisticsModel.hotseat.partResult.v1);
    data.v2 = @(usersStatisticsModel.hotseat.partResult.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    NSMutableArray *secondSection=[NSMutableArray new];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"DateRangeFilterTableViewCell";
    DateRangeFilterModel *filterData = [DateRangeFilterModel new];
    if ([self.countGamesHotseat intValue] == 0) {
        filterData.title = @"Нет игр в выбранном периоде";
    }else{
      filterData.title = [NSString stringWithFormat:@"Показано игр %d в промежутке:", [self.countGamesHotseat intValue]];
    }
    
    filterData.dateFrom = self.dateFromHotseat;
    filterData.dateTo = self.dateToHotseat;
    item.data = filterData;
    [secondSection addObject:item];
    
    for (UsersStatisticsHotseatHistoryModel *game in self.hotseatGames) {
        item=[ObjectTableViewCell new];
        item.cellId=@"HotseatGameStatisticsHeaderTableViewCell";
        item.data = game;
        
        [secondSection addObject:item];
        
        for (UsersStatisticsGame *player in game.diaryItems) {
            item=[ObjectTableViewCell new];
            item.cellId=@"HotseatGameStatisticTableViewCell";
            item.data = player;
            
            [secondSection addObject:item];
        }
        
    }
    
    _data =@[firstSection,secondSection];

}

- (void)prepareSolo
{
   NSMutableArray *firstSection=[NSMutableArray new];
    
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    item.height=56;
    MainAchievementModel *data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypeSoloScore;
    data.v1 = @(usersStatisticsModel.solo.parts.v1);
    data.v2 = @(usersStatisticsModel.solo.parts.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    item.height=56;
    data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypePoints;
    data.v1 = @(usersStatisticsModel.solo.points.v1);
    data.v2 = @(usersStatisticsModel.solo.points.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    item.height=56;
    data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypeQualitySmoke;
    data.v1 = @(usersStatisticsModel.solo.quality.v1);
    data.v2 = @(usersStatisticsModel.solo.quality.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    item.height=56;
    data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypeSpeedSmoke;
    data.v1 = @(usersStatisticsModel.solo.speed.v1);
    data.v2 = @(usersStatisticsModel.solo.speed.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"MainAchievementTableViewCell";
    item.height=56;
    data=[MainAchievementModel new];
    data.isDarkThem = YES;
    data.type = AverageAndMaxResultTypeVolume;
    data.v1 = @(usersStatisticsModel.solo.volume.v1);
    data.v2 = @(usersStatisticsModel.solo.volume.v2);
    
    item.data = data;
    [firstSection addObject:item];
    
    
    NSMutableArray *secondSection=[NSMutableArray new];

    item=[ObjectTableViewCell new];
    item.cellId=@"DateRangeFilterTableViewCell";
    DateRangeFilterModel *filterData = [DateRangeFilterModel new];
    filterData.title = [NSString stringWithFormat:@"Показано игр %d в промежутке:", [self.countGamesSolo intValue]];
    filterData.dateFrom = self.dateFromSolo;
    filterData.dateTo = self.dateToSolo;
    item.data = filterData;
    
    [secondSection addObject:item];
    
    for (UsersStatisticsSoloHistoryModel *game in self.soloGames) {
        item=[ObjectTableViewCell new];
        item.cellId=@"SoloGameStatisticsTableViewCell";
        item.data = game;
        
        [secondSection addObject:item];
    }
    
    _data =@[firstSection,secondSection];
    
}

-(NSInteger)countItemsInSection:(NSInteger)section
{
    return _data[section].count;
}
-(NSInteger)countSection{
    return _data.count;
}

-(CGFloat)heightAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].height;
}

-(NSString *)cellIdAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].cellId;
}

-(id)dataAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].data;
}
-(NSArray*)cellsIdentifire{
    return @[@"MainAchievementTableViewCell",@"DateRangeFilterTableViewCell", @"SoloGameStatisticsTableViewCell", @"HotseatGameStatisticTableViewCell", @"HotseatGameStatisticsHeaderTableViewCell"];
}
@end
