//
//  UsersStatisticsDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UsersStatisticsModel;
@protocol UsersStatisticsDatasourceOutput <NSObject>

@property (nonatomic) UsersStatisticsModel *usersStatisticsModel;

- (void)prepareForSolo:(BOOL)isSolo;

@property (nonatomic) NSString *dateFromSolo;

@property (nonatomic) NSString *dateToSolo;

@property (nonatomic) NSNumber *countGamesSolo;

@property (nonatomic) NSString *dateFromHotseat;

@property (nonatomic) NSString *dateToHotseat;

@property (nonatomic) NSNumber *countGamesHotseat;

@property (nonatomic) NSArray *soloGames;

@property (nonatomic) NSArray *hotseatGames;
@end
