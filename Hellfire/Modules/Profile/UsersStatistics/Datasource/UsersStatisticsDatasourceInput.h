//
//  UsersStatisticsDatasourceInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UsersStatisticsDatasourceInput <NSObject>

@property (nonatomic, assign, readonly) NSInteger countSection;

- (CGFloat) heightAtIndex:(NSIndexPath*)index;

- (id) dataAtIndex:(NSIndexPath*)index;

- (NSString*) cellIdAtIndex:(NSIndexPath*)index;

- (NSArray*) cellsIdentifire;

- (NSInteger) countItemsInSection:(NSInteger)section;

@end
