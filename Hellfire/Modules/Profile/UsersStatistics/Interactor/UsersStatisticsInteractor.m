//
//  UsersStatisticsInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsInteractor.h"
#import "UsersStatisticsInteractorOutput.h"
#import "StatisticsRepository.h"
#import "UsersStatisticsModel.h"
#import "StatisticRequestModel.h"
#import "UserRepository.h"
#import "CDPlayer+CoreDataClass.h"

@implementation UsersStatisticsInteractor
@synthesize dateFromHotseat, dateToHotseat, dateFromSolo, dateToSolo;
#pragma mark - Методы UsersStatisticsInteractorInput

- (void)configure
{
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd  MMMM  yyyy"];

    NSDateComponents *fromDateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
//    fromDateComponents.month -=12;
    
    NSDate *fromDate = [[NSCalendar currentCalendar] dateFromComponents:fromDateComponents];
    
    self.dateToSolo = [NSDate date];
    self.dateToHotseat = [NSDate date];
    
    self.dateFromSolo = fromDate;
    self.dateFromHotseat = fromDate;
}

- (NSString*)formateDateToSolo
{
    return [_dateFormatter stringFromDate:self.dateToSolo];
}

- (NSString*)formateDateToHotseat
{
    return [_dateFormatter stringFromDate:self.dateToHotseat];
}

- (NSString*)formateDateFromSolo
{
    return [_dateFormatter stringFromDate:self.dateFromSolo];
}

- (NSString*)formateDateFromHotseat
{
    return [_dateFormatter stringFromDate:self.dateFromHotseat];
}

- (void)soloGames
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yyyy"];
    
    StatisticRequestModel *soloRequest = [StatisticRequestModel new];
    soloRequest.from = [dateFormatter stringFromDate:self.dateFromSolo];
    soloRequest.to = [dateFormatter stringFromDate:self.dateToSolo];
    soloRequest.take = @25;
    soloRequest.page = @1;
    [StatisticsRepository statisticsSoloWithParam:soloRequest
                                       completion:^(NSArray *receive, NSNumber *count)
     {
         [_output successfulWithSoloGames:receive count:count];
                                       }];
}

- (void)hotseatGames
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yyyy"];
    
    StatisticRequestModel *hotseatRequest = [StatisticRequestModel new];
    hotseatRequest.from = [dateFormatter stringFromDate:self.dateFromHotseat];
    hotseatRequest.to = [dateFormatter stringFromDate:self.dateToHotseat];
    hotseatRequest.take = @25;
    hotseatRequest.page = @1;
    [StatisticsRepository statisticsHotseatWithParam:hotseatRequest
                                          completion:^(NSArray *receive, NSNumber *count)
    {
        [_output successfulWithHotseatGames:receive count:count];
                                          }];
    
}

- (void)getStatistics
{
    [StatisticsRepository statisticsWithCompletion:^(UsersStatisticsModel *receive) {
        [_output successfulWithStatistics:receive];
    }];
   
}

- (void)selectedPlayerWithId:(NSNumber *)playerId
{
    _selectedPlayer = [playerId intValue];
}

-(CDPlayer *)selectedItem
{
    CDPlayer *player = [CDPlayer MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"self.id = %ld", self.selectedPlayer]];
    
    if (!player)
    {
        player = [CDPlayer MR_createEntity];
        player.id = _selectedPlayer;
    }
    
    return player;
}

- (void)removeFriend
{
    [UserRepository removeFriend:[self selectedItem]
                      completion:^(BOOL response){}];
}

- (void)sendFriendRequest
{
    [UserRepository addFriend:[self selectedItem]
                   completion:^(BOOL response){}];
}

- (void)acceptFriendRequest
{
    [UserRepository acceptFriend:[self selectedItem]
                      completion:^(BOOL response){}];
}

- (void)rejectFriendRequest
{
    [UserRepository rejectFriend:[self selectedItem]
                      completion:^(BOOL response){}];
}

@end
