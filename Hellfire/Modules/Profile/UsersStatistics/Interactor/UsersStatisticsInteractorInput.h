//
//  UsersStatisticsInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;

@protocol UsersStatisticsInteractorInput <NSObject>

- (void)configure;

- (void)getStatistics;

- (void)soloGames;

- (void)hotseatGames;

- (NSString*)formateDateToSolo;

- (NSString*)formateDateToHotseat;

- (NSString*)formateDateFromSolo;

- (NSString*)formateDateFromHotseat;

@property (nonatomic) NSDate *dateFromSolo;

@property (nonatomic) NSDate *dateToSolo;

@property (nonatomic) NSDate *dateFromHotseat;

@property (nonatomic) NSDate *dateToHotseat;

- (void)selectedPlayerWithId:(NSNumber*)playerId;

- (void)sendFriendRequest;
- (void)acceptFriendRequest;
- (void)rejectFriendRequest;
- (void)removeFriend;
-(CDPlayer *)selectedItem;
@end
