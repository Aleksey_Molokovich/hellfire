//
//  StatisticRequestModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"



@interface StatisticRequestModel : BaseModel
@property (nonatomic) NSString *from;
@property (nonatomic) NSString *to;
@property (nonatomic) NSNumber *page;
@property (nonatomic) NSNumber *take;
@end
