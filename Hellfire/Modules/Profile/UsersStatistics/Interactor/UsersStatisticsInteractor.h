//
//  UsersStatisticsInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsInteractorInput.h"

@protocol UsersStatisticsInteractorOutput;

@interface UsersStatisticsInteractor : NSObject <UsersStatisticsInteractorInput>

@property (nonatomic, weak) id<UsersStatisticsInteractorOutput> output;

@property (nonatomic) NSDateFormatter *dateFormatter;

@property (nonatomic, assign) NSInteger selectedPlayer;


@end
