//
//  UsersStatisticsInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UsersStatisticsModel;


@protocol UsersStatisticsInteractorOutput <NSObject>
- (void)successfulWithStatistics:(UsersStatisticsModel*)data;
- (void)successfulWithSoloGames:(NSArray*)games count:(NSNumber*)count;
- (void)successfulWithHotseatGames:(NSArray*)games count:(NSNumber*)count;
- (void)showProgress;
- (void)hideProgress;
@end
