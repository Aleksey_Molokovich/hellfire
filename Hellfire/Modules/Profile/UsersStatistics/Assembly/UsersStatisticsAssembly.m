//
//  UsersStatisticsAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsAssembly.h"

#import "UsersStatisticsViewController.h"
#import "UsersStatisticsInteractor.h"
#import "UsersStatisticsPresenter.h"
#import "UsersStatisticsRouter.h"
#import "UsersStatisticsDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation UsersStatisticsAssembly

- (UsersStatisticsViewController *)viewUsersStatistics {
    return [TyphoonDefinition withClass:[UsersStatisticsViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterUsersStatistics]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterUsersStatistics]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceUsersStatistics]];
                          }];
}

- (UsersStatisticsInteractor *)interactorUsersStatistics {
    return [TyphoonDefinition withClass:[UsersStatisticsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterUsersStatistics]];
                          }];
}

- (UsersStatisticsPresenter *)presenterUsersStatistics{
    return [TyphoonDefinition withClass:[UsersStatisticsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewUsersStatistics]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorUsersStatistics]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerUsersStatistics]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceUsersStatistics]];
                          }];
}

- (UsersStatisticsDatasource *)datasourceUsersStatistics{
    return [TyphoonDefinition withClass:[UsersStatisticsDatasource class]];
}

- (UsersStatisticsRouter *)routerUsersStatistics{
    return [TyphoonDefinition withClass:[UsersStatisticsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewUsersStatistics]];
                          }];
}

- (RamblerViperModuleFactory *)factoryUsersStatistics  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardUsersStatistics]];
                                                  [initializer injectParameterWith:@"UsersStatisticsViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardUsersStatistics {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"User"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
