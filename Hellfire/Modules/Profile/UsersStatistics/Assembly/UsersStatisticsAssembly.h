//
//  UsersStatisticsAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 UsersStatistics module
 */
@interface UsersStatisticsAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryUsersStatistics;
@end
