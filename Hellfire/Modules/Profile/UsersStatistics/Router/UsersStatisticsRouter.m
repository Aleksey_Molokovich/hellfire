//
//  UsersStatisticsRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "PlayerProfileAssembly.h"
#import "PlayerProfileModuleInput.h"
#import "PlayerProfileModuleOutput.h"
#import "ShareModuleAssembly.h"
#import "ShareModuleModuleInput.h"
#import "UserModuleAssembly.h"
#import "BluetoothModuleAssembly.h"

@interface UsersStatisticsRouter()

@property (strong, nonatomic) PlayerProfileAssembly *playerProfileAssembly;
@property (nonatomic) ShareModuleAssembly *shareModuleAssembly;
@property (strong, nonatomic) UserModuleAssembly *userModuleAssembly;
@property (strong, nonatomic) BluetoothModuleAssembly *bluetoothModuleAssembly;

@end


@implementation UsersStatisticsRouter

#pragma mark - Методы UsersStatisticsRouterInput
- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule
{
    _playerProfileAssembly=[[PlayerProfileAssembly new] activated];
    self.factory=[_playerProfileAssembly factoryPlayerProfile];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<PlayerProfileModuleOutput>(id<PlayerProfileModuleInput> moduleInput) {
                       [moduleInput configureModuleWithPlayer:player];
                       return callbackModule;
                   }];
}

- (void)presentShareGameId:(NSNumber*)gameId solo:(BOOL)isSolo
{
    _shareModuleAssembly=[[ShareModuleAssembly new] activated];
    self.factory=[_shareModuleAssembly factoryShareModule];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<ShareModuleModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithGameId:gameId solo:isSolo];
                                                  return nil;
                                              }];
}

- (void)pushUserInfo:(id<RamblerViperModuleOutput>)callbackModule
{
    self.userModuleAssembly = [[UserModuleAssembly new] activated];
    self.factory=[self.userModuleAssembly factoryUserModule];
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return callbackModule;
                   }];
}

- (void)pushBluetoothModule{
    _bluetoothModuleAssembly=[[BluetoothModuleAssembly new] activated];
    self.factory=[_bluetoothModuleAssembly factoryBluetoothModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
