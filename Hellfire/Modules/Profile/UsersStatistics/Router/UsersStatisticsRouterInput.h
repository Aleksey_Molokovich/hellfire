//
//  UsersStatisticsRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol PlayerProfileModuleOutput;
@protocol UsersStatisticsRouterInput <NSObject>
- (void)closeCurrentModule;

- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule;

- (void)presentShareGameId:(NSNumber*)gameId solo:(BOOL)isSolo;
- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player;

- (void)pushBluetoothModule;
@end
