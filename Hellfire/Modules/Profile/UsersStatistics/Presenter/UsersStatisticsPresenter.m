//
//  UsersStatisticsPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsPresenter.h"

#import "UsersStatisticsViewInput.h"
#import "UsersStatisticsInteractorInput.h"
#import "UsersStatisticsRouterInput.h"
#import "DateRangeFilterTableViewCellAction.h"
#import "GameStatisticsAction.h"
#import "UIColor+HF.h"
#import "HellfireDefines.h"
#import "CDPlayer+CoreDataClass.h"
#import "PopUpCellModel.h"
#import "AccountService.h"
#import "UsersStatisticsModel.h"
#import "BluetoothService.h"

@implementation UsersStatisticsPresenter
@synthesize isLoadingData = _isLoadingData;
#pragma mark - Методы UsersStatisticsModuleInput

- (void)configureModuleForSolo:(BOOL)isSolo {
    _isSolo = isSolo;
    
    [_interactor configure];
    
    _datasource.dateToHotseat = [_interactor formateDateToHotseat];
    _datasource.dateToSolo = [_interactor formateDateToSolo];
    _datasource.dateFromHotseat = [_interactor formateDateFromHotseat];
    _datasource.dateFromSolo = [_interactor formateDateFromHotseat];
    
    [_interactor getStatistics];
    [_interactor soloGames];
    [_interactor hotseatGames];
    self.isLoadingData = YES;
    
}
-(BOOL)isLoadingData{
    return _isLoadingData;
}

- (void)setIsLoadingData:(BOOL)isLoadingData
{
    _isLoadingData = isLoadingData;
    if (isLoadingData) {
        [_view showProgressWithColor:[UIColor hfBlue]];
    }else{
        [_view hideProgress];
    }
}

- (void)goBack
{
    [_router closeCurrentModule];
}
#pragma mark - Методы UsersStatisticsViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialStateForSolo:_isSolo];
}

- (void)didPressSolo
{
    _isSolo = YES;
    [_datasource prepareForSolo:YES];
    [_view update];
}

- (void)didPressHotseat
{
    _isSolo = NO;
    [_datasource prepareForSolo:NO];
    [_view update];
}
#pragma mark - Методы UsersStatisticsInteractorOutput


- (void)successfulWithStatistics:(UsersStatisticsModel *)data
{
    self.isLoadingData = NO;
    _datasource.usersStatisticsModel = data;
    [_datasource prepareForSolo:_isSolo];
    [_view update];
}

- (void)successfulWithSoloGames:(NSArray *)games count:(NSNumber *)count
{
    self.isLoadingData = NO;
    _datasource.soloGames = games;
    _datasource.countGamesSolo = count;
    [_datasource prepareForSolo:_isSolo];
    [_view update];
}

- (void)successfulWithHotseatGames:(NSArray *)games count:(NSNumber *)count
{
    self.isLoadingData = NO;
    _datasource.hotseatGames = games;
    _datasource.countGamesHotseat = count;
    [_datasource prepareForSolo:_isSolo];
    [_view update];
}




- (void)callBackObject:(CallBackActionModel *)object
{
    _action = object.action;
    NSString *title;
    if ([object.action isEqualToString:kDateRangeFilterFromDate]) {
        title = @"Укажите начало перода";
        [_view showDatePickerWithTitle:title date:object.data];
    }else if ([object.action isEqualToString:kDateRangeFilterToDate]) {
        title = @"Укажите конец перода";
        [_view showDatePickerWithTitle:title date:object.data];
    }else if ([object.action isEqualToString:kShareButtonSoloStatisticAction]) {
        [_router presentShareGameId:object.data solo:YES];
    }else if ([object.action isEqualToString:kShareButtonHotseatStatisticAction]) {
        [_router presentShareGameId:object.data solo:NO];
    }
    
    if ([object.action isEqualToString:kPopUpButtonHotseatStatisticAction]) {
        UserInfo *player=object.data;
        [_interactor selectedPlayerWithId:player.id];
        
        if ([player.status intValue] == PlayerStatusTypeFriend)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataInviteFriendRequestToHotseat],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRemove],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        }
        else if ([player.status intValue] == PlayerStatusTypeRequest)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestAccept],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestReject],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        }
        else if ([player.status intValue] == PlayerStatusTypeOther)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestSend],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        }
    }
    
}

#pragma mark - Методы PopUpProtocol

- (void)popUpAction:(PopUpCellDataAction)action
{
    CDPlayer *player = [_interactor selectedItem];
    
    switch (action) {
        case PopUpCellDataOpenPlayerInfo:
            [_router pushPlayerProfileWithPlayer:player callbackModule:self];
            break;
        case PopUpCellDataFriendRemove:
            [_interactor removeFriend];
            self.isLoadingData = YES;
            break;
        case PopUpCellDataFriendRequestSend:
            if ([AccountService state] == AccountStateTypeNonRegister)
            {
                [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            }
            else
            {
                [_interactor sendFriendRequest];
            }
            break;
        case PopUpCellDataFriendRequestReject:
            [_interactor rejectFriendRequest];
            self.isLoadingData = YES;
            break;
        case PopUpCellDataFriendRequestAccept:
            [_interactor acceptFriendRequest];
            self.isLoadingData = YES;
            break;
        case PopUpCellDataInviteFriendRequestToHotseat:
            if ([[BluetoothService sharedInstance] isReady]) {
                [self.router presentHotseatPlayerListWithPlayer:player];
            }else{
                [[BluetoothService sharedInstance] showAlertBLENotReadyActionHandler:^{
                    [self.router pushBluetoothModule];
                }];
            }
            break;
        default:
            break;
    }
    [_view hidePopUp];
}


- (void)selectedDate:(NSDate *)date
{
    
    if (_isSolo) {
        if ([_action isEqualToString:kDateRangeFilterFromDate]) {
            _interactor.dateFromSolo = date;
            _datasource.dateFromSolo =[_interactor formateDateFromSolo];;
        }else if ([_action isEqualToString:kDateRangeFilterToDate]) {
            _interactor.dateToSolo = date;
            _datasource.dateToSolo =[_interactor formateDateToSolo];;
        }
        [_interactor soloGames];
        
    }
    else
    {
        if ([_action isEqualToString:kDateRangeFilterFromDate]) {
            _interactor.dateFromHotseat = date;
            _datasource.dateFromHotseat = [_interactor formateDateFromHotseat];
        }else if ([_action isEqualToString:kDateRangeFilterToDate]) {
            _interactor.dateToHotseat = date;
            _datasource.dateToHotseat =[_interactor formateDateToHotseat];;
        }
        [_interactor hotseatGames];
    }
    [_datasource prepareForSolo:_isSolo];
    [_view update];
    self.isLoadingData = YES;
}
@end
