//
//  UsersStatisticsPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsViewOutput.h"
#import "UsersStatisticsInteractorOutput.h"
#import "UsersStatisticsModuleInput.h"
#import "UsersStatisticsDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol UsersStatisticsViewInput;
@protocol UsersStatisticsInteractorInput;
@protocol UsersStatisticsRouterInput;
@protocol UsersStatisticsDatasourceOutput;

@interface UsersStatisticsPresenter : NSObject <UsersStatisticsModuleInput, UsersStatisticsViewOutput, UsersStatisticsInteractorOutput>

@property (nonatomic, weak) id<UsersStatisticsViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<UsersStatisticsInteractorInput> interactor;
@property (nonatomic, strong) id<UsersStatisticsRouterInput> router;
@property (nonatomic, strong) id<UsersStatisticsDatasourceOutput> datasource;
@property (nonatomic, assign) BOOL isSolo;
@property (nonatomic) NSString *action;

@end
