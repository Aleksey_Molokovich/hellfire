//
//  UsersStatisticsViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
#import "PopUpTableViewProtocol.h"
@protocol UsersStatisticsViewOutput <CallBackActionProtocol, PopUpTableViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)goBack;
- (void)didPressSolo;
- (void)didPressHotseat;
- (BOOL)isSolo;
- (void)selectedDate:(NSDate*)date;
- (void)showProgress;
@property (nonatomic, assign) BOOL isLoadingData;



@end
