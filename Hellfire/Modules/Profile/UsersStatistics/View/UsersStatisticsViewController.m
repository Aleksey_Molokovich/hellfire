//
//  UsersStatisticsViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UsersStatisticsViewController.h"
#import "UsersStatisticsViewOutput.h"
#import "BaseTableViewCell.h"
#import "CellManager.h"
#import "ThinGrayHeader.h"
#import "BaseViewController+PopUp.h"
#import "UIColor+HF.h"
@implementation UsersStatisticsViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (IBAction)goBack:(id)sender {
    [_output goBack];
}
#pragma mark - Методы UsersStatisticsViewInput

- (void)setupInitialStateForSolo:(BOOL)isSolo {
    [_segment selectSolo:isSolo];
    [self prepareStyleWithHFBGColorType:HFBackgroundColorBlue];
    [CellManager registerCellsWithId:[_datasource cellsIdentifire]
                           tableView:_tableView];
    _segment.delegate = self;
}

- (void)update
{
    [_tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.output.isLoadingData){
        [self showProgressWithColor:[UIColor hfBlue]];
    }else{
        [self hideProgress];
    }
}

- (void)showPopUpWithData:(NSArray*)data
{
    [self configurePopUpWithData:data output:_output];
}
- (void)hidePopUp
{
    [super hidePopUp];
}
#pragma mark - Методы TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_datasource countItemsInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource cellIdAtIndex:indexPath]];
    
    cell.output = _output;
    [cell configureWithData:[_datasource dataAtIndex:indexPath]];
    
    return cell;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        ThinGrayHeader *header = [ThinGrayHeader new];
        header.label.text = [_output isSolo] ? @"Дневник Solo" : @"Дневник Hotseat";
        
        return header;
    }
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1 )
    {
        return 24;
    }
    
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

#pragma mark SegmentView
- (void)pressSolo
{
    [_output didPressSolo];
}

- (void)pressHotseat
{
    [_output didPressHotseat];
}

- (void)showDatePickerWithTitle:(NSString*)title date:(NSString *)date
{
    _datePicker = [UIDatePicker new];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    _datePicker.date = [dateFormatter dateFromString:date];
    _datePicker.frame = CGRectMake(0.0, 0.0, _datePicker.frame.size.width, 160.0);
    
    self.alert = [[LGAlertView alloc] initWithViewAndTitle:nil
                                       message:title
                                         style:LGAlertViewStyleActionSheet
                                          view:_datePicker
                                  buttonTitles:@[@"Ok"]
                             cancelButtonTitle:@"Закрыть"
                        destructiveButtonTitle:nil
                                      delegate:self];
    
    self.alert.delegate = self;
    
    self.alert.buttonsHeight = 44;
    [self.alert showAnimated];
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output selectedDate:_datePicker.date];
}

- (void)alertViewDidDismiss:(LGAlertView *)alertView
{
    if (self.output.isLoadingData){
        [self showProgressWithColor:[UIColor hfBlue]];
    }
}

@end
