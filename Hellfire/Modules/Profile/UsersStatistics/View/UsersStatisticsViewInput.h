//
//  UsersStatisticsViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UsersStatisticsViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateForSolo:(BOOL)isSolo;
- (void)update;
- (void)showDatePickerWithTitle:(NSString*)title date:(NSString*)date;
- (void)showPopUpWithData:(NSArray*)data;
- (void)hidePopUp;
@end
