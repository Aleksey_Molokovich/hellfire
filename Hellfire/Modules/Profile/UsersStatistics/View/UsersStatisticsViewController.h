//
//  UsersStatisticsViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 16/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UsersStatisticsViewInput.h"
#import "BaseViewController.h"
#import "UsersStatisticsDatasourceInput.h"
#import "SegmentView.h"

@protocol UsersStatisticsViewOutput;
@protocol UsersStatisticsDatasourceInput;

@interface UsersStatisticsViewController : BaseViewController <UsersStatisticsViewInput, UITableViewDelegate, UITableViewDataSource, SegmentViewProtocol>

@property (nonatomic, strong) id<UsersStatisticsViewOutput> output;
@property (nonatomic, strong) id<UsersStatisticsDatasourceInput> datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet SegmentView *segment;
@property (nonatomic) UIDatePicker *datePicker;
@end
