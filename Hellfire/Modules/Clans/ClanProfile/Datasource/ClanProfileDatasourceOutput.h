//
//  ClanProfileDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDClan;
@protocol ClanProfileDatasourceOutput <NSObject>
- (void)prepareDatasource;
@property (nonatomic, strong) CDClan *clan;
@property (nonatomic, strong) NSArray *soloTop;
@property (nonatomic, strong) NSArray *hotseatTop;
@property (nonatomic, strong) NSArray *posts;
@end
