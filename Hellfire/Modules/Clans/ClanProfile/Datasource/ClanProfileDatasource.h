//
//  ClanProfileDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileDatasourceInput.h"
@class CDPlayer;
@protocol ClanProfileDatasourceOutput;

@interface ClanProfileDatasource : NSObject <ClanProfileDatasourceInput, ClanProfileDatasourceOutput>
@property (nonatomic, strong) NSArray<CDPlayer*> *soloTop;
@property (nonatomic, strong) NSArray<CDPlayer*> *hotseatTop;


@end
