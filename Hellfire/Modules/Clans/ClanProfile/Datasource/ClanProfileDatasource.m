//
//  ClanProfileDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileDatasource.h"

#import "ClanProfileDatasourceOutput.h"
#import "ObjectTableViewCell.h"
#import "CallBackActionProtocol.h"
#import "HFButtonData.h"
#import "UIColor+HF.h"
#import "CDClan+CoreDataClass.h"
#import "CDPlace+CoreDataClass.h"
#import "AccountService.h"

@interface ClanProfileDatasource()

@property (nonatomic, strong) NSMutableArray<ObjectTableViewCell*> *info;
@property (nonatomic, strong) NSMutableArray *data;


@end

@implementation ClanProfileDatasource
@synthesize clan,posts;
#pragma mark - Методы ClanProfileDatasourceInput

- (void)prepareDatasource
{
    _info=[NSMutableArray new];
    _data=[NSMutableArray new];
    
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"PlayerAndClanProfileTableViewCell";
    item.data = self.clan;
    [_info addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"ButtonTableViewCell";
    HFButtonData *buttonData = [HFButtonData new];
    
    if (clan.isMember)
    {
        buttonData.title = @"ВЫЙТИ";
    }
    else
    {
        buttonData.title = @"ВСТУПИТЬ";
    }
    
    buttonData.bgColor = [UIColor hfBlue];
    item.data=buttonData;
    
    if ([AccountService user] && [AccountService state] == AccountStateTypeSuccessfulRegister) {
        [_info addObject:item];
    }
    
    
    if ([clan.places count]>0)
    {
        for (CDPlace *bar in clan.places)
        {
            item=[ObjectTableViewCell new];
            item.cellId=@"LocationTableViewCell";
            item.data=bar;
            [_info addObject:item];
        }
        
    }
    
    if (clan.info.length)
    {
        item=[ObjectTableViewCell new];
        item.cellId=@"DescriptionTableViewCell";
        item.data=clan.info;
        [_info addObject:item];
    }
    
    if (clan.siteUrl.length)
    {
        item=[ObjectTableViewCell new];
        item.cellId=@"HomePageTableViewCell";
        item.data=clan;
        [_info addObject:item];
    }
    
    [_data addObject:_info];
    
    
    
    
    if (_soloTop)
    {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"soloPoints" ascending:NO];
        [_data addObject:[_soloTop sortedArrayUsingDescriptors:@[descriptor]]];
    }
    
    if (_hotseatTop)
    {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"hotseatPoints" ascending:NO];
        [_data addObject:[_hotseatTop sortedArrayUsingDescriptors:@[descriptor]]];
    }
    
    if (self.posts)
    {
        [_data addObject:self.posts];
    }
    
    
}

-(NSString *)cellIdAtIndexPath:(NSIndexPath*)path
{
    if (path.section == 0)
    {
        return _info[path.row].cellId;
    }
    if (path.section == 1 || path.section == 2)
    {
        return @"PlayerTableViewCell";
    }
    if (path.section == 3)
    {
        return @"NewsTableViewCell";
    }
    return nil;
}

-(NSInteger)countRowInSection:(NSInteger)section
{
    return [_data[section] count];
}

-(NSInteger)countSection{
    return _data.count;
}

-(id)dataAtIndexPath:(NSIndexPath*)index{
    switch (index.section) {
        case 0:
            return _info[index.row].data;
            break;
        case 1:
            return _soloTop[index.row];
            break;
        case 2:
            return _hotseatTop[index.row];
            break;
        case 3:
            return self.posts[index.row];
            break;
            
        default:
            return nil;
            break;
    }
}
-(NSArray*)getCellsId{
    NSMutableArray *cellIds = [NSMutableArray new];
    
    for (ObjectTableViewCell *item in _info) {
        [cellIds addObject:item.cellId];
    }
    
    [cellIds addObject:@"HomePageTableViewCell"];

    [cellIds addObject:@"DescriptionTableViewCell"];
    [cellIds addObject:@"LocationTableViewCell"];
    [cellIds addObject:@"PlayerTableViewCell"];
    [cellIds addObject:@"NewsTableViewCell"];
    return cellIds;
}


@end
