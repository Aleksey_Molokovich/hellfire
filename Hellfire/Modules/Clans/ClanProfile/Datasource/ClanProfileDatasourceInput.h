//
//  ClanProfileDatasourceInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClanProfileDatasourceInput <NSObject>

@property (nonatomic, assign, readonly) NSInteger countSection;

- (NSInteger)countRowInSection:(NSInteger)section;
- (CGFloat) heightAtIndexPath:(NSIndexPath*)index;
- (id)dataAtIndexPath:(NSIndexPath*)index;
- (NSString *)cellIdAtIndexPath:(NSIndexPath*)path;
- (NSArray*) getCellsId;
@end
