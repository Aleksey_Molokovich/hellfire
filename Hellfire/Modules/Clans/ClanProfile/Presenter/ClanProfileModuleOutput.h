//
//  ClanProfileModuleOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef ClanProfileModuleOutput_h
#define ClanProfileModuleOutput_h
#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
@protocol ClanProfileModuleOutput <RamblerViperModuleOutput>

- (void)shoudUpdate;

@end

#endif /* ClanProfileModuleOutput_h */
