//
//  ClanProfilePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfilePresenter.h"

#import "ClanProfileViewInput.h"
#import "ClanProfileInteractorInput.h"
#import "ClanProfileRouterInput.h"
#import "ButtonTableViewAction.h"
#import "LocationTableViewCellAction.h"
#import "HomePageTableViewCellAction.h"
#import "NewsTableViewCellAction.h"
#import "CDClan+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDPlace+CoreDataClass.h"
#import "CDAddress+CoreDataClass.h"
#import "HellfireDefines.h"
#import "UIColor+HF.h"
#import "PlayerTableViewAction.h"
#import "PlayerAndClanProfileCellAction.h"
#import "PopUpCellModel.h"
#import "AccountService.h"
#import "HFNotification.h"
#import "BluetoothService.h"


@import MapKit;



@implementation ClanProfilePresenter

#pragma mark - Методы ClanProfileModuleInput

- (void)configureModuleWithClan:(CDClan *)clan
{
//    _isMember = clan.isMember;
    [_interactor configureWithClan:clan];
//    [_interactor membersOfClan:clan];
    [_interactor postsClan:clan];
    [_interactor infoAboutClan];
    _datasource.clan = clan;
    [_datasource prepareDatasource];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(update:)
     name:HFUpdatePlayersList object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear
{
     [_interactor cachedInfoAboutClan];
}

- (void)update:(NSNotification*)notification
{
//    [_interactor cachedInfoAboutClan];
    [_interactor infoAboutClan];
}

#pragma mark - Методы ClanProfileViewOutput

- (void)goBack
{
    [[NSNotificationCenter defaultCenter] postNotificationName:HFUpdatePlayersList object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:HFUpdateClansList object:nil];
    [self.router closeCurrentModule];
}

- (void)didTriggerViewReadyEvent
{
    
	[self.view setupInitialStateAsMyClan:_datasource.clan.isMember];
}

- (void)openItemWithIndex:(NSIndexPath *)indexPath
{
    CDPlayer *player;
    if (indexPath.section == 1)
    {
        player = _datasource.soloTop[indexPath.row];
    }
    else if (indexPath.section == 2)
    {
        player = _datasource.hotseatTop[indexPath.row];
    }
    
    if ([AccountService itMyId:player.id]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HFShowUserProfile object:nil];
        [self.view goRoot];
    }else{
        [_router pushPlayerProfileWithPlayer:player callbackModule:self];
    }
}

- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index
{
    if (index == 0)
    {
        [_router presentRegistration];
    }
}
#pragma mark - Методы ClanProfileInteractorOutput

- (void)successfulWithPosts:(NSArray*)posts
{
    _datasource.posts = [posts copy];
    [_datasource prepareDatasource];
    [_view update];
}

- (void)successfulWithSoloTop:(NSArray *)solo hotseatTop:(NSArray *)hotseat clan:(CDClan *)clan
{
    _datasource.soloTop = solo;
    _datasource.hotseatTop = hotseat;
    _datasource.clan = clan;
    [_datasource prepareDatasource];
    [_view update];
//     [self.moduleOutput shoudUpdate];
}

- (void)successfulWithMessage:(NSString *)message
{
    [self.view showAlertWithMessage:message];
    if (_moduleOutput)
    {
        [self.moduleOutput shoudUpdate];
    }
}

- (void)requestStart
{
    [_view showProgressWithColor:[UIColor hfBlue]];
}

- (void)requestFinished
{
    [_view hideProgress];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    if ([object.action isEqualToString:kHomePageTableViewCellShare]) {
        [_view showShareWithUrl:object.data[0] text:object.data[1]];
        return;
    }
    
    if ([object.action isEqualToString:kHomePageTableViewCellOpenSite]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:object.data]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellChangeCellHeight]) {
        [_view updateCellWithIndexPath:object.cellIndex height:[object.data intValue]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellShare]) {
        [_view showShareWithUrl:object.data[1] text:object.data[0]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellOpenSite]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:object.data]];
        return;
    }
    
    if ([object.action isEqualToString:kPlayerAndClanProfileCellShowMembersOfClan]) {
        [self.router showMembersOfClanId:@(_datasource.clan.id) name:_datasource.clan.name];
        return;
    }
    
    if ([object.action isEqualToString:ButtonTableViewActionPress])
    {
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:SessionToken];
        if (!token)
        {
            [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            return;
        }
        
        if (_datasource.clan.isMember)
        {
           [_interactor leave];
        }
        else
        {
            [_interactor join];
        }

    }
    else if ([object.action isEqualToString:HFActionPlayerTableView])
    {
        CDPlayer *player = nil;

        if (object.cellIndex.section == 1)
        {
            player = _datasource.soloTop[object.cellIndex.row];
        }
        else if (object.cellIndex.section == 2)
        {
            player = _datasource.hotseatTop[object.cellIndex.row];
        }
        
        _interactor.selectedItem = player;
        
        if (player.status == PlayerStatusTypeFriend)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataInviteFriendRequestToHotseat],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRemove],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        }
        else if (player.status == PlayerStatusTypeRequest)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestAccept],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestReject],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        }
        else if (player.status == PlayerStatusTypeOther)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestSend],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        }
    }else if ([object.action isEqualToString:ButtonLocationTableViewCellActionPress])
    {
        CDPlace *placeData = object.data;
        
        CLLocationDegrees lon = placeData.address.lon;//55.8304307;
        CLLocationDegrees lat = placeData.address.lat;//49.0660806;
        
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake(lat, lon);
        MKPlacemark* place = [[MKPlacemark alloc] initWithCoordinate: coordinate addressDictionary: nil];
        MKMapItem* destination = [[MKMapItem alloc] initWithPlacemark: place];
        destination.name = placeData.name;
        NSArray* items = [[NSArray alloc] initWithObjects: destination, nil];
        NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 MKLaunchOptionsDirectionsModeDefault,
                                 MKLaunchOptionsDirectionsModeKey, nil];
        [MKMapItem openMapsWithItems: items launchOptions: options];
    }
}

#pragma mark - Методы PopUpProtocol

- (void)popUpAction:(PopUpCellDataAction)action
{
    CDPlayer *player = [_interactor selectedItem];
    
    switch (action) {
        case PopUpCellDataOpenPlayerInfo:
            [_router pushPlayerProfileWithPlayer:player callbackModule:self];
            break;
        case PopUpCellDataFriendRemove:
            [_interactor removeFriend:player];
            break;
        case PopUpCellDataFriendRequestSend:
            if ([AccountService state] == AccountStateTypeNonRegister)
            {
                [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            }
            else
            {
                [_interactor sendFriendRequestToPlayer:player];
            }
            break;
        case PopUpCellDataFriendRequestReject:
            [_interactor rejectFriendRequestFromPlayer:player];
            break;
        case PopUpCellDataFriendRequestAccept:
            [_interactor acceptFriendRequestFromPlayer:player];
            break;
        case PopUpCellDataInviteFriendRequestToHotseat:
            if ([[BluetoothService sharedInstance] isReady]) {
                [self.router presentHotseatPlayerListWithPlayer:player];
            }else{
                [[BluetoothService sharedInstance] showAlertBLENotReadyActionHandler:^{
                    [self.router pushBluetoothModule];
                }];
            }
            break;
        default:
            break;
    }
    [_view hidePopUp];
}



@end
