//
//  ClanProfilePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileViewOutput.h"
#import "ClanProfileInteractorOutput.h"
#import "ClanProfileModuleInput.h"
#import "ClanProfileDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"
#import "ClanProfileModuleOutput.h"

@protocol BaseViewControllerProtocol;
@protocol ClanProfileViewInput;
@protocol ClanProfileInteractorInput;
@protocol ClanProfileRouterInput;
@protocol ClanProfileDatasourceOutput;
@protocol ClanProfileModuleOutput;

@interface ClanProfilePresenter : NSObject <ClanProfileModuleInput, ClanProfileViewOutput, ClanProfileInteractorOutput, ClanProfileModuleOutput>

@property (nonatomic, weak) id<ClanProfileViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<ClanProfileInteractorInput> interactor;
@property (nonatomic, strong) id<ClanProfileRouterInput> router;
@property (nonatomic, strong) id<ClanProfileDatasourceOutput> datasource;
@property (nonatomic, weak) id<ClanProfileModuleOutput> moduleOutput;

//@property (nonatomic, assign) BOOL isMember;

@end
