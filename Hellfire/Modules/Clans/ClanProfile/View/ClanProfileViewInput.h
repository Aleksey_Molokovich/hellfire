//
//  ClanProfileViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClanProfileViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateAsMyClan:(BOOL)isMyClan;
- (void)update;

- (void)showPopUpWithData:(NSArray*)data;
- (void)hidePopUp;
- (void)goRoot;
- (void)updateCellWithIndexPath:(NSIndexPath *)indexPath height:(NSInteger)height;

@end
