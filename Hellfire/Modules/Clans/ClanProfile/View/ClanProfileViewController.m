//
//  ClanProfileViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileViewController.h"
#import "ClanProfileViewOutput.h"

#import "BaseTableViewCell.h"
#import "CellManager.h"
#import "ThinGrayHeader.h"
#import "PostsOfClanHeaderView.h"
#import "BaseViewController+PopUp.h"

@interface ClanProfileViewController()

@property (nonatomic) NSDictionary *cellHeights;
@end

@implementation ClanProfileViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.estimatedRowHeight = 400;
    _cellHeights = @{};
    [self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.output viewWillAppear];
    self.navigationController.navigationBar.hidden=NO;
}

- (IBAction)goBack:(id)sender
{
    [self.output goBack];//[self.navigationController popViewControllerAnimated:YES];
}



- (void)updateCellWithIndexPath:(NSIndexPath *)indexPath height:(NSInteger)height
{
    NSMutableDictionary *mCellHeight = [_cellHeights mutableCopy];
    
    if ([_cellHeights[indexPath] intValue]==height) {
        return ;
    }
    
    [mCellHeight setValue:@(height) forKey:indexPath];
    _cellHeights = [mCellHeight copy];
    
    [self.tableView reloadData];
    
//    if (indexPath.row == 1) {
//        [self.tableView reloadData];
////        [self.tableView beginUpdates];
////        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
////        [self.tableView endUpdates];
//    }
}
#pragma mark - Методы ClanProfileViewInput

- (void)setupInitialStateAsMyClan:(BOOL)isMyClan {

    if (!isMyClan)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self prepareNavigationBarWithHFBGColorType:HFBackgroundColorClear];
    [CellManager registerCellsWithId:[_datasource getCellsId]
                           tableView:_tableView];
}

-(void)goRoot
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)update
{
    [_tableView reloadData];
}

- (void)showPopUpWithData:(NSArray*)data
{
    [self configurePopUpWithData:data output:_output];
}

- (void)hidePopUp
{
    [super hidePopUp];
}
#pragma mark - Методы TableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _datasource.countSection;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_datasource countRowInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource cellIdAtIndexPath:indexPath]];
    [cell configureWithData:[_datasource dataAtIndexPath:indexPath]];
    cell.cellIndex = indexPath;
    cell.output = _output;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        ThinGrayHeader *header = [ThinGrayHeader new];
        header.label.text = @"Лидеры Solo";
        
        return header;
    }
    
    if (section == 2)
    {
        ThinGrayHeader *header = [ThinGrayHeader new];
        header.label.text = @"Лидеры Hotseat";
        
        return header;
    }
    
    if (section == 3) {
        PostsOfClanHeaderView *header = [PostsOfClanHeaderView new];
        return header;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 ||  [_datasource countRowInSection:section] == 0)
    {
        return 0;
    }
    
    if (section == 3)
    {
        return 20;
    }

    return 24; 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_cellHeights[indexPath]) {
        return [_cellHeights[indexPath] intValue];
    }
    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_output openItemWithIndex:indexPath];
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output alertWithTag:alertView.tag clickedButtonAtIndex:index];
}
@end
