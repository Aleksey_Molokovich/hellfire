//
//  ClanProfileViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClanProfileViewInput.h"
#import "BaseViewController.h"
#import "ClanProfileDatasourceInput.h"

@protocol ClanProfileViewOutput;
@protocol ClanProfileDatasourceInput;

@interface ClanProfileViewController : BaseViewController <ClanProfileViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<ClanProfileViewOutput> output;
@property (nonatomic, strong) id<ClanProfileDatasourceInput> datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
