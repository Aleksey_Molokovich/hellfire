//
//  ClanProfileInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDClan;
@protocol ClanProfileInteractorOutput <NSObject>
- (void)members:(NSArray *)members;
- (void)successfulWithSoloTop:(NSArray*)solo hotseatTop:(NSArray*)hotseat clan:(CDClan*)clan;
- (void)successfulWithPosts:(NSArray*)posts;
- (void)successfulWithMessage:(NSString*)message;
- (void)requestStart;
- (void)requestFinished;
@end
