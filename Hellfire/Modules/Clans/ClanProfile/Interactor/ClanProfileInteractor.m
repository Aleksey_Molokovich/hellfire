//
//  ClanProfileInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileInteractor.h"
#import "ClanProfileInteractorOutput.h"
#import "ClanRepository.h"
#import "ClansRequest.h"
#import "CDClan+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "HFPost.h"
#import "UserRepository.h"

@implementation ClanProfileInteractor
@synthesize selectedItem;
#pragma mark - Методы ClanProfileInteractorInput
- (void)configureWithClan:(CDClan*)clan
{
    _request = [ClansRequest new];
    _request.clanId = @(clan.id);
    self.isLoadingInfoClan = NO;
}

- (void)cachedInfoAboutClan
{
    if (self.isLoadingInfoClan) {
        return;
    }
    NSPredicate *predicateSolo = [NSPredicate predicateWithFormat:@"self.id in %@", self.soloLeadersId];
    NSPredicate *predicateHot = [NSPredicate predicateWithFormat:@"self.id in %@", self.hotseatLeadersId];
    NSPredicate *predicateClan = [NSPredicate predicateWithFormat:@"self.id == %@", self.request.clanId];
    
    [self.output successfulWithSoloTop:[CDPlayer MR_findAllWithPredicate:predicateSolo]
                            hotseatTop:[CDPlayer MR_findAllWithPredicate:predicateHot]
                                  clan:[CDClan MR_findFirstWithPredicate:predicateClan]];

}



- (void)infoAboutClan
{
    if (self.isLoadingInfoClan) {
        return;
    }
    self.isLoadingInfoClan = YES;
     [_output requestStart];
    __weak typeof(self) weakSelf = self;
    [ClanRepository info:_request completion:^(CDClan *clanw, NSArray *soloLead, NSArray *hotseatLead)
    {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        blockSelf.isLoadingInfoClan = NO;
        blockSelf.hotseatLeadersId = [NSMutableSet new];
        blockSelf.soloLeadersId = [NSMutableSet new];
        [blockSelf.soloLeadersId addObjectsFromArray:[soloLead valueForKey: @"id"]];
        [blockSelf.hotseatLeadersId addObjectsFromArray:[hotseatLead valueForKey: @"id"]];

        [blockSelf.output successfulWithSoloTop:soloLead hotseatTop:hotseatLead clan:clanw];
        [blockSelf.output requestFinished];
    }];
}

- (void)infoPlayer:(CDPlayer*)player
{
    [_output requestStart];
    [UserRepository info:player
              completion:^(CDPlayer *user, NSArray *clans)
     {
     }];
}

- (void)postsClan:(CDClan *)clan
{
    [ClanRepository posts:_request completion:^(NSArray *receive) {
        [_output successfulWithPosts:receive];
    }];
}

- (void) join
{
    [ClanRepository joinToClan:_request completion:^(BOOL isSend)
    {
        if (isSend) {
            [self infoAboutClan];
        }else{
           [_output successfulWithMessage:@"Что-то пошло не так"];
        }
    }];
}

- (void) leave
{
    [ClanRepository leaveClan:_request completion:^(BOOL isSend)
     {
         if (isSend) {
             [self infoAboutClan];
         }else{
             [_output successfulWithMessage:@"Что-то пошло не так"];
         }
     }];
}

- (void)removeFriend:(CDPlayer*)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository removeFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (response)
         {
             [blockSelf infoAboutClan];
         }
     }];
}

- (void)sendFriendRequestToPlayer:(CDPlayer*)player
{
    __weak typeof(self) weakSelf = self;
    
    [UserRepository addFriend:player
                   completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (response)
         {
             [blockSelf infoAboutClan];
         }
     }];
}

- (void)acceptFriendRequestFromPlayer:(CDPlayer *)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository acceptFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (response)
         {
             [blockSelf infoAboutClan];
         }
     }];
}

- (void)rejectFriendRequestFromPlayer:(CDPlayer *)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository rejectFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (response)
         {
             [blockSelf infoAboutClan];
         }
     }];
}
@end
