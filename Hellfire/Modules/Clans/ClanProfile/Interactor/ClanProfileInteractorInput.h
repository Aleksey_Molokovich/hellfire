//
//  ClanProfileInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDClan;
@class CDPlayer;

@protocol ClanProfileInteractorInput <NSObject>

@property (nonatomic, strong) CDPlayer *selectedItem;

- (void)configureWithClan:(CDClan*)clan;
- (void)membersOfClan:(CDClan*)clan;
- (void)postsClan:(CDClan*)clan;
- (void)infoAboutClan;
- (void)cachedInfoAboutClan;
- (void)infoPlayer:(CDPlayer*)player;
- (void) join;
- (void) leave;

- (void)sendFriendRequestToPlayer:(CDPlayer*)player;
- (void)acceptFriendRequestFromPlayer:(CDPlayer*)player;
- (void)rejectFriendRequestFromPlayer:(CDPlayer*)player;
- (void)removeFriend:(CDPlayer*)player;
@end
