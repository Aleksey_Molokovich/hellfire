//
//  ClanProfileInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileInteractorInput.h"
@class ClansRequest;
@protocol ClanProfileInteractorOutput;

@interface ClanProfileInteractor : NSObject <ClanProfileInteractorInput>
@property (nonatomic, strong) CDPlayer *selectedItem;

@property (nonatomic, weak) id<ClanProfileInteractorOutput> output;
@property (nonatomic, strong) ClansRequest *request;
@property (nonatomic, assign) BOOL isLoadingInfoClan;
@property (nonatomic) NSMutableSet *hotseatLeadersId;
@property (nonatomic) NSMutableSet *soloLeadersId;
@end
