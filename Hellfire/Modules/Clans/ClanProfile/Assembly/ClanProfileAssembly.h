//
//  ClanProfileAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 ClanProfile module
 */
@interface ClanProfileAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryClanProfile;
@end
