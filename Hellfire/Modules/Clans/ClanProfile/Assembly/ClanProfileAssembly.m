//
//  ClanProfileAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileAssembly.h"

#import "ClanProfileViewController.h"
#import "ClanProfileInteractor.h"
#import "ClanProfilePresenter.h"
#import "ClanProfileRouter.h"
#import "ClanProfileDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ClanProfileAssembly

- (ClanProfileViewController *)viewClanProfile {
    return [TyphoonDefinition withClass:[ClanProfileViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterClanProfile]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterClanProfile]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceClanProfile]];
                          }];
}

- (ClanProfileInteractor *)interactorClanProfile {
    return [TyphoonDefinition withClass:[ClanProfileInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterClanProfile]];
                          }];
}

- (ClanProfilePresenter *)presenterClanProfile{
    return [TyphoonDefinition withClass:[ClanProfilePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewClanProfile]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorClanProfile]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerClanProfile]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceClanProfile]];
                          }];
}

- (ClanProfileDatasource *)datasourceClanProfile{
    return [TyphoonDefinition withClass:[ClanProfileDatasource class]];
}

- (ClanProfileRouter *)routerClanProfile{
    return [TyphoonDefinition withClass:[ClanProfileRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewClanProfile]];
                          }];
}

- (RamblerViperModuleFactory *)factoryClanProfile  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardClanProfile]];
                                                  [initializer injectParameterWith:@"ClanProfileViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardClanProfile {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Clans"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
