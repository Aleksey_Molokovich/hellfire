//
//  ClanProfileRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol PlayerProfileModuleOutput;
@protocol ClanProfileRouterInput <NSObject>
- (void)closeCurrentModule;

- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule;
- (void)presentRegistration;
- (void)showMembersOfClanId:(NSNumber*)clanId name:(NSString*)name;
- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player;
- (void)pushBluetoothModule;
@end
