//
//  ClanProfileRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileRouter.h"
#import "PlayerProfileAssembly.h"
#import "PlayerProfileModuleInput.h"
#import "PlayerProfileModuleOutput.h"
#import "ClanMembersListAssembly.h"
#import "ClanMembersListModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "BluetoothModuleAssembly.h"

@interface ClanProfileRouter()

@property (strong, nonatomic) PlayerProfileAssembly *playerProfileAssembly;
@property (nonatomic) ClanMembersListAssembly *clanMembersListAssembly;
@property (strong, nonatomic) BluetoothModuleAssembly *bluetoothModuleAssembly;

@end


@implementation ClanProfileRouter

#pragma mark - Методы ClanProfileRouterInput
- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule
{
    _playerProfileAssembly=[[PlayerProfileAssembly new] activated];
    self.factory=[_playerProfileAssembly factoryPlayerProfile];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<PlayerProfileModuleOutput>(id<PlayerProfileModuleInput> moduleInput) {
                       [moduleInput configureModuleWithPlayer:player];
                       return callbackModule;
                   }];
}

- (void)showMembersOfClanId:(NSNumber *)clanId name:(NSString*)name
{
    _clanMembersListAssembly=[[ClanMembersListAssembly new] activated];
    self.factory=[_clanMembersListAssembly factoryClanMembersList];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<ClanMembersListModuleInput> moduleInput) {
                       [moduleInput configureModuleWithClanId:clanId name:name];
                       return nil;
                   }];
}
- (void)pushBluetoothModule{
    _bluetoothModuleAssembly=[[BluetoothModuleAssembly new] activated];
    self.factory=[_bluetoothModuleAssembly factoryBluetoothModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
