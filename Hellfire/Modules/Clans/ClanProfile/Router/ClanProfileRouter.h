//
//  ClanProfileRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanProfileRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface ClanProfileRouter : BaseRouter <ClanProfileRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
