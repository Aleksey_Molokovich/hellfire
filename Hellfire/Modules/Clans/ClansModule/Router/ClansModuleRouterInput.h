//
//  ClansModuleRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDClan;
@protocol ClanProfileModuleOutput;
@protocol FilterForPlayersAndClansModuleOutput;

@protocol ClansModuleRouterInput <NSObject>
- (void)pushClanProfileWithClan:(CDClan*)clan callbackModule:(id<ClanProfileModuleOutput>)callbackModule;
- (void)pushFilterWithOutput:(id<FilterForPlayersAndClansModuleOutput>)output;
- (void)presentRegistration;
@end
