//
//  ClansModuleRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClansModuleRouter.h"
#import "ClanProfileAssembly.h"
#import "ClanProfileModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ClanProfileModuleOutput.h"
#import "FilterForPlayersAndClansAssembly.h"
#import "FilterForPlayersAndClansModuleInput.h"
#import "FilterForPlayersAndClansModuleOutput.h"

@interface ClansModuleRouter()

@property (strong, nonatomic) FilterForPlayersAndClansAssembly *filterForPlayersAndClansAssembly;

@property (strong, nonatomic) ClanProfileAssembly *clanProfileAssembly;

@end


@implementation ClansModuleRouter

#pragma mark - Методы ClansModuleRouterInput
- (void)pushClanProfileWithClan:(CDClan*)clan callbackModule:(id<ClanProfileModuleOutput>)callbackModule{
    _clanProfileAssembly=[[ClanProfileAssembly new] activated];
    self.factory=[_clanProfileAssembly factoryClanProfile];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<ClanProfileModuleOutput>(id<ClanProfileModuleInput> moduleInput) {
                       [moduleInput configureModuleWithClan:clan];
                       return callbackModule;
                   }];
}
- (void)pushFilterWithOutput:(id<FilterForPlayersAndClansModuleOutput>)callbackModule
{
    _filterForPlayersAndClansAssembly = [[FilterForPlayersAndClansAssembly new] activated];
    self.factory=[_filterForPlayersAndClansAssembly factoryFilterForPlayersAndClans];
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<FilterForPlayersAndClansModuleOutput>(id<FilterForPlayersAndClansModuleInput> moduleInput) {
                       [moduleInput configureModuleForPlayers:NO];
                       return callbackModule;
                   }];
}
@end
