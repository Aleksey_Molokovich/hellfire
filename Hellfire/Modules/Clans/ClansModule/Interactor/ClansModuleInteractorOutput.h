//
//  ClansModuleInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClansModuleInteractorOutput <NSObject>
- (void)successfulWithData:(NSArray *)data;
- (void)failWithError:(NSString*)error;
- (void)successfulWithMessage:(NSString*)message;

@end
