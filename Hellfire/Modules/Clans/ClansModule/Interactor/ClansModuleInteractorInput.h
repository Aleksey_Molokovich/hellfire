//
//  ClansModuleInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDClan;
@protocol ClansModuleInteractorInput <NSObject>

@property (nonatomic, assign, readonly) NSInteger clansCount;
@property (nonatomic, assign, readonly) NSInteger myClansCount;

- (void)configure;
//- (NSArray*)loadCacheDataForSection:(NSInteger)section;
- (void)updateData;
- (void)clearCache;
- (void)updateListIfNeedForIndexCell:(NSInteger)cellIndex;
- (void)changeSection:(NSInteger)section;
- (void)changeSearchQuery:(NSString*)query;
- (CDClan *)itemWithIndex:(NSInteger)index;
- (CDClan *)selectedItem;
- (void) sendRequestToClan:(CDClan*)clan;
- (void) removeClan:(CDClan*)clan;
@end
