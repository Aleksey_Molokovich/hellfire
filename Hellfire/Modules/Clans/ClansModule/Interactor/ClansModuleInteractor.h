//
//  ClansModuleInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClansModuleInteractorInput.h"
#import "ClansRequest.h"

@protocol ClansModuleInteractorOutput;

@interface ClansModuleInteractor : NSObject <ClansModuleInteractorInput>

@property (nonatomic, assign) NSInteger clansCount;
@property (nonatomic, assign) NSInteger myClansCount;

@property (nonatomic, weak) id<ClansModuleInteractorOutput> output;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger currentSection;
@property (nonatomic, assign) NSInteger updateSection;
@property (nonatomic, strong) ClansRequest *request;
@property (nonatomic, assign) NSInteger selectedClan;
@property (nonatomic, strong) NSArray *allClans;
@property (nonatomic, strong) NSArray *myClans;

@end
