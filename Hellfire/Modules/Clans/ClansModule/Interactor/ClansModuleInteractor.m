//
//  ClansModuleInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClansModuleInteractor.h"
#import "ClanRepository.h"
#import "ClansModuleInteractorOutput.h"
#import "PlayersInteractorOutput.h"
#import "CDClan+CoreDataClass.h"
#import "HellfireDefines.h"
#import "ClansRequest.h"
#import "UserRepository.h"
#import "AccountService.h"
#import "CDFilterSetting+CoreDataClass.h"
#import "CDSortOfFilter+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
#import "CoreDataService.h"
@implementation ClansModuleInteractor

#pragma mark - Методы ClansModuleInteractorInput


- (void)configure
{
    _request=[ClansRequest new];
    [self updateData];
}


- (void)clearCache
{
    [CDClan MR_truncateAll];
    _currentPage = 1;
}

- (void)updateData
{
    [self prepareRequest];
    [self prepareCacheData];
    
    __weak typeof(self) weakSelf = self;
    [ClanRepository clansWithRequest:[_request copy]
                          completion:^(NSArray *result, NSInteger count)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         blockSelf.clansCount = count;
         if ([AccountService state] == AccountStateTypeSuccessfulRegister)
         {
             ClansRequest *requestMy = [_request copy];
             requestMy.includeMyClans = @"true";
             requestMy.query = @"";
             [ClanRepository userClansWithParam:requestMy completion:^(NSArray *result) {
                 [blockSelf prepareCacheData];
             }];
         }else
         {
            [blockSelf prepareCacheData];
         }
     }];
}


- (void)prepareRequest
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.filterForPlayers = %@ and self.saved = 1", @(NO)];
    
    CDFilterSetting *filter = [CDFilterSetting MR_findAllWithPredicate:predicate].firstObject;
    _request.includeMyClans = filter.includeMy?@"true":@"false";
    _request.sortType = nil;
    _request.countryId = (filter.country.id ==0 )?nil:@(filter.country.id);
    _request.cityId = (filter.city.id ==0 )?nil:@(filter.city.id);
    _request.isSortAsc = @"true";
    
    if (filter.bySoloPoints.selected)
    {
        _request.sortType = @"solo";
        _request.isSortAsc = filter.bySoloPoints.ascending?@"true":@"false";
    }
    else if (filter.byHotseatPoints.selected)
    {
        _request.sortType = @"hotseat";
        _request.isSortAsc = filter.byHotseatPoints.ascending?@"true":@"false";
    }
    else if (filter.byMembers.selected)
    {
        _request.sortType = @"userCount";
        _request.isSortAsc = filter.byMembers.ascending?@"true":@"false";
    }
    else
    {
        _request.sortType = @"name";
        _request.isSortAsc = filter.byName.ascending?@"true":@"false";
    }
    
    _request.page = @(_currentPage);
}

- (void)prepareCacheData
{
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        [_output successfulWithData:@[[self loadCacheDataForSection:0]]];
    }
    else
    {
        [_output successfulWithData:@[[self loadCacheDataForSection:0],
                                      [self loadCacheDataForSection:1]]];
    }
}

- (void)changeSearchQuery:(NSString *)query
{
    _request.query = query;
    _currentPage = 1;
    [self updateData];
}

- (void)changeSection:(NSInteger)section
{
    _currentSection = section;
    _currentPage = 0;
}

- (void)updateListIfNeedForIndexCell:(NSInteger)cellIndex
{
    NSInteger page = cellIndex / 24.0 + 1;
    if (_currentPage<page) {
        _currentPage = page;
        [self updateData];
    }
}

-(CDClan *)itemWithIndex:(NSInteger)index
{
    _selectedClan = index;
    return [self loadCacheDataForSection:_currentSection][index];
}

-(CDClan *)selectedItem
{
    return [self loadCacheDataForSection:_currentSection][_selectedClan];
}

-(NSInteger)getCommonSectionNumber
{
    if ([AccountService state] == AccountStateTypeNonRegister) {
        return 0;
    }
    else
    {
        return 1;
    }
}

- (NSArray*)loadCacheDataForSection:(NSInteger)section
{
    BOOL isMember = NO;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:SessionToken])
    {
        if (section == 0)
        {
            isMember = YES;
        }
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isMember == %@",[NSNumber numberWithBool: isMember]];
    if (([_request.includeMyClans isEqualToString:@"true"] &&
         [self getCommonSectionNumber] == section) ||
        [AccountService state] == AccountStateTypeNonRegister) {
        predicate=nil;
    }
    
    NSString *sortField = @"name";

    if (!isMember) {
        if ([_request.sortType isEqualToString:@"solo"])
        {
            sortField = @"soloPoints";
        }
        else if ([_request.sortType isEqualToString:@"hotseat"])
        {
            sortField = @"hotseatPoints";
        }
        else if ([_request.sortType isEqualToString:@"userCount"])
        {
            sortField = @"memberCount";
        }
    }
    
   
    
    BOOL ascending = [_request.isSortAsc isEqualToString:@"true"]?YES:NO;
    
    NSArray *data = [CDClan MR_findAllSortedBy:sortField
                                     ascending:isMember?YES:ascending
                                 withPredicate:predicate];
    
    if (_request.query.length)
      {
        predicate = [NSPredicate predicateWithFormat:@"netName contains[cd] %@ OR name contains[cd] %@",_request.query,_request.query];
        data = [[data copy] filteredArrayUsingPredicate:predicate];

    }
    
    if ([AccountService state] == AccountStateTypeSuccessfulRegister &&
        section == 0)
    {
        _myClansCount = [data count];
    }
    
    return (section == _currentSection)? data:@[];
}



- (void) sendRequestToClan:(CDClan*)clan
{
    _request = [ClansRequest new];
    _request.clanId = @(clan.id);
    __weak typeof(self) weakSelf = self;
    [ClanRepository joinToClan:_request completion:^(BOOL isSend)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (isSend) {
             blockSelf.currentPage = 0;
             [blockSelf updateData];
         }else{
             [blockSelf.output successfulWithMessage:@"Что-то пошло не так"];
         }
     }];
}

- (void) removeClan:(CDClan *)clan
{
    _request = [ClansRequest new];
    _request.clanId = @(clan.id);
    __weak typeof(self) weakSelf = self;
    [ClanRepository leaveClan:_request completion:^(BOOL isSend)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (isSend)
         {
             blockSelf.currentPage = 0;
             [blockSelf updateData];
         }else{
             [blockSelf.output successfulWithMessage:@"Что-то пошло не так"];
         }
     }];
}


@end
