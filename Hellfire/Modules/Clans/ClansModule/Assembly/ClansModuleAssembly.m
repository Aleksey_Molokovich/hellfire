//
//  ClansModuleAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClansModuleAssembly.h"

#import "ClansModuleViewController.h"
#import "ClansModuleInteractor.h"
#import "ClansModulePresenter.h"
#import "ClansModuleRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ClansModuleAssembly

- (ClansModuleViewController *)viewClansModule {
    return [TyphoonDefinition withClass:[ClansModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterClansModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterClansModule]];
                          }];
}

- (ClansModuleInteractor *)interactorClansModule {
    return [TyphoonDefinition withClass:[ClansModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterClansModule]];
                          }];
}

- (ClansModulePresenter *)presenterClansModule{
    return [TyphoonDefinition withClass:[ClansModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewClansModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorClansModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerClansModule]];
                          }];
}


- (ClansModuleRouter *)routerClansModule{
    return [TyphoonDefinition withClass:[ClansModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewClansModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryClansModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardClansModule]];
                                                  [initializer injectParameterWith:@"ClansModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardClansModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Clans"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
