//
//  ClansModulePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClansModuleViewOutput.h"
#import "ClansModuleInteractorOutput.h"
#import "ClansModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "FilterForPlayersAndClansModuleOutput.h"
#import "PlayerProfileModuleOutput.h"

@protocol BaseViewControllerProtocol;
@protocol ClansModuleViewInput;
@protocol ClansModuleInteractorInput;
@protocol ClansModuleRouterInput;
@protocol PlayerProfileModuleOutput;
@protocol FilterForPlayersAndClansModuleOutput;

@interface ClansModulePresenter : NSObject <ClansModuleModuleInput, ClansModuleViewOutput, ClansModuleInteractorOutput, PlayerProfileModuleOutput, FilterForPlayersAndClansModuleOutput>

@property (nonatomic, weak) id<ClansModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<ClansModuleInteractorInput> interactor;
@property (nonatomic, strong) id<ClansModuleRouterInput> router;

@property (nonatomic, assign) NSInteger sectionExpand;

@end
