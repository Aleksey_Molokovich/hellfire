//
//  ClansModulePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//
@import UIKit;

#import "ClansModulePresenter.h"
#import "ClansModuleViewInput.h"
#import "ClansModuleInteractorInput.h"
#import "ClansModuleRouterInput.h"
#import "CDAccount+CoreDataClass.h"
#import "HellfireDefines.h"
#import "UIColor+HF.h"
#import "AccountService.h"
#import "CDClan+CoreDataClass.h"
#import "PopUpCellModel.h"

@implementation ClansModulePresenter

#pragma mark - Методы ClansModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы PlayersViewOutput

- (void)didTriggerViewReadyEvent {
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showData)
     name:HFNotificationChangeAccountStatus object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(shoudUpdate)
     name:HFUpdateClansList object:nil];
    
    
    [self.view setupInitialState];
    [self showData];
}



- (void)showData{
    
    [_view showProgressWithColor:[UIColor hfBlue]];

    _sectionExpand = -1;
    [_interactor changeSection:-1];
    [_interactor configure];
    [_interactor updateData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSInteger)numberOfSections
{
    if ([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)
    {
        return 1;
    }
    
    return 2;
}

-(NSInteger)numberOfSectionsShouldShow
{
    return _sectionExpand;
}

- (void)willDisplayCell:(NSIndexPath*)index
{
    [_interactor updateListIfNeedForIndexCell:index.row];
}

- (void)didSelectClan:(CDClan *)clan
{
    [_router pushClanProfileWithClan:clan callbackModule:self];
}
#pragma mark - Методы PlayersInteractorOutput


- (void)successfulWithData:(NSArray *)data
{
    
    for (int sec=0; sec<[self numberOfSections]; sec++)
    {
        [_view updateHeaderSection:sec];
    }
    
    [_view updateWithData:data];
    [_view hideProgress];
    
}
- (void)failWithError:(NSString *)error {
    
}

- (void)successfulWithMessage:(NSString *)message {
    [_view showAlertWithMessage:message];
}


-(NSString *)titleForSection:(NSInteger)section
{
    NSString *sectionName;
    if ([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)
    {
        sectionName = [NSString stringWithFormat: @"Сообщество (%ld)", (long)[_interactor clansCount]];
    }
    else
    {
        if (section == 0)
        {
            sectionName = [NSString stringWithFormat: @"Мои кланы (%ld)", (long)[_interactor myClansCount]];
        }
        else
        {
            sectionName = [NSString stringWithFormat: @"Сообщество (%ld)", (long)[_interactor clansCount]];
        }
    }
    
    return sectionName;
}

- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index
{
    if (index == 0) {
        [_router presentRegistration];
    }
}

#pragma mark - Методы PlayersHeaderViewCellProtocol



- (void)openFilterAtIndex:(NSInteger)index
{
    [_router pushFilterWithOutput:self];
}

- (void)pressExpandAtIndex:(NSInteger)index
{
    if (_sectionExpand == index && _sectionExpand != -1)
    {
        _sectionExpand = -1;
        [self successfulWithData:@[] ];
    }
    else
    {
        _sectionExpand = index;
        [_interactor changeSection:index];
        [_interactor updateData];
        [_interactor updateListIfNeedForIndexCell:0];
    }
    
}

#pragma mark - Методы SearchViewProtocol

- (void)didChangeSearchString:(NSString *)string
{
    [_interactor changeSearchQuery:string];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    CDClan *clan=[_interactor itemWithIndex:object.cellIndex.row];
    
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataClanRequestSend],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenClanInfo]]];
        return;
    }
    
    if (clan.isMember)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataClanRemove],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenClanInfo]]];
    }
    else
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataClanRequestSend],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenClanInfo]]];
    }

}

#pragma mark - Методы PopUpProtocol

- (void)popUpAction:(PopUpCellDataAction)action
{
    CDClan *clan = [_interactor selectedItem];
    
    switch (action) {
        case PopUpCellDataOpenClanInfo:
            [_router pushClanProfileWithClan:clan callbackModule:self];
            break;
        case PopUpCellDataClanRemove:
            [_interactor removeClan:clan];
            break;
        case PopUpCellDataClanRequestSend:
            if ([AccountService state] == AccountStateTypeNonRegister)
            {
                [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            }
            else
            {
                [_interactor sendRequestToClan:clan];
            }
            break;

        default:
            break;
    }
    [_view hidePopUp];
}

#pragma mark - Методы FilterForPlayersAndClansModuleOutput

- (void)filterSettingsDidChange
{
        [_interactor clearCache];
        [_interactor updateData];
}

#pragma mark ClanProfileModuleOutput

- (void)shoudUpdate
{
    [_interactor updateData];
}

@end
