//
//  ClansModuleViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClansModuleViewController.h"
#import "BaseTableViewCell.h"
#import "ClansModuleViewOutput.h"
#import "CellManager.h"
#import "BaseTableViewCell.h"
#import "PlayersHeaderViewCell.h"
#import "BaseViewController+PopUp.h"

static NSString *const cellId=@"ClanBrifTableViewCell";
static NSString *const headerId=@"PlayersHeaderViewCell";

@implementation ClansModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    [self showBottomBar];
}


#pragma mark - Методы PlayersViewInput

- (void)setupInitialState {
     [self prepareStatusBarWithHFBGColorType:HFBackgroundColorBlue];
    [self.searchView placeholder: @"Искать кланы..."];
    self.searchView.delegate = _output;
    [CellManager registerCellsWithId:@[cellId]
                             headers:@[headerId]
                           tableView:_tableView];
    
}

- (void)updateWithData:(NSArray *)data
{
    _datasource = data;
    [_tableView reloadData];
}

- (void)updateHeaderSection:(NSInteger)index
{
    PlayersHeaderViewCell *header = [self.tableView headerViewForSection:index];
    header.lblTitle.text = [_output titleForSection:index];
}

- (void)showPopUpWithData:(NSArray*)data
{
    [self configurePopUpWithData:data output:_output];
}

- (void)hidePopUp
{
    [super hidePopUp];
}
#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_datasource.count>section) {
        return _datasource[section].count;
    }
    
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_output numberOfSections];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    [cell configureWithData:_datasource[indexPath.section][indexPath.row]];
    cell.output = _output;
    cell.cellIndex = indexPath;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 108;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_output didSelectClan:_datasource[indexPath.section][indexPath.row]];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    PlayersHeaderViewCell *view = [_tableView dequeueReusableHeaderFooterViewWithIdentifier:headerId];
    view.section = section;
    view.delegate = _output;
    view.lblTitle.text = [_output titleForSection:section];
    view.backgroundColor = [UIColor whiteColor];
    
    
    view.hiddenFilter = NO;
    if ([_output numberOfSections] > 1 && section != 1) {
        view.hiddenFilter = YES;
    }
    
    if (section == [_output numberOfSectionsShouldShow])
    {
        [view expandList:YES];
    }
    else
    {
        [view expandList:NO];
    }
    
    [view setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    return view;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  56;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_output willDisplayCell:indexPath];
}



#pragma mark - Методы LGAlertViewDelegate
- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output alertWithTag:alertView.tag clickedButtonAtIndex:index];
}
@end
