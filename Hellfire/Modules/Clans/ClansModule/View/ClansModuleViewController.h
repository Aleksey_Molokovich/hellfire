//
//  ClansModuleViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClansModuleViewInput.h"
#import "BaseViewController.h"
#import "SearchViewPlayersAndClans.h"
#import "PopUpTableView.h"

@protocol ClansModuleViewOutput;

@interface ClansModuleViewController : BaseViewController <ClansModuleViewInput, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (nonatomic, strong) id<ClansModuleViewOutput> output;
@property (nonatomic, strong) NSArray<NSArray*> *datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet SearchViewPlayersAndClans *searchView;
@end
