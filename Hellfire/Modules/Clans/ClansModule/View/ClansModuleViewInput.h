//
//  ClansModuleViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClansModuleViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)updateWithData:(NSArray*)data;
- (void)updateHeaderSection:(NSInteger)index;

- (void)showPopUpWithData:(NSArray*)data;
- (void)hidePopUp;
@end
