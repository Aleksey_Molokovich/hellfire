//
//  ClansModuleViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayersHeaderViewCellProtocol.h"
#import "SearchViewProtocol.h"
#import "PopUpTableViewProtocol.h"
#import "CallBackActionProtocol.h"

@class CDClan;
@protocol ClansModuleViewOutput <PlayersHeaderViewCellProtocol, CallBackActionProtocol, SearchViewProtocol, PopUpTableViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)willDisplayCell:(NSIndexPath*)index;
- (NSInteger)numberOfSections;
- (NSInteger)numberOfSectionsShouldShow;
- (void)didSelectClan:(CDClan*)clan;
- (NSString*)titleForSection:(NSInteger)section;
- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index;
@end
