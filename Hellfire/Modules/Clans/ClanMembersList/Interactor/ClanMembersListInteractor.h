//
//  ClanMembersListInteractor.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListInteractorInput.h"

@protocol ClanMembersListInteractorOutput;

@interface ClanMembersListInteractor : NSObject <ClanMembersListInteractorInput>

@property (nonatomic, weak) id<ClanMembersListInteractorOutput> output;

@property (nonatomic) NSNumber *clanId;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic, assign) NSInteger selectedPlayer;
@property (nonatomic) NSMutableSet *playersIdSet;
@end
