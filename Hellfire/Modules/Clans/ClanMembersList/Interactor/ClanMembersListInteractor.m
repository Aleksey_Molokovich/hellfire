//
//  ClanMembersListInteractor.m
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListInteractor.h"
#import "ClanMembersListInteractorOutput.h"
#import "ClanRepository.h"
#import "ClansRequest.h"
#import "UserRepository.h"
#import "CDPlayer+CoreDataClass.h"
#import "UserRepository.h"

@implementation ClanMembersListInteractor

#pragma mark - Методы ClanMembersListInteractorInput

- (void)clearCache
{
    [CDPlayer MR_truncateAll];
    _currentPage = 0;
    self.playersIdSet = [NSMutableSet new];
}

- (void)loadMembersOfClanId:(NSNumber *)clanId
{
    
    self.clanId = clanId;
    [self loadMembers];
    
}


- (void) updateListForIndexCell:(NSInteger)cellIndex
{
    NSInteger page = cellIndex / 24.0 + 1;
    if (_currentPage<page) {
        _currentPage = page;
        [self loadMembers];
    }
}


- (void)loadMembers
{
    ClansRequest *request = [ClansRequest new];
    request.clanId = self.clanId;
    request.page = @(self.currentPage);
    
    __weak typeof(self) weakSelf = self;
    
    [ClanRepository membersWithRequest:request
                            completion:^(NSArray *results, NSInteger count)
    {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf.playersIdSet addObjectsFromArray:[results valueForKey: @"id"]];
        [blockSelf.output succesful:[blockSelf members]];
    }];
}

-(NSArray*)members
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id in %@", self.playersIdSet];
    
    return [CDPlayer MR_findAllSortedBy:@"userName" ascending:YES withPredicate:predicate];
}

-(CDPlayer *)selectedItem
{
    return self.members[_selectedPlayer];
}

-(CDPlayer *)itemWithIndex:(NSInteger)index
{
    _selectedPlayer = index;
    return [self members][index];
}

- (void)removeFriend:(CDPlayer*)player
{
    [UserRepository removeFriend:player
                      completion:^(BOOL response){
                          [self.output succesful:[self members]];
                      }];
}

- (void)sendFriendRequestToPlayer:(CDPlayer*)player
{
    [UserRepository addFriend:player
                   completion:^(BOOL response){
                       [self.output succesful:[self members]];
                   }];
}

- (void)acceptFriendRequestFromPlayer:(CDPlayer *)player
{
    [UserRepository acceptFriend:player
                      completion:^(BOOL response){
                          [self.output succesful:[self members]];
                      }];
}

- (void)rejectFriendRequestFromPlayer:(CDPlayer *)player
{
    [UserRepository rejectFriend:player
                      completion:^(BOOL response){
                          [self.output succesful:[self members]];
                      }];
}


@end
