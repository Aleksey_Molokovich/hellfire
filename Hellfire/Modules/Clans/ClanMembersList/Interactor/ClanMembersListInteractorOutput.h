//
//  ClanMembersListInteractorOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClanMembersListInteractorOutput <NSObject>

- (void)succesful:(NSArray*)data;
@end
