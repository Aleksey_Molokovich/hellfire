//
//  ClanMembersListInteractorInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol ClanMembersListInteractorInput <NSObject>

- (void)clearCache;
- (void)loadMembersOfClanId:(NSNumber*)clanId;
- (void) updateListForIndexCell:(NSInteger)cellIndex;
- (CDPlayer*)itemWithIndex:(NSInteger)index;
- (CDPlayer *)selectedItem;

- (void)sendFriendRequestToPlayer:(CDPlayer*)player;
- (void)acceptFriendRequestFromPlayer:(CDPlayer*)player;
- (void)rejectFriendRequestFromPlayer:(CDPlayer*)player;
- (void)removeFriend:(CDPlayer*)player;
@end
