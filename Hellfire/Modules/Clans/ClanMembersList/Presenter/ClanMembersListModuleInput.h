//
//  ClanMembersListModuleInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol ClanMembersListModuleInput <RamblerViperModuleInput>

/**
 @author Алексей Молокович

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithClanId:(NSNumber*)clanId name:(NSString*)name;

@end
