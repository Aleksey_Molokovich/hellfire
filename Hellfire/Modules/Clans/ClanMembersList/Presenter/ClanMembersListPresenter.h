//
//  ClanMembersListPresenter.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListViewOutput.h"
#import "ClanMembersListInteractorOutput.h"
#import "ClanMembersListModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol ClanMembersListViewInput;
@protocol ClanMembersListInteractorInput;
@protocol ClanMembersListRouterInput;

@interface ClanMembersListPresenter : NSObject <ClanMembersListModuleInput, ClanMembersListViewOutput, ClanMembersListInteractorOutput>

@property (nonatomic, weak) id<ClanMembersListViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<ClanMembersListInteractorInput> interactor;
@property (nonatomic, strong) id<ClanMembersListRouterInput> router;

@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *clanid;

@end
