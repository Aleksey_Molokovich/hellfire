//
//  ClanMembersListPresenter.m
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListPresenter.h"

#import "ClanMembersListViewInput.h"
#import "ClanMembersListInteractorInput.h"
#import "ClanMembersListRouterInput.h"
#import "HellfireDefines.h"
#import "UIColor+HF.h"
#import "PopUpCellModel.h"
#import "AccountService.h"
#import "CDPlayer+CoreDataClass.h"
#import "BluetoothService.h"

@implementation ClanMembersListPresenter
@synthesize isLoadingData = _isLoadingData;

#pragma mark - Методы ClanMembersListModuleInput

- (void)configureModuleWithClanId:(NSNumber *)clanId name:(NSString *)name{
    [self.interactor clearCache];
    [self.interactor loadMembersOfClanId:clanId];
    self.name = name;
    self.clanid = clanId;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(update)
     name:HFUpdatePlayersList object:nil];

}

-(BOOL)isLoadingData{
    return _isLoadingData;
}

- (void)setIsLoadingData:(BOOL)isLoadingData
{
    _isLoadingData = isLoadingData;
    if (isLoadingData) {
        [_view showProgressWithColor:[UIColor hfBlue]];
    }else{
        [_view hideProgress];
    }
}


- (void)update{
    self.isLoadingData = YES;
    [self.interactor loadMembersOfClanId:self.clanid];
}

- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Методы ClanMembersListViewOutput

- (void)didTriggerViewReadyEvent {
    [self.view setupInitialStateWirhName:self.name];
}
- (void)back
{
    [self.router closeCurrentModule];
}

- (void)didSelectPlayer:(CDPlayer *)player
{
    if ([AccountService itMyId:player.id]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HFShowUserProfile object:nil];
        [self.view goRoot];
    }else{
        [_router pushPlayerProfileWithPlayer:player callbackModule:self];
    }
}

- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index
{
    if (index == 0) {
        [_router presentRegistration];
    }
}
#pragma mark - Методы ClanMembersListInteractorOutput

- (void)succesful:(NSArray *)data
{
    self.isLoadingData = NO;
    [self.view updateWithData:data];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    CDPlayer *player=[_interactor itemWithIndex:object.cellIndex.row];
    
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestSend],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        return;
    }
    
    if (player.status == PlayerStatusTypeFriend)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataInviteFriendRequestToHotseat],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRemove],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
    }
    else if (player.status == PlayerStatusTypeRequest)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestAccept],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestReject],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
    }
    else if (player.status == PlayerStatusTypeOther)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestSend],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
    }
}


#pragma mark - Методы PopUpProtocol

- (void)popUpAction:(PopUpCellDataAction)action
{
    CDPlayer *player = [_interactor selectedItem];
    
    switch (action) {
        case PopUpCellDataOpenPlayerInfo:
            [_router pushPlayerProfileWithPlayer:player callbackModule:self];
            break;
        case PopUpCellDataFriendRemove:
            [_interactor removeFriend:player];
            break;
        case PopUpCellDataFriendRequestSend:
            if ([AccountService state] == AccountStateTypeNonRegister)
            {
                [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            }
            else
            {
                [_interactor sendFriendRequestToPlayer:player];
            }
            break;
        case PopUpCellDataFriendRequestReject:
            self.isLoadingData = YES;
            [_interactor rejectFriendRequestFromPlayer:player];
            break;
        case PopUpCellDataFriendRequestAccept:
            self.isLoadingData = YES;
            [_interactor acceptFriendRequestFromPlayer:player];
            break;
        case PopUpCellDataInviteFriendRequestToHotseat:
            if ([[BluetoothService sharedInstance] isReady]) {
                [self.router presentHotseatPlayerListWithPlayer:player];
            }else{
                [[BluetoothService sharedInstance] showAlertBLENotReadyActionHandler:^{
                    [self.router pushBluetoothModule];
                }];
            }
            break;
        default:
            break;
    }
    [_view hidePopUp];
}
@end
