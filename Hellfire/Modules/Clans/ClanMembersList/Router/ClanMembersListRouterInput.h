//
//  ClanMembersListRouterInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol PlayerProfileModuleOutput;
@protocol ClanMembersListRouterInput <NSObject>

- (void)presentRegistration;
- (void)closeCurrentModule;
- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule;
- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player;
- (void)pushBluetoothModule;
@end
