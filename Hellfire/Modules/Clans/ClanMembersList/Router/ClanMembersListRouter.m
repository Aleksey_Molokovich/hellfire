//
//  ClanMembersListRouter.m
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListRouter.h"
#import "PlayerProfileAssembly.h"
#import "PlayerProfileModuleInput.h"
#import "PlayerProfileModuleOutput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "BluetoothModuleAssembly.h"

@interface ClanMembersListRouter()

@property (strong, nonatomic) PlayerProfileAssembly *playerProfileAssembly;
@property (strong, nonatomic) BluetoothModuleAssembly *bluetoothModuleAssembly;

@end


@implementation ClanMembersListRouter

#pragma mark - Методы ClanMembersListRouterInput
- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule
{
    _playerProfileAssembly=[[PlayerProfileAssembly new] activated];
    self.factory=[_playerProfileAssembly factoryPlayerProfile];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<PlayerProfileModuleOutput>(id<PlayerProfileModuleInput> moduleInput) {
                       [moduleInput configureModuleWithPlayer:player];
                       return callbackModule;
                   }];
}
- (void)pushBluetoothModule{
    _bluetoothModuleAssembly=[[BluetoothModuleAssembly new] activated];
    self.factory=[_bluetoothModuleAssembly factoryBluetoothModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
