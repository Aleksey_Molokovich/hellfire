//
//  ClanMembersListRouter.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface ClanMembersListRouter : BaseRouter <ClanMembersListRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
