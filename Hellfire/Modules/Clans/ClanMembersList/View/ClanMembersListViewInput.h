//
//  ClanMembersListViewInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClanMembersListViewInput <NSObject>

/**
 @author Алексей Молокович

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateWirhName:(NSString*)name;
- (void)updateWithData:(NSArray*)data;

- (void)showPopUpWithData:(NSArray*)data;
- (void)hidePopUp;
- (void)goRoot;
@end
