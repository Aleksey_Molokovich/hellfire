//
//  ClanMembersListViewController.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClanMembersListViewInput.h"
#import "BaseViewController.h"

@protocol ClanMembersListViewOutput;

@interface ClanMembersListViewController : BaseViewController <ClanMembersListViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<ClanMembersListViewOutput> output;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *datasource;
@end
