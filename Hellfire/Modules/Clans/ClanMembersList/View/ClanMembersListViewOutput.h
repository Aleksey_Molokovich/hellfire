//
//  ClanMembersListViewOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
#import "PopUpTableViewProtocol.h"
@class CDPlayer;
@protocol ClanMembersListViewOutput <NSObject,CallBackActionProtocol, PopUpTableViewProtocol>

/**
 @author Алексей Молокович

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didSelectPlayer:(CDPlayer*)player;
- (void)back;
- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index;
@property (nonatomic, assign) BOOL isLoadingData;

@end
