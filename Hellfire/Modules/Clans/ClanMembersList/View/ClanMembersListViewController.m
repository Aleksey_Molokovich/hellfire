//
//  ClanMembersListViewController.m
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListViewController.h"
#import "ClanMembersListViewOutput.h"
#import "BaseTableViewCell.h"
#import "BaseViewController+PopUp.h"
#import "BaseTableViewCell.h"
#import "CellManager.h"
#import "UIColor+HF.h"

static NSString *const cellId=@"PlayerTableViewCell";

@implementation ClanMembersListViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([_output isLoadingData]) {
        [self showProgressWithColor:[UIColor hfBlue]];
    }
}

#pragma mark - Методы ClanMembersListViewInput

- (void)setupInitialStateWirhName:(NSString *)name {
	[self prepareStyleWithHFBGColorType:HFBackgroundColorBlue];
    
    self.navigationItem.title = name;
    
    [CellManager registerCellsWithId:@[cellId]
                             headers:nil
                           tableView:_tableView];
}

-(void)goRoot
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)updateWithData:(NSArray *)data
{
    _datasource = data;
    [_tableView reloadData];
}

- (void)showPopUpWithData:(NSArray*)data
{
    [self configurePopUpWithData:data output:_output];
}

- (void)hidePopUp
{
    [super hidePopUp];
}

- (IBAction)back:(id)sender {
    [self.output back];
}


#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.output = _output;
    cell.cellIndex = indexPath;
    [cell configureWithData:_datasource[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_output didSelectPlayer:_datasource[indexPath.row]];
}


- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output alertWithTag:alertView.tag clickedButtonAtIndex:index];
}

@end
