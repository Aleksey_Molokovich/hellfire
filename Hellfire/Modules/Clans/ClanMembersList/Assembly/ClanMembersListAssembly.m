//
//  ClanMembersListAssembly.m
//  Hellfire
//
//  Created by Алексей Молокович on 28/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ClanMembersListAssembly.h"

#import "ClanMembersListViewController.h"
#import "ClanMembersListInteractor.h"
#import "ClanMembersListPresenter.h"
#import "ClanMembersListRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ClanMembersListAssembly

- (ClanMembersListViewController *)viewClanMembersList {
    return [TyphoonDefinition withClass:[ClanMembersListViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterClanMembersList]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterClanMembersList]];

                          }];
}

- (ClanMembersListInteractor *)interactorClanMembersList {
    return [TyphoonDefinition withClass:[ClanMembersListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterClanMembersList]];
                          }];
}

- (ClanMembersListPresenter *)presenterClanMembersList{
    return [TyphoonDefinition withClass:[ClanMembersListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewClanMembersList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorClanMembersList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerClanMembersList]];

                          }];
}



- (ClanMembersListRouter *)routerClanMembersList{
    return [TyphoonDefinition withClass:[ClanMembersListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewClanMembersList]];
                          }];
}

- (RamblerViperModuleFactory *)factoryClanMembersList  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardClanMembersList]];
                                                  [initializer injectParameterWith:@"ClanMembersListViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardClanMembersList {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Clans"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
