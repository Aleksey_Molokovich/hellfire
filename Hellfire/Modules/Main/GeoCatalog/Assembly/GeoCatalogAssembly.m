//
//  GeoCatalogAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogAssembly.h"

#import "GeoCatalogViewController.h"
#import "GeoCatalogInteractor.h"
#import "GeoCatalogPresenter.h"
#import "GeoCatalogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>


@implementation GeoCatalogAssembly


- (GeoCatalogViewController *)viewGeoCatalog {
    return [TyphoonDefinition withClass:[GeoCatalogViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGeoCatalog]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterGeoCatalog]];
                            
                          }];
}

- (GeoCatalogInteractor *)interactorGeoCatalog {
    return [TyphoonDefinition withClass:[GeoCatalogInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGeoCatalog]];
                          }];
}



- (GeoCatalogPresenter *)presenterGeoCatalog{
    return [TyphoonDefinition withClass:[GeoCatalogPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewGeoCatalog]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorGeoCatalog]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerGeoCatalog]];
//                              [definition injectProperty:@selector(catalogType)
//                                                    with:@(CatalogCountry)];
//                              [definition injectProperty:@selector(catalogStyle)
//                                                    with:@(1)];
                          }];
}



- (GeoCatalogRouter *)routerGeoCatalog{
    return [TyphoonDefinition withClass:[GeoCatalogRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewGeoCatalog]];
                          }];
}

- (RamblerViperModuleFactory *)factoryGeoCatalog  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardGeoCatalog]];
                                                  [initializer injectParameterWith:@"GeoCatalogViewController"];
                                              }];
                          }];
}


- (UIStoryboard*)storyboardGeoCatalog {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

@end
