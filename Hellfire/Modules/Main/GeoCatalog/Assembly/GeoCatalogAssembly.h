//
//  GeoCatalogAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "HellfireDefines.h"
/**
 @author AlekseyMolokovich

 GeoCatalog module
 */
@interface GeoCatalogAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryGeoCatalog;
- (RamblerViperModuleFactory *)factoryGeoCatalogNC;
//- (void)presenterGeoCatalogWithStyle:(NSNumber*)style;


@end
