//
//  GeoCatalogViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogViewController.h"
#import "BaseTableViewCell.h"
#import "GeoCatalogViewOutput.h"
#import "CellManager.h"
#import "BaseNavigationController.h"
#import "UIColor+HF.h"

@implementation GeoCatalogViewController


#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (IBAction)goBack:(id)sender
{
    [self hideProgress];
    if (self.navigationController.viewControllers.count>1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (IBAction)saveChoose:(id)sender {
    [_output saveChoose];
}

- (void)setupCommon
{
    _roundSearchView.layer.cornerRadius = _roundSearchView.bounds.size.height / 2.0;
    _roundSearchView.clipsToBounds = YES;
    _cellId=@"CatalogTableViewCell";
    if (![_output allowCustomSelection]) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self hideBackButton];
    [CellManager registerCellsWithId:@[_cellId]
                           tableView:_tableView];
}


#pragma mark - Методы GeoCatalogViewInput
- (void)setupInitialStateDark
{
    [self setupCommon];
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];
    _roundSearchView.layer.borderWidth = 1;
    _roundSearchView.layer.borderColor = [UIColor whiteColor].CGColor;
    

}

- (void)setupInitialStateLight
{
    [self setupCommon];
    [self prepareStatusBarWithHFBGColorType:HFBackgroundColorBlue];
    [self prepareNavigationBarWithHFBGColorType:HFBackgroundColorBlue];
    [self prepareBackgroundWithHFBGColorType:HFBackgroundColorWhite];
    _roundSearchView.layer.borderWidth = 1;
    _roundSearchView.layer.borderColor = [UIColor blackColor].CGColor;

}

- (void)setupInitialState
{
    [self setupCommon];
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)updateWithData:(NSArray *)data
{
    _datasource=data;
    [_tableView reloadData];
}

- (void)navigationTitle:(NSString *)text
{
    self.navigationItem.title=text;
}

- (void)placeHolder:(NSString *)text
{
    _tfSearch.placeholder=text;
}

- (void)changeText:(NSString *)text
{
    _text.text=text;
}
#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:_cellId forIndexPath:indexPath];
    cell.output = _output;
    [cell configureWithData:_datasource[indexPath.row]];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_output didSelectCatalogItem:_datasource[indexPath.row]];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
        NSString *substring = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [_output didChangeSearchString:substring];
    
    return YES;
}
@end
