//
//  GeoCatalogViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchViewProtocol.h"
@class CatalogModel;
@protocol GeoCatalogViewOutput <SearchViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didSelectCatalogItem:(CatalogModel*)catalogItem;
- (void)saveChoose;
- (BOOL)allowCustomSelection;
@end

