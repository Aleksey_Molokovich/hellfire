//
//  GeoCatalogViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeoCatalogViewInput.h"
#import "BaseViewController.h"
#import "SearchView.h"

@protocol GeoCatalogViewOutput;

@interface GeoCatalogViewController : BaseViewController <GeoCatalogViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<GeoCatalogViewOutput> output;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *datasource;
@property (weak, nonatomic) IBOutlet UILabel *text;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UIView *roundSearchView;
@property (strong, nonatomic) NSString *cellId;
@end
