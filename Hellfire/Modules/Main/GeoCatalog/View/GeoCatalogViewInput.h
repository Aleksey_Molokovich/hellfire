//
//  GeoCatalogViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerProtocol.h"

@protocol GeoCatalogViewInput <BaseViewControllerProtocol>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateDark;
- (void)setupInitialStateLight;
- (void)updateWithData:(NSArray*)data;
- (void)changeText:(NSString*)text;
- (void)navigationTitle:(NSString *)text;
- (void)placeHolder:(NSString *)text;
- (void)dismiss;
@end
