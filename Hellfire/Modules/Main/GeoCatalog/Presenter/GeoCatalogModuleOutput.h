//
//  GeoCatalogModuleOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 10.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef GeoCatalogModuleOutput_h
#define GeoCatalogModuleOutput_h

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "HellfireDefines.h"
@class CatalogModel;

@protocol GeoCatalogModuleOutput <RamblerViperModuleOutput>

- (void)changeCountry:(CatalogModel*)model;
- (void)changeCity:(CatalogModel*)model;
@end

#endif /* GeoCatalogModuleOutput_h */
