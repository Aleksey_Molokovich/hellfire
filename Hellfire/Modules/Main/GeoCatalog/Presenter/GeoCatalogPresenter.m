//
//  GeoCatalogPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogPresenter.h"
#import "GeoCatalogViewInput.h"
#import "GeoCatalogInteractorInput.h"
#import "GeoCatalogRouterInput.h"
#import "GeoCatalogProtocol.h"
#import "CDCatalog+CoreDataClass.h"
#import "CatalogModel.h"
#import <objc/runtime.h>


static NSString *const noCountry=@"Похоже, такой страны нет. Проверьте правильность написания.";

static NSString *const countryBegin=@"Начните печатать и выберите страну из предлагаемого списка.\n\nМестоположение можно будет в последствии поменять.";
static NSString *const countryChoose=@"Подтвердите страну, выбрав ее из списка:";

static NSString *const cityBegin=@"Начните печатать и выберите город из предлагаемого списка.\n\nМестоположение можно будет в последствии поменять.";
static NSString *const noCity=@"Похоже, такого города нет. Проверьте правильность написания.";
static NSString *const cityChoose=@"Подтвердите город, выбрав его из списка:";



@interface GeoCatalogPresenter()<GeoCatalogProtocol>
{
    NSString *searchString;
}

@property (nonatomic, assign) CatalogStyleType style;
@property (nonatomic, assign) CatalogActionType action;


@end

@implementation GeoCatalogPresenter


#pragma mark - Методы GeoCatalogModuleInput

- (void)configureModuleWithType:(CatalogType)type
                          style:(CatalogStyleType)style
                         action:(CatalogActionType)action
                        country:(CDCatalog *)country
{
    _catalogType = @(type);
    _style = style;
    _action = action;
    [_interactor configureWithCountry:country];
}


#pragma mark - Методы GeoCatalogViewOutput

- (void)didTriggerViewReadyEvent
{
    if (_style == CatalogStyleDark)
    {
        [self.view setupInitialStateDark];
    }
    else
    {
        [self.view setupInitialStateLight];
    }
	
    
    if ([_catalogType intValue] == CatalogCountry)
    {
        [_view changeText:countryBegin];
        [_view navigationTitle:@"Укажите страну"];
        [_view placeHolder:@"Ваша страна"];
#ifdef DEBUG
        [self didChangeSearchString:@"рос"];
#endif
    }
    else
    {
        [_view changeText:cityBegin];
        [_view navigationTitle:@"Укажите город"];
        [_view placeHolder:@"Ваш город"];
#ifdef DEBUG
        [self didChangeSearchString:@"каз"];
#endif
    }
    
}

-(BOOL)allowCustomSelection
{
    return (_action == CatalogActionFilter)?YES:NO;
}

- (void)saveChoose
{
    if ([_catalogType intValue] == CatalogCountry)
    {
        if (_moduleOutput)
        {
            [_moduleOutput changeCountry:nil];
        }
    }
    else
    {
        [_moduleOutput changeCity:nil];
    }
    [_view dismiss];
}

#pragma mark - Методы GeoCatalogInteractorOutput

- (void)successfulWithData:(NSArray *)data
{
    [_view hideProgress];
    if ([_catalogType intValue] == CatalogCountry) {
        if (data.count == 0 && searchString.length>=3)
        {
            [_view changeText:noCountry];
        }
        else if (data.count == 0 & searchString.length<3)
        {
            [_view changeText:countryBegin];
        }
        else
        {
            [_view changeText:countryChoose];
        }
    }else
    {
        if (data.count == 0 && searchString.length>=3)
        {
            [_view changeText:noCity];
        }
        else if (data.count == 0 & searchString.length<3)
        {
            [_view changeText:cityBegin];
        }
        else
        {
            [_view changeText:cityChoose];
        }
    }
    
    [_view updateWithData:data];
}

- (void)failWithError:(NSString *)error {
    
}


- (void)didSelectCatalogItem:(CatalogModel *)catalogItem
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id = %@ AND self.name = %@",@(catalogItem.id),catalogItem.name];
    CDCatalog *location = [CDCatalog MR_findFirstWithPredicate:predicate];
    if (!location) {
        location = [CDCatalog MR_createEntity];
        location.id = catalogItem.id;
        location.name = catalogItem.name;
    }
    
    if ([_catalogType intValue] == CatalogCountry)
    {
        if (_moduleOutput)
        {
            [_moduleOutput changeCountry:catalogItem];
        }
        else
        {
            [_interactor saveCountry:catalogItem];
        }
        
        [_router pushCityCatalogWithStyle:_style
                                   action:_action
                                  country:location
                                   output:_moduleOutput];
    }
    else
    {
        if (_moduleOutput)
        {
            [_moduleOutput changeCity:catalogItem];
        }
        else
        {
            [_interactor saveCity:catalogItem];
        }
        
        
        if (_action == CatalogActionRegistration)
        {
            [_router pushAuth];
        }
        else if (_action == CatalogActionEditUserLocation ||
                 _action == CatalogActionFilter)
        {
            [_view dismiss];
        }
        
        
    }
    
}

#pragma mark - Методы SearchViewProtocol

- (void)didChangeSearchString:(NSString *)string
{
    searchString=string;
    if (string.length <3) {
        [self successfulWithData:@[]];
        return;
    }
    
    [_view showProgressWithColor:[UIColor whiteColor]];
    
    if ([_catalogType intValue] == CatalogCountry)
    {
        [_interactor searchAutocompleteTypeCountry:string];
    }
    else
    {
        [_interactor searchAutocompleteTypeCity:string];
        
    }
}

#pragma mark GeoCatalogProtocol

- (BOOL)isDarkStyle {
    return _style == CatalogStyleDark;
}
- (NSNumber *)resp_style
{
    return (NSNumber *)objc_getAssociatedObject(self, &kGMResponsibilityKey);
}

- (void)resp_setStyle:(NSNumber *)value
{
    objc_setAssociatedObject(self, &kGMResponsibilityKey,value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
