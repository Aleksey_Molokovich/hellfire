//
//  GeoCatalogModuleInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "HellfireDefines.h"

@class CDCatalog;
@protocol GeoCatalogModuleInput <RamblerViperModuleInput>

/**
 @author AlekseyMolokovich

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithType:(CatalogType)type
                          style:(CatalogStyleType)style
                         action:(CatalogActionType)action
                        country:(CDCatalog*)country;
@end
