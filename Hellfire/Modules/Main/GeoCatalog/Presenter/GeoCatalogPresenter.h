//
//  GeoCatalogPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogViewOutput.h"
#import "GeoCatalogInteractorOutput.h"
#import "GeoCatalogModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "GeoCatalogModuleOutput.h"

@protocol GeoCatalogModuleOutput;
@protocol BaseViewControllerProtocol;
@protocol GeoCatalogViewInput;
@protocol GeoCatalogInteractorInput;
@protocol GeoCatalogRouterInput;

static char kGMResponsibilityKey;

@interface GeoCatalogPresenter : NSObject <GeoCatalogModuleInput, GeoCatalogViewOutput, GeoCatalogInteractorOutput>


@property (nonatomic, weak) id<GeoCatalogViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<GeoCatalogInteractorInput> interactor;
@property (nonatomic, strong) id<GeoCatalogRouterInput> router;
@property (nonatomic, strong) id<GeoCatalogModuleOutput> moduleOutput;

@property (readwrite, nonatomic, strong, setter=setCatalogType:) NSNumber *catalogType;

@property (readwrite, nonatomic, strong, setter=resp_setStyle:) NSNumber *resp_style;

@end
