//
//  GeoCatalogRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogRouter.h"
#import "AuthModuleAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GeoCatalogModuleOutput.h"
@interface GeoCatalogRouter()

@property (nonatomic, strong) AuthModuleAssembly *authModuleAssembly;

@end


@implementation GeoCatalogRouter

#pragma mark - Методы GeoCatalogRouterInput

- (void)pushCityCatalogWithStyle:(CatalogStyleType)style
                          action:(CatalogActionType)action
                         country:(CDCatalog*)country
                          output:(id<GeoCatalogModuleOutput>)output
{
    self.geoCatalogAssembly=[[GeoCatalogAssembly new] activated];
    self.factory=[self.geoCatalogAssembly factoryGeoCatalog];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<GeoCatalogModuleOutput>(id<GeoCatalogModuleInput> moduleInput) {
                       [moduleInput configureModuleWithType:CatalogCity
                                                      style:style action:action
                                                    country:country];
                       return output;
                   }];
}

- (void)pushAuth{
    _authModuleAssembly=[[AuthModuleAssembly new] activated];
    self.factory=[_authModuleAssembly factoryAuthModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}

//- (void)pushRegistration{
//    _registrationAssembly=[[RegistrationAssembly new] activated];
//    self.factory=[_registrationAssembly factoryRegistration];
//    
//    [self pushModuleUsingFactory:self.factory
//                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
//                       return nil;
//                   }];
//}
@end
