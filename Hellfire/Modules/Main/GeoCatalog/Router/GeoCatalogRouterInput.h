//
//  GeoCatalogRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HellfireDefines.h"
#import "GeoCatalogModuleOutput.h"

@class CDCatalog;
@protocol GeoCatalogRouterInput <NSObject>
- (void)pushCityCatalogWithStyle:(CatalogStyleType)style
                          action:(CatalogActionType)action
                         country:(CDCatalog*)country
                          output:(id<GeoCatalogModuleOutput>)output;
- (void)pushAuth;
- (void)closeCurrentModule;
@end
