//
//  GeoCatalogRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface GeoCatalogRouter : BaseRouter <GeoCatalogRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
