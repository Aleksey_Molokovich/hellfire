//
//  GeoCatalogInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CatalogModel;
@class CDCatalog;
@protocol GeoCatalogInteractorInput <NSObject>
- (void)configureWithCountry:(CDCatalog*)country;

- (void)searchAutocompleteTypeCountry:(NSString *)string;
- (void)saveCountry:(CatalogModel*)catalog;
- (void)saveCity:(CatalogModel*)catalog;

- (void)searchAutocompleteTypeCity:(NSString *)string;

@end
