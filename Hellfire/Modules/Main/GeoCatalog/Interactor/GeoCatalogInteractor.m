//
//  GeoCatalogInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogInteractor.h"
#import "GeoCatalogInteractorOutput.h"
#import "CatalogsRepository.h"
#import "CDCatalog+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"

@implementation GeoCatalogInteractor

#pragma mark - Методы GeoCatalogInteractorInput
- (void)configureWithCountry:(CDCatalog *)country
{
    _selectedCountry = country;
}

- (void)searchAutocompleteTypeCountry:(NSString *)string
{
    
    [CatalogsRepository getCountryContain:string
                               completion:^(NSArray<CatalogModel *> *receive, NSError *error) {
                                   [_output successfulWithData:receive];
                               }];
}

- (void)saveCountry:(CatalogModel*)catalog
{
    CDAccount *account = [CDAccount MR_findFirst];
    if (!account)
        account = [CDAccount MR_createEntity];
    
    if(!account.country)
        account.country = [CDCatalog MR_createEntity];
        
    account.country.id = catalog.id;
    account.country.name = catalog.name;
    

}

- (void)saveCity:(CatalogModel*)catalog
{
    CDAccount *account = [CDAccount MR_findFirst];
    if (!account)
        account = [CDAccount MR_createEntity];
    
    account.city = [catalog toCoreData];

//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];

}


- (void)searchAutocompleteTypeCity:(NSString *)string
{
        
    [CatalogsRepository getCityContain:string
                               country:_selectedCountry
                            completion:^(NSArray<CatalogModel *> *receive, NSError *error) {
                                [_output successfulWithData:receive];
                            }];
}

@end
