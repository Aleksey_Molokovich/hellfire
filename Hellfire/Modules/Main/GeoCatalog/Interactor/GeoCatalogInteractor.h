//
//  GeoCatalogInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 11/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoCatalogInteractorInput.h"
#import "CatalogModel.h"

@protocol GeoCatalogInteractorOutput;

@interface GeoCatalogInteractor : NSObject <GeoCatalogInteractorInput>

@property (nonatomic, weak) id<GeoCatalogInteractorOutput> output;
@property (nonatomic, strong) CatalogModel *selectedCountry;
@end
