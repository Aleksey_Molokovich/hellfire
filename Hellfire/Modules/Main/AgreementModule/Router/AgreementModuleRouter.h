//
//  AgreementModuleRouter.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModuleRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface AgreementModuleRouter : BaseRouter <AgreementModuleRouterInput>

- (void)closeCurrentModule;
@end
