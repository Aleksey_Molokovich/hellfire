//
//  AgreementModuleInteractorInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AgreementModuleInteractorInput <NSObject>
- (void)loadAgreement;

@end
