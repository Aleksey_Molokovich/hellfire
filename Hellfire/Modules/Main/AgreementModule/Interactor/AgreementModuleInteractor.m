//
//  AgreementModuleInteractor.m
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModuleInteractor.h"
#import "AgreementModuleInteractorOutput.h"
#import "CatalogsRepository.h"

@implementation AgreementModuleInteractor

#pragma mark - Методы AgreementModuleInteractorInput
- (void)loadAgreement
{
    [CatalogsRepository agreementWithComplition:^(AgreementResponse *agreement) {
        [_output succesfulLoadAgreement:agreement];
    }];
}
@end
