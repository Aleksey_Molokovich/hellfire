//
//  AgreementModuleInteractor.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModuleInteractorInput.h"

@protocol AgreementModuleInteractorOutput;

@interface AgreementModuleInteractor : NSObject <AgreementModuleInteractorInput>

@property (nonatomic, weak) id<AgreementModuleInteractorOutput> output;

@end
