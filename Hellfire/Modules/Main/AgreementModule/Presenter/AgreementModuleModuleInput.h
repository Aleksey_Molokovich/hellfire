//
//  AgreementModuleModuleInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol AgreementModuleModuleInput <RamblerViperModuleInput>

/**
 @author Алексей Молокович

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleFirstTime:(BOOL)isFirst;

@end
