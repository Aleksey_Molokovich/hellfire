//
//  AgreementModulePresenter.m
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModulePresenter.h"

#import "AgreementModuleViewInput.h"
#import "AgreementModuleInteractorInput.h"
#import "AgreementModuleRouterInput.h"
#import "AgreementResponse.h"
#import "HellfireDefines.h"

@implementation AgreementModulePresenter

#pragma mark - Методы AgreementModuleModuleInput

- (void)configureModuleFirstTime:(BOOL)isFirst {
    _isFirst = isFirst;
}

#pragma mark - Методы AgreementModuleViewOutput

- (void)didTriggerViewReadyEvent {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:AgreementFullText] &&
        [[NSUserDefaults standardUserDefaults] objectForKey:AgreementMessage]) {
        [_view showMessage:[[NSUserDefaults standardUserDefaults] objectForKey:AgreementMessage]];
        [_view showFullAgreement:[[NSUserDefaults standardUserDefaults] objectForKey:AgreementFullText]];
        [self.view setupInitialStateFirstTime:_isFirst];
    }else{
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Agreement" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
        [_view showMessage:dict[@"message"]];
        [_view showFullAgreement:dict[@"fullText"]];
        [self.view setupInitialStateFirstTime:_isFirst];
    }
    
    
    [_interactor loadAgreement];
}

- (void)close
{
    [_router closeCurrentModule];
}

#pragma mark - Методы AgreementModuleInteractorOutput

- (void)succesfulLoadAgreement:(AgreementResponse *)agreement
{
    if (!agreement) {
        return;
    }
    [_view showMessage:agreement.message];
    [_view showFullAgreement:agreement.terms];
    [[NSUserDefaults standardUserDefaults] setObject:agreement.message forKey:AgreementMessage];
    [[NSUserDefaults standardUserDefaults] setObject:agreement.terms forKey:AgreementFullText];
}

@end
