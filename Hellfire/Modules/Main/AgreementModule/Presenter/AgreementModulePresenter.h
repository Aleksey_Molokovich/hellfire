//
//  AgreementModulePresenter.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModuleViewOutput.h"
#import "AgreementModuleInteractorOutput.h"
#import "AgreementModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol AgreementModuleViewInput;
@protocol AgreementModuleInteractorInput;
@protocol AgreementModuleRouterInput;

@interface AgreementModulePresenter : NSObject <AgreementModuleModuleInput, AgreementModuleViewOutput, AgreementModuleInteractorOutput>

@property (nonatomic, weak) id<AgreementModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<AgreementModuleInteractorInput> interactor;
@property (nonatomic, strong) id<AgreementModuleRouterInput> router;
@property (nonatomic, assign) BOOL isFirst;
@end
