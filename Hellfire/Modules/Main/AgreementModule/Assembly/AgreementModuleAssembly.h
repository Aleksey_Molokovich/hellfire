//
//  AgreementModuleAssembly.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author Алексей Молокович

 AgreementModule module
 */
@interface AgreementModuleAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryAgreementModule;
@end
