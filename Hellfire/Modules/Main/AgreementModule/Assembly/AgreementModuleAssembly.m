//
//  AgreementModuleAssembly.m
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModuleAssembly.h"

#import "AgreementModuleViewController.h"
#import "AgreementModuleInteractor.h"
#import "AgreementModulePresenter.h"
#import "AgreementModuleRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation AgreementModuleAssembly

- (AgreementModuleViewController *)viewAgreementModule {
    return [TyphoonDefinition withClass:[AgreementModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAgreementModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterAgreementModule]];
                          }];
}

- (AgreementModuleInteractor *)interactorAgreementModule {
    return [TyphoonDefinition withClass:[AgreementModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAgreementModule]];
                          }];
}

- (AgreementModulePresenter *)presenterAgreementModule{
    return [TyphoonDefinition withClass:[AgreementModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAgreementModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAgreementModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAgreementModule]];
                          }];
}

- (AgreementModuleRouter *)routerAgreementModule{
    return [TyphoonDefinition withClass:[AgreementModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAgreementModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryAgreementModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardAgreementModule]];
                                                  [initializer injectParameterWith:@"AgreementModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardAgreementModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
