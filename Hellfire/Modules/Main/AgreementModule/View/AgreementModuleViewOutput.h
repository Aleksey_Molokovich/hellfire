//
//  AgreementModuleViewOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AgreementModuleViewOutput <NSObject>

/**
 @author Алексей Молокович

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)close;
@end
