//
//  AgreementModuleViewInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AgreementModuleViewInput <NSObject>

/**
 @author Алексей Молокович

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateFirstTime:(BOOL)isFirst;
- (void)showMessage:(NSString*)text;
- (void)showFullAgreement:(NSString*)text;
@end
