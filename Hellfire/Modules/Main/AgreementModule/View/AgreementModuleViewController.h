//
//  AgreementModuleViewController.h
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//
@import WebKit;
#import <UIKit/UIKit.h>
#import "AgreementModuleViewInput.h"
#import "BaseViewController.h"
#import "RoundedButton.h"
@protocol AgreementModuleViewOutput;

@interface AgreementModuleViewController : BaseViewController <AgreementModuleViewInput, UIWebViewDelegate>

@property (nonatomic, strong) id<AgreementModuleViewOutput> output;
@property (weak, nonatomic) IBOutlet UIScrollView *AgreementView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightWebView;

@property (weak, nonatomic) IBOutlet UIScrollView *fullAgreementView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewFullAgreement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightWebViewFull;
@property (weak, nonatomic) IBOutlet RoundedButton *btnAgreeFull;

@property (weak, nonatomic) IBOutlet UIButton *btnShowFullAgree;
@property (weak, nonatomic) IBOutlet RoundedButton *btnAgree;

@end
