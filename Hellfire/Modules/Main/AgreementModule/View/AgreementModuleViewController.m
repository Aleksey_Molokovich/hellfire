//
//  AgreementModuleViewController.m
//  Hellfire
//
//  Created by Алексей Молокович on 31/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "AgreementModuleViewController.h"

#import "AgreementModuleViewOutput.h"
#import "UIFont+HF.h"
#import "UIColor+HF.h"
#import "Definitions.h"

#define HTML_BODY @"<html><head><style> body {font-family:'Raleway-v4020-Medium'; font-size:12} img{max-width:100%%;height:auto !important;width:auto !important;};</style></head><body style='margin:0;text-align:justify; padding:0;'>%@</body></html>"

@implementation AgreementModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы AgreementModuleViewInput

- (void)setupInitialStateFirstTime:(BOOL)isFirst {
    [self hideNaviagtionBar];
    [self prepareBackgroundWithHFBGColorType:HFBackgroundColorGray];
    
    [self.btnAgree configureTitle:isFirst?@"Я СОГЛАСЕН":@"ЗАКРЫТЬ"
                          bgColor:[UIColor hfDeepPink]];
    

    [self.btnAgreeFull configureTitle:isFirst?@"Я СОГЛАСЕН":@"ЗАКРЫТЬ"
                              bgColor:[UIColor hfDeepPink]];
    
    self.webViewMessage.delegate = self;
    self.webViewFullAgreement.delegate = self;
    
    NSAttributedString *str= [[NSAttributedString new]
                              initWithString:@"ОТКРЫТЬ ПРАВИЛА ДОСТУПА"
                              attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                           NSKernAttributeName:@(2),
                                           NSForegroundColorAttributeName:[UIColor hfBlue]}];
    
    [self.btnShowFullAgree setAttributedTitle:str forState:UIControlStateNormal];
    self.AgreementView.hidden = NO;
    self.fullAgreementView.hidden = YES;
}

- (void)showMessage:(NSString*)text
{
    NSString *strTemplateHTML = [NSString stringWithFormat:HTML_BODY, text];
    
    [self.webViewMessage loadHTMLString:text?strTemplateHTML:nil baseURL:nil];
}

- (void)showFullAgreement:(NSString*)text
{
    
    NSString *strTemplateHTML = [NSString stringWithFormat:HTML_BODY, text];
    
    
    [self.webViewFullAgreement loadHTMLString:text?strTemplateHTML:nil baseURL:nil];
}

- (IBAction)clickAccept:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:kUserDefaultsAgreement];
    [_output close];
}


- (IBAction)clickSHowFullInfo:(id)sender {
    self.AgreementView.hidden = YES;
    self.fullAgreementView.hidden = NO;
}

#pragma - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        webView.scrollView.scrollEnabled=NO;
        CGRect frame = webView.frame;
        frame.size.height = 1;        // Set the height to a small one.
        
        webView.frame = frame;
        NSInteger height=webView.scrollView.contentSize.height;
        if ([webView isEqual:self.webViewMessage])
        {
            self.constraintHeightWebView.constant = height;
        }
        
        if ([webView isEqual:self.webViewFullAgreement])
        {
            self.constraintHeightWebViewFull.constant = height;
        }
        
        frame.size.height = height ;
        webView.frame = frame;
        [webView.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
}
@end
