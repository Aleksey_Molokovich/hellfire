//
//  RegistrationAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationAssembly.h"

#import "RegistrationViewController.h"
#import "RegistrationInteractor.h"
#import "RegistrationPresenter.h"
#import "RegistrationRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation RegistrationAssembly

- (RegistrationViewController *)viewRegistration {
    return [TyphoonDefinition withClass:[RegistrationViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRegistration]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterRegistration]];
                          }];
}

- (RegistrationInteractor *)interactorRegistration {
    return [TyphoonDefinition withClass:[RegistrationInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRegistration]];
                          }];
}

- (RegistrationPresenter *)presenterRegistration{
    return [TyphoonDefinition withClass:[RegistrationPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewRegistration]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorRegistration]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerRegistration]];
                          }];
}

- (RegistrationRouter *)routerRegistration{
    return [TyphoonDefinition withClass:[RegistrationRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewRegistration]];
                          }];
}

- (RamblerViperModuleFactory *)factoryRegistration  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardRegistration]];
                                                  [initializer injectParameterWith:@"RegistrationViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardRegistration {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
