//
//  RegistrationPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationPresenter.h"

#import "RegistrationViewInput.h"
#import "RegistrationInteractorInput.h"
#import "RegistrationRouterInput.h"

#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"

@interface RegistrationPresenter (){
    BOOL isSuccessfulEntry;
}

@end


@implementation RegistrationPresenter

#pragma mark - Методы RegistrationModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы RegistrationViewOutput

- (void)didTriggerViewReadyEvent {
    isSuccessfulEntry=NO;
	[self.view setupInitialState];
    

    
}

- (void)didChangeNik:(NSString *)nik
{
    if (![CDAccount MR_findFirst].user)
    {
        [CDAccount MR_findFirst].user = [CDUser MR_createEntity];
    }
    
    [CDAccount MR_findFirst].user.nickname = nik;
    [_view changeStateRegistrtionButton: [_interactor checkFullRegistrationData]];
}

- (void)didChangeEmail:(NSString *)email
{
    [CDAccount MR_findFirst].email = email;
    [_view changeStateRegistrtionButton: [_interactor checkFullRegistrationData]];
}

- (void)didChangePassword:(NSString *)password
{
    [CDAccount MR_findFirst].password = password;
    [_view changeStateRegistrtionButton: [_interactor checkFullRegistrationData]];
}

- (void)didChangeConfirmPassword:(NSString *)password
{
    [CDAccount MR_findFirst].confirmPassword = password;
    [_view changeStateRegistrtionButton: [_interactor checkFullRegistrationData]];
}

- (void)didClickFacebook{
    if([_interactor checkFullRegistrationData]){
        if([_interactor isExistFacebookToken]){
            [_interactor registrationWithFacebook];
        }else{
            [_view doActionAuthFB];
        }
    }
}


- (void)didClickVk{
    if([_interactor checkFullRegistrationData])
        [_interactor checkAuthVK];
}
- (void)didReceiveFB:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    
    [_interactor registrationWithFacebook];
}

- (void)didReciveVK:(VKAuthorizationResult *)result{
        [_interactor registrationWithVK:result];
}
- (void)didClickRegistration{
    if([_interactor checkFullRegistrationData])
        [_interactor registration];
}

- (void)fillUserData{
    
    [_view fillUserData:[_interactor getUserData]];
}

#pragma mark - Методы RegistrationInteractorOutput

- (void)showProgess
{
    [_view showProgressWithColor:[UIColor whiteColor]];
}

- (void)hideProgess
{
    [_view hideProgress];
}


- (void)failWithError:(NSString *)message{
    [_view showAlertWithMessage:message];
}

- (void)successful{
    if(!isSuccessfulEntry){
        [_view goToRoot];
        isSuccessfulEntry=YES;
    }
}

@end
