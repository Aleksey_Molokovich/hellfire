//
//  RegistrationRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationRouterInput.h"
#import "BaseRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface RegistrationRouter : BaseRouter <RegistrationRouterInput>


@end
