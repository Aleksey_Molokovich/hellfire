//
//  RegistrationRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationRouter.h"
#import "MainTabBarAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface RegistrationRouter()

@property (nonatomic, strong) MainTabBarAssembly *mainTabBarAssembly;

@end


@implementation RegistrationRouter

#pragma mark - Методы RegistrationRouterInput
- (void)presentTabBar{
    _mainTabBarAssembly=[[MainTabBarAssembly new] activated];
    self.factory=[_mainTabBarAssembly factoryMainTabBar];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
