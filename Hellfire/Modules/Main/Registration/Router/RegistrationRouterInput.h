//
//  RegistrationRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RegistrationRouterInput <NSObject>
- (void)presentTabBar;
@end
