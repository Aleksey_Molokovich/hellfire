//
//  RegistrationViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerProtocol.h"
@class AccountNew;
@protocol RegistrationViewInput <NSObject, BaseViewControllerProtocol>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)showDropDownListWithData:(NSArray*)data;
- (void)setHeightDropDownList:(NSInteger)height;
- (void)doActionAuthFB;
- (AccountNew*)getRegistrationData;
- (void)fillUserData:(AccountNew*)data;
- (void)changeStateRegistrtionButton:(BOOL)state;

- (void)goToRoot;
@end
