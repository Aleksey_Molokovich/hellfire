//
//  RegistrationViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationViewController.h"
#import "RegistrationViewOutput.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <VK_ios_sdk/VKSdk.h>
#import "AccountNew.h"
#import "UIColor+HF.h"
#import "UIButtonEye.h"
#import "RoundedButton.h"
#import "TextFieldWithWhitePlaceholder.h"

@interface RegistrationViewController ()<UITextFieldDelegate,UIScrollViewDelegate, UIGestureRecognizerDelegate, FBSDKLoginButtonDelegate,VKSdkDelegate,VKSdkUIDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UITextField *activeField;
@property (weak, nonatomic) IBOutlet TextFieldWithWhitePlaceholder *tfNickname;
@property (weak, nonatomic) IBOutlet TextFieldWithWhitePlaceholder *tfEmail;
@property (weak, nonatomic) IBOutlet TextFieldWithWhitePlaceholder *tfPassword;
@property (weak, nonatomic) IBOutlet TextFieldWithWhitePlaceholder *tfPasswordConfirm;

@property (strong, nonatomic) NSDictionary *keyboardInfo;
@property (weak, nonatomic) IBOutlet UIButtonEye *BtnEyeConfirm;
@property (weak, nonatomic) IBOutlet UIButtonEye *BtnEyePassword;

@property(strong, nonatomic) FBSDKLoginButton *loginButton;
@property (weak, nonatomic) IBOutlet RoundedButton *BtnRegistration;

@end

@implementation RegistrationViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
   
}
- (IBAction)pushBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickVK:(id)sender
{
    [_output didClickVk];
}

- (void)doActionAuthFB
{
    [_loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)clickFacebook:(id)sender
{
    [self.output didClickFacebook];
}

- (IBAction)clickRegistration:(id)sender
{
    [_output didClickRegistration];
}

- (IBAction)clickToEyePassConfirm:(id)sender
{
    _tfPasswordConfirm.secureTextEntry=!_tfPasswordConfirm.secureTextEntry;
    _BtnEyeConfirm.hide=!_BtnEyeConfirm.hide;
}

- (IBAction)clickToEyePass:(id)sender
{
    _tfPassword.secureTextEntry=!_tfPassword.secureTextEntry;
    _BtnEyePassword.hide=!_BtnEyePassword.hide;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_output fillUserData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    _keyboardInfo = [aNotification userInfo];
    NSInteger kbOffset=150;
    CGSize kbSize = [[_keyboardInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+kbOffset, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= (kbSize.height+kbOffset);
    CGRect frameTF=self.activeField.superview.frame;
    
    
    frameTF.origin.y += aRect.origin.y;
    if (!CGRectContainsPoint(aRect, frameTF.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeField.superview.frame.origin.y-kbSize.height+kbOffset);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
    
    self.activeField.superview.backgroundColor=[UIColor whiteColor];
    self.activeField.textColor=[UIColor blackColor];
    
    
    if([_activeField isEqual:_tfPassword])
        _BtnEyePassword.white=NO;
    
    if([_activeField isEqual:_tfPasswordConfirm])
        _BtnEyeConfirm.white=NO;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)dismissKeyboard
{
    [self.activeField resignFirstResponder];
}


- (AccountNew*)getRegistrationData{
    AccountNew *accountNew=[AccountNew new];
    accountNew.nickname=_tfNickname.text;
    accountNew.email=_tfEmail.text;
    accountNew.password=_tfPassword.text;
    accountNew.passwordConfirm=_tfPasswordConfirm.text;
    return accountNew;
}

- (void)fillUserData:(AccountNew *)data{
    _tfNickname.text=data.nickname;
    _tfEmail.text=data.email;
    _tfPassword.text=data.password;
    _tfPasswordConfirm.text=data.passwordConfirm;
}

#pragma mark - Методы RegistrationViewInput

- (void)setupInitialState {
    
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];
    
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"6132082"];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
    
    _loginButton = [[FBSDKLoginButton alloc] init];
    _loginButton.delegate=self;
    _loginButton.readPermissions = @[@"email"];
    
    [_tfNickname configurePlaceholder:@"Псевдоним"];
    [_tfEmail configurePlaceholder:@"Электронная почта"];
    [_tfPassword configurePlaceholder:@"Пароль"];
    [_tfPasswordConfirm configurePlaceholder:@"Повторите пароль"];
    
    _BtnEyeConfirm.hide=YES;
    _BtnEyePassword.hide=YES;
    _BtnEyeConfirm.white=YES;
    _BtnEyePassword.white=YES;
    [_BtnRegistration configureTitle:@"ЗАРЕГИСТРИРОВАТЬСЯ"
                             bgColor:[UIColor hfDeepPink]];
    _BtnRegistration.enabled=NO;
    
#if DEBUG
    _tfEmail.text=@"lexnikov@yandex.ru";
#endif
    
}

- (void)goToRoot
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)changeStateRegistrtionButton:(BOOL)state
{
    _BtnRegistration.enabled=state;
}

#pragma mark - Методы TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.activeField=textField;
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.superview.backgroundColor=[UIColor clearColor];
    textField.textColor=[UIColor hfGray];
    
    if([textField isEqual:_tfPassword])
        _BtnEyePassword.white=YES;
    
    if([textField isEqual:_tfPasswordConfirm])
        _BtnEyeConfirm.white=YES;
    
    if(textField.text.length>0){
        textField.textColor=[UIColor whiteColor];
    }else{
        textField.textColor=[UIColor hfGray];
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField=textField;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if([textField isEqual:_tfEmail]){
        [_output didChangeEmail:text];
    }else if([textField isEqual:_tfNickname]){
        [_output didChangeNik:text];
    }else if([textField isEqual:_tfPassword]){
        [_output didChangePassword:text];
    }else if([textField isEqual:_tfPasswordConfirm]){
        [_output didChangeConfirmPassword:text];
    }
    
    return YES;
}

#pragma mark - Методы ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.x != 0)
        scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
}

#pragma mark - Методы FBSDKLoginButton

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    [_output didReceiveFB:result error:error];
}


- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
        [_output didReciveVK:result];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller{
    //    https://github.com/VKCOM/vk-ios-sdk/issues/284#issuecomment-171597916
    [self presentViewController:controller animated:YES completion:nil];
}
@end
