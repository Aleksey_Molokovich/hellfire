//
//  RegistrationViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegistrationViewInput.h"

@protocol RegistrationViewOutput;

@interface RegistrationViewController : BaseViewController <RegistrationViewInput>

@property (nonatomic, strong) id<RegistrationViewOutput> output;

@end
