//
//  RegistrationViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CatalogModel;
@class FBSDKLoginManagerLoginResult;
@class VKAuthorizationResult;
@protocol RegistrationViewOutput <NSObject>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

- (void)didClickFacebook;
- (void)didClickVk;
- (void)didReceiveFB:(FBSDKLoginManagerLoginResult*)result error:(NSError*)error;
- (void)didReciveVK:(VKAuthorizationResult*)result;
- (void)didClickRegistration;
- (void)didChangeUserData;
- (void)fillUserData;

- (void)didChangeEmail:(NSString*)email;
- (void)didChangeNik:(NSString*)nik;
- (void)didChangePassword:(NSString*)password;
- (void)didChangeConfirmPassword:(NSString*)password;
@end
