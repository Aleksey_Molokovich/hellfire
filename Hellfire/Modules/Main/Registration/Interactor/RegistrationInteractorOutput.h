//
//  RegistrationInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RegistrationInteractorOutput <NSObject>

- (void)successfulReceivedDropDownData:(NSArray*)data;
- (void)successful;
- (void)successfulWithMessage:(NSString*)message;
- (void)failWithError:(NSString*)error;
- (void)showProgess;
- (void)hideProgess;
@end
