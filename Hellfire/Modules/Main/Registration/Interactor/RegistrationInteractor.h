//
//  RegistrationInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationInteractorInput.h"

@protocol RegistrationInteractorOutput;

@interface RegistrationInteractor : NSObject <RegistrationInteractorInput>

@property (nonatomic, weak) id<RegistrationInteractorOutput> output;

@end
