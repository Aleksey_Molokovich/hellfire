//
//  RegistrationInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AccountNew;
@class VKAuthorizationResult;

@protocol RegistrationInteractorInput <NSObject>


- (BOOL)checkFullRegistrationData;

- (BOOL)isExistFacebookToken;
- (BOOL)isExistVKToken;
- (void)checkAuthVK;
- (void)registrationWithFacebook;
- (void)registrationWithVK:(VKAuthorizationResult *)result;
- (void)registration;
- (AccountNew*)getUserData;

@end
