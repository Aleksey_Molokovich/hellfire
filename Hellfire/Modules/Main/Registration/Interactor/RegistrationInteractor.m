//
//  RegistrationInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RegistrationInteractor.h"
#import "RegistrationInteractorOutput.h"

#import "AccountRepository.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <VK_ios_sdk/VKSdk.h>

#import "Account.h"
#import "AccountNew.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "StatusModel.h"

#import "AccountService.h"

#import "UserDataValidator.h"



@implementation RegistrationInteractor

#pragma mark - Методы RegistrationInteractorInput


- (AccountNew *)getUserData
{
    return [AccountNew fromCoreData:[CDAccount MR_findFirst]];
}

- (BOOL)checkFullRegistrationData
{
    CDAccount *cdAccount = [CDAccount MR_findFirst];
    
    StatusModel *status=[UserDataValidator validateUserData:[AccountNew fromCoreData:cdAccount]];
    
    return status.status;
}

- (BOOL)isExistFacebookToken{
    return [FBSDKAccessToken currentAccessToken]?YES:NO;
}

- (BOOL)isExistVKToken{
    return [VKSdk accessToken]?YES:NO;
}

- (void)checkAuthVK{
    NSArray *scope = @[@"friends", @"email"];
    
    [VKSdk wakeUpSession:scope completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            [self registrationWithVK:nil];
        } else if (error) {
            // Some error happend, but you may try later
        } else if (state == VKAuthorizationInitialized){
            [VKSdk authorize:scope];
        }
        
    }];
}

- (BOOL)validateEmail
{
    return [UserDataValidator isValidEmail:[CDAccount MR_findFirst].email].status;
}

- (BOOL)validatePassword
{
    return [CDAccount MR_findFirst].password.length>5;
}

- (BOOL)validateConfirmPassword
{
    return [[CDAccount MR_findFirst].password isEqualToString:[CDAccount MR_findFirst].confirmPassword];
}

- (void)registrationWithFacebook
{
    if(![self isExistFacebookToken])
        return;
    [_output showProgess];
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                  NSDictionary *result, NSError *error) {
         if (!error && result[@"email"])
         {
             CDAccount *cdAccount = [CDAccount MR_findFirst];
             if (!result[@"email"]) {
                 [_output failWithError:@"Необходимо указать email "];
                 return ;
             }
             cdAccount.email = result[@"email"];
             cdAccount.accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
             
             if (![CDAccount MR_findFirst].user)
                 [CDAccount MR_findFirst].user = [CDUser MR_createEntity];
             
             [CDAccount MR_findFirst].user.fullName = result[@"name"];
             
             __weak typeof(self) weakSelf = self;
             
             [AccountRepository registrationWithFacebook:[AccountNew fromCoreData:[CDAccount MR_findFirst]]
                                              completion:^(ResponseModel *response)
              {
                  __strong typeof(weakSelf) blockSelf = weakSelf;
                  
                  if (response.status == 1) {
                      [blockSelf proccesingResponse:response];
                  }
                  [_output hideProgess];
              }];
         }
         
     }];
    
    
}

- (void)registrationWithVK:(VKAuthorizationResult *)result
{
    [_output showProgess];
    CDAccount *cdAccount = [CDAccount MR_findFirst];
    
    VKRequest * user = [[VKApi users] get];
    [user executeWithResultBlock:^(VKResponse *response) {
        if (![CDAccount MR_findFirst].user)
            [CDAccount MR_findFirst].user = [CDUser MR_createEntity];
        
        if (response.json &&
            [response.json isKindOfClass:[NSArray class]] &&
            ((NSArray*)response.json).count>0 &&
            [response.json[0] isKindOfClass:[NSDictionary class]]) {
            cdAccount.user.fullName = [NSString stringWithFormat:@"%@ %@",response.json[0][@"first_name"], response.json[0][@"last_name"]];
        }
        
        
    } errorBlock:nil];
    
    
    cdAccount.email= result.token.email;
    cdAccount.accessToken = result.token.accessToken;
    
    __weak typeof(self) weakSelf = self;
    [AccountRepository registrationWithVK:[AccountNew fromCoreData:[CDAccount MR_findFirst]]
                               completion:^(ResponseModel *response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (response.status == 1) {
             [blockSelf proccesingResponse:response];
         }
         [_output hideProgess];
     }];
    
}

- (void)registration
{
    [_output showProgess];
    CDAccount *cdAccount = [CDAccount MR_findFirst];
     [AccountRepository registration:[AccountNew fromCoreData:cdAccount]
                                completion:^(ResponseModel *response) {
                                    [self proccesingResponse:response];
                                    [_output hideProgess];
                                }];
}

- (void)proccesingResponse:(ResponseModel*)response{
    if(response.status == 1 || response.status == 2){
        
        [AccountService createAvatarWithColor];
        [AccountService createFilterSettingsForPlayers:YES];
        [AccountService createFilterSettingsForPlayers:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAvatar object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAccountStatus object:nil];
        
        [_output successful];
    }else{
        [_output failWithError:response.error.message];
    }
}
@end
