//
//  MainTabBarPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarViewOutput.h"
#import "MainTabBarInteractorOutput.h"
#import "MainTabBarModuleInput.h"
#import "HFNotification.h"
#import <LGAlertView/LGAlertView.h>

@protocol MainTabBarViewInput;
@protocol MainTabBarInteractorInput;
@protocol MainTabBarRouterInput;

@interface MainTabBarPresenter : NSObject <MainTabBarModuleInput, MainTabBarViewOutput, MainTabBarInteractorOutput,LGAlertViewDelegate>

@property (nonatomic, weak) id<MainTabBarViewInput> view;
@property (nonatomic, strong) id<MainTabBarInteractorInput> interactor;
@property (nonatomic, strong) id<MainTabBarRouterInput> router;

@property (nonatomic, strong) HFNotification *notification;

@property (nonatomic) NSNumber *isShowedAgreement;

@end
