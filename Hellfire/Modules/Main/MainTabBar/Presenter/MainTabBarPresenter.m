//
//  MainTabBarPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarPresenter.h"

#import "MainTabBarViewInput.h"
#import "MainTabBarInteractorInput.h"
#import "MainTabBarRouterInput.h"
#import "HellfireDefines.h"
#import "HFNotification.h"
#import <AFNetworking/AFNetworking.h>

@implementation MainTabBarPresenter

#pragma mark - Методы MainTabBarModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы MainTabBarViewOutput

- (void)didTriggerViewReadyEvent
{
	[self.view setupInitialState];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeAvatar)
     name:HFNotificationChangeAvatar object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeAvatar)
     name:HFNotificationChangeAccountStatus object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showPlayerCard:)
     name:HFLaunchAppByPushNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showNotification:)
     name:HFPushNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateApp)
     name:HFNotificationUpdateApp object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(unvalidToken)
     name:HFNotificationUnvalidToken object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showUserProfile)
     name:HFShowUserProfile object:nil];
    
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        if (status == AFNetworkReachabilityStatusNotReachable) {
            [[LGAlertView alertViewWithTitle:@"Внимание"
                                     message:@"Отсутсвует соединение с интеренетом"
                                       style:LGAlertViewStyleAlert
                                buttonTitles:nil
                           cancelButtonTitle:@"Ok"
                      destructiveButtonTitle:nil] showAnimated];
        }
        
    }];
//    [self updateApp];
}

-(void)showUserProfile
{
    [self.view showUserProfile];
}

- (void)unvalidToken
{
    LGAlertView *alert = [LGAlertView alertViewWithTitle:@"Ошибка авторизации"
                                                 message:@"Ошибка авторизации, возможно вы вошли в аккаунт с помощью другого устройства"
                                                   style:LGAlertViewStyleAlert
                                            buttonTitles:@[@"Ok"]
                                       cancelButtonTitle:nil
                                  destructiveButtonTitle:nil];
    alert.innerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [alert showAnimated];
}

- (void)updateApp
{
   LGAlertView *alert = [LGAlertView alertViewWithTitle:@"Внимание"
                             message:@"Текущая версия приложения устарела, пожалуйста обновите приложение"
                               style:LGAlertViewStyleAlert
                        buttonTitles:@[@"перейти в AppStore"]
                   cancelButtonTitle:nil
              destructiveButtonTitle:nil];
    alert.innerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    alert.dismissOnAction = NO;
    alert.cancelOnTouch = NO;
    alert.delegate = self;
    [alert showAnimated];
}

- (void)showPlayerCard:(NSNotification*)notification
{
    HFNotification *notif=notification.object;
    [_router pushPlayerProfileWithPlayer:notif.data.player];
}

- (void)showPlayerCard
{
    [_router pushPlayerProfileWithPlayer:_notification.data.player];
    _notification = nil;
}




- (void)showNotification:(NSNotification*)notification
{
    _notification = notification.object;
    [_view showNotification:_notification];
}

- (void)changeAvatar
{
    [_view changeAvatar];
}
#pragma mark - Методы MainTabBarInteractorOutput

#pragma mark LGALertViewDelegate

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}
@end
