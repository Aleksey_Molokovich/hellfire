//
//  MainTabBarInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarInteractorInput.h"

@protocol MainTabBarInteractorOutput;

@interface MainTabBarInteractor : NSObject <MainTabBarInteractorInput>

@property (nonatomic, weak) id<MainTabBarInteractorOutput> output;

@end
