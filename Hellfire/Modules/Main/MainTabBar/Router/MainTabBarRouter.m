//
//  MainTabBarRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarRouter.h"
#import "PlayerProfileAssembly.h"
#import "PlayerProfileModuleOutput.h"
#import "PlayerProfileModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface MainTabBarRouter()

@property (strong, nonatomic) RamblerViperModuleFactory *factory;
@property (strong, nonatomic) PlayerProfileAssembly *playerProfileAssembly;

@end


@implementation MainTabBarRouter

#pragma mark - Методы MainTabBarRouterInput
- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player
{
    _playerProfileAssembly=[[PlayerProfileAssembly new] activated];
    self.factory=[_playerProfileAssembly factoryPlayerProfile];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                   withLinkBlock:^id<PlayerProfileModuleOutput>(id<PlayerProfileModuleInput> moduleInput) {
                       [moduleInput configureModuleWithPlayer:player];
                       return nil;
                   }];
}



@end
