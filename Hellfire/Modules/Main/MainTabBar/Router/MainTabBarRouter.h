//
//  MainTabBarRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarRouterInput.h"
#import "BaseRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface MainTabBarRouter : BaseRouter <MainTabBarRouterInput>

@end
