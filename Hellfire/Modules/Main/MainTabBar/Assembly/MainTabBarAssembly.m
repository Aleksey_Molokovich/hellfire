//
//  MainTabBarAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarAssembly.h"

#import "MainTabBarViewController.h"
#import "MainTabBarInteractor.h"
#import "MainTabBarPresenter.h"
#import "MainTabBarRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation MainTabBarAssembly

- (MainTabBarViewController *)viewMainTabBar {
    return [TyphoonDefinition withClass:[MainTabBarViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMainTabBar]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterMainTabBar]];
                          }];
}

- (MainTabBarInteractor *)interactorMainTabBar {
    return [TyphoonDefinition withClass:[MainTabBarInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMainTabBar]];
                          }];
}

- (MainTabBarPresenter *)presenterMainTabBar{
    return [TyphoonDefinition withClass:[MainTabBarPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewMainTabBar]];
                              [definition injectProperty:@selector(isShowedAgreement)
                                                    with:@(NO)];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorMainTabBar]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerMainTabBar]];
                          }];
}

- (MainTabBarRouter *)routerMainTabBar{
    return [TyphoonDefinition withClass:[MainTabBarRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewMainTabBar]];
                          }];
}

- (RamblerViperModuleFactory *)factoryMainTabBar  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardMainTabBar]];
                                                  [initializer injectParameterWith:@"MainTabBarViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardMainTabBar {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
