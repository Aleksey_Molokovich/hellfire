//
//  MainTabBarViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGAlertView.h"
#import "MainTabBarViewInput.h"

@protocol MainTabBarViewOutput;

@interface MainTabBarViewController : UITabBarController <MainTabBarViewInput, LGAlertViewDelegate>

@property (nonatomic, strong) id<MainTabBarViewOutput> output;
@property (nonatomic, strong) LGAlertView *alert;
@end
