//
//  MainTabBarViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "MainTabBarViewOutput.h"

#import "CDAccount+CoreDataProperties.h"

#import "AccountService.h"
#import "HFNotification.h"
#import "HellfireDefines.h"
#import "NotificationService.h"
@interface MainTabBarViewController (){
    UINavigationController *gameNC;
    UINavigationController *peopleNC;
    UINavigationController *clansNC;
    UINavigationController *userNC;
}


@end

@implementation MainTabBarViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы MainTabBarViewInput

- (void)setupInitialState {
	    gameNC = [[UIStoryboard storyboardWithName:@"Game" bundle:NULL] instantiateViewControllerWithIdentifier:@"GameModuleNavigationController"];
    peopleNC = [[UIStoryboard storyboardWithName:@"People" bundle:NULL] instantiateViewControllerWithIdentifier:@"PlayersNavigationController"];
    clansNC = [[UIStoryboard storyboardWithName:@"Clans" bundle:NULL] instantiateViewControllerWithIdentifier:@"ClansModuleNavigationController"];
    userNC=[[UIStoryboard storyboardWithName:@"User" bundle:NULL] instantiateViewControllerWithIdentifier:@"UserModuleNavigationController"];
    
    self.viewControllers = @[ gameNC,peopleNC,clansNC, userNC];
    self.tabBar.items[0].title = @"Игра";
    self.tabBar.items[0].image = [UIImage imageNamed:@"home"];

    self.tabBar.items[1].title = @"Люди";
    self.tabBar.items[1].image = [UIImage imageNamed:@"human-greeting"];

    self.tabBar.items[2].title = @"Кланы";
    self.tabBar.items[2].image = [UIImage imageNamed:@"castle"];
    self.tabBar.items[3].title = nil;
    self.tabBar.items[3].imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [self changeAvatar];
    
    UIImage *bg=[[UIImage imageNamed:@"bg_tabbar.jpg"]
                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    [self.tabBar setBackgroundImage:bg];
    self.tabBar.tintColor = [UIColor whiteColor];
    self.tabBar.layer.masksToBounds=YES;
    

}

-(void)showUserProfile
{
    [self setSelectedIndex:3];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([NotificationService shared].push) {
        [[NotificationService shared] updateWithPush:[NotificationService shared].push];
        [NotificationService shared].needToSave =NO;
        [NotificationService shared].push = nil;
    }
}

- (void)changeAvatar
{
    CDAccount *cdAccount = [CDAccount MR_findFirst];
    
    if (cdAccount.state == AccountStateTypeSuccessfulRegister) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"avatar.png"];
            
            UIImage *avatar=[UIImage imageWithContentsOfFile:filePath];
            
            self.tabBar.items[3].image = [avatar imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            
            filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"avatar_selected.png"];
            
            UIImage *avatar_selected=[UIImage imageWithContentsOfFile:filePath];
            
            self.tabBar.items[3].selectedImage = [avatar_selected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }];
    }else{
        self.tabBar.items[3].image = [UIImage imageNamed:@"avatar"];
        self.tabBar.items[3].selectedImage = [UIImage imageNamed:@"avatar"];
    }
 
}

- (void)showNotification:(HFNotification*)notification
{
   
    _alert = [[LGAlertView alloc] initWithTitle:notification.alert.title
                                        message:nil
                                          style:LGAlertViewStyleActionSheet
                                   buttonTitles:@[notification.alert.body]
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                                       delegate:self];
    [_alert showAnimated];
}



- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output showPlayerCard];
}

@end
