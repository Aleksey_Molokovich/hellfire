//
//  MainTabBarViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HFNotification;
@protocol MainTabBarViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)changeAvatar;
- (void)showNotification:(HFNotification*)notification;
- (void)showUserProfile;
@end
