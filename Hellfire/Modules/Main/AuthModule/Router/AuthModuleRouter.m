//
//  AuthModuleRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "RegistrationAssembly.h"
#import "MainTabBarAssembly.h"

@interface AuthModuleRouter()

@property (nonatomic, strong) RegistrationAssembly *registrationAssembly;
@property (nonatomic, strong) MainTabBarAssembly *mainTabBarAssembly;
@end


@implementation AuthModuleRouter

#pragma mark - Методы AuthModuleRouterInput

- (void)pushRegistration{
    _registrationAssembly=[[RegistrationAssembly new] activated];
    self.factory=[_registrationAssembly factoryRegistration];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}

- (void)presentTabBar{
    _mainTabBarAssembly=[[MainTabBarAssembly new] activated];
    self.factory=[_mainTabBarAssembly factoryMainTabBar];
    
    [self presentModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}

@end
