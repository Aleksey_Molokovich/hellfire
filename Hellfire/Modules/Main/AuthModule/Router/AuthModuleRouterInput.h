//
//  AuthModuleRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AuthModuleRouterInput <NSObject>


- (void)pushRegistration;
- (void)presentTabBar;
@end
