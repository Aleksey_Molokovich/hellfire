//
//  AuthModuleRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface AuthModuleRouter : BaseRouter <AuthModuleRouterInput>


@end
