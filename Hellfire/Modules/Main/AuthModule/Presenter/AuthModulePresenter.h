//
//  AuthModulePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleViewOutput.h"
#import "AuthModuleInteractorOutput.h"
#import "AuthModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol AuthModuleViewInput;
@protocol AuthModuleInteractorInput;
@protocol AuthModuleRouterInput;

@interface AuthModulePresenter : NSObject <AuthModuleModuleInput, AuthModuleViewOutput, AuthModuleInteractorOutput>

@property (nonatomic, weak) id<AuthModuleViewInput,BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<AuthModuleInteractorInput> interactor;
@property (nonatomic, strong) id<AuthModuleRouterInput> router;

@end
