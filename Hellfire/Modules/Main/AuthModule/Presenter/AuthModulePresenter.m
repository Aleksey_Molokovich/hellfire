//
//  AuthModulePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModulePresenter.h"

#import "AuthModuleViewInput.h"
#import "AuthModuleInteractorInput.h"
#import "AuthModuleRouterInput.h"
#import "CDAccount+CoreDataClass.h"
@interface AuthModulePresenter(){
    BOOL isSuccessfulEntry;
}
@end

@implementation AuthModulePresenter

#pragma mark - Методы AuthModuleModuleInput

- (void)configureModule
{

    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

- (void)didClickFacebook
{
    [_view showProgressWithColor:[UIColor whiteColor]];
    if([_interactor isExistFacebookToken]){
        [_interactor loginWithFacebook];
    }else{
        [_view doActionAuthFB];
    }
}

- (void)didClickVk
{
    [_view showProgressWithColor:[UIColor whiteColor]];
    [_interactor checkAuthVK];
}
- (void)didReceiveFB:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    [_interactor loginWithFacebook];
}

- (void)didReciveVK:(VKAuthorizationResult *)result
{
    [_interactor loginWithVK:result];
}
- (void)didClickLoginWithLogin:(NSString *)login password:(NSString *)password{
    [_view showProgressWithColor:[UIColor whiteColor]];
    [_interactor loginWithUser:login password:password];
}

- (void)didClickRegistration
{
    [_view removeVKDelegate];
    [_router pushRegistration];
}

- (void)didChangeEmail:(NSString *)email
{
    [_interactor saveEmail:email];
    
    [_view changeStateRegistrtionButton:[_interactor validateEmail]];
    [_view changeStateEntryButton:[_interactor validateEmail] && [_interactor validatePassword]];
    
}

- (void) didChangePassword:(NSString *)password
{
    [_interactor savePassword:password];
    [_view changeStateEntryButton:[_interactor validateEmail] && [_interactor validatePassword]];
}

- (void)didClickRecovery
{
    [_interactor recovery];
}

#pragma mark - Методы AuthModuleViewOutput

- (void)didTriggerViewReadyEvent
{
    NSLog(@"didTriggerViewReadyEvent");
        isSuccessfulEntry=NO;
	[self.view setupInitialState];
    
    if (![CDAccount MR_findFirst]) 
        [CDAccount MR_createEntity];
    
}

#pragma mark - Методы AuthModuleInteractorOutput

- (void)needRegistration
{
    [_view removeVKDelegate];
    [_router pushRegistration];
}

- (void)successful
{
    [_view hideProgress];
    if(!isSuccessfulEntry)
    {
        [_view goToRoot];
        isSuccessfulEntry=YES;
    }
}
- (void)successfulWithMessage:(NSString *)message
{
    [_view showAlertWithMessage:message];
}

- (void)failWithError:(NSString *)error
{
    [_view hideProgress];
    [_view showAlertWithMessage:error];
}
@end
