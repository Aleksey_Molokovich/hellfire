//
//  AuthModuleAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleAssembly.h"

#import "AuthModuleViewController.h"
#import "AuthModuleInteractor.h"
#import "AuthModulePresenter.h"
#import "AuthModuleRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation AuthModuleAssembly

- (AuthModuleViewController *)viewAuthModule {
    return [TyphoonDefinition withClass:[AuthModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAuthModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterAuthModule]];
                          }];
}

- (AuthModuleInteractor *)interactorAuthModule {
    return [TyphoonDefinition withClass:[AuthModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAuthModule]];
                          }];
}

- (AuthModulePresenter *)presenterAuthModule{
    return [TyphoonDefinition withClass:[AuthModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAuthModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAuthModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAuthModule]];
                          }];
}

- (AuthModuleRouter *)routerAuthModule{
    return [TyphoonDefinition withClass:[AuthModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAuthModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryAuthModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardAuthModule]];
                                                  [initializer injectParameterWith:@"AuthModuleViewController"];
                                              }];
                          }];
}
- (RamblerViperModuleFactory *)factoryAuthModuleNC  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardAuthModule]];
                                                  [initializer injectParameterWith:@"AuthNavigationController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardAuthModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
