//
//  AuthModuleViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FBSDKLoginManagerLoginResult;
@class VKAuthorizationResult;
@protocol AuthModuleViewOutput <NSObject>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didClickFacebook;
- (void)didClickVk;
- (void)didReceiveFB:(FBSDKLoginManagerLoginResult*)result error:(NSError*)error;
- (void)didReciveVK:(VKAuthorizationResult*)result;
- (void)didClickLoginWithLogin:(NSString*)login password:(NSString*)password;
- (void)didClickRegistration;
- (void)didClickRecovery;
- (void)didChangeEmail:(NSString*)email;
- (void)didChangePassword:(NSString*)password;

@end
