//
//  AuthModuleViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AuthModuleViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)doActionAuthFB;
- (void)removeVKDelegate;
- (void)changeStateEntryButton:(BOOL)state;
- (void)changeStateRegistrtionButton:(BOOL)state;
- (void)goToRoot;
@end
