//
//  AuthModuleViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleViewController.h"
#import "AuthModuleViewOutput.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <VK_ios_sdk/VKSdk.h>
#import "UIColor+HF.h"
#import "UIFont+HF.h"
#import "RoundedButton.h"
#import "UIButtonEye.h"
#import "TextFieldWithWhitePlaceholder.h"


@interface AuthModuleViewController ()<FBSDKLoginButtonDelegate, VKSdkDelegate, VKSdkUIDelegate, UIGestureRecognizerDelegate,UITextFieldDelegate>

@property(strong, nonatomic) FBSDKLoginManager *loginFB;
@property (weak, nonatomic) IBOutlet TextFieldWithWhitePlaceholder *tfLogin;
@property (weak, nonatomic) IBOutlet TextFieldWithWhitePlaceholder *tfPassword;
@property (strong, nonatomic) UITextField *activeField;
@property (assign, nonatomic) BOOL isKeyboard;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButtonEye *eyeButton;

@property (weak, nonatomic) IBOutlet RoundedButton *entryButton;
@property (weak, nonatomic) IBOutlet RoundedButton *registrationButton;
@property (weak, nonatomic) IBOutlet RoundedButton *recoveryButton;

@end

@implementation AuthModuleViewController

#pragma mark - Методы жизненного цикла


- (void)viewDidLoad
{
	[super viewDidLoad];
	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"6155723"];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
    
    
#ifdef DEBUG

    _tfLogin.text = @"test100@test.ru";
    _tfPassword.text = @"testtest";
    [_output didChangePassword:_tfPassword.text];
    [_output didChangeEmail:_tfLogin.text];
    _entryButton.enabled=YES;
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
   
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGSize kbSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    if([_activeField isEqual:_tfPassword])
        _eyeButton.white=NO;
    self.activeField.superview.backgroundColor=[UIColor whiteColor];
    self.activeField.textColor=[UIColor blackColor];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

}

- (void)dismissKeyboard
{
    [self.activeField resignFirstResponder];
}

#pragma mark - Методы TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.activeField=textField;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    
    if([textField isEqual:_tfPassword])
        _eyeButton.white=YES;

}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.superview.backgroundColor=[UIColor clearColor];
    textField.textColor=[UIColor lightGrayColor];
    if(textField.text.length>0){
        textField.textColor=[UIColor whiteColor];
    }else{
        textField.textColor=[UIColor hfGray];
    }
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    
    textField.superview.backgroundColor=[UIColor whiteColor];
    textField.textColor=[UIColor blackColor];
    if([textField isEqual:_tfPassword])
        _eyeButton.white=NO;
    
    
    if([textField isEqual:_tfLogin]){
        [_output didChangeEmail:text];
    }else{
        [_output didChangePassword:text];
    }
    
    
    return YES;
}

#pragma mark - Методы AuthModuleViewInput

- (void)setupInitialState
{
    
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];

    _entryButton.enabled=NO;
    
    [self hideBackButton];
    
    _tfPassword.font=[UIFont hfRegularSize:18];
    _tfLogin.font=[UIFont hfRegularSize:18];
    [_tfPassword configurePlaceholder:@"Пароль"];
    [_tfLogin configurePlaceholder:@"Электронная почта"];
    [_tfLogin becomeFirstResponder];
    

    
    [_registrationButton configureTitle:@"ЗАРЕГИСТРИРОВАТЬСЯ" bgColor:[UIColor hfDeepPink]];
    _registrationButton.enabled = NO;
    [_entryButton configureTitle:@"ВОЙТИ" bgColor:[UIColor hfBlue]];
    _entryButton.enabled = NO;
    
    
    [_recoveryButton configureTitle:@"ВОССТАНОВИТЬ ПАРОЛЬ" bgColor:[UIColor clearColor]];
     _recoveryButton.enabled = NO;
    
    _tfPassword.tag=100;
    _eyeButton.white=YES;
    _eyeButton.hide=YES;

    
//#if DEBUG
////    [_output didClickVk];
//
////    _tfLogin.text=@"lexnikov@yandex.ru";
////    
//    [[FBSDKLoginManager new] logOut];
//    [VKSdk forceLogout];
//#endif
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate=self;
    [self.scrollView addGestureRecognizer:tap];
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goToRoot
{
    [self.navigationController.viewControllers[0] dismissViewControllerAnimated:YES completion:nil];
}

- (void)changeStateEntryButton:(BOOL)state
{
    _entryButton.enabled=state;
}

- (void)changeStateRegistrtionButton:(BOOL)state
{
    _registrationButton.enabled=state;
    _recoveryButton.enabled = state;
}

- (void)removeVKDelegate
{
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"6155723"];
    [sdkInstance unregisterDelegate:self];
}

- (IBAction)clickVK:(id)sender
{
    [_output didClickVk];
}
- (void)doActionAuthFB
{
    self.loginFB = [[FBSDKLoginManager alloc] init];

    self.loginFB.loginBehavior = FBSDKLoginBehaviorBrowser;
    [self.loginFB
     logInWithReadPermissions: @[@"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             [_output didReceiveFB:result error:error];
         }
     }];
}

- (IBAction)clickFacebook:(id)sender
{
    [self.output didClickFacebook];
}
- (IBAction)clickEntry:(id)sender {
    [_output didClickLoginWithLogin:_tfLogin.text password:_tfPassword.text];
}

- (IBAction)clickRegistration:(id)sender
{
    [_output didClickRegistration];
}

- (IBAction)clickEyePass:(id)sender
{
    _eyeButton.hide=!_eyeButton.hide;
    if([_activeField isEqual:_tfPassword])
    {
        _eyeButton.white=NO;
    }
    else
    {
        _eyeButton.white=YES;
    }
    
    _tfPassword.secureTextEntry=!_tfPassword.secureTextEntry;
    
#ifdef DEBUG
    [self showUsers];
#endif
}

- (IBAction)clickRecovery:(id)sender
{
    [_output didClickRecovery];
}

#ifdef DEBUG
- (void)showUsers
{
    [[[LGAlertView alloc] initWithTitle:nil
                                message:nil
                                  style:LGAlertViewStyleActionSheet
                           buttonTitles:@[@"test100",@"test101",@"test102",@"test103",@"test104",@"test105",@"test106",@"test107",@"test108",@"test109",@"android1@test.test",@"android2@test.test"]
                      cancelButtonTitle:@"cancel"
                 destructiveButtonTitle:nil
                          actionHandler:^(LGAlertView * _Nonnull alertView, NSUInteger index, NSString * _Nullable title) {
                              _tfPassword.text = @"testtest";
                              switch (index) {
                                  case 0:
                                      _tfLogin.text = @"test100@test.ru";
                                      break;
                                  case 1:
                                      _tfLogin.text = @"test101@test.ru";
                                      break;
                                  case 2:
                                      _tfLogin.text = @"test102@test.ru";
                                      break;
                                  case 3:
                                      _tfLogin.text = @"test103@test.ru";
                                      break;
                                  case 4:
                                      _tfLogin.text = @"test104@test.ru";
                                      break;
                                  case 5:
                                      _tfLogin.text = @"test105@test.ru";
                                      break;
                                  case 6:
                                      _tfLogin.text = @"test106@test.ru";
                                      break;
                                  case 7:
                                      _tfLogin.text = @"test107@test.ru";
                                      break;
                                  case 8:
                                      _tfLogin.text = @"test108@test.ru";
                                      break;
                                  case 9:
                                      _tfLogin.text = @"test109@test.ru";
                                      break;
                                  case 10:
                                      _tfLogin.text = @"android1@test.test";
                                      _tfPassword.text = @"12344321";
                                      break;
                                  case 11:
                                      _tfLogin.text = @"android2@test.test";
                                      _tfPassword.text = @"12344321";
                                      break;
                                  default:
                                      break;
                              }
                              [_output didChangePassword:_tfPassword.text];
                              [_output didChangeEmail:_tfLogin.text];
                          } cancelHandler:nil
                     destructiveHandler:nil] showAnimated];
}
#endif

#pragma mark - Методы FBSDKLoginButton



- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    [_output didReciveVK:result];
}

@end
