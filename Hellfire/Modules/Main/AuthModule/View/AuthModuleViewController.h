//
//  AuthModuleViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AuthModuleViewInput.h"

@protocol AuthModuleViewOutput;

@interface AuthModuleViewController : BaseViewController <AuthModuleViewInput>

@property (nonatomic, strong) id<AuthModuleViewOutput> output;

@end
