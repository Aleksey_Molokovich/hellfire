//
//  AuthModuleInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleInteractor.h"
#import "AuthModuleInteractorOutput.h"

#import "Account.h"
#import "AccountNew.h"
#import "ResponseModel.h"
#import "StatusModel.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"

#import "AccountRepository.h"
#import "AccountService.h"
#import <LGHelper/UIColor+LGHelper.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <VK_ios_sdk/VKSdk.h>
#import <SDWebImage/SDWebImageManager.h>
#import "HellfireDefines.h"

#import "UserDataValidator.h"

@interface AuthModuleInteractor()<VKSdkDelegate>

@end

@implementation AuthModuleInteractor

#pragma mark - Методы AuthModuleInteractorInput
- (BOOL)isExistFacebookToken{
    return [FBSDKAccessToken currentAccessToken]?YES:NO;
}

- (BOOL)isExistVKToken{
    return [VKSdk accessToken]?YES:NO;
}

- (void)checkAuthVK{
    NSArray *scope = @[VK_PER_PHOTOS, VK_PER_FRIENDS, VK_PER_EMAIL, VK_PER_MESSAGES, VK_PER_WALL];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"6132082"];
    [sdkInstance registerDelegate:self];
    [VKSdk wakeUpSession:scope completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            [VKSdk authorize:scope];
        } else if (error) {
            // Some error happend, but you may try later
        } else if (state == VKAuthorizationInitialized){
            [VKSdk authorize:scope];
        }
        
    }];
}

- (void)saveEmail:(NSString*)email
{
    [CDAccount MR_findFirst].email=email;
}

- (void)savePassword:(NSString*)password
{
    [CDAccount MR_findFirst].password=password;
}

-(BOOL)validateEmail
{
    return [UserDataValidator isValidEmail:[CDAccount MR_findFirst].email].status;
}

-(BOOL)validatePassword
{
    return [CDAccount MR_findFirst].password.length>5;
}

- (void)loginWithFacebook{
    if(![self isExistFacebookToken])
        return;
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email,name"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                  NSDictionary *result, NSError *error) {
         if (!error)
         {
             CDAccount *cdAccount = [CDAccount MR_findFirst];
             if (!result[@"email"]) {
                 [_output failWithError:@"Необходимо указать email "];
                 return ;
             }
             cdAccount.email = result[@"email"];
             cdAccount.accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
             
             if (![CDAccount MR_findFirst].user)
                 [CDAccount MR_findFirst].user = [CDUser MR_createEntity];
             
             [CDAccount MR_findFirst].user.fullName = result[@"name"];
             
             __weak typeof(self) weakSelf = self;

             [AccountRepository loginWithFacebook:[AccountNew fromCoreData:cdAccount]
                                       completion:^(ResponseModel *response) {
                                           
                                           __strong typeof(weakSelf) blockSelf = weakSelf;

                                           if (response.status == 0 && response.error.code == 6)
                                           {
                                               [blockSelf registrationWithFacebook];
                                           }
                                           else
                                           {
                                               [blockSelf proccesingResponse:response];
                                           }
                                       }];
         }
         
     }];
    
    
}

- (void)loginWithVK:(VKAuthorizationResult *)result
{
    CDAccount *cdAccount = [CDAccount MR_findFirst];
    if (!result.token.email) {
        [_output failWithError:@"Необходимо указать email "];
        return ;
    }
    
    VKRequest * user = [[VKApi users] get];
    [user executeWithResultBlock:^(VKResponse *response) {
        if (!cdAccount.user)
            cdAccount.user = [CDUser MR_createEntity];
        
        if (response.json &&
            [response.json isKindOfClass:[NSArray class]] &&
            ((NSArray*)response.json).count>0 &&
            [response.json[0] isKindOfClass:[NSDictionary class]]) {
            if (!cdAccount.user) 
                cdAccount.user = [CDUser MR_createEntity];
            
            cdAccount.user.fullName = [NSString stringWithFormat:@"%@ %@",response.json[0][@"first_name"], response.json[0][@"last_name"]];
        }
        
        
        
    } errorBlock:nil];
    
    
    cdAccount.email= result.token.email;
    cdAccount.accessToken = result.token.accessToken;
    
    __weak typeof(self) weakSelf = self;
    [AccountRepository loginWithVK:[AccountNew fromCoreData:cdAccount]
                        completion:^(ResponseModel *response)
    {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (response.status == 0 && response.error.code == 6)
        {
            [blockSelf registrationWithVK];
        }
        else
        {
            [blockSelf proccesingResponse:response];
        }
                        }];

}
- (void)registrationWithFacebook
{
    __weak typeof(self) weakSelf = self;
    
    [AccountRepository registrationWithFacebook:[AccountNew fromCoreData:[CDAccount MR_findFirst]]
                               completion:^(ResponseModel *response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (response.status == 1) {
             [blockSelf proccesingResponse:response];
         }
     }];
}

- (void)registrationWithVK
{
    __weak typeof(self) weakSelf = self;
    
    [AccountRepository registrationWithVK:[AccountNew fromCoreData:[CDAccount MR_findFirst]]
                               completion:^(ResponseModel *response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (response.status == 1) {
             [blockSelf proccesingResponse:response];
         }
     }];
}

- (void)loginWithUser:(NSString *)login password:(NSString *)password{
    CDAccount *cdAccount = [CDAccount MR_findFirst];
    cdAccount.email=login;
    cdAccount.password=password;
    
    [AccountRepository logIn:[AccountNew fromCoreData:cdAccount]
                  completion:^(ResponseModel *response) {
                      [self proccesingResponse:response];
                  }];
}

- (void)recovery
{
    [AccountRepository recoveryPasswordWithEmail:[CDAccount MR_findFirst].email
                                      completion:^(ResponseModel *response) {
                                          if (response.status == 0) {
                                              [_output failWithError:response.error.message];
                                          }else{
                                              
                                              [_output successfulWithMessage:@"на вашу почту отправлено письмо"];
                                          }
                                      }];
}

- (void)proccesingResponse:(ResponseModel*)response
{
    if(response.status == 1){
        [AccountService clearAllEntityWithAccount:NO];
        CDAccount *cdAccount = [CDAccount MR_findFirst];
        cdAccount.state = AccountStateTypeSuccessfulRegister;
        
        [AccountService createFilterSettingsForPlayers:YES];
        [AccountService createFilterSettingsForPlayers:NO];
        
        cdAccount.colorAvatar = [[UIColor whiteColor] hex];
        if (!cdAccount.avatarPath.length)
        {
            [AccountService createAvatarWithColor];
        }
        else
        {
            [[SDWebImageManager sharedManager]
             downloadImageWithURL:[NSURL URLWithString: cdAccount.avatarPath]
             options:SDWebImageLowPriority
             progress:nil
             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
             {
                 [AccountService createAvatarWithImage:image];
                 [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAvatar object:nil];
                 
             }];
        }
        
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAvatar object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAccountStatus object:nil];
        
//        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
        [_output successful];
    }else{
        [_output failWithError:response.error.message];
        
    }
}



@end
