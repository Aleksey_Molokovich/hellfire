//
//  AuthModuleInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AuthModuleInteractorOutput <NSObject>

- (void)successful;
- (void)successfulWithMessage:(NSString*)message;
- (void)failWithError:(NSString*)error;
- (void)needRegistration;

@end
