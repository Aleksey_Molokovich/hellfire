//
//  AuthModuleInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AuthModuleInteractorInput.h"

@protocol AuthModuleInteractorOutput;

@interface AuthModuleInteractor : NSObject <AuthModuleInteractorInput>

@property (nonatomic, weak) id<AuthModuleInteractorOutput> output;

@end
