//
//  AuthModuleInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 20/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class VKAuthorizationResult;
@protocol AuthModuleInteractorInput <NSObject>
- (BOOL)isExistFacebookToken;
- (void)loginWithFacebook;
- (void)loginWithVK:(VKAuthorizationResult *)result;
- (void)loginWithUser:(NSString*)login password:(NSString*)password;
- (void)saveEmail:(NSString*)email;
- (void)savePassword:(NSString*)password;
- (void)recovery;
- (BOOL)validateEmail;
- (BOOL)validatePassword;

- (BOOL)isExistVKToken;
- (void)checkAuthVK;
@end
