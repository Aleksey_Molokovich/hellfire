//
//  FilterForPlayersAndClansActions.h
//  Hellfire
//
//  Created by Алексей Молокович on 18.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef FilterForPlayersAndClansActions_h
#define FilterForPlayersAndClansActions_h



static NSString *const kKindOfSortDirectionSortAscending = @"KindOfSortDirectionSortAscending";
static NSString *const kKindOfSortDirectionSortDescending = @"KindOfSortDirectionSortDescending";


static NSString *const kKindOfSortTypeBySoloPoints = @"По рейтингу Solo";
static NSString *const kKindOfSortTypeByHotseatPoints = @"По рейтингу Hotseat";
static NSString *const kKindOfSortTypeByName = @"По имени";
static NSString *const kKindOfSortTypeByClan = @"По названию";

static NSString *const kKindOfSortClanTypeByAmountOfMembers = @"По количеству участников";


#endif /* FilterForPlayersAndClansActions_h */
