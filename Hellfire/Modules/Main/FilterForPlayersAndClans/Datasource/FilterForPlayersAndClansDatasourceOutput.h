//
//  FilterForPlayersAndClansDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterForPlayersAndClansDatasourceOutput <NSObject>
- (void)prepareForPlayers:(BOOL)forPlayers registred:(BOOL)isRegistred;
@end
