//
//  FilterForPlayersAndClansDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansDatasource.h"
#import "FilterForPlayersAndClansDatasourceOutput.h"
#import "ObjectTableViewCell.h"
#import "FilterForPlayersAndClansActions.h"
#import "CDAccount+CoreDataClass.h"
#import "CDSortOfFilter+CoreDataClass.h"
#import "CDFilterSetting+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
#import "CheckBoxFilterCellData.h"

@interface FilterForPlayersAndClansDatasource()

@property (nonatomic, strong) NSArray<NSArray<ObjectTableViewCell*>*> *data;

@end

@implementation FilterForPlayersAndClansDatasource

#pragma mark - Методы FilterForPlayersAndClansDatasourceInput
- (void)prepareForPlayers:(BOOL)forPlayers
                registred:(BOOL)isRegistred
{
	NSMutableArray *mData=[NSMutableArray new];

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.filterForPlayers = %@ and self.saved = 0", @(forPlayers)];
    
    CDFilterSetting *filter = [CDFilterSetting MR_findAllWithPredicate:predicate].firstObject;
    
    
    NSString *location;
    if(filter.country.name)
    {
        if (filter.city.name) {
            location = [NSString stringWithFormat:@"%@, %@", filter.country.name,filter.city.name];
        }
        else
        {
            location = filter.country.name;
        }
    }
    else
    {
       location = @"Весь мир";
    }

    
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"GeoLocationFilterCell";
    item.height=96;
    item.data = location;
    [mData addObject:@[item]];
    
    NSMutableArray *mSort=[NSMutableArray new];
    item=[ObjectTableViewCell new];
    item.cellId=@"KindOfSortFilterCell";
    item.height=44;
    item.data = filter.bySoloPoints;
    
    [mSort addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"KindOfSortFilterCell";
    item.height=44;
    item.data = filter.byHotseatPoints;
    
    [mSort addObject:item];
    
    item=[ObjectTableViewCell new];
    item.cellId=@"KindOfSortFilterCell";
    item.height=44;
    item.data = filter.byName;
    
    [mSort addObject:item];
    
    if (!forPlayers) {
        item=[ObjectTableViewCell new];
        item.cellId=@"KindOfSortFilterCell";
        item.height=44;
        item.data = filter.byMembers;
        
        [mSort addObject:item];
    }
    
    [mData addObject:mSort];
    
    if (isRegistred) {
        item=[ObjectTableViewCell new];
        item.cellId=@"CheckBoxFilterCell";
        item.height=44;
        CheckBoxFilterCellData *cellData = [CheckBoxFilterCellData new];
        cellData.title = forPlayers?@"Показывать друзей в общем списке игроков":@"Показывать мои кланы в общем списке";
        cellData.check = filter.includeMy;
        item.data = cellData;
        [mData addObject:@[item]];
    }
    
    
    _data = mData;
}

-(NSInteger)countItemsInSection:(NSInteger)section
{
    return _data[section].count;
}
-(NSInteger)countSection{
    return _data.count;
}

-(CGFloat)heightAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].height;
}

-(NSString *)cellIdAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].cellId;
}

-(id)dataAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].data;
}
-(NSArray*)cellsIdentifire{
    return @[@"GeoLocationFilterCell",@"KindOfSortFilterCell",@"CheckBoxFilterCell"];
}
@end
