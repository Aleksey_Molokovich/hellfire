//
//  FilterForPlayersAndClansDatasourceInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterForPlayersAndClansDatasourceInput <NSObject>

@property (nonatomic, assign, readonly) NSInteger countSection;

- (CGFloat) heightAtIndex:(NSIndexPath*)index;
- (id) dataAtIndex:(NSIndexPath*)index;
- (NSString*) cellIdAtIndex:(NSIndexPath*)index;
- (NSArray*) cellsIdentifire;
- (NSInteger) countItemsInSection:(NSInteger)section;
@end
