//
//  FilterForPlayersAndClansDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansDatasourceInput.h"

@protocol FilterForPlayersAndClansDatasourceOutput;

@interface FilterForPlayersAndClansDatasource : NSObject <FilterForPlayersAndClansDatasourceInput,FilterForPlayersAndClansDatasourceOutput>


@end
