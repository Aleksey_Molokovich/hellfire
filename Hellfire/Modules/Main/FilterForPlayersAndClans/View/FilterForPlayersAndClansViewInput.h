//
//  FilterForPlayersAndClansViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterForPlayersAndClansViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)update;
@end
