//
//  KindOfSortFilterCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 15.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "KindOfSortFilterCell.h"
#import "CDSortOfFilter+CoreDataClass.h"
#import "UIFont+HF.h"
#import "FilterForPlayersAndClansActions.h"

@implementation KindOfSortFilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageRadiobtn.image = [UIImage imageNamed:@"radiobox-blank"];
}

- (void)configureWithData:(id)object
{
    if (![object isKindOfClass:[CDSortOfFilter class]])
    {
        return;
    }
    
    
    CDSortOfFilter *_data = object;
    _title.text = _data.title;
    if (_data.selected)
    {
        _imageRadiobtn.image = [UIImage imageNamed:@"radiobox-marked"];
    }
    else
    {
        _imageRadiobtn.image = [UIImage imageNamed:@"radiobox-blank"];
    }
    
    _lblSortDirection.font = [UIFont hfBoldSize:12];
    
    if ([_data.title isEqualToString:kKindOfSortTypeByName] ||
        [_data.title isEqualToString:kKindOfSortTypeByClan])
    {
        _lblSortDirection.text = _data.ascending? @"А — Я" : @"Я — А";
    }
    else
    {
        _lblSortDirection.text = _data.ascending? @"0 — 9": @"9 — 0";
    }
}

- (IBAction)switchSortDirection:(id)sender
{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;

        model.action = _title.text;
        [self.output callBackObject:model];
        
        model.action = nil;
        [self.output callBackObject:model];
    }
}

- (IBAction)selectedSort:(id)sender
{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = _title.text;
        [self.output callBackObject:model];
    }
}

@end
