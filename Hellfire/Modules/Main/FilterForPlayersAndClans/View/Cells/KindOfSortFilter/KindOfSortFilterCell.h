//
//  KindOfSortFilterCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 15.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@class CDSortOfFilter;

@interface KindOfSortFilterCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSortDirection;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *imageRadiobtn;

@end
