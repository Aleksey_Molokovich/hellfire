//
//  CheckBoxFilterCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 15.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CheckBoxFilterCell.h"
#import "CheckBoxFilterCellData.h"

@implementation CheckBoxFilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureWithData:(id)data
{
    if ([data isKindOfClass:[CheckBoxFilterCellData class]])
    {
        _label.text = ((CheckBoxFilterCellData*)data).title;
        if (((CheckBoxFilterCellData*)data).check)
        {
            _image.image = [UIImage imageNamed:@"checkbox-marked"];
        }
        else
        {
            _image.image = [UIImage imageNamed:@"checkbox-blank"];
        }
        
    }
}

- (IBAction)clickGeoLocation:(id)sender
{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        [self.output callBackObject:model];
    }
}
@end
