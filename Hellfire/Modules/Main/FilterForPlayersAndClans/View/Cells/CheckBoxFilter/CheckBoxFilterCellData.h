//
//  CheckBoxFilterCellData.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckBoxFilterCellData : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL check;
@end
