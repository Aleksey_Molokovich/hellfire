//
//  GeoLocationFilterCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 15.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GeoLocationFilterCell.h"

@implementation GeoLocationFilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureWithData:(id)data
{
    if ([data isKindOfClass:[NSString class]])
    {
        _label.text = data;
    }
}


- (IBAction)clickGeoLocation:(id)sender
{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        [self.output callBackObject:model];
    }
}

@end
