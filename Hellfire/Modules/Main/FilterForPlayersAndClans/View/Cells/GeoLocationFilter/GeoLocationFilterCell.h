//
//  GeoLocationFilterCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 15.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@interface GeoLocationFilterCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
