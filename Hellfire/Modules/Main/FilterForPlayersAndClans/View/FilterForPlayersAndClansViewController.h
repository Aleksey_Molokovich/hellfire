//
//  FilterForPlayersAndClansViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterForPlayersAndClansViewInput.h"
#import "BaseViewController.h"
#import "FilterForPlayersAndClansDatasourceInput.h"

@protocol FilterForPlayersAndClansViewOutput;
@protocol FilterForPlayersAndClansDatasourceInput;

@interface FilterForPlayersAndClansViewController : BaseViewController <FilterForPlayersAndClansViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<FilterForPlayersAndClansViewOutput> output;
@property (nonatomic, strong) id<FilterForPlayersAndClansDatasourceInput> datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
