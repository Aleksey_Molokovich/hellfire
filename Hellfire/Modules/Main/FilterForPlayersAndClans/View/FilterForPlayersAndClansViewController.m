//
//  FilterForPlayersAndClansViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansViewController.h"
#import "FilterForPlayersAndClansViewOutput.h"
#import "BaseTableViewCell.h"
#import "CellManager.h"
#import "FilterHeaderSort.h"

@implementation FilterForPlayersAndClansViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (IBAction)goBack:(id)sender {
    [_output exitWithoutSave];
}

- (IBAction)saveFilter:(id)sender {
    [_output saveFilterSettings];
}


#pragma mark - Методы FilterForPlayersAndClansViewInput

- (void)setupInitialState {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorBlue];
    [self hideBottomBar];
    [CellManager registerCellsWithId:[_datasource cellsIdentifire]
                           tableView:_tableView];
}

- (void)update
{
    [_tableView reloadData];
}



#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_datasource countItemsInSection:section];
}

-  (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_datasource countSection];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource cellIdAtIndex:indexPath] forIndexPath:indexPath];
    
    cell.output = _output;
    cell.cellIndex = indexPath;
    [cell configureWithData:[_datasource dataAtIndex:indexPath]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [_datasource heightAtIndex:indexPath];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        return [[[NSBundle mainBundle] loadNibNamed:@"FilterHeaderSort" owner:self options:nil] firstObject];
    }
    
    if (section == 2)
    {
        return [[[NSBundle mainBundle] loadNibNamed:@"FilterHeaderLineView" owner:self options:nil] firstObject];
    }
    
    return [UIView new];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    
    return 44;
}

@end
