//
//  FilterForPlayersAndClansRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GeoCatalogModuleOutput.h"

@interface FilterForPlayersAndClansRouter()

//@property (strong, nonatomic) RamblerViperModuleFactory *factory;

@end


@implementation FilterForPlayersAndClansRouter

#pragma mark - Методы FilterForPlayersAndClansRouterInput



- (void)pushGeoCatalogWithCallBackModule:(id<GeoCatalogModuleOutput>)moduleOutput
{
    self.geoCatalogAssembly = [[GeoCatalogAssembly new] activated];
    self.factory = [self.geoCatalogAssembly factoryGeoCatalog];
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<GeoCatalogModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithType:CatalogCountry
                                                                                 style:CatalogStyleLight
                                                                                action:CatalogActionFilter
                                                                               country:nil];
                                                  return moduleOutput;
                                              }];
}
@end
