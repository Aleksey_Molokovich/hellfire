//
//  FilterForPlayersAndClansRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol GeoCatalogModuleOutput;

@protocol FilterForPlayersAndClansRouterInput <NSObject>
- (void)pushGeoCatalogWithCallBackModule:(id<GeoCatalogModuleOutput>)moduleOutput;
- (void)closeCurrentModule;
@end
