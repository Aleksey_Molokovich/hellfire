//
//  FilterForPlayersAndClansPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansPresenter.h"

#import "FilterForPlayersAndClansViewInput.h"
#import "FilterForPlayersAndClansInteractorInput.h"
#import "FilterForPlayersAndClansRouterInput.h"
#import "AccountService.h"
#import "FilterForPlayersAndClansActions.h"

@implementation FilterForPlayersAndClansPresenter

#pragma mark - Методы FilterForPlayersAndClansModuleInput

- (void)configureModuleForPlayers:(BOOL)forPlayers
{
    _forPlayers = forPlayers;
    [_interactor configureWithPlayer:_forPlayers];
}

#pragma mark - Методы FilterForPlayersAndClansViewOutput

- (void)didTriggerViewReadyEvent {
    
    [self.view setupInitialState];
    [_datasource prepareForPlayers:_forPlayers
                         registred:([AccountService state] == AccountStateTypeSuccessfulRegister)? YES : NO ];
}

- (void)saveFilterSettings
{
    [_interactor saveSettings];

    if (self.moduleOutput)
    {
        [_moduleOutput filterSettingsDidChange];
    }
    [self.router closeCurrentModule];
}

- (void)exitWithoutSave
{
    [_interactor undoChangeSettings];
    [self.router closeCurrentModule];
}
#pragma mark - Методы FilterForPlayersAndClansInteractorOutput
- (void)successful
{
    [_datasource prepareForPlayers:_forPlayers
                         registred:([AccountService state] == AccountStateTypeSuccessfulRegister)? YES : NO ];
    [_view update];
}

#pragma mark GeoCatalogModuleOutput

- (void)changeCountry:(CatalogModel *)model
{
    [_interactor changeCountry:model];
}

- (void)changeCity:(CatalogModel *)model
{
    [_interactor changeCity:model];
}

#pragma mark CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object {
    switch ([object.cellIndex section])
    {
        case 0:
            [_router pushGeoCatalogWithCallBackModule:self];
            break;
        case 1:
            if ([object.action isEqualToString: kKindOfSortTypeByName] ||
                [object.action isEqualToString: kKindOfSortTypeByClan])
            {
                [_interactor selecteSortByName];
            }
            else if ([object.action isEqualToString: kKindOfSortTypeBySoloPoints])
            {
                [_interactor selecteSortBySoloPoinst];
            }
            else if ([object.action isEqualToString: kKindOfSortTypeByHotseatPoints])
            {
                [_interactor selecteSortByHotseatPoints];
            }
            else if ([object.action isEqualToString: kKindOfSortClanTypeByAmountOfMembers])
            {
                [_interactor selecteSortByMembers];
            }
            else
            {
                [_interactor changeSortDirectionSelectedFilter];
            }
            break;
        case 2:
            [_interactor changeIncludeMy];
            break;
        default:
            break;
    }
    [self successful];
}

@end
