//
//  FilterForPlayersAndClansPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansViewOutput.h"
#import "FilterForPlayersAndClansInteractorOutput.h"
#import "FilterForPlayersAndClansModuleInput.h"
#import "FilterForPlayersAndClansDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"
#import "GeoCatalogModuleOutput.h"
#import "FilterForPlayersAndClansModuleOutput.h"

@protocol BaseViewControllerProtocol;
@protocol FilterForPlayersAndClansViewInput;
@protocol FilterForPlayersAndClansInteractorInput;
@protocol FilterForPlayersAndClansRouterInput;
@protocol FilterForPlayersAndClansDatasourceOutput;

@interface FilterForPlayersAndClansPresenter : NSObject <FilterForPlayersAndClansModuleInput, FilterForPlayersAndClansViewOutput, FilterForPlayersAndClansInteractorOutput, GeoCatalogModuleOutput>

@property (nonatomic, weak) id<FilterForPlayersAndClansViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<FilterForPlayersAndClansInteractorInput> interactor;
@property (nonatomic, strong) id<FilterForPlayersAndClansRouterInput> router;
@property (nonatomic, strong) id<FilterForPlayersAndClansDatasourceOutput> datasource;

@property (nonatomic) id<FilterForPlayersAndClansModuleOutput> moduleOutput;

@property (assign, nonatomic) BOOL forPlayers;

@end
