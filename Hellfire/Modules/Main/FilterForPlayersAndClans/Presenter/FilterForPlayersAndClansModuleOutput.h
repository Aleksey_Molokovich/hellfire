//
//  FilterForPlayersAndClansModuleOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 15.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol FilterForPlayersAndClansModuleOutput <RamblerViperModuleOutput>


- (void)filterSettingsDidChange;

@end


