//
//  FilterForPlayersAndClansAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansAssembly.h"

#import "FilterForPlayersAndClansViewController.h"
#import "FilterForPlayersAndClansInteractor.h"
#import "FilterForPlayersAndClansPresenter.h"
#import "FilterForPlayersAndClansRouter.h"
#import "FilterForPlayersAndClansDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FilterForPlayersAndClansAssembly

- (FilterForPlayersAndClansViewController *)viewFilterForPlayersAndClans {
    return [TyphoonDefinition withClass:[FilterForPlayersAndClansViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFilterForPlayersAndClans]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFilterForPlayersAndClans]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceFilterForPlayersAndClans]];
                          }];
}

- (FilterForPlayersAndClansInteractor *)interactorFilterForPlayersAndClans {
    return [TyphoonDefinition withClass:[FilterForPlayersAndClansInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFilterForPlayersAndClans]];
                          }];
}

- (FilterForPlayersAndClansPresenter *)presenterFilterForPlayersAndClans{
    return [TyphoonDefinition withClass:[FilterForPlayersAndClansPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFilterForPlayersAndClans]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFilterForPlayersAndClans]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFilterForPlayersAndClans]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceFilterForPlayersAndClans]];
                          }];
}

- (FilterForPlayersAndClansDatasource *)datasourceFilterForPlayersAndClans{
    return [TyphoonDefinition withClass:[FilterForPlayersAndClansDatasource class]];
}

- (FilterForPlayersAndClansRouter *)routerFilterForPlayersAndClans{
    return [TyphoonDefinition withClass:[FilterForPlayersAndClansRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFilterForPlayersAndClans]];
                          }];
}

- (RamblerViperModuleFactory *)factoryFilterForPlayersAndClans  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardFilterForPlayersAndClans]];
                                                  [initializer injectParameterWith:@"FilterForPlayersAndClansViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardFilterForPlayersAndClans {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
