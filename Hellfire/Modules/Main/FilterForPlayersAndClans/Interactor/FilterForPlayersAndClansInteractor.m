//
//  FilterForPlayersAndClansInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansInteractor.h"
#import "FilterForPlayersAndClansInteractorOutput.h"
#import "CDAccount+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
#import "CDSortOfFilter+CoreDataClass.h"
#import "CDFilterSetting+CoreDataClass.h"
#import "FilterForPlayersAndClansActions.h"
#import "CatalogModel.h"
#import "HFFilterModel.h"
#import "HellfireDefines.h"
#import "AccountService.h"

@implementation FilterForPlayersAndClansInteractor

#pragma mark - Методы FilterForPlayersAndClansInteractorInput

- (void)configureWithPlayer:(BOOL)forPlayers
{
    [AccountService createFilterSettingsForPlayers:forPlayers];
    
    _isSortForPlayers = forPlayers;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.filterForPlayers = %@ and self.saved = 1", @(forPlayers)];
    self.filterSaved = [CDFilterSetting MR_findAllWithPredicate:predicate].firstObject;
    
    
    
    predicate = [NSPredicate predicateWithFormat:@"self.filterForPlayers = %@ and self.saved = 0", @(forPlayers)];
    self.filter = [CDFilterSetting MR_findAllWithPredicate:predicate].firstObject;
    
    self.filter.includeMy = self.filterSaved.includeMy;
    
    self.filter.filterForPlayers = self.filterSaved.filterForPlayers;
    self.filter.byName = [CDSortOfFilter MR_createEntity];
    self.filter.byName.title = self.filterSaved.byName.title;
    self.filter.byName.selected = self.filterSaved.byName.selected;
    self.filter.byName.ascending = self.filterSaved.byName.ascending;
    
    self.filter.bySoloPoints = [CDSortOfFilter MR_createEntity];
    self.filter.bySoloPoints.title = self.filterSaved.bySoloPoints.title;
    self.filter.bySoloPoints.selected = self.filterSaved.bySoloPoints.selected;
    self.filter.bySoloPoints.ascending = self.filterSaved.bySoloPoints.ascending;

    self.filter.byHotseatPoints = [CDSortOfFilter MR_createEntity];
    self.filter.byHotseatPoints.title = self.filterSaved.byHotseatPoints.title;
    self.filter.byHotseatPoints.selected = self.filterSaved.byHotseatPoints.selected;
    self.filter.byHotseatPoints.ascending = self.filterSaved.byHotseatPoints.ascending;

    if (!forPlayers) {
        self.filter.byMembers = [CDSortOfFilter MR_createEntity];
        self.filter.byMembers.title = self.filterSaved.byMembers.title;
        self.filter.byMembers.selected = self.filterSaved.byMembers.selected;
        self.filter.byMembers.ascending = self.filterSaved.byMembers.ascending;
    }
    
    HFFilterModel *filterSettings =[[HFFilterModel alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:forPlayers?FilterPlayer:FilterClan]
                                                                       error:nil] ;
    self.filter.city = [CDCatalog MR_createEntity];
    self.filter.city.id = filterSettings ? filterSettings.cityId : self.filterSaved.city.id;
    self.filter.city.name = filterSettings ? filterSettings.cityName : self.filterSaved.city.name;
    
    self.filter.country = [CDCatalog MR_createEntity];
    self.filter.country.id = filterSettings ? filterSettings.countryId : self.filterSaved.country.id;
    self.filter.country.name =  filterSettings ? filterSettings.countryName : self.filterSaved.country.name;
    self.filter.saved = NO;
}


- (void)saveSettings
{
    self.filterSaved.includeMy = self.filter.includeMy;
    
    self.filterSaved.filterForPlayers = self.filter.filterForPlayers;
    self.filterSaved.byName = [CDSortOfFilter MR_createEntity];
    self.filterSaved.byName.title = self.filter.byName.title;
    self.filterSaved.byName.selected = self.filter.byName.selected;
    self.filterSaved.byName.ascending = self.filter.byName.ascending;
    
    self.filterSaved.bySoloPoints = [CDSortOfFilter MR_createEntity];
    self.filterSaved.bySoloPoints.title = self.filter.bySoloPoints.title;
    self.filterSaved.bySoloPoints.selected = self.filter.bySoloPoints.selected;
    self.filterSaved.bySoloPoints.ascending = self.filter.bySoloPoints.ascending;
    
    self.filterSaved.byHotseatPoints = [CDSortOfFilter MR_createEntity];
    self.filterSaved.byHotseatPoints.title = self.filter.byHotseatPoints.title;
    self.filterSaved.byHotseatPoints.selected = self.filter.byHotseatPoints.selected;
    self.filterSaved.byHotseatPoints.ascending = self.filter.byHotseatPoints.ascending;
    
    if (!self.isSortForPlayers) {
        self.filterSaved.byMembers = [CDSortOfFilter MR_createEntity];
        self.filterSaved.byMembers.title = self.filter.byMembers.title;
        self.filterSaved.byMembers.selected = self.filter.byMembers.selected;
        self.filterSaved.byMembers.ascending = self.filter.byMembers.ascending;
    }
    
    self.filterSaved.city = [CDCatalog MR_createEntity];
    self.filterSaved.city.id = self.filter.city.id;
    self.filterSaved.city.name = self.filter.city.name;
    
    self.filterSaved.country = [CDCatalog MR_createEntity];
    self.filterSaved.country.id = self.filter.country.id;
    self.filterSaved.country.name = self.filter.country.name;
    self.filterSaved.saved = YES;
    
    HFFilterModel *filterSettings = [HFFilterModel new];
    filterSettings.countryId = self.filter.country.id;
    filterSettings.countryName = self.filter.country.name;
    filterSettings.cityId = self.filter.city.id;
    filterSettings.cityName = self.filter.city.name;
    
    [[NSUserDefaults standardUserDefaults] setObject:[filterSettings toDictionary] forKey:self.isSortForPlayers?FilterPlayer:FilterClan];
}

- (void)undoChangeSettings
{
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.filterForPlayers = %@ and self.saved = 1", @(self.isSortForPlayers)];
//
//    self.filter  = [CDFilterSetting MR_findAllWithPredicate:predicate].firstObject;
}

- (void)changeSortDirectionSelectedFilter
{
    
    if ([_selectedFilter isEqualToString: kKindOfSortTypeByName] ||
        [_selectedFilter isEqualToString: kKindOfSortTypeByClan])
    {
        _filter.byName.ascending = !_filter.byName.ascending;
    }
    else if ([_selectedFilter isEqualToString: kKindOfSortTypeBySoloPoints])
    {
        _filter.bySoloPoints.ascending = !_filter.bySoloPoints.ascending;
    }
    else if ([_selectedFilter isEqualToString: kKindOfSortTypeByHotseatPoints])
    {
        _filter.byHotseatPoints.ascending = !_filter.byHotseatPoints.ascending;
    }
    else if ([_selectedFilter isEqualToString: kKindOfSortClanTypeByAmountOfMembers])
    {
        _filter.byMembers.ascending = !_filter.byMembers.ascending;
    }
}


- (void)changeCity:(CatalogModel *)catalog {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@ AND name = %@",@(catalog.id),catalog.name];
    _filter.city = [CDCatalog MR_findFirstWithPredicate:predicate];
    [_output successful];
}

- (void)changeCountry:(CatalogModel *)catalog {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@ AND name = %@",@(catalog.id),catalog.name];
    _filter.country = [CDCatalog MR_findFirstWithPredicate:predicate];
    _filter.city = nil;
    [_output successful];
}

- (void)selecteSortByName
{
    [self deselectSortType];
    _selectedFilter = kKindOfSortTypeByName;
    _filter.byName.selected = YES;
}

- (void)selecteSortBySoloPoinst
{
    [self deselectSortType];
    _selectedFilter = kKindOfSortTypeBySoloPoints;
    _filter.bySoloPoints.selected = YES;
}

- (void)selecteSortByHotseatPoints
{
    [self deselectSortType];
    _selectedFilter = kKindOfSortTypeByHotseatPoints;
    _filter.byHotseatPoints.selected = YES;
}

- (void)selecteSortByMembers
{
    [self deselectSortType];
    _selectedFilter = kKindOfSortClanTypeByAmountOfMembers;
    _filter.byMembers.selected = YES;
}

- (void)deselectSortType
{
    _filter.byName.selected = NO;
    _filter.bySoloPoints.selected = NO;
    _filter.byHotseatPoints.selected = NO;
    _filter.byMembers.selected = NO;
}

- (void)changeIncludeMy {
    _filter.includeMy = !_filter.includeMy;
    [_output successful];
}

@end
