//
//  FilterForPlayersAndClansInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FilterForPlayersAndClansInteractorInput.h"
@class  CDFilterSetting;
@protocol FilterForPlayersAndClansInteractorOutput;

@interface FilterForPlayersAndClansInteractor : NSObject <FilterForPlayersAndClansInteractorInput>

@property (nonatomic, weak) id<FilterForPlayersAndClansInteractorOutput> output;

@property (nonatomic, assign) BOOL isSortForPlayers;

@property (nonatomic) CDFilterSetting *filter;
@property (nonatomic) CDFilterSetting *filterSaved;

@property (nonatomic) NSString *selectedFilter;

@end
