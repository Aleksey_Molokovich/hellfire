//
//  FilterForPlayersAndClansInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 15/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDCatalog+CoreDataClass.h"
@class CatalogModel;
@protocol FilterForPlayersAndClansInteractorInput <NSObject>

- (void)configureWithPlayer:(BOOL)forPlayers;

- (void)changeIncludeMy;
- (void)changeCountry:(CatalogModel*)catalog;
- (void)changeCity:(CatalogModel*)catalog;

- (void)changeSortDirectionSelectedFilter;

- (void)selecteSortByName;
- (void)selecteSortBySoloPoinst;
- (void)selecteSortByHotseatPoints;
- (void)selecteSortByMembers;

- (void)saveSettings;
- (void)undoChangeSettings;
@end
