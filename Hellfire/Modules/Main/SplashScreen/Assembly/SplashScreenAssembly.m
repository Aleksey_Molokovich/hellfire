//
//  SplashScreenAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenAssembly.h"

#import "SplashScreenViewController.h"
#import "SplashScreenInteractor.h"
#import "SplashScreenPresenter.h"
#import "SplashScreenRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation SplashScreenAssembly

- (SplashScreenViewController *)viewSplashScreen {
    return [TyphoonDefinition withClass:[SplashScreenViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSplashScreen]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSplashScreen]];
                          }];
}

- (SplashScreenInteractor *)interactorSplashScreen {
    return [TyphoonDefinition withClass:[SplashScreenInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSplashScreen]];
                          }];
}

- (SplashScreenPresenter *)presenterSplashScreen{
    return [TyphoonDefinition withClass:[SplashScreenPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSplashScreen]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSplashScreen]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSplashScreen]];
                          }];
}

- (SplashScreenRouter *)routerSplashScreen{
    return [TyphoonDefinition withClass:[SplashScreenRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSplashScreen]];
                          }];
}

- (RamblerViperModuleFactory *)factorySplashScreen  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardSplashScreen]];
                                                  [initializer injectParameterWith:@"SplashScreenViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardSplashScreen {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
