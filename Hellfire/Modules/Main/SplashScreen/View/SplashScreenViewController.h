//
//  SplashScreenViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashScreenViewInput.h"
#import "BaseViewController.h"
#import "RoundedButton.h"

@protocol SplashScreenViewOutput;

@interface SplashScreenViewController : BaseViewController <SplashScreenViewInput>

@property (nonatomic, strong) id<SplashScreenViewOutput> output;



@end
