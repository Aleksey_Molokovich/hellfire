//
//  SplashScreenViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//
@import WebKit;
#import "SplashScreenViewController.h"
#import "UIColor+HF.h"
#import "SplashScreenViewOutput.h"
#import "UIFont+HF.h"




@interface SplashScreenViewController()

@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *text;
@property (nonatomic, strong) CAShapeLayer *progressLayer;
@property (assign, nonatomic) NSInteger counter;
@property (strong, nonatomic) NSArray<NSString*> *titles;
@property (strong, nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIView *splashView;




@end


@implementation SplashScreenViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы SplashScreenViewInput

- (void)setupInitialState {
    
    _progressView.layer.borderWidth = 0;
    _titles = @[@"Моем колбу",
                @"Продуваем шланги",
                @"Вселяем джинна в чашу",
                @"Разжигаем уголь",
                @"Глубокий вдох...",
                @"Йооожик!"];
}

- (void)setupProgress{
    
    self.progressLayer = [CAShapeLayer layer];
    [self.progressView.layer addSublayer:self.progressLayer];
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint begPoint = CGPointMake(5, _progressView.bounds.size.height/2);
    CGPoint endPoint = CGPointMake(_progressView.bounds.size.width-5, _progressView.frame.size.height/2);

    [path moveToPoint:begPoint];
    [path addLineToPoint:endPoint];
    
    
    _progressLayer.path=path.CGPath;
    
    // Configure the apperence of the circle
    _progressLayer.fillColor = [UIColor clearColor].CGColor;
    _progressLayer.strokeColor = [UIColor hfOrange].CGColor;
    _progressLayer.lineWidth = 10;
    _progressLayer.lineCap = kCALineCapRound;
    _progressLayer.rasterizationScale = 2 * [UIScreen mainScreen].scale;
    _progressLayer.shouldRasterize = YES;

    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration  = 4.5;
    #ifdef DEBUG
        drawAnimation.duration  = 0.1;
    #endif
    drawAnimation.fromValue = [NSNumber numberWithFloat:0];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1];
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    // Add the animation to the circle
    [_progressLayer addAnimation:drawAnimation forKey:@"strokeEndAnimation"];
    
    _counter = 0;
    _timer=[NSTimer scheduledTimerWithTimeInterval:(4.0/_titles.count)
                                     target:self
                                   selector:@selector(changeTitle)
                                   userInfo:nil
                                    repeats:YES];
    
    
}

- (void)changeTitle{
    if (_counter<_titles.count) {
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[_titles[_counter] uppercaseString] attributes:@{NSFontAttributeName:[UIFont hfBoldSize:16]}];

        [attributedString addAttribute:NSKernAttributeName
                                 value:@(2)
                                 range:NSMakeRange(0, [_titles[_counter] length])];

        _text.attributedText=attributedString;
        _counter++;
    }else{
        [_timer invalidate];
        
        [_output didFinish];

    }
}



- (void)viewDidAppear:(BOOL)animated{
   [self setupProgress];
}

@end
