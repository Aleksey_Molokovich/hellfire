//
//  SplashScreenViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SplashScreenViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

@end
