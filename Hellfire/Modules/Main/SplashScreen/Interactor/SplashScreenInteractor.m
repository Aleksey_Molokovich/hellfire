//
//  SplashScreenInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenInteractor.h"
#import "AccountRepository.h"
#import "SplashScreenInteractorOutput.h"
#import "CDAccount+CoreDataClass.h"
#import "HellfireDefines.h"
#import "ResponseModel.h"
#import <SDWebImage/SDWebImageManager.h>
#import "AccountService.h"
#import "BluetoothService.h"
#import "ProfileResponse.h"


@implementation SplashScreenInteractor

#pragma mark - Методы SplashScreenInteractorInput

- (void) checkUserToken
{
    
    [AccountRepository autoLoginWithCompletion:^(ResponseModel *response) {
        
        if (response.status == 0)
        {
            [AccountService deleteAvatar];
            [AccountService clearAllEntityWithAccount:YES];
//            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
            return ;
        }
        
        CDAccount *cdAccount = [CDAccount MR_findFirst];
        if (response.status == 1)
        {
            ProfileResponse *profile = [[ProfileResponse alloc] initWithDictionary:response.data[@"profile"] error:nil];
            cdAccount.state = AccountStateTypeSuccessfulRegister;
            cdAccount.soloPoints =profile.soloPoints;
            cdAccount.hotseatPoints = profile.hotseatPoints;
            [BluetoothService sharedInstance].isSendMetrics = profile.isSendMetrics;
        }
        
        [AccountService createFilterSettingsForPlayers:YES];
        [AccountService createFilterSettingsForPlayers:NO];
        
        [[SDWebImageManager sharedManager]
         downloadImageWithURL:[NSURL URLWithString: cdAccount.avatarPath]
         options:SDWebImageLowPriority
         progress:nil
         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
         {
             [AccountService createAvatarWithImage:image];
             [[NSNotificationCenter defaultCenter] postNotificationName:HFNotificationChangeAvatar object:nil];

         }];
        
        
        if ([[NSManagedObjectContext MR_defaultContext] hasChanges])
        {
//            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
        }
        
        
    }];
    
}



@end
