//
//  SplashScreenInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenInteractorInput.h"
#import "AgreementResponse.h"

@protocol SplashScreenInteractorOutput;

@interface SplashScreenInteractor : NSObject <SplashScreenInteractorInput>

@property (nonatomic, weak) id<SplashScreenInteractorOutput> output;

@property (nonatomic) AgreementResponse *agreement;
@end
