//
//  SplashScreenRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenRouter.h"
#import "MainTabBarAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SplashScreenRouter()

@property (strong, nonatomic) MainTabBarAssembly *mainTabBarAssembly;

@end


@implementation SplashScreenRouter

#pragma mark - Методы SplashScreenRouterInput

- (void)presentTabbar{
    _mainTabBarAssembly=[[MainTabBarAssembly new] activated];
    self.factory=[_mainTabBarAssembly factoryMainTabBar];
    
    [self presentModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
