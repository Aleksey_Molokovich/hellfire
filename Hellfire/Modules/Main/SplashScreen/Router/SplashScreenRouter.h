//
//  SplashScreenRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SplashScreenRouter : BaseRouter <SplashScreenRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
