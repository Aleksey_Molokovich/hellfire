//
//  SplashScreenPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenPresenter.h"

#import "SplashScreenViewInput.h"
#import "SplashScreenInteractorInput.h"
#import "SplashScreenRouterInput.h"

#import <AFNetworking/AFNetworking.h>
#import <LGAlertView/LGAlertView.h>

@implementation SplashScreenPresenter

#pragma mark - Методы SplashScreenModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SplashScreenViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [_interactor checkUserToken];
    
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        if (status == AFNetworkReachabilityStatusNotReachable) {
            [[LGAlertView alertViewWithTitle:@"Внимание"
                                     message:@"Отсутсвует соединение с интеренетом"
                                       style:LGAlertViewStyleAlert
                                buttonTitles:nil
                           cancelButtonTitle:@"Ok"
                      destructiveButtonTitle:nil] showAnimated];
        }
        
    }];
    
    [self needsUpdate];
    
}

-(BOOL) needsUpdate{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    if (!url) {
        return NO;
    }
    NSData* data = [NSData dataWithContentsOfURL:url];
    
    if (!data) {
        return NO;
    }
    
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if ([lookup[@"resultCount"] integerValue] == 1){
        NSString* appStoreVersion = lookup[@"results"][0][@"version"];
        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
        if (![appStoreVersion isEqualToString:currentVersion]){
            [[LGAlertView alertViewWithTitle:@"Внимание"
                                     message:[NSString stringWithFormat: @"Вышла новая версия %@", appStoreVersion]
                                       style:LGAlertViewStyleAlert
                                buttonTitles:nil
                           cancelButtonTitle:@"Ok"
                      destructiveButtonTitle:nil] showAnimated];
            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
            return YES;
        }
    }
    return NO;
}

#pragma mark - Методы SplashScreenInteractorOutput



- (void)didFinish{
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    [_router presentTabbar];
}
@end
