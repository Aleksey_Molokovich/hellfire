//
//  SplashScreenPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 19/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SplashScreenViewOutput.h"
#import "SplashScreenInteractorOutput.h"
#import "SplashScreenModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol SplashScreenViewInput;
@protocol SplashScreenInteractorInput;
@protocol SplashScreenRouterInput;

@interface SplashScreenPresenter : NSObject <SplashScreenModuleInput, SplashScreenViewOutput, SplashScreenInteractorOutput>

@property (nonatomic, weak) id<SplashScreenViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<SplashScreenInteractorInput> interactor;
@property (nonatomic, strong) id<SplashScreenRouterInput> router;

@end
