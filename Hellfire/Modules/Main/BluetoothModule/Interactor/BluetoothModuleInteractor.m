//
//  BluetoothModuleInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothModuleInteractor.h"
#import "BluetoothModuleInteractorOutput.h"
#import "BluetoothService.h"
#import "CBPeripheralModel.h"

@interface BluetoothModuleInteractor()<BluetoothServiceDelegate>
{
    BluetoothService *service;
}

@property (nonatomic, strong) NSMutableArray<CBPeripheralModel*> *peripherals;

@end

@implementation BluetoothModuleInteractor

#pragma mark - Методы BluetoothModuleInteractorInput

- (void)startBluetoothService{
    if (!_peripherals)
    {
        _peripherals = [NSMutableArray new];
    }
    service=[BluetoothService sharedInstance];
    service.delegate = self;
    if (![service isPoweredOn])
    {
        [_output showMessage:@"Необходимо включить Bluetooth в настройках"];
        return;
    }
    
    [service startScan];
    
    [NSTimer scheduledTimerWithTimeInterval:20
                                     target:self
                                   selector:@selector(scanTimeOut)
                                   userInfo:nil
                                    repeats:NO];
    
}

- (void)scanTimeOut
{
    [service stopScan];
    [_output didStopBluetoothService];
    
}

- (void)connectTimeOut
{
    
}

- (void)connectToPeripheral:(CBPeripheralModel *)model
{
    if (model.peripheral.state == CBPeripheralStateConnected)
    {
        [service disconnect];
    }
    else
    {
        [service stopScan];
        [service connectToPeripheral:model.peripheral];
    }
    
}

#pragma mark BluetoothSerialDelegate

- (void)serialDidDiscoverPeripheral:(CBPeripheral *)peripheral RSSI:(NSNumber *)RSSI
{
    
    // check whether it is a duplicate
    for( CBPeripheralModel *exisiting in _peripherals)
    {
        if (exisiting.peripheral.identifier == peripheral.identifier)
        {
            return;
        }
    }
    
    // add to the array, next sort & reload
    CBPeripheralModel *model = [CBPeripheralModel new];
    model.peripheral = peripheral;
    model.RSSI = RSSI?[RSSI floatValue]:0.0;
    
    [_peripherals addObject:model];
    [_output successfulWithData:[_peripherals copy]];
}

- (void)serialDidConnect:(CBPeripheral *)peripheral
{
    [_output successfulWithData:_peripherals];
}

- (void)serialDidFailToConnect:(CBPeripheral *)peripheral error:(NSError *)error
{
    [_output successfulWithData:_peripherals];
}

- (void)serialDidDisconnect:(CBPeripheral *)peripheral error:(NSError *)error
{
    [_output successfulWithData:_peripherals];
}

- (void)serialIsReady:(CBPeripheral *)peripheral
{
   //[_output showMessage:@"success"];
}

- (void)serialDidChangeState
{
    
}
@end
