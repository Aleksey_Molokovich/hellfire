//
//  BluetoothModuleInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BluetoothModuleInteractorOutput <NSObject>

- (void)successfulWithData:(NSArray*)data;
- (void)showMessage:(NSString*)text;
- (void)didStopBluetoothService;
@end
