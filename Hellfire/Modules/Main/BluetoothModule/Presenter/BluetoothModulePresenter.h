//
//  BluetoothModulePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothModuleViewOutput.h"
#import "BluetoothModuleInteractorOutput.h"
#import "BluetoothModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "BluetoothDataSource.h"

@protocol BaseViewControllerProtocol;
@protocol BluetoothModuleViewInput;
@protocol BluetoothModuleInteractorInput;
@protocol BluetoothModuleRouterInput;

@interface BluetoothModulePresenter : NSObject <BluetoothModuleModuleInput, BluetoothModuleViewOutput, BluetoothModuleInteractorOutput>

@property (nonatomic, weak) id<BluetoothModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<BluetoothModuleInteractorInput> interactor;
@property (nonatomic, strong) id<BluetoothModuleRouterInput> router;

@property (nonatomic, strong) BluetoothDataSource *data;

@end
