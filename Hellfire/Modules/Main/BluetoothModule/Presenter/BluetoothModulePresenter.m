//
//  BluetoothModulePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothModulePresenter.h"

#import "BluetoothModuleViewInput.h"
#import "BluetoothModuleInteractorInput.h"
#import "BluetoothModuleRouterInput.h"
#import "UIColor+HF.h"

@implementation BluetoothModulePresenter

#pragma mark - Методы BluetoothModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы BluetoothModuleViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [_view hideProgress];
    [_interactor startBluetoothService];
    [_view showProgressWithColor:[UIColor hfBlue]];
}

-(ObjectTableViewCell *)itemAtPath:(NSIndexPath *)indexPath{
    return [_data itemAtPath:indexPath];
}


- (void)connectToItemAtPath:(NSIndexPath *)indexPath
{
    [_interactor connectToPeripheral:[_data itemAtPath:indexPath].data];
}

-(NSArray *)getSections{
    return [_data getSections];
}

- (void)didTapUpdate{
    [_view showProgressWithColor:[UIColor hfBlue]];
    [_interactor startBluetoothService];
}

#pragma mark - Методы BluetoothModuleInteractorOutput

- (void)didStopBluetoothService
{
    [_view hideProgress];
}

- (void)successfulWithData:(NSArray *)data{
    _data.nearDevices=data;
    [_view hideProgress];
    [_view reloadData];
}

- (void)showMessage:(NSString *)text{
    [_view showAlertWithMessage:text];
}

@end
