//
//  BluetoothModuleAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 BluetoothModule module
 */
@interface BluetoothModuleAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryBluetoothModule;
@end
