//
//  BluetoothModuleAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothModuleAssembly.h"

#import "BluetoothModuleViewController.h"
#import "BluetoothModuleInteractor.h"
#import "BluetoothModulePresenter.h"
#import "BluetoothModuleRouter.h"
#import "BluetoothDataSource.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation BluetoothModuleAssembly

- (BluetoothModuleViewController *)viewBluetoothModule {
    return [TyphoonDefinition withClass:[BluetoothModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterBluetoothModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterBluetoothModule]];

                              
}];
}

-(BluetoothDataSource *)datasourceBluetoothDataSource{
    return [TyphoonDefinition withClass:[BluetoothDataSource class]];
}


- (BluetoothModuleInteractor *)interactorBluetoothModule {
    return [TyphoonDefinition withClass:[BluetoothModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterBluetoothModule]];
                          }];
}

- (BluetoothModulePresenter *)presenterBluetoothModule{
    return [TyphoonDefinition withClass:[BluetoothModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewBluetoothModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorBluetoothModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerBluetoothModule]];
                              [definition injectProperty:@selector(data)
                                                    with:[self datasourceBluetoothDataSource]];
                          }];
}

- (BluetoothModuleRouter *)routerBluetoothModule{
    return [TyphoonDefinition withClass:[BluetoothModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewBluetoothModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryBluetoothModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardBluetoothModule]];
                                                  [initializer injectParameterWith:@"BluetoothModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardBluetoothModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
