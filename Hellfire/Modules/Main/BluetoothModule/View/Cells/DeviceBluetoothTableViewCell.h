//
//  DeviceBluetoothTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 24.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DeviceBluetoothTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *labelConnected;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
