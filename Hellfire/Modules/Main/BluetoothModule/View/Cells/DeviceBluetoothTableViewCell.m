//
//  DeviceBluetoothTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 24.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "DeviceBluetoothTableViewCell.h"
#import "CBPeripheralModel.h"

@implementation DeviceBluetoothTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithData:(CBPeripheralModel*)data{
    _label.text=data.peripheral.name;
    
    if (data.peripheral.state == CBPeripheralStateConnected) {
        _labelConnected.text = @"Подключено";
        _image.image = [UIImage imageNamed:@"radiobox-check"];
    }
    else
    {
        _labelConnected.text = nil;
        _image.image = [UIImage imageNamed:@"radiobox-blank"];
    }
}
@end
