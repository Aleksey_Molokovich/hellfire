//
//  HeaderBluetoothTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface HeaderBluetoothTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
