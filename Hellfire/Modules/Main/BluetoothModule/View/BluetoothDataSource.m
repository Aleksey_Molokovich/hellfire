//
//  BluetoothDataSource.m
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothDataSource.h"
#import "HFPipeDevice.h"
#import "CBPeripheralModel.h"
#import "SectionsTableView.h"

@interface BluetoothDataSource(){
    NSMutableArray *nearDevicesList;
    NSMutableArray *savedDevicesList;
}

@end

@implementation BluetoothDataSource
- (void)prepareDataSource{
    nearDevicesList=[NSMutableArray new];
    
    for (CBPeripheralModel *item in _nearDevices) {
        ObjectTableViewCell *cell=[ObjectTableViewCell new];
        cell.cellId=CellIdDevice;
        cell.height=40;
        cell.data=item;
        
        [nearDevicesList addObject:cell];
    }
    
    savedDevicesList=[NSMutableArray new];
    
    for (CBPeripheralModel *item in _savedDevices) {
        ObjectTableViewCell *cell=[ObjectTableViewCell new];
        cell.cellId=CellIdDevice;
        cell.height=40;
        cell.data=item;
        
        [savedDevicesList addObject:cell];
    }
}

-(NSArray*)getSections{
    NSMutableArray *sections=[NSMutableArray new];
    if (nearDevicesList.count) {
        SectionsTableView *section=[SectionsTableView new];
        section.title=@"Доступные устройства";
        section.countItems=nearDevicesList.count;
        [sections addObject:section];
    }
    if (savedDevicesList.count) {
        SectionsTableView *section=[SectionsTableView new];
        section.title=@"Сохранненый устройства";
        section.countItems=savedDevicesList.count;
        [sections addObject:section];
    }
    return sections;
}

-(ObjectTableViewCell *)itemAtPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return nearDevicesList[indexPath.row];
    }else{
        return savedDevicesList[indexPath.row];
    }
}

- (void)setNearDevices:(NSArray *)nearDevices{
    _nearDevices=nearDevices;
    [self prepareDataSource];
}
@end
