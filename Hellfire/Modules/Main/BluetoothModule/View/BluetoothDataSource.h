//
//  BluetoothDataSource.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectTableViewCell.h"

static NSString *const CellIdDevice = @"DeviceBluetoothTableViewCell";

@interface BluetoothDataSource : NSObject

@property (nonatomic, assign, readonly) NSInteger count;
@property (strong, nonatomic) NSArray *nearDevices;
@property (strong, nonatomic) NSArray *savedDevices;

-(ObjectTableViewCell*) itemAtPath:(NSIndexPath*)indexPath ;
-(NSArray*)getSections;
@end
