//
//  BluetoothModuleCellType.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef BluetoothModuleCellType_h
#define BluetoothModuleCellType_h

typedef NS_ENUM(NSInteger, BluetoothModuleCellType){
    BluetoothModuleCellTypeHeader,
    BluetoothModuleCellTypeDevice
};

typedef NS_ENUM(NSInteger, BluetoothModuleCellActionType){
    BluetoothModuleCellActionSwitchOn,
    BluetoothModuleCellActionSwitchOff
};
#endif /* BluetoothModuleCellType_h */
