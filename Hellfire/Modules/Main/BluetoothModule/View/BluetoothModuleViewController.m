//
//  BluetoothModuleViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothModuleViewController.h"
#import "BaseTableViewCell.h"
#import "BluetoothModuleViewOutput.h"
#import "SectionsTableView.h"


@interface BluetoothModuleViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewActionUpdate;

@end

@implementation BluetoothModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (IBAction)goBack:(id)sender {
    [self hideProgress];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Методы BluetoothModuleViewInput

- (void)setupInitialState {
    [self prepareNavigationBarAndStatusBar];
    self.tabBarController.tabBar.hidden=YES;
    [_viewActionUpdate addGestureRecognizer:
     [[UITapGestureRecognizer alloc] initWithTarget:self
                                             action:@selector(tapUpdate)]];
    
}

- (void)tapUpdate{
    [_output didTapUpdate];
}

- (void)reloadData{
    [_tableView reloadData];
}

#pragma mark - Методы UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger count=[_output getSections].count;
    return count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_output getSections][section].countItems;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_output itemAtPath:indexPath].cellId forIndexPath:indexPath];
    [cell configureWithData:[_output itemAtPath:indexPath].data];
    cell.output = _output;
    cell.cellIndex=indexPath;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_output connectToItemAtPath:indexPath];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return [_output itemAtPath:indexPath].height;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    BaseTableViewCell *header=[tableView dequeueReusableCellWithIdentifier:@"HeaderBluetoothTableViewCell"];
    [header configureWithData:[_output getSections][section].title];
    return header;
}

@end
