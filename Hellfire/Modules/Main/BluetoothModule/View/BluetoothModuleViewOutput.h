//
//  BluetoothModuleViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
@class SectionsTableView;

@class ObjectTableViewCell;
@protocol BluetoothModuleViewOutput <CallBackActionProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (ObjectTableViewCell*) itemAtPath:(NSIndexPath*)indexPath;
- (NSArray<SectionsTableView*>*)getSections;
- (void)didTapUpdate;
- (void)connectToItemAtPath:(NSIndexPath*)indexPath;
@end
