//
//  BluetoothModuleViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BluetoothModuleViewInput.h"
#import "BaseViewController.h"
#import "BluetoothDataSource.h"
@protocol BluetoothModuleViewOutput;

@interface BluetoothModuleViewController : BaseViewController <BluetoothModuleViewInput>

@property (nonatomic, strong) id<BluetoothModuleViewOutput> output;

@end
