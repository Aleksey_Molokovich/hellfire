//
//  BluetoothModuleRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 23/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothModuleRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface BluetoothModuleRouter : BaseRouter <BluetoothModuleRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
