//
//  HotseatPlayersListInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol HotseatPlayersListInteractorInput <NSObject>
- (void) configureWithSelectedPlayer:(CDPlayer*)selectedPlayer;
- (void) friendsWithRequest;
- (void) didSelectRowAtIndexPath:(NSIndexPath*)index;
- (void) acceptInviteFromPlayer:(NSNumber*)playerId;
- (void) rejectInviteFromPlayer:(NSNumber*)playerId;
- (void) sendInvitionForSelectedUsers;
@end
