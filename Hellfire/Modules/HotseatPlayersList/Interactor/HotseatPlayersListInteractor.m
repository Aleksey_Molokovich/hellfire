//
//  HotseatPlayersListInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListInteractor.h"
#import "HotseatPlayersListInteractorOutput.h"
#import "UserRepository.h"
#import "GameRepository.h"
#import "HotseatGameService.h"
#import "GameRequest.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
#import "HFGameSettings.h"

@implementation HotseatPlayersListInteractor

#pragma mark - Методы HotseatPlayersListInteractorInput


- (void) configureWithSelectedPlayer:(CDPlayer*)selectedPlayer
{
    
    if (selectedPlayer) {
        __weak typeof(self) weakSelf = self;
        [[HotseatGameService sharedInstance] createHotseatGameWithSelectedPlayer:selectedPlayer completion:^(BOOL isCreated) {
            __strong typeof(weakSelf) blockSelf = weakSelf;
            [blockSelf friendsWithRequest];
        }];
    }else{
        
    }
    _hotseatPlayers = [CDHotseatPlayer MR_findAll];
    [self reCheckPlayers];
}

- (void)acceptInviteFromPlayer:(NSNumber *)playerId
{
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"id = %@",playerId];
    CDHotseatPlayer *player = [CDHotseatPlayer MR_findFirstWithPredicate:predicate];
    player.status = PlayerHotseatStatusAccept;
    [_output successfulWithData: [CDHotseatPlayer MR_findAllSortedBy:@"userName" ascending:YES]];
}

- (void)rejectInviteFromPlayer:(NSNumber *)playerId
{
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"id = %@",playerId];
    CDHotseatPlayer *player = [CDHotseatPlayer MR_findFirstWithPredicate:predicate];
    player.status = PlayerHotseatStatusReject;
    
    [self reCheckPlayers];
}

- (void)friendsWithRequest
{
    [_output successfulWithData: [CDHotseatPlayer MR_findAllSortedBy:@"userName" ascending:YES ]];
}


- (void)didSelectRowAtIndexPath:(NSIndexPath*)index
{

    if (index.row>=[[CDHotseatPlayer MR_findAll] count])
        return;
    
    __block CDHotseatPlayer *player = [CDHotseatPlayer MR_findAllSortedBy:@"userName" ascending:YES ][index.row];
    
    if (player.status == PlayerHotseatStatusAcceptLock)
        return;
    
    if (player.status == PlayerHotseatStatusNone) {
        player.status = PlayerHotseatStatusMarked;
        player.index = [self getLastIndex];
    }
    else
    {
        player.status = PlayerHotseatStatusNone;
    }
    
    [self reCheckPlayers];
    
}

- (void)reCheckPlayers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld OR status == %ld OR status == %ld", PlayerHotseatStatusAccept,PlayerHotseatStatusInvite,PlayerHotseatStatusAcceptLock,PlayerHotseatStatusMarked ];
    NSArray *hotseatPlayers = [CDHotseatPlayer MR_findAll];
    if ([[CDHotseatPlayer MR_findAllWithPredicate:predicate ] count] == 6) {
        for (CDHotseatPlayer *item in hotseatPlayers) {
            if (item.status == PlayerHotseatStatusNone) {
                item.status = PlayerHotseatStatusLock;
            }
        }
        
    }
    else
    {
        for (CDHotseatPlayer *item in hotseatPlayers) {
            if (item.status == PlayerHotseatStatusLock) {
                item.status = PlayerHotseatStatusNone;
            }
        }
    }
    
    [_output successfulWithData: [CDHotseatPlayer MR_findAllSortedBy:@"userName" ascending:YES ]];
}

- (void)sendInvitionForSelectedUsers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld OR status == %ld OR status == %ld", PlayerHotseatStatusAccept, PlayerHotseatStatusAcceptLock,PlayerHotseatStatusInvite,PlayerHotseatStatusMarked ];
    NSMutableArray *mHotseatPlayers = [NSMutableArray new];
    
    NSArray *hotseatPlayers = [CDHotseatPlayer MR_findAllWithPredicate:predicate];
    
    for (CDHotseatPlayer *item in hotseatPlayers) {
        [mHotseatPlayers addObject:@(item.id)];
        if (item.status == PlayerHotseatStatusMarked) {
            item.status = PlayerHotseatStatusInvite;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    [GameRepository invitePlayers:mHotseatPlayers game:@([HFGameSettings load].gameId) completion:^(BOOL value) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        
        [GameRepository checkPlayers:mHotseatPlayers game:@([HFGameSettings load].gameId) completion:^(NSArray *players) {
            
            if (players) {
                for (NSNumber *itemId in players) {
                    CDHotseatPlayer *player = [CDHotseatPlayer MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"id = %@",itemId]];
                    if (player.status != PlayerHotseatStatusAcceptLock) {
                        player.status = PlayerHotseatStatusAccept;
                    }
                    
                }
            }
            
            [blockSelf.output successfulWithData: [CDHotseatPlayer MR_findAllSortedBy:@"userName" ascending:YES]];
            
        }];
        
    }];
}

- (NSInteger)getLastIndex
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status != %ld", PlayerHotseatStatusNone];
    CDHotseatPlayer *item = [[CDHotseatPlayer MR_findAllSortedBy:@"index" ascending:YES withPredicate:predicate]  lastObject];
    
    return item.index+1;
}
@end
