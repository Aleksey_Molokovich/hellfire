//
//  HotseatPlayersListInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HotseatPlayersListInteractorOutput <NSObject>
- (void)successfulWithData:(NSArray *)data;
- (void)successfulWithMessage:(NSString*)message;

@end
