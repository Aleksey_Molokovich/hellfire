//
//  HotseatPlayersListInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListInteractorInput.h"

@protocol HotseatPlayersListInteractorOutput;

@interface HotseatPlayersListInteractor : NSObject <HotseatPlayersListInteractorInput>

@property (nonatomic, weak) id<HotseatPlayersListInteractorOutput> output;
@property (nonatomic) NSArray *hotseatPlayers;
@property (nonatomic) NSArray *friends;

@end
