//
//  HotseatPlayersListAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListAssembly.h"

#import "HotseatPlayersListViewController.h"
#import "HotseatPlayersListInteractor.h"
#import "HotseatPlayersListPresenter.h"
#import "HotseatPlayersListRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation HotseatPlayersListAssembly

- (HotseatPlayersListViewController *)viewHotseatPlayersList {
    return [TyphoonDefinition withClass:[HotseatPlayersListViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterHotseatPlayersList]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterHotseatPlayersList]];
                          }];
}

- (HotseatPlayersListInteractor *)interactorHotseatPlayersList {
    return [TyphoonDefinition withClass:[HotseatPlayersListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterHotseatPlayersList]];
                          }];
}

- (HotseatPlayersListPresenter *)presenterHotseatPlayersList{
    return [TyphoonDefinition withClass:[HotseatPlayersListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewHotseatPlayersList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorHotseatPlayersList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerHotseatPlayersList]];
                          }];
}

- (HotseatPlayersListRouter *)routerHotseatPlayersList{
    return [TyphoonDefinition withClass:[HotseatPlayersListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewHotseatPlayersList]];
                          }];
}

- (RamblerViperModuleFactory *)factoryHotseatPlayersList  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardHotseatPlayersList]];
                                                  [initializer injectParameterWith:@"HotseatPlayersListViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardHotseatPlayersList {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
