//
//  HotseatPlayersListViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotseatPlayersListViewInput.h"
#import "BaseViewController.h"

@protocol HotseatPlayersListViewOutput;

@interface HotseatPlayersListViewController : BaseViewController <HotseatPlayersListViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<HotseatPlayersListViewOutput> output;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *datasource;
@end
