//
//  HotseatPlayersListViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListViewController.h"
#import "HotseatPlayersListViewOutput.h"
#import "BaseTableViewCell.h"
#import "CellManager.h"
static NSString *const kHotseatPlayerTableViewCell = @"HotseatPlayerTableViewCell";

@implementation HotseatPlayersListViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    [CellManager registerCellsWithId:@[kHotseatPlayerTableViewCell] tableView:_tableView];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-v4020-Regular" size:24]}];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
	[self.output didTriggerViewReadyEvent];
}
- (IBAction)clickSendInvition:(id)sender {
    [_output didClickSendInvition];
}

#pragma mark - Методы HotseatPlayersListViewInput

- (void)setupInitialState {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGreenHotseat];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)updateWithData:(NSArray *)data
{
    _datasource = data;
    [_tableView reloadData];
}

- (IBAction)goBack:(id)sender {
    [_output goBack];
}


#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_datasource count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:kHotseatPlayerTableViewCell];
    cell.cellIndex = indexPath;
    cell.output = _output;
    [cell configureWithData:_datasource[indexPath.row]];
    return cell;
}


@end
