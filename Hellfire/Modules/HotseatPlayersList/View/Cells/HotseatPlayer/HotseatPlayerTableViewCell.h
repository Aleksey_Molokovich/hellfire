//
//  HotseatPlayerTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 31.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "HotseatProgressView.h"

@interface HotseatPlayerTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *points;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoximg;
@property (weak, nonatomic) IBOutlet HotseatProgressView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@end
