//
//  HotseatPlayerTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 31.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayerTableViewCell.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HotseatPlayerTableViewCellAction.h"

@implementation HotseatPlayerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configureWithData:(id)data
{
    if(![data isKindOfClass:[CDHotseatPlayer class] ])
    {
        return;
    }
    _checkBtn.enabled = YES;
    CDHotseatPlayer *person = (CDHotseatPlayer*)data;
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: person.avatarPath]
                   placeholderImage:[UIImage imageNamed:@"avatar"]
                            options:SDWebImageRetryFailed
                          completed:nil];
    
    _name.text = person.userName;
    _points.text = [NSString stringWithFormat:@"Solo %lld / %lld Hotseat", person.soloPoints,person.hotseatPoints];
    
    switch (person.status) {
        case PlayerHotseatStatusNone:
            _checkBoximg.image = [UIImage imageNamed:@"checkbox-blank"];
            _indicator.hidden = YES;
            break;
        case PlayerHotseatStatusMarked:
            _checkBoximg.image = [UIImage imageNamed:@"checkbox-marked"];
            _indicator.hidden = YES;
            break;
        case PlayerHotseatStatusInvite:
            _checkBoximg.image = [UIImage imageNamed:@"checkbox-marked"];
            _indicator.hidden = NO;
            [_indicator run];
            break;
        case PlayerHotseatStatusAccept:
            _checkBoximg.image = [UIImage imageNamed:@"checkbox-marked"];
            _indicator.hidden = NO;
            [_indicator stopWithSuccess];
            break;
        case PlayerHotseatStatusLock:
            _checkBoximg.image = nil;
            _indicator.hidden = YES;
            _checkBtn.enabled = NO;
            break;
        case PlayerHotseatStatusAcceptLock:
            _checkBoximg.image = nil;
            _indicator.hidden = NO;
            [_indicator stopWithSuccess];
            _checkBtn.enabled = NO;
            break;
        case PlayerHotseatStatusReject:
            _checkBoximg.image = nil;
            _indicator.hidden = NO;
            [_indicator stopWithFail];
            _checkBtn.enabled = NO;
            break;
            
        default:
            break;
    }
        
}

- (IBAction)clickPlayer:(id)sender {
    CallBackActionModel *object=[CallBackActionModel new];
    object.cellIndex = self.cellIndex;
    object.action = HotseatPlayerActionSelect;
    [self.output callBackObject:object];
    
}


@end
