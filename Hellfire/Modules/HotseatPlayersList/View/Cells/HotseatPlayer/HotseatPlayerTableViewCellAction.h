//
//  HotseatPlayerTableViewCellAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef HotseatPlayerTableViewCellAction_h
#define HotseatPlayerTableViewCellAction_h

static NSString *const HotseatPlayerActionInfo = @"HotseatPlayerActionInfo";
static NSString *const HotseatPlayerActionSelect = @"HotseatPlayerActionSelect";
#endif /* HotseatPlayerTableViewCellAction_h */
