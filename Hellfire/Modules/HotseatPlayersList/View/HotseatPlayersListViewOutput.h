//
//  HotseatPlayersListViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
@protocol HotseatPlayersListViewOutput <CallBackActionProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)goBack;
- (void)didClickSendInvition;
@end
