//
//  HotseatPlayersListPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListPresenter.h"

#import "HotseatPlayersListViewInput.h"
#import "HotseatPlayersListInteractorInput.h"
#import "HotseatPlayersListRouterInput.h"
#import "HotseatPlayerTableViewCellAction.h"
#import "HFNotification.h"

@implementation HotseatPlayersListPresenter

#pragma mark - Методы HotseatPlayersListModuleInput

- (void)configureModuleWithPlayer:(CDPlayer*)player {
    
    self.pushFromSettings = player?NO:YES;
    
    [_interactor configureWithSelectedPlayer:player];
}

#pragma mark - Методы HotseatPlayersListViewOutput

- (void)didTriggerViewReadyEvent {
    [_interactor friendsWithRequest];
	[self.view setupInitialState];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(acceptInviteFromPlayer:)
     name:HFPushNotificationInviteToHotseatAccept object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(rejectInviteFromPlayer:)
     name:HFPushNotificationInviteToHotseatReject object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)goBack
{
    if (self.pushFromSettings) {
         [_router closeCurrentModule];
    }else{
       [_router pushSettings];
    }
}

- (void)acceptInviteFromPlayer:(NSNotification*)notif
{
    [_interactor acceptInviteFromPlayer:((HFNotification*)notif.object).data.Id];
}

- (void)rejectInviteFromPlayer:(NSNotification*)notif
{
    [_interactor rejectInviteFromPlayer:((HFNotification*)notif.object).data.Id];
}

- (void)didClickSendInvition
{
    [_interactor sendInvitionForSelectedUsers];
    if (self.pushFromSettings) {
        [_router closeCurrentModule];
    }else{
        [_router pushSettings];
    }
}
#pragma mark - Методы HotseatPlayersListInteractorOutput

- (void)successfulWithData:(NSArray *)data
{
    [_view updateWithData:data];
}

- (void)successfulWithMessage:(NSString *)message
{
    [_view showAlertWithMessage:message];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    if ([object.action isEqualToString:HotseatPlayerActionSelect]) {
        [_interactor didSelectRowAtIndexPath:object.cellIndex];
    }
}

@end



