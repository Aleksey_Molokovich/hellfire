//
//  HotseatPlayersListPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListViewOutput.h"
#import "HotseatPlayersListInteractorOutput.h"
#import "HotseatPlayersListModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol HotseatPlayersListViewInput;
@protocol HotseatPlayersListInteractorInput;
@protocol HotseatPlayersListRouterInput;

@interface HotseatPlayersListPresenter : NSObject <HotseatPlayersListModuleInput, HotseatPlayersListViewOutput, HotseatPlayersListInteractorOutput>

@property (nonatomic, weak) id<HotseatPlayersListViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<HotseatPlayersListInteractorInput> interactor;
@property (nonatomic, strong) id<HotseatPlayersListRouterInput> router;

@property (nonatomic) BOOL pushFromSettings;
@end
