//
//  HotseatPlayersListRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 31/12/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayersListRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GameSettingsAssembly.h"
#import "GameSettingsModuleInput.h"

@interface HotseatPlayersListRouter()

@property (strong, nonatomic) GameSettingsAssembly *gameSettingsAssembly;

@end


@implementation HotseatPlayersListRouter

#pragma mark - Методы HotseatPlayersListRouterInput

- (void)pushSettings{
    self.gameSettingsAssembly=[[GameSettingsAssembly new] activated];
    self.factory=[ self.gameSettingsAssembly factoryGameSettings];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<GameSettingsModuleInput> moduleInput) {
                       [moduleInput configureModuleForSoloGame:NO];
                       return nil;
                   }];
}

@end
