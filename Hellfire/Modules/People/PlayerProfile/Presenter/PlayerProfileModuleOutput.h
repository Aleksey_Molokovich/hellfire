//
//  PlayerProfileModuleOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 06.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef PlayerProfileModuleOutput_h
#define PlayerProfileModuleOutput_h
#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
@protocol PlayerProfileModuleOutput <RamblerViperModuleOutput>

- (void)shoudUpdateFriendList;

@end
#endif /* PlayerProfileModuleOutput_h */
