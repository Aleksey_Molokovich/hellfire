//
//  PlayerProfilePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileViewOutput.h"
#import "PlayerProfileInteractorOutput.h"
#import "PlayerProfileModuleInput.h"
#import "PlayerProfileDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"
#import "PlayerProfileModuleOutput.h"

@protocol BaseViewControllerProtocol;
@protocol PlayerProfileViewInput;
@protocol PlayerProfileInteractorInput;
@protocol PlayerProfileRouterInput;
@protocol PlayerProfileDatasourceOutput;
@protocol PlayerProfileModuleOutput;

@interface PlayerProfilePresenter : NSObject <PlayerProfileModuleInput, PlayerProfileViewOutput, PlayerProfileInteractorOutput>

@property (nonatomic, weak) id<PlayerProfileViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<PlayerProfileInteractorInput> interactor;
@property (nonatomic, strong) id<PlayerProfileRouterInput> router;
@property (nonatomic, strong) id<PlayerProfileDatasourceOutput> datasource;
@property (nonatomic, assign) NSInteger selectedPlayerId;
@property (nonatomic, strong) id<PlayerProfileModuleOutput> moduleOutput;
@end
