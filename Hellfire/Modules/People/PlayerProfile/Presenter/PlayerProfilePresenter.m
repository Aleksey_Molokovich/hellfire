//
//  PlayerProfilePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfilePresenter.h"

#import "PlayerProfileViewInput.h"
#import "PlayerProfileInteractorInput.h"
#import "PlayerProfileRouterInput.h"
#import "HellfireDefines.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDClan+CoreDataClass.h"
#import "HFNotification.h"

#import "UIColor+HF.h"
#import "PopUpCellModel.h"
#import "AccountService.h"
#import "ClanBrifTableViewAction.h"
#import "ButtonTableViewAction.h"

@implementation PlayerProfilePresenter

#pragma mark - Методы PlayerProfileModuleInput

- (void)configureModuleWithPlayer:(CDPlayer *)player
{
    self.selectedPlayerId = player.id;
    [_interactor infoPlayer:player.id];
    _datasource.playerId = player.id;
    [_datasource prepareDatasource];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(update:)
     name:HFUpdatePlayersList object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)update:(NSNotification*)notification
{
    HFNotification *aps =notification.object;
    [_interactor infoPlayer:aps.data.player?aps.data.player.id:self.selectedPlayerId];
}

- (void)viewWillAppear
{
    [_interactor cachedInfoPlayer];
}

#pragma mark - Методы PlayerProfileViewOutput

- (void)didTriggerViewReadyEvent
{
    [self.view setupInitialWithState:[_interactor playerWithId:self.selectedPlayerId].status];
}

- (void)remove
{
    if ([_interactor playerWithId:self.selectedPlayerId].status == PlayerStatusTypeFriend)
    {
        [_interactor removeFriend:[_interactor playerWithId:self.selectedPlayerId]];
    }
    else if ([_interactor playerWithId:self.selectedPlayerId].status == PlayerStatusTypeRequest)
    {
        [_interactor removeFriendRequest:[_interactor playerWithId:self.selectedPlayerId]];
    }
    
}

- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index
{
    if (index == 0)
    {
        [_router presentRegistration];
    }
}
#pragma mark - Методы PlayerProfileInteractorOutput

- (void)successfulWithMessage:(NSString *)message
{
    [_view showAlertWithMessage:message];
}

- (void)goBack
{
    [[NSNotificationCenter defaultCenter] postNotificationName:HFUpdatePlayersList object:nil];
    [self.router closeCurrentModule];
}

- (void)clans:(NSArray *)clans
{
    _datasource.clans = clans;
    [_datasource prepareDatasource];
    [_view update];
}

- (void)updateInfo:(CDPlayer *)info
{
    if (info.id) {
        self.selectedPlayerId = info.id;
        _datasource.playerId = info.id;
        [_datasource prepareDatasource];
        [self.view setupInitialWithState:[_interactor playerWithId:self.selectedPlayerId].status];
    }
    
}

- (void)requestStart
{
    [_view showProgressWithColor:[UIColor hfBlue]];
}

- (void)requestFinished
{
    [_view hideProgress];
}
#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    if ([object.action isEqualToString:ButtonTableViewActionPress])
    {
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:SessionToken];
        if (!token)
        {
            [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            return;
        }
        
        switch ([_interactor playerWithId:self.selectedPlayerId].status) {
            case PlayerStatusTypeRequest:
                [_interactor acceptFriendRequestFromPlayer:[_interactor playerWithId:self.selectedPlayerId]];
                break;
            case PlayerStatusTypeOther:
                [_interactor sendFriendRequestToPlayer:[_interactor playerWithId:self.selectedPlayerId]];
                break;
            case PlayerStatusTypeFriend:
                [self.router presentHotseatPlayerListWithPlayer:[_interactor playerWithId:self.selectedPlayerId]];
                break;
            default:
                break;
        }
    }
    else if ([object.action isEqualToString:HFActionClanTableView])
    {
        CDClan *clan = _datasource.clans[object.cellIndex.row];
        _interactor.selectedItem = clan;
        if (clan.isMember)
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataClanRemove],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenClanInfo]]];
        }
        else
        {
            [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataClanRequestSend],
                                       [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenClanInfo]]];
        }
    }
   
}

- (void)openClanWithIndex:(NSInteger)index
{
    [_router pushClanProfileWithClan:_datasource.clans[index]];
}

#pragma mark - Методы PopUpProtocol

- (void)popUpAction:(PopUpCellDataAction)action
{
    CDClan *clan = [_interactor selectedItem];
    
    switch (action) {
        case PopUpCellDataOpenClanInfo:
            [_router pushClanProfileWithClan:clan];
            break;
        case PopUpCellDataClanRemove:
            [_interactor removeClan:clan];
            break;
        case PopUpCellDataClanRequestSend:
            if ([AccountService state] == AccountStateTypeNonRegister)
            {
                [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            }
            else
            {
                [_interactor sendRequestToClan:clan];
            }
            break;
            
        default:
            break;
    }
    [_view hidePopUp];
}

@end
