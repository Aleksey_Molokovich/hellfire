//
//  PlayerProfileDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol PlayerProfileDatasourceOutput <NSObject>
- (void)prepareDatasource;
@property (nonatomic, assign) NSInteger playerId;
@property (nonatomic, strong) NSArray *clans;

@end
