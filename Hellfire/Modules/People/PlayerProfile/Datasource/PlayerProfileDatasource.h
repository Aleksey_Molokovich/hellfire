//
//  PlayerProfileDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileDatasourceInput.h"

@protocol PlayerProfileDatasourceOutput;

@interface PlayerProfileDatasource : NSObject <PlayerProfileDatasourceInput, PlayerProfileDatasourceOutput>
@property (nonatomic, strong) NSArray *clans;

@end
