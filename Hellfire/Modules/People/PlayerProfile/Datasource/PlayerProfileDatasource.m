//
//  PlayerProfileDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//
#import "PlayerProfileDatasourceOutput.h"
#import "PlayerProfileDatasource.h"
#import "ObjectTableViewCell.h"
#import "CallBackActionProtocol.h"
#import "HFButtonData.h"
#import "UIColor+HF.h"
#import "CDPlayer+CoreDataClass.h"
#import "HellfireDefines.h"
#import "AccountService.h"

@interface PlayerProfileDatasource()

@property (nonatomic, strong) NSMutableArray<ObjectTableViewCell*> *info;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic) CDPlayer *player;

@end

@implementation PlayerProfileDatasource
@synthesize playerId;

#pragma mark - Методы PlayerProfileDatasourceInput
- (void)prepareDatasource
{
    _info=[NSMutableArray new];
    NSPredicate *predicateUser = [NSPredicate predicateWithFormat:@"self.id == %@", @(self.playerId)];
    self.player = [CDPlayer MR_findFirstWithPredicate:predicateUser];
    
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=@"PlayerAndClanProfileTableViewCell";
    item.height=392;
    
    item.data = self.player;
    [_info addObject:item];
    
    if([AccountService user] && playerId !=0 && ![AccountService itMyId:self.playerId])
    {
        item=[ObjectTableViewCell new];
        item.cellId=@"ButtonTableViewCell";
        item.height=72;
        HFButtonData *buttonData = [HFButtonData new];
        if (self.player.status == PlayerStatusTypeRequest)
        {
            buttonData.title = @"ПРИНЯТЬ ЗАПРОС";
        }
        else if (self.player.status == PlayerStatusTypeOther)
        {
            buttonData.title = @"ДОБАВИТЬ В ДРУЗЬЯ";
        }
        else
        {
            buttonData.title = @"ПРИГЛАСИТЬ В HOTSEAT";
        }
        
        buttonData.bgColor = [UIColor hfBlue];
        item.data=buttonData;
        [_info addObject:item];
        
    }
    
    
    
        
    
    if ([_clans count])
    {
        _data = @[_info, _clans];
    }
    else
    {
        _data = @[_info, @[], @[[ObjectTableViewCell new]]];
    }
    
}

-(NSString *)cellIdAtIndexPath:(NSIndexPath*)path
{
    if (path.section == 0)
    {
        return _info[path.row].cellId;
    }
    else if (path.section == 1)
    {
        return @"ClanBrifTableViewCell";
    }
    else
    {
        return @"NoUsersClansTableViewCell";
    }
    

}

-(NSInteger)countRowInSection:(NSInteger)section
{
    return [_data[section] count];
}

-(NSInteger)countSection
{
    return _data.count;
}

-(id)dataAtIndexPath:(NSIndexPath*)index

{
    if (index.section == 0)
    {
        return _info[index.row].data;
    }
    else if (index.section == 2)
    {
        return nil;
    }
    else
    {
        return _clans[index.row];
    }
}
-(NSArray*)getCellsId{
    NSMutableArray *cellIds = [NSMutableArray new];
    
    for (ObjectTableViewCell *item in _info) {
        [cellIds addObject:item.cellId];
    }
    [cellIds addObject:@"ClanBrifTableViewCell"];
    [cellIds addObject:@"NoUsersClansTableViewCell"];
    return cellIds;
}


@end
