//
//  PlayerProfileViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileViewController.h"
#import "BaseTableViewCell.h"
#import "PlayerProfileViewOutput.h"
#import "CellManager.h"
#import "ThinGrayHeader.h"
#import "BaseViewController+PopUp.h"

@implementation PlayerProfileViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    _tableView.estimatedRowHeight = 400;
    _tableView.rowHeight = UITableViewAutomaticDimension;
	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.output viewWillAppear];
    self.navigationController.navigationBar.hidden=NO;
    [self hideBottomBar];

}


- (IBAction)goBack:(id)sender {
    [self.output goBack];
}

//- (void)goBack
//{
//    if ([[self.navigationController viewControllers] count] == 1)
//    {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
//    else
//    {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}

- (void)remove
{
    [_output remove];
}

#pragma mark - Методы PlayerProfileViewInput

- (void)setupInitialWithState:(PlayerStatusType)status
{
    [self prepareNavigationBarWithHFBGColorType:HFBackgroundColorClear];

    if (status == PlayerStatusTypeOther)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        UIBarButtonItem *bi = [[UIBarButtonItem alloc]
                               initWithImage:[UIImage imageNamed:@"recycle_bin"] style:UIBarButtonItemStyleDone target:self action:@selector(remove)];
        self.navigationItem.rightBarButtonItem = bi;
    }
    [self hideBottomBar];
    [CellManager registerCellsWithId:[_datasource getCellsId]
                           tableView:_tableView];
}

- (void)update
{
    [_tableView reloadData];
}

- (void)showPopUpWithData:(NSArray*)data
{
    [self configurePopUpWithData:data output:_output];
}

- (void)hidePopUp
{
    [super hidePopUp];
}
#pragma mark - Методы TableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return _datasource.countSection;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_datasource countRowInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource cellIdAtIndexPath:indexPath] forIndexPath:indexPath];
    [cell configureWithData:[_datasource dataAtIndexPath:indexPath]];
    cell.output = _output;
    cell.cellIndex = indexPath;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        ThinGrayHeader *header = [ThinGrayHeader new];
        header.label.text = [NSString stringWithFormat:@"Кланы (%ld)", (long)[_datasource countRowInSection:section]];
        
        return header;
    }
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        return 24;
    }

    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        [_output openClanWithIndex:indexPath.row];
    }
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output alertWithTag:alertView.tag clickedButtonAtIndex:index];
}
@end
