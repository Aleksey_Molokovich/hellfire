//
//  PlayerProfileViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerProfileViewInput.h"
#import "BaseViewController.h"
#import "PlayerProfileDatasourceInput.h"

@protocol PlayerProfileViewOutput;
@protocol PlayerProfileDatasourceInput;

@interface PlayerProfileViewController : BaseViewController <PlayerProfileViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<PlayerProfileViewOutput> output;
@property (nonatomic, strong) id<PlayerProfileDatasourceInput> datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
