//
//  PlayerProfileViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
#import "PopUpTableViewProtocol.h"

@protocol PlayerProfileViewOutput <CallBackActionProtocol, PopUpTableViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)openClanWithIndex:(NSInteger)index;
- (void)remove;
- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index;
- (void)goBack;
- (void)viewWillAppear;
@end
