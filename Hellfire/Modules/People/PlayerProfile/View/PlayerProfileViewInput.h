//
//  PlayerProfileViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HellfireDefines.h"

@protocol PlayerProfileViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialWithState:(PlayerStatusType)status;
- (void)update;
- (void)goBack;
- (void)showPopUpWithData:(NSArray*)data;
- (void)hidePopUp;
@end
