//
//  PlayerProfileInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileInteractorInput.h"

@protocol PlayerProfileInteractorOutput;

@interface PlayerProfileInteractor : NSObject <PlayerProfileInteractorInput>

@property (nonatomic, weak) id<PlayerProfileInteractorOutput> output;
@property (assign, nonatomic) NSInteger playerId;
@property (nonatomic) NSMutableSet *clansIdSet;
@property (nonatomic) BOOL isLoadingProfile;
@end
