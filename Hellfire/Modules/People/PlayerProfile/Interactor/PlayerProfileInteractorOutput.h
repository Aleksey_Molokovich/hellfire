//
//  PlayerProfileInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol PlayerProfileInteractorOutput <NSObject>
- (void)successful;
- (void)successfulWithMessage:(NSString*)message;
- (void)failWithError:(NSString*)error;
- (void)clans:(NSArray*)clans;
- (void)updateInfo:(CDPlayer*)info;
- (void)requestStart;
- (void)requestFinished;
- (void)goBack;
@end
