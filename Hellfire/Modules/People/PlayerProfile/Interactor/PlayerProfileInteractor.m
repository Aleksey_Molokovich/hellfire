//
//  PlayerProfileInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileInteractor.h"
#import "PlayerProfileInteractorOutput.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDClan+CoreDataClass.h"
#import "UserRepository.h"
#import "PlayersRequest.h"
#import "ClanRepository.h"
#import "ClansRequest.h"

@implementation PlayerProfileInteractor
@synthesize selectedItem;

#pragma mark - Методы PlayerProfileInteractorInput
- (void)removeFriend:(CDPlayer*)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository removeFriend:player
                   completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;

         if (response)
         {
             [blockSelf infoPlayer:blockSelf.playerId];
         }
     }];
}
- (void)removeFriendRequest:(CDPlayer*)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository rejectFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (response)
         {
             [blockSelf infoPlayer:blockSelf.playerId];
         }
     }];
}
- (void)sendFriendRequestToPlayer:(CDPlayer*)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository addFriend:player
                   completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;

         if (response)
         {
             [blockSelf infoPlayer:blockSelf.playerId];
         }
     }];
}

- (void)acceptFriendRequestFromPlayer:(CDPlayer *)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository acceptFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;

         if (response) {
             [blockSelf infoPlayer:blockSelf.playerId];
         }
     }];
}

- (void)cachedInfoPlayer{
    
    if (self.isLoadingProfile) {
        return;
    }

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id in %@", self.clansIdSet];
     NSPredicate *predicateUser = [NSPredicate predicateWithFormat:@"self.id == %@", @(self.playerId)];
    [self.output clans:[CDClan MR_findAllWithPredicate:predicate]];
    [self.output updateInfo:[CDPlayer MR_findFirstWithPredicate:predicateUser]];
}

- (CDPlayer*)playerWithId:(NSInteger)playerId
{
    NSPredicate *predicateUser = [NSPredicate predicateWithFormat:@"self.id == %@", @(playerId)];
    return [CDPlayer MR_findFirstWithPredicate:predicateUser];
}

- (void)infoPlayer:(NSInteger)playerId
{
    if (self.isLoadingProfile || playerId == 0) {
        return;
    }
    self.playerId = playerId;
    
    self.isLoadingProfile = YES;
    
    __weak typeof(self) weakSelf = self;
    [_output requestStart];
    [UserRepository info:playerId
              completion:^(CDPlayer *user, NSArray *clans)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         blockSelf.isLoadingProfile = NO;
         blockSelf.clansIdSet = [NSMutableSet new];
         [blockSelf.clansIdSet addObjectsFromArray:[clans valueForKey: @"id"]];
         [blockSelf cachedInfoPlayer];
//         [blockSelf.output clans:clans];
//         [blockSelf.output updateInfo:user];
         [_output requestFinished];
         
     }];
}


- (void) sendRequestToClan:(CDClan*)clan
{
    ClansRequest *request = [ClansRequest new];
    request.clanId = @(clan.id);
    __weak typeof(self) weakSelf = self;
    [ClanRepository joinToClan:request completion:^(BOOL isSend)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (isSend) {
             [blockSelf infoPlayer:blockSelf.playerId];
         }else{
             [blockSelf.output successfulWithMessage:@"Что-то пошло не так"];
         }
     }];
}

- (void) removeClan:(CDClan *)clan
{
    ClansRequest *request = [ClansRequest new];
    request.clanId = @(clan.id);
    __weak typeof(self) weakSelf = self;
    [ClanRepository leaveClan:request completion:^(BOOL isSend)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (isSend)
         {
             [blockSelf infoPlayer:blockSelf.playerId];
         }else{
             [blockSelf.output successfulWithMessage:@"Что-то пошло не так"];
         }
     }];
}
@end
