//
//  PlayerProfileInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@class CDClan;
@protocol PlayerProfileInteractorInput <NSObject>
@property (strong, nonatomic) CDClan *selectedItem;

- (void)sendFriendRequestToPlayer:(CDPlayer*)player;
- (void)acceptFriendRequestFromPlayer:(CDPlayer*)player;
- (void)removeFriend:(CDPlayer*)player;
- (void)removeFriendRequest:(CDPlayer*)player;

- (void)infoPlayer:(NSInteger)playerId;
- (void)cachedInfoPlayer;

- (void) sendRequestToClan:(CDClan*)clan;
- (void) removeClan:(CDClan*)clan;
- (CDPlayer*)playerWithId:(NSInteger)playerId;
@end
