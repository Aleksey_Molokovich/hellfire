//
//  PlayerProfileAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileAssembly.h"

#import "PlayerProfileViewController.h"
#import "PlayerProfileInteractor.h"
#import "PlayerProfilePresenter.h"
#import "PlayerProfileRouter.h"
#import "PlayerProfileDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation PlayerProfileAssembly

- (PlayerProfileViewController *)viewPlayerProfile {
    return [TyphoonDefinition withClass:[PlayerProfileViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPlayerProfile]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterPlayerProfile]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourcePlayerProfile]];
                          }];
}

- (PlayerProfileInteractor *)interactorPlayerProfile {
    return [TyphoonDefinition withClass:[PlayerProfileInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPlayerProfile]];
                          }];
}

- (PlayerProfilePresenter *)presenterPlayerProfile{
    return [TyphoonDefinition withClass:[PlayerProfilePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewPlayerProfile]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorPlayerProfile]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerPlayerProfile]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourcePlayerProfile]];
                          }];
}

- (PlayerProfileDatasource *)datasourcePlayerProfile{
    return [TyphoonDefinition withClass:[PlayerProfileDatasource class]];
}

- (PlayerProfileRouter *)routerPlayerProfile{
    return [TyphoonDefinition withClass:[PlayerProfileRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewPlayerProfile]];
                          }];
}

- (RamblerViperModuleFactory *)factoryPlayerProfile  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardPlayerProfile]];
                                                  [initializer injectParameterWith:@"PlayerProfileViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardPlayerProfile {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"People"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
