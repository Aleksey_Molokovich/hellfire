//
//  PlayerProfileRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerProfileRouter.h"
#import "ClanProfileAssembly.h"
#import "ClanProfileModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface PlayerProfileRouter()

@property (strong, nonatomic) ClanProfileAssembly *clanProfileAssembly;

@end


@implementation PlayerProfileRouter

#pragma mark - Методы PlayerProfileRouterInput
- (void)pushClanProfileWithClan:(CDClan*)clan
{
    _clanProfileAssembly=[[ClanProfileAssembly new] activated];
    self.factory=[_clanProfileAssembly factoryClanProfile];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<ClanProfileModuleInput> moduleInput)
     {
         [moduleInput configureModuleWithClan:clan];
         return nil;
     }];
}
@end
