//
//  PlayersInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol PlayersInteractorInput <NSObject>

@property (nonatomic, assign, readonly) NSInteger friendsCount;
@property (nonatomic, assign, readonly) NSInteger playersCount;
@property (nonatomic, assign, readonly) NSInteger requestCount;

- (void)configure;
- (void)clearCache;

- (void)prepareData;

- (void)updateListIfNeedForIndexCell:(NSIndexPath*)cellIndex;
- (void)updateDataWithFirendsAndRequests:(BOOL)updateAll;
- (void)friendsWithRequest;

- (void)changeSection:(NSInteger)section;
- (void)changeSearchQuery:(NSString*)query;
- (CDPlayer*)itemWithIndex:(NSInteger)index;
- (CDPlayer *)selectedItem;

- (void)sendFriendRequestToPlayer:(CDPlayer*)player;
- (void)acceptFriendRequestFromPlayer:(CDPlayer*)player;
- (void)rejectFriendRequestFromPlayer:(CDPlayer*)player;
- (void)removeFriend:(CDPlayer*)player;
- (void)infoPlayer:(CDPlayer*)player;

@property (nonatomic, assign, readonly) NSInteger amountSection;

@end
