//
//  PlayersInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PlayersInteractorOutput <NSObject>
- (void)successfulWithData:(NSArray *)data;
- (void)updateFriendRequestWithData:(NSArray*)data;
- (void)updateFriendWithData:(NSArray *)data;
- (void)showProgress;
- (void)hideProgress;
- (void)successfulWithMessage:(NSString*)message;
- (void)failWithError:(NSString*)error;
@end
