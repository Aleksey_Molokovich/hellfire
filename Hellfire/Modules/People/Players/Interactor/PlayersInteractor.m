//
//  PlayersInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersInteractor.h"
#import "UserRepository.h"
#import "PlayersInteractorOutput.h"
#import "CDPlayer+CoreDataClass.h"
#import "HellfireDefines.h"
#import "CDAccount+CoreDataClass.h"
#import "SubscribeService.h"
#import "AccountService.h"
#import "CDFilterSetting+CoreDataClass.h"
#import "CDSortOfFilter+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
#import "CoreDataService.h"

@implementation PlayersInteractor

#pragma mark - Методы PlayersInteractorInput



- (void)configure
{
    _currentPage = 0;
    self.friendsIdSet = [NSMutableSet new];
    self.playersIdSet = [NSMutableSet new];
    self.requestsIdSet = [NSMutableSet new];
    
  _request=[PlayersRequest new];
    
    _request.includeMyFriends = @"false";
    _request.isSortAsc = @"true";
    if ([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)
    {
        _amountSection = 1;
        [self updateDataWithFirendsAndRequests:NO];
    }
    else
    {
        _amountSection = 3;
        [self updateDataWithFirendsAndRequests:YES];
    }
    
}

-(NSArray *)allPlayers
{
    return [self players];
}

-(NSInteger)amountSectionю
{
    return _amountSection;
}

- (void)prepareRequest
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.filterForPlayers = %@ and self.saved = 1", @(YES)];
    
    CDFilterSetting *filter = [CDFilterSetting MR_findAllWithPredicate:predicate].firstObject;
    _request.includeMyFriends = filter.includeMy?@"true":@"false";
    _request.sortType = nil;
    _request.countryId = (filter.country.id ==0 )?nil:@(filter.country.id);
    _request.cityId = (filter.city.id ==0 )?nil:@(filter.city.id);
    _request.isSortAsc = filter.bySoloPoints.ascending?@"true":@"false";
    
    if (filter.bySoloPoints.selected)
    {
        _request.sortType = @"solo";
        _request.isSortAsc = filter.bySoloPoints.ascending?@"true":@"false";
    }
    
    if (filter.byHotseatPoints.selected)
    {
        _request.sortType = @"hotseat";
        _request.isSortAsc = filter.byHotseatPoints.ascending?@"true":@"false";
    }
    
    if (filter.byName.selected)
    {
        _request.sortType = @"name";
        _request.isSortAsc = filter.byName.ascending?@"true":@"false";
    }
    
    _request.page = @(_currentPage);
}

- (void)updateDataWithFirendsAndRequests:(BOOL)updateAll
{
    [_output showProgress];
    [self prepareRequest];
    if ([_request.page intValue]<1) {
        self.playersIdSet = [NSMutableSet new];
    }
    __weak typeof(self) weakSelf = self;
    [UserRepository playersWithParam:_request
                          completion:^(NSArray *results,NSInteger count)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
//         blockSelf.allPlayers = [blockSelf players];
//         [blockSelf.playersIdSet addObjectsFromArray:[results valueForKey: @"id"]];

         blockSelf.playersCount = count;
         if (updateAll)
         {
             [blockSelf friendsWithRequest];
         }
         else
         {
             [blockSelf.output hideProgress];
             [blockSelf prepareData];
         }
     }];
}


#pragma mark FriendRequest

- (void)friendsWithRequest
{
    [_output showProgress];
    _request.sortType = @"name";
    _request.isSortAsc = @"true";
    
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        [self prepareData];
        return;
    }
    
     [self prepareRequest];
    __weak typeof(self) weakSelf = self;
    
    _request.sortType = @"name";
    _request.isSortAsc = @"true";
    
    if ([_request.page intValue]<1) {
        self.friendsIdSet = [NSMutableSet new];
        self.requestsIdSet = [NSMutableSet new];
    }
    
    [UserRepository friendsWithParam:_request
                          completion:^(NSArray *results)
     {
         
         __strong typeof(weakSelf) blockSelf = weakSelf;
         [blockSelf.output hideProgress];
         blockSelf.friends = [[CDPlayer MR_findAllSortedBy:@"userName"
                                                 ascending:YES
                                             withPredicate:[NSPredicate predicateWithFormat:@"status == 1 "]
                                                 inContext:[CoreDataService sharedInstance].friends] copy];
         
         blockSelf.friendsCount = [blockSelf.friends count];
         
         blockSelf.requests = [CDPlayer MR_findAllSortedBy:@"userName"
                                                 ascending:YES
                                             withPredicate:[NSPredicate predicateWithFormat:@"status == 0 "]
                                                 inContext:[CoreDataService sharedInstance].friends];;
         
         blockSelf.requestCount = [blockSelf.requests count];
         
         [blockSelf prepareData];
     }];
}



- (void)prepareData
{
    [self prepareRequest];
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        [_output successfulWithData:@[(_currentSection == 2)?self.allPlayers:@[]]];
    }
    else
    {
        [_output successfulWithData:@[(_currentSection == 0)?_requests:@[],
                                      (_currentSection == 1)?_friends:@[],
                                      (_currentSection == 2)?self.allPlayers:@[]]];
    }
}

- (void)changeSearchQuery:(NSString *)query
{
    _request.query = query;
    _currentPage = 1;

    if ([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)
    {
        [self updateDataWithFirendsAndRequests:NO];
    }
    else
    {
        [self updateDataWithFirendsAndRequests:YES];
    }
    
}

- (void)changeSection:(NSInteger)section
{
    _currentSection = section;
    if (section == 0 && [AccountService state] == AccountStateTypeNonRegister)
    {
        _currentSection = 2;
    }
}

- (void)updateListIfNeedForIndexCell:(NSIndexPath*)cellIndex
{
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        if ((cellIndex.row+1) >= [self.allPlayers count]) {
            [self updateListForIndexCell:cellIndex.row];
        }
    }
    else
    {
        if (cellIndex.section == 2 && (cellIndex.row+1) >= [self.allPlayers count]) {
            [self updateListForIndexCell:cellIndex.row];
        }
    }
    
}

- (void) updateListForIndexCell:(NSInteger)cellIndex
{
    NSInteger page = cellIndex / 24.0 + 1;
    if (_currentPage<page) {
        _currentPage = page;
        [self updateDataWithFirendsAndRequests:NO];
    }
}

-(CDPlayer *)itemWithIndex:(NSInteger)index
{
    _selectedPlayer = index;
    return [self getData][index];
}

- (NSArray*)getData
{
    NSArray *data;
    switch (_currentSection) {
        case 0:
            data = self.requests;
            break;
        case 1:
            data = self.friends;
            break;
        case 2:
            data = self.allPlayers;
            break;
        default:
            break;
    }
    
    if (_request.query.length)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userName contains[cd] %@",_request.query];
        data = [[data copy] filteredArrayUsingPredicate:predicate];
    }
    return data;
}

-(CDPlayer *)selectedItem
{
    return [self itemWithIndex:_selectedPlayer];
}

- (NSArray*)players
{
    
    NSPredicate *predicate;

    if (([AccountService state] == AccountStateTypeSuccessfulRegister) ||
        ( [AccountService state] == AccountStateTypeNonRegister))
    {
        predicate = [NSPredicate predicateWithFormat:@"status == 0 || status == 1 || status == 2 || status == 3 "];
        
        if ([_request.includeMyFriends isEqualToString:@"false"])
        {
           predicate = [NSPredicate predicateWithFormat:@"status == 0 || status == 2"];
        }
        
    }
    
    NSString *sortField = @"userName";
    if ([_request.sortType isEqualToString:@"solo"])
    {
        sortField = @"soloPoints";
    }
    else if ([_request.sortType isEqualToString:@"hotseat"])
    {
        sortField = @"hotseatPoints";
    }
    
    NSArray *data = [CDPlayer MR_findAllSortedBy:sortField ascending:[_request.isSortAsc isEqualToString:@"true"]?YES:NO withPredicate:predicate];
    
    if (_request.query.length)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userName contains[cd] %@",_request.query];
        data = [[data copy] filteredArrayUsingPredicate:predicate];
    }
    
    return data;
}

- (void)clearCache
{
    [CDPlayer MR_truncateAll];
    _currentPage = 1;
}

- (void)removeFriend:(CDPlayer*)player
{
    NSInteger playerId = player.id;
    __weak typeof(self) weakSelf = self;
    [UserRepository removeFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;

         if (response)
         {
             [blockSelf friendsWithRequest];
             [UserRepository info:playerId
                       completion:^(CDPlayer *user, NSArray *clans) {
                           
                       }];
         }
     }];
}

- (void)sendFriendRequestToPlayer:(CDPlayer*)player
{
    __weak typeof(self) weakSelf = self;
    
    [UserRepository addFriend:player
                   completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (response)
         {
             [blockSelf friendsWithRequest];
         }
     }];
}

- (void)acceptFriendRequestFromPlayer:(CDPlayer *)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository acceptFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;

         if (response)
         {
             [blockSelf friendsWithRequest];
         }
     }];
}

- (void)rejectFriendRequestFromPlayer:(CDPlayer *)player
{
    __weak typeof(self) weakSelf = self;
    [UserRepository rejectFriend:player
                      completion:^(BOOL response)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (response)
         {
             [blockSelf friendsWithRequest];
         }
     }];
}


@end
