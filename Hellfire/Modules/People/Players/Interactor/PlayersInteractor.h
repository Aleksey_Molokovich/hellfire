//
//  PlayersInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersInteractorInput.h"
#import "PlayersRequest.h"

@protocol PlayersInteractorOutput;

@interface PlayersInteractor : NSObject <PlayersInteractorInput>

@property (nonatomic, weak) id<PlayersInteractorOutput> output;

@property (nonatomic, assign) NSInteger friendsCount;
@property (nonatomic, assign) NSInteger playersCount;
@property (nonatomic, assign) NSInteger requestCount;

@property (nonatomic, assign) NSInteger selectedPlayer;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger currentSection;
@property (nonatomic, assign) NSInteger amountSection;
@property (nonatomic, strong) PlayersRequest *request;
@property (nonatomic, strong) NSArray *allPlayers;
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, strong) NSArray *requests;

@property (nonatomic) NSMutableSet *playersIdSet;
@property (nonatomic) NSMutableSet *friendsIdSet;
@property (nonatomic) NSMutableSet *requestsIdSet;
@end
