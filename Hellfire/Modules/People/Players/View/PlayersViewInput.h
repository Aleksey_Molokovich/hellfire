//
//  PlayersViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerProtocol.h"

@protocol PlayersViewInput <BaseViewControllerProtocol>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)updateWithData:(NSArray*)data;
- (void)showActions:(NSArray*)actions;
- (void)updateHeaderSection:(NSInteger)index;

- (void)showPopUpWithData:(NSArray*)data;
- (void)hidePopUp;
@end
