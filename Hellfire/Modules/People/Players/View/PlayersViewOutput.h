//
//  PlayersViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayersHeaderViewCellProtocol.h"
#import "SearchViewProtocol.h"
#import "CallBackActionProtocol.h"
#import "PopUpTableViewProtocol.h"
@class CDPlayer;
@protocol PlayersHeaderViewCellProtocol;
@protocol PlayersViewOutput <PlayersHeaderViewCellProtocol, SearchViewProtocol, CallBackActionProtocol, PopUpTableViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didTrigerViewWillAppear;
- (void)willDisplayCell:(NSIndexPath*)index;
- (NSInteger)numberOfSections;
- (NSInteger)indexOfSectionsShouldShow;
- (void)didSelectPlayer:(NSInteger)playerIndex;
- (NSString*)titleForSection:(NSInteger)section;
- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index;
@end
