//
//  PlayersViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersViewController.h"
#import "CellManager.h"
#import "PlayersViewOutput.h"
#import "BaseTableViewCell.h"
#import "PlayersHeaderViewCell.h"
#import "BaseViewController+PopUp.h"

static NSString *const cellId=@"PlayerTableViewCell";
static NSString *const headerId=@"PlayersHeaderViewCell";

@implementation PlayersViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.output didTriggerViewReadyEvent];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    [self.output didTrigerViewWillAppear];
    [self showBottomBar];
}

#pragma mark - Методы PlayersViewInput

- (void)setupInitialState {
    
    [self prepareStatusBarWithHFBGColorType:HFBackgroundColorBlue];

    [self.searchView placeholder: @"Искать людей..."];
    self.searchView.delegate = _output;
    [CellManager registerCellsWithId:@[cellId]
                             headers:@[headerId]
                           tableView:_tableView];
    
}

- (void)showActions:(NSArray *)actions
{
    self.alert = [[LGAlertView alloc] initWithTitle:nil
                                message:nil
                                  style:LGAlertViewStyleAlert
                           buttonTitles:actions
                      cancelButtonTitle:nil
                 destructiveButtonTitle:nil
                               delegate:self];
    self.alert.width = 150;
    [self.alert showAnimated];
}

- (void)updateHeaderSection:(NSInteger)index
{
    PlayersHeaderViewCell *header = [self.tableView headerViewForSection:index];
    header.lblTitle.text = [_output titleForSection:index];
}

- (void)updateWithData:(NSArray *)data
{
    _datasource = data;
    [_tableView reloadData];
}

- (void)showPopUpWithData:(NSArray*)data
{
    [self configurePopUpWithData:data output:_output];
}

- (void)hidePopUp
{
    [super hidePopUp];
}
#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_datasource) {
        return _datasource[section].count;
    }
    
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_output numberOfSections];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.output = _output;
    cell.cellIndex = indexPath;
    [cell configureWithData:_datasource[indexPath.section][indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [_output didSelectPlayer:indexPath.row];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    PlayersHeaderViewCell *view = [_tableView dequeueReusableHeaderFooterViewWithIdentifier:headerId];
    view.section = section;
    view.delegate = _output;
    view.lblTitle.text = [_output titleForSection:section];
    
    view.hiddenFilter = NO;
    if ([_output numberOfSections] > 1 && section != 2) {
        view.hiddenFilter = YES;
    }
    
    if (section == [_output indexOfSectionsShouldShow])
    {
        [view expandList:YES];
    }
    else
    {
        [view expandList:NO];
    }
    
    [view setAutoresizingMask:UIViewAutoresizingNone];
    return view;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  56;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_output willDisplayCell:indexPath];
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output alertWithTag:alertView.tag clickedButtonAtIndex:index];
}


@end
