//
//  PlayersViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayersViewInput.h"
#import "BaseViewController.h"
#import "SearchViewPlayersAndClans.h"
#import "PopUpTableView.h"

@protocol PlayersViewOutput;

@interface PlayersViewController : BaseViewController <PlayersViewInput, UITableViewDelegate, UITableViewDataSource, LGAlertViewDelegate>
@property (weak, nonatomic) IBOutlet SearchViewPlayersAndClans *searchView;

@property (nonatomic, strong) id<PlayersViewOutput> output;
@property (nonatomic, strong) NSArray<NSArray*> *datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
