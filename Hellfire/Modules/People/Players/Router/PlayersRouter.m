//
//  PlayersRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersRouter.h"
#import "PlayerProfileAssembly.h"
#import "PlayerProfileModuleInput.h"
#import "PlayerProfileModuleOutput.h"
#import "FilterForPlayersAndClansAssembly.h"
#import "FilterForPlayersAndClansModuleInput.h"
#import "FilterForPlayersAndClansModuleOutput.h"
#import "UserModuleAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "BaseNavigationController.h"
#import "BluetoothModuleAssembly.h"

@interface PlayersRouter()

@property (strong, nonatomic) FilterForPlayersAndClansAssembly *filterForPlayersAndClansAssembly;
@property (strong, nonatomic) BluetoothModuleAssembly *bluetoothModuleAssembly;

@property (strong, nonatomic) PlayerProfileAssembly *playerProfileAssembly;

@property (strong, nonatomic) UserModuleAssembly *userModuleAssembly;

@end


@implementation PlayersRouter

#pragma mark - Методы PlayersRouterInput
- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule
{
    _playerProfileAssembly=[[PlayerProfileAssembly new] activated];
    self.factory=[_playerProfileAssembly factoryPlayerProfile];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<PlayerProfileModuleOutput>(id<PlayerProfileModuleInput> moduleInput) {
                       [moduleInput configureModuleWithPlayer:player];
                       return callbackModule;
                   }];
}

- (void)pushFilterWithOutput:(id<FilterForPlayersAndClansModuleOutput>)callbackModule
{
    _filterForPlayersAndClansAssembly = [[FilterForPlayersAndClansAssembly new] activated];
    self.factory=[_filterForPlayersAndClansAssembly factoryFilterForPlayersAndClans];
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<FilterForPlayersAndClansModuleOutput>(id<FilterForPlayersAndClansModuleInput> moduleInput) {
                       [moduleInput configureModuleForPlayers:YES];
                       return callbackModule;
                   }];
}

- (void)pushUserInfo:(id<RamblerViperModuleOutput>)callbackModule
{
    self.userModuleAssembly = [[UserModuleAssembly new] activated];
    self.factory=[self.userModuleAssembly factoryUserModule];
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<FilterForPlayersAndClansModuleInput> moduleInput) {
                       return callbackModule;
                   }];
}

- (void)pushBluetoothModule{
    _bluetoothModuleAssembly=[[BluetoothModuleAssembly new] activated];
    self.factory=[_bluetoothModuleAssembly factoryBluetoothModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
