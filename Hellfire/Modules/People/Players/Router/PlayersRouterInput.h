//
//  PlayersRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol RamblerViperModuleOutput;
@protocol PlayerProfileModuleOutput;
@protocol FilterForPlayersAndClansModuleOutput;

@protocol PlayersRouterInput <NSObject>

- (void)pushPlayerProfileWithPlayer:(CDPlayer*)player callbackModule:(id<PlayerProfileModuleOutput>)callbackModule;

- (void)presentRegistration;
- (void)pushFilterWithOutput:(id<FilterForPlayersAndClansModuleOutput>)output;

- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player;
- (void)pushUserInfo:(id<RamblerViperModuleOutput>)callbackModule;

- (void)pushBluetoothModule;
@end
