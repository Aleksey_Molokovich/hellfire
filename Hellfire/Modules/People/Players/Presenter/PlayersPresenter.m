//
//  PlayersPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersPresenter.h"

#import "PlayersViewInput.h"
#import "PlayersInteractorInput.h"
#import "PlayersRouterInput.h"
#import "CDAccount+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "AccountService.h"
#import "HellfireDefines.h"
#import "UIColor+HF.h"
#import "PopUpCellModel.h"
#import "BluetoothService.h"

@implementation PlayersPresenter

#pragma mark - Методы PlayersModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы PlayersViewOutput

- (void)didTriggerViewReadyEvent {
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showData)
     name:HFNotificationChangeAccountStatus object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateFriendAndRequest)
     name:HFPushNotification object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateFriendAndRequest)
     name:HFUpdatePlayersList object:nil];

	[self.view setupInitialState];
    _sectionExpand = -1;
    [self showData];
}
- (void)didTrigerViewWillAppear
{
    if (_sectionExpand == 1)
    {
        [_interactor friendsWithRequest];
    }
    
    [_interactor prepareData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)showData{
    [_view showProgressWithColor:[UIColor hfBlue]];
    [_interactor changeSection:_sectionExpand];
    [_interactor configure];
}

- (void)updateFriendAndRequest
{
    [_interactor updateDataWithFirendsAndRequests:YES];
}

- (NSInteger)numberOfSections
{
    return [_interactor amountSection];
}

-(NSInteger)indexOfSectionsShouldShow
{
    return _sectionExpand;
}

- (void)willDisplayCell:(NSIndexPath*)index
{
    [_interactor updateListIfNeedForIndexCell:index];
}
#pragma mark - Методы PlayersInteractorOutput


- (void)successfulWithData:(NSArray *)data
{
    
    for (int sec=0; sec<[self numberOfSections]; sec++)
    {
        [_view updateHeaderSection:sec];
    }
    
    [_view hideProgress];
    [_view updateWithData:data];
}
- (void)failWithError:(NSString *)error {
    
}

- (void)successfulWithMessage:(NSString *)message
{
    [_view showAlertWithMessage:message];
}

-(NSString *)titleForSection:(NSInteger)section
{
    NSString *sectionName;
    if ([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)
    {
        sectionName = [NSString stringWithFormat: @"Сообщество (%ld)", (long)[_interactor playersCount]];
    }
    else
    {
        if (section == 0)
        {
            sectionName = [NSString stringWithFormat: @"Запросы (%ld)", (long)[_interactor requestCount]];
        }
        else if (section == 1)
        {
            sectionName = [NSString stringWithFormat: @"Друзья (%ld)", (long)[_interactor friendsCount]];
        }
        else{
            
            sectionName = [NSString stringWithFormat: @"Сообщество (%ld)", (long)[_interactor playersCount]];
        }
    }
    NSLog(@"%@", sectionName);
    return sectionName;
    
}
- (void)didSelectPlayer:(NSInteger)playerIndex
{

    CDPlayer *player = [self.interactor itemWithIndex:playerIndex];
    if ([AccountService itMyId:player.id]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HFShowUserProfile object:nil];
    }else{
        [_router pushPlayerProfileWithPlayer:player callbackModule:self];
    }
}

- (void)showProgress
{
    [_view showProgressWithColor:[UIColor hfBlue]];
}

- (void)hideProgress
{
    [_view hideProgress];
}

- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index
{
    if (index == 0) {
        [_router presentRegistration];
    }
}
#pragma mark - Методы PlayersHeaderViewCellProtocol



- (void)openFilterAtIndex:(NSInteger)index
{
//    [_interactor clearCache];
    [_router pushFilterWithOutput:self];
}

- (void)pressExpandAtIndex:(NSInteger)index
{
    if (_sectionExpand == index && _sectionExpand != -1)
    {
        _sectionExpand = -1;
        [self successfulWithData:@[@[],@[],@[]]];
    }
    else
    {
       _sectionExpand = index;
        [_interactor updateDataWithFirendsAndRequests:YES];
    }
     [_interactor changeSection:_sectionExpand];
    
}

#pragma mark - Методы SearchViewProtocol

- (void)didChangeSearchString:(NSString *)string
{
    [_interactor changeSearchQuery:string];
}

#pragma mark - Методы PlayerProfleProtocol


- (void)shoudUpdateFriendList
{
    [_interactor updateDataWithFirendsAndRequests:YES];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    CDPlayer *player=[_interactor itemWithIndex:object.cellIndex.row];
    
    if ([AccountService state] == AccountStateTypeNonRegister)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestSend],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
        return;
    }

    if (player.status == PlayerStatusTypeFriend)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataInviteFriendRequestToHotseat],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRemove],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
    }
    else if (player.status == PlayerStatusTypeRequest)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestAccept],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestReject],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
    }
    else if (player.status == PlayerStatusTypeOther)
    {
        [_view showPopUpWithData:@[[[PopUpCellModel alloc] initWithAction:PopUpCellDataFriendRequestSend],
                                   [[PopUpCellModel alloc] initWithAction:PopUpCellDataOpenPlayerInfo]]];
    }
}

#pragma mark - Методы PopUpProtocol

- (void)popUpAction:(PopUpCellDataAction)action
{
    CDPlayer *player = [_interactor selectedItem];
    
    switch (action) {
        case PopUpCellDataOpenPlayerInfo:
            [_router pushPlayerProfileWithPlayer:player callbackModule:self];
            break;
        case PopUpCellDataFriendRemove:
            [_interactor removeFriend:player];
            break;
        case PopUpCellDataFriendRequestSend:
            if ([AccountService state] == AccountStateTypeNonRegister)
            {
                [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
            }
            else
            {
                [_interactor sendFriendRequestToPlayer:player];
            }
            break;
        case PopUpCellDataFriendRequestReject:
            [_interactor rejectFriendRequestFromPlayer:player];
            break;
        case PopUpCellDataFriendRequestAccept:
            [_interactor acceptFriendRequestFromPlayer:player];
            break;
        case PopUpCellDataInviteFriendRequestToHotseat:
            if ([[BluetoothService sharedInstance] isReady]) {
                [self.router presentHotseatPlayerListWithPlayer:player];
            }else{
                [[BluetoothService sharedInstance] showAlertBLENotReadyActionHandler:^{
                    [self.router pushBluetoothModule];
                }];
            }
            
            break;
        default:
            break;
    }
    [_view hidePopUp];
}


#pragma mark - Методы FilterForPlayersAndClansModuleOutput

- (void)filterSettingsDidChange
{
    [_interactor clearCache];
    [_interactor updateDataWithFirendsAndRequests:YES];
}




@end

