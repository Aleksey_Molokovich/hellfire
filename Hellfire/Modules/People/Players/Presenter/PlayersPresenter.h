//
//  PlayersPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersViewOutput.h"
#import "PlayersInteractorOutput.h"
#import "PlayersModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "PlayerProfileModuleOutput.h"
#import "FilterForPlayersAndClansModuleOutput.h"

@protocol BaseViewControllerProtocol;
@protocol PlayersViewInput;
@protocol PlayersInteractorInput;
@protocol PlayersRouterInput;
@protocol PlayerProfileModuleOutput;
@protocol FilterForPlayersAndClansModuleOutput;

@interface PlayersPresenter : NSObject <PlayersModuleInput, PlayersViewOutput, PlayersInteractorOutput, PlayerProfileModuleOutput,FilterForPlayersAndClansModuleOutput >

@property (nonatomic, weak) id<PlayersViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<PlayersInteractorInput> interactor;
@property (nonatomic, strong) id<PlayersRouterInput> router;

@property (nonatomic, assign) NSInteger sectionExpand;
@end
