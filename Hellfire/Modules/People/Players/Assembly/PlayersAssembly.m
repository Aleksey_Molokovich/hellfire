//
//  PlayersAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 25/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersAssembly.h"

#import "PlayersViewController.h"
#import "PlayersInteractor.h"
#import "PlayersPresenter.h"
#import "PlayersRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation PlayersAssembly

- (PlayersViewController *)viewPlayers {
    return [TyphoonDefinition withClass:[PlayersViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPlayers]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterPlayers]];
                          }];
}

- (PlayersInteractor *)interactorPlayers {
    return [TyphoonDefinition withClass:[PlayersInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPlayers]];
                          }];
}

- (PlayersPresenter *)presenterPlayers{
    return [TyphoonDefinition withClass:[PlayersPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewPlayers]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorPlayers]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerPlayers]];
                          }];
}

- (PlayersRouter *)routerPlayers{
    return [TyphoonDefinition withClass:[PlayersRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewPlayers]];
                          }];
}

- (RamblerViperModuleFactory *)factoryPlayers  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardPlayers]];
                                                  [initializer injectParameterWith:@"PlayersViewController"];
                                              }];
                          }];
}

- (RamblerViperModuleFactory *)factoryPlayersNC  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardPlayers]];
                                                  [initializer injectParameterWith:@"PlayersNavigationController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardPlayers {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
