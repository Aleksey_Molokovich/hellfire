//
//  FriendRequestPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "FriendRequestPresenter.h"

#import "FriendRequestViewInput.h"
#import "FriendRequestInteractorInput.h"
#import "FriendRequestRouterInput.h"

@implementation FriendRequestPresenter

#pragma mark - Методы FriendRequestModuleInput

- (void)configureModuleWithRequestFromPlayer:(CDPlayer *)player andMessage:(NSString*)message
{
    [_interactor savePlayer:player];
    _message = message;
}

#pragma mark - Методы FriendRequestViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialStateWithPlayerName:_message];
}

- (void)didClickReject
{
    [_interactor reject];
    [_router closeCurrentModule];
}

- (void)didClickAccept
{
    [_interactor accept];
    [_router closeCurrentModule];
}
#pragma mark - Методы FriendRequestInteractorOutput

@end
