//
//  FriendRequestModuleInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
@class CDPlayer;
@protocol FriendRequestModuleInput <RamblerViperModuleInput>

/**
 @author AlekseyMolokovich

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithRequestFromPlayer:(CDPlayer*)player andMessage:(NSString*)message;

@end
