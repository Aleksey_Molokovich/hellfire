//
//  FriendRequestPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "FriendRequestViewOutput.h"
#import "FriendRequestInteractorOutput.h"
#import "FriendRequestModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol FriendRequestViewInput;
@protocol FriendRequestInteractorInput;
@protocol FriendRequestRouterInput;

@interface FriendRequestPresenter : NSObject <FriendRequestModuleInput, FriendRequestViewOutput, FriendRequestInteractorOutput>

@property (nonatomic, weak) id<FriendRequestViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<FriendRequestInteractorInput> interactor;
@property (nonatomic, strong) id<FriendRequestRouterInput> router;
@property (nonatomic) NSString *message;
@end
