//
//  FriendRequestInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "FriendRequestInteractor.h"
#import "FriendRequestInteractorOutput.h"
#import "UserRepository.h"

@implementation FriendRequestInteractor

#pragma mark - Методы FriendRequestInteractorInput
- (void)savePlayer:(CDPlayer *)player
{
    _player = player;
}


- (void)accept
{
    [UserRepository acceptFriend:_player
                      completion:^(BOOL response)
     {
         [[NSNotificationCenter defaultCenter] postNotificationName:HFUpdatePlayersList object:nil];
     }];
}

- (void)reject
{
    [UserRepository rejectFriend:_player
                      completion:^(BOOL response)
     {
[[NSNotificationCenter defaultCenter] postNotificationName:HFUpdatePlayersList object:nil];
     }];
}



@end
