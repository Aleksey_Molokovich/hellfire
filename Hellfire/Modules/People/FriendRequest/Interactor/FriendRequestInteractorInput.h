//
//  FriendRequestInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol FriendRequestInteractorInput <NSObject>
- (void)savePlayer:(CDPlayer*)player;
- (void)accept;
- (void)reject;
@end
