//
//  FriendRequestInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "FriendRequestInteractorInput.h"
#import "CDPlayer+CoreDataClass.h"

@protocol FriendRequestInteractorOutput;

@interface FriendRequestInteractor : NSObject <FriendRequestInteractorInput>

@property (nonatomic, weak) id<FriendRequestInteractorOutput> output;
@property (nonatomic) CDPlayer *player;
@end
