//
//  FriendRequestAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "FriendRequestAssembly.h"

#import "FriendRequestViewController.h"
#import "FriendRequestInteractor.h"
#import "FriendRequestPresenter.h"
#import "FriendRequestRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FriendRequestAssembly

- (FriendRequestViewController *)viewFriendRequest {
    return [TyphoonDefinition withClass:[FriendRequestViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFriendRequest]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFriendRequest]];
                          }];
}

- (FriendRequestInteractor *)interactorFriendRequest {
    return [TyphoonDefinition withClass:[FriendRequestInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFriendRequest]];
                          }];
}

- (FriendRequestPresenter *)presenterFriendRequest{
    return [TyphoonDefinition withClass:[FriendRequestPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFriendRequest]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFriendRequest]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFriendRequest]];
                          }];
}

- (FriendRequestRouter *)routerFriendRequest{
    return [TyphoonDefinition withClass:[FriendRequestRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFriendRequest]];
                          }];
}

- (RamblerViperModuleFactory *)factoryFriendRequest  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardFriendRequest]];
                                                  [initializer injectParameterWith:@"FriendRequestViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardFriendRequest {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"People"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
