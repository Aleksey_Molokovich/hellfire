//
//  FriendRequestRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FriendRequestRouterInput <NSObject>
- (void)closeCurrentModule;
@end
