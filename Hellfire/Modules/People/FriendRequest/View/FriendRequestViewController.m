//
//  FriendRequestViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "FriendRequestViewController.h"
#import "FriendRequestViewOutput.h"
#import "UIColor+HF.h"

@implementation FriendRequestViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы FriendRequestViewInput

- (void)setupInitialStateWithPlayerName:(NSString*)text {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];
    
    _lblText.text = text;
    
    [_btnReject configureTitle:@"ОТКЛОНИТЬ"
                       bgColor:[UIColor hfDeepPink]];
    [_btnAccept configureTitle:@"ПРИНЯТЬ ЗАПРОС"
                     bgColor:[UIColor hfBlue]];
}

- (IBAction)clickAccept:(id)sender {
    [_output didClickAccept];
}

- (IBAction)clickReject:(id)sender {
    [_output didClickReject];
}


@end
