//
//  FriendRequestViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 29/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendRequestViewInput.h"
#import "BaseViewController.h"
#import "RoundedButton.h"

@protocol FriendRequestViewOutput;

@interface FriendRequestViewController : BaseViewController <FriendRequestViewInput>

@property (nonatomic, strong) id<FriendRequestViewOutput> output;


@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet RoundedButton *btnReject;
@property (weak, nonatomic) IBOutlet RoundedButton *btnAccept;
@end
