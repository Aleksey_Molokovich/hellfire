//
//  GameSettingsAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 GameSettings module
 */
@interface GameSettingsAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryGameSettings;
@end
