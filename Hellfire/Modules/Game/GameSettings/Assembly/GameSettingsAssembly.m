//
//  GameSettingsAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsAssembly.h"

#import "GameSettingsViewController.h"
#import "GameSettingsInteractor.h"
#import "GameSettingsPresenter.h"
#import "GameSettingsRouter.h"
#import "GameSettingsDataSource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation GameSettingsAssembly

- (GameSettingsViewController *)viewGameSettings {
    return [TyphoonDefinition withClass:[GameSettingsViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameSettings]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterGameSettings]];
                              [definition injectProperty:@selector(data)
                                                    with:[self datasourceGameSettingsDataSource]];
                          }];
}


-(GameSettingsDataSource *)datasourceGameSettingsDataSource{
    return [TyphoonDefinition withClass:[GameSettingsDataSource class]];
}

- (GameSettingsInteractor *)interactorGameSettings {
    return [TyphoonDefinition withClass:[GameSettingsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameSettings]];
                          }];
}

- (GameSettingsPresenter *)presenterGameSettings{
    return [TyphoonDefinition withClass:[GameSettingsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewGameSettings]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorGameSettings]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerGameSettings]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceGameSettingsDataSource]];
                          }];
}

- (GameSettingsRouter *)routerGameSettings{
    return [TyphoonDefinition withClass:[GameSettingsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewGameSettings]];
                          }];
}

- (RamblerViperModuleFactory *)factoryGameSettings  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardGameSettings]];
                                                  [initializer injectParameterWith:@"GameSettingsViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardGameSettings {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
