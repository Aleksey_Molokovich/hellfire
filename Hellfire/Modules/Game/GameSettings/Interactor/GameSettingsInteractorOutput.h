//
//  GameSettingsInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameSettingsInteractorOutput <NSObject>
- (void)didChangeData;
- (void)failWithError:(NSString*)error;
- (void)successfulCreateHotseatGame;
- (void)successfulWithMessage:(NSString*)message;
- (void)updatePlayersList;
- (void)showProgress;
- (void)hideProgress;
- (void)startGame;
- (void)showAlertBeforeStartGame;
- (BOOL)isSoloConfig;
@end
