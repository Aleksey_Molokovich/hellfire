//
//  GameSettingsInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsInteractorInput.h"

@protocol GameSettingsInteractorOutput;

@interface GameSettingsInteractor : NSObject <GameSettingsInteractorInput>

@property (nonatomic, weak) id<GameSettingsInteractorOutput> output;

@property (nonatomic) NSNumber *hotseatGameId;
@end
