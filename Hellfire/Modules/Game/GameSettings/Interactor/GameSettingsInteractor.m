//
//  GameSettingsInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsInteractor.h"
#import "GameSettingsInteractorOutput.h"
#import "HFGameSettings.h"
#import "CDAccount+CoreDataClass.h"
#import "HFGameSettings.h"
#import "GameRequest.h"
#import "GameRepository.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "UserRepository.h"
#import "HellfireDefines.h"
#import "CDGameResults+CoreDataClass.h"
#import "GameService.h"
#import "HotseatGameService.h"


@interface GameSettingsInteractor(){
    NSArray<NSNumber*> *roundLength;
    NSArray<NSNumber*> *breakLength;
    NSInteger curentRound;
    NSInteger curentBreak;
    NSInteger maxRounds;
}

@property (nonatomic) GameService *gameService;

@end


@implementation GameSettingsInteractor

#pragma mark - Методы GameSettingsInteractorInput

- (void)configure
{
//    [CDHotseatPlayer MR_truncateAll];
    _gameService = [GameService new];
}

- (void)setupSettings{
    
    
    curentRound=0;
    curentBreak=0;
    roundLength=@[@(30),@(60)];
    HFGameSettings *soloSettings = [HFGameSettings load];
    if ([self.output isSoloConfig]) {
        breakLength=@[@(60),@(90),@(120),@(150),@(180)];
        maxRounds = 5;
    }else{
        breakLength=@[@(30),@(60)];
        maxRounds = 3;
    }
    
    if (!soloSettings)
        soloSettings = [HFGameSettings load];

#ifdef DEBUG
    soloSettings.currentRound=1;
    soloSettings.countRounds = 1;
    soloSettings.durationRound = 3;
    soloSettings.durationBreak = 1;
    soloSettings.durationPreparation = 1;
    soloSettings.durationUserPreparation = 1;
#else
    soloSettings.currentRound=1;
    soloSettings.countRounds = 3;
    soloSettings.durationRound = [roundLength[0] intValue];
    soloSettings.durationBreak = [breakLength[0] intValue];
    soloSettings.durationPreparation = 30;
    soloSettings.durationUserPreparation = 10;
#endif
    
    [soloSettings save];

    
}


-(HFGameSettings*)getGameSettings{
    return [HFGameSettings load];
}

- (void)changeCountRound:(NSInteger)value{
    HFGameSettings *soloSettings = [HFGameSettings load];
    soloSettings.countRounds += value;
    if (soloSettings.countRounds<1)
        soloSettings.countRounds=1;
    else if (soloSettings.countRounds>maxRounds)
        soloSettings.countRounds=maxRounds;
    [soloSettings save];
}

- (void)changeDurationRound:(NSInteger)value{
    
    curentRound+=value;
    if (curentRound<0) curentRound=0;
    if (curentRound>=roundLength.count) curentRound=roundLength.count-1;
    
    HFGameSettings *soloSettings = [HFGameSettings load];
    soloSettings.durationRound = [roundLength[curentRound] intValue];
    
    [soloSettings save];
}

- (void)changeDurationBreak:(NSInteger)value{
    
    curentBreak+=value;
    if (curentBreak<0) curentBreak=0;
    if (curentBreak>=breakLength.count) curentBreak=breakLength.count-1;
    
    HFGameSettings *soloSettings = [HFGameSettings load];
    soloSettings.durationBreak = [breakLength[curentBreak] intValue];
    
    [soloSettings save];
}

- (void)outFromGame
{
    [HFGameSettings load].gameId = 0;
    [CDHotseatPlayer MR_truncateAll];
    [CDGameResults MR_truncateAll];
    [HotseatGameService sharedInstance].status = HotseatGameServiceStatusReadyToCreateGame;
}
- (void)checkForcreateHotseatGame
{
    __weak typeof(self) weakSelf = self;
    [[HotseatGameService sharedInstance] createHotseatGameWithSelectedPlayer:nil
                                               completion:^(BOOL isCreated) {
                                                   __strong typeof(weakSelf) blockSelf = weakSelf;
                                                 [blockSelf.output successfulCreateHotseatGame];
                                               }];
    

}


- (void)sendSettingsForGame
{
    [GameRepository setSettingsForGame:@([HFGameSettings load].gameId)
                            roundCount:@([HFGameSettings load].countRounds)
                              duration:@([HFGameSettings load].durationRound)
                            completion:^(BOOL result) {
                                
                            }];
}

- (void)acceptInviteFromPlayer:(NSNumber *)playerId
{
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"id = %@",playerId];
    CDHotseatPlayer *player = [CDHotseatPlayer MR_findFirstWithPredicate:predicate];
    player.status = PlayerHotseatStatusAccept;
    [_output updatePlayersList];
}

- (void)rejectInviteFromPlayer:(NSNumber *)playerId
{
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"id = %@",playerId];
    CDHotseatPlayer *player = [CDHotseatPlayer MR_findFirstWithPredicate:predicate];
    player.status = PlayerHotseatStatusReject;
    [_output updatePlayersList];
}

- (BOOL)checkForHotseatGame
{
    return [[HotseatGameService sharedInstance] checkForHotseatGame];
}

- (void)checkPlayersToStartPlayHotseat
{
    [_output showProgress];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld OR status == %ld OR status == %ld", PlayerHotseatStatusAccept, PlayerHotseatStatusAcceptLock,PlayerHotseatStatusInvite,PlayerHotseatStatusMarked ];
    NSMutableArray *mHotseatPlayers = [NSMutableArray new];
    
    NSArray *hotseatPlayers = [CDHotseatPlayer MR_findAllWithPredicate:predicate];
    
    for (CDHotseatPlayer *item in hotseatPlayers) {
        [mHotseatPlayers addObject:@(item.id)];
        if (item.status == PlayerHotseatStatusMarked) {
            item.status = PlayerHotseatStatusInvite;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    
    [GameRepository checkPlayers:mHotseatPlayers game:@([HFGameSettings load].gameId) completion:^(NSArray *players) {
    __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf.output hideProgress];
        if (players) {
            for (NSNumber *itemId in players)
            {
                CDHotseatPlayer *player = [CDHotseatPlayer MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"id = %@",itemId]];
                if (player.status != PlayerHotseatStatusAcceptLock) {
                    player.status = PlayerHotseatStatusAccept;
                }
                
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld", PlayerHotseatStatusInvite,PlayerHotseatStatusMarked ];
            
            NSArray *hotseatPlayers = [CDHotseatPlayer MR_findAllWithPredicate:predicate];
            
            if ([hotseatPlayers count]>0) {
                [blockSelf.output showAlertBeforeStartGame];
            }
            else{
                [blockSelf sendSettingsForGame];
                [blockSelf.output startGame];
            }
        }
        else
        {
            [blockSelf.output failWithError:@"Добавьте игроков"];
        }
    }];
}

- (void)sendInvitionForSelectedUsers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld OR status == %ld OR status == %ld", PlayerHotseatStatusAccept, PlayerHotseatStatusAcceptLock,PlayerHotseatStatusInvite,PlayerHotseatStatusMarked ];
    NSMutableArray *mHotseatPlayers = [NSMutableArray new];
    
    NSArray *hotseatPlayers = [CDHotseatPlayer MR_findAllWithPredicate:predicate];
    
    for (CDHotseatPlayer *item in hotseatPlayers) {
        [mHotseatPlayers addObject:@(item.id)];
        if (item.status == PlayerHotseatStatusMarked) {
            item.status = PlayerHotseatStatusInvite;
        }
    }
    
    [GameRepository invitePlayers:mHotseatPlayers game:@([HFGameSettings load].gameId) completion:nil];
}
@end
