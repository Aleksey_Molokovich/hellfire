//
//  GameSettingsInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HFGameSettings;

@protocol GameSettingsInteractorInput <NSObject>
- (void)setupSettings;
- (void)configure;
- (void)changeCountRound:(NSInteger)value;
- (void)changeDurationBreak:(NSInteger)value;
- (void)changeDurationRound:(NSInteger)value;

- (HFGameSettings*)getGameSettings;


- (void)checkForcreateHotseatGame;
- (void) acceptInviteFromPlayer:(NSNumber*)playerId;
- (void) rejectInviteFromPlayer:(NSNumber*)playerId;

- (void)sendSettingsForGame;
- (void)checkPlayersToStartPlayHotseat;
- (BOOL)checkForHotseatGame;
- (void)sendInvitionForSelectedUsers;
- (void)outFromGame;
@end
