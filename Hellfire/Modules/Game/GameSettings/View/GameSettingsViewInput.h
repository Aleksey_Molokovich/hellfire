//
//  GameSettingsViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerGameInput.h"

@class HFGameSettings;
@protocol GameSettingsViewInput <BaseViewControllerGameInput>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateForSolo;
- (void)setupInitialStateForHotseat;
- (void)updateView;
- (void)updateCellAtIndex:(NSInteger)index;
- (void)showAlertBeforeStartGame;
@end
