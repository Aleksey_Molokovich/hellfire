//
//  GameSettingsViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameSettingsViewInput.h"
#import "BaseViewController.h"
#import "GameSettingsDataSource.h"
#import "GameSettingsDataSourceInput.h"

@protocol GameSettingsViewOutput;

@interface GameSettingsViewController : BaseViewController <GameSettingsViewInput>
@property (nonatomic, strong) id<GameSettingsDataSourceInput> data;
@property (nonatomic, strong) id<GameSettingsViewOutput> output;
@end
