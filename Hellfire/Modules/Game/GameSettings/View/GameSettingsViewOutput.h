//
//  GameSettingsViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"

@protocol GameSettingsViewOutput <CallBackActionProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (id)getDataAtIndex:(NSInteger)index;
- (void)viewWillAppear;
- (void)startGame;
- (void)sendRequestToPlayers;
- (void)outFromGame;
@end
