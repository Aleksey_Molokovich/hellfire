//
//  GameSettingsViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsViewController.h"
#import "BaseTableViewCell.h"
#import "GameSettingsViewOutput.h"
#import "BaseViewController+Game.h"
#import "CellManager.h"

@interface GameSettingsViewController()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation GameSettingsViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.output didTriggerViewReadyEvent];
    [CellManager registerCellsWithId:[_data cellIds] tableView:_tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateBLEStatusView];
    [_output viewWillAppear];
}

- (IBAction)goOut:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)updateView{
    [_tableView reloadData];
}


#pragma mark - Методы GameSettingsViewInput

- (void)setupInitialStateForSolo {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGreen];
    [self hideBackButton];
    self.navigationItem.title = @"Solo";
    [self hideBottomBar];
}
- (void)setupInitialStateForHotseat {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGreenHotseat];
    [self hideBackButton];
    self.navigationItem.title = @"Hotseat";
    [self hideBottomBar];
}

- (void)updateCellAtIndex:(NSInteger)index{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];

    [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}


- (void)showAlertBeforeStartGame
{
    self.alert = [[LGAlertView alloc] initWithTitle:@"Некоторые игроки не подтвердили участие в игре"
                                        message:@"Исключить таких игроков и продолжить?"
                                          style:LGAlertViewStyleActionSheet
                                   buttonTitles:@[@"Продолжить",@"Повторить запрос"]
                              cancelButtonTitle:@"Отмена"
                         destructiveButtonTitle:nil] ;
    self.alert.delegate = self;
    
    self.alert.buttonsHeight = 44;
    [self.alert showAnimated];
}


#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_data itemAtIndex:indexPath.row].cellId forIndexPath:indexPath];
    [cell configureWithData:[_data itemAtIndex:indexPath.row].data];
    cell.output = _output;
    cell.cellIndex=indexPath;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [_data itemAtIndex:indexPath.row].height;
}

#pragma mark - LGAlertViewDelegate

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    if (index == 0) {
        [_output startGame];
    }
    else
    {
        [self.alert dismissAnimated:YES completionHandler:^{
                [_output sendRequestToPlayers];
        }];
        
    }
}
@end
