//
//  HotseatPlayerCollectionViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayerCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CDHotseatPlayer+CoreDataClass.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation HotseatPlayerCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.avatar.image = [UIImage imageNamed:@"hotseat_member"];
}

- (void)configureWithData:(CDHotseatPlayer *)data
{
    

    if (data.status == PlayerHotseatStatusInvite) {
        _indicator.hidden = NO;
        [_indicator run];
    }
    else if (data.status == PlayerHotseatStatusAccept ||
             data.status == PlayerHotseatStatusAcceptLock)
    {
        _indicator.hidden = NO;
        [_indicator stopWithSuccess];
    }else
    {
        _indicator.hidden = YES;
    }
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: data.avatarPath]
                   placeholderImage:[UIImage imageNamed:@"hotseat_member"]
                            options:SDWebImageCacheMemoryOnly
                          completed:nil];
}


-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    
    CGFloat parentWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    
    CGRect frame = attributes.frame;
    frame.size.height = 44;
    frame.origin.x = (parentWidth -6*(42+4))/2 + attributes.indexPath.item *46;
    frame.size.width = 42;
    attributes.frame = frame;
    
    return attributes;
}

@end
