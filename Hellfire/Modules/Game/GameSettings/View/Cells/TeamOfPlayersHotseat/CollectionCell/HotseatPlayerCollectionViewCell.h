//
//  HotseatPlayerCollectionViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleProgressView.h"
#import "HotseatProgressView.h"

@class CDHotseatPlayer;
@interface HotseatPlayerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (nonatomic,assign) NSInteger angle;
@property (weak, nonatomic) IBOutlet HotseatProgressView *indicator;

- (void)configureWithData:(CDHotseatPlayer*)data;

@end
