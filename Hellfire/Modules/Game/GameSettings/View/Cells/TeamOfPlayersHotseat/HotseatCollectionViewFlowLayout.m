//
//  HotseatCollectionViewFlowLayout.m
//  Hellfire
//
//  Created by Алексей Молокович on 12.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatCollectionViewFlowLayout.h"

@implementation HotseatCollectionViewFlowLayout
- (instancetype)init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    [self configure];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]) == nil)
    {
        return  nil;
    }
    [self configure];
    return self;
}

- (void)configure
{
    self.sectionInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.minimumInteritemSpacing = 4;
    self.estimatedItemSize = CGSizeMake(40, 40);
}

-(NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray *mAttributes = [NSMutableArray new];
    for (UICollectionViewLayoutAttributes *attribute in attributes) {
        CGRect frame = attribute.frame;
        CGFloat parentWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
        
        frame.origin.x = (parentWidth -6*(42+4))/2 + attribute.indexPath.item *46;
        attribute.frame = frame;
        [mAttributes addObject:attribute];
    }
    return [mAttributes copy];
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attribute = [super layoutAttributesForItemAtIndexPath:indexPath];
    CGRect frame = attribute.frame;
    CGFloat parentWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    
    frame.origin.x = (parentWidth -6*(42+4))/2 + attribute.indexPath.item *46;
    attribute.frame = frame;
    return attribute;
}
@end
