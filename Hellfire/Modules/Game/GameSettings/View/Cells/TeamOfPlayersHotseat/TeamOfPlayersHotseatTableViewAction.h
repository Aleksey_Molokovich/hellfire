//
//  TeamOfPlayersHotseatTableViewAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 31.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef TeamOfPlayersHotseatTableViewAction_h
#define TeamOfPlayersHotseatTableViewAction_h


static NSString *const TeamOfPlayersHotseatTableViewSecondPlayerAction = @"TeamOfPlayersHotseatTableViewSecondPlayerAction";
static NSString *const TeamOfPlayersHotseatTableViewThirdPlayerAction = @"TeamOfPlayersHotseatTableViewThirdPlayerAction";
static NSString *const TeamOfPlayersHotseatTableViewFourthPlayerAction = @"TeamOfPlayersHotseatTableViewFourthPlayerAction";
static NSString *const TeamOfPlayersHotseatTableViewFifthPlayerAction = @"TeamOfPlayersHotseatTableViewFifthPlayerAction";
static NSString *const TeamOfPlayersHotseatTableViewSixthPlayerAction = @"TeamOfPlayersHotseatTableViewSixthPlayerAction";
#endif /* TeamOfPlayersHotseatTableViewAction_h */
