//
//  TeamOfPlayersHotseatTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 31.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface TeamOfPlayersHotseatTableViewCell : BaseTableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectioView;
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (nonatomic) NSArray *datasource;
@end
