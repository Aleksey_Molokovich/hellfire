//
//  TeamOfPlayersHotseatTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 31.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "TeamOfPlayersHotseatTableViewCell.h"
#import "TeamOfPlayersHotseatTableViewAction.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "HotseatPlayerCollectionViewCell.h"
#import "HotseatCollectionViewFlowLayout.h"

@implementation TeamOfPlayersHotseatTableViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_collectioView registerNib:[UINib nibWithNibName:@"HotseatPlayerCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HotseatPlayerCollectionViewCell"];
    
    _collectioView.collectionViewLayout =[HotseatCollectionViewFlowLayout new];
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[NSArray class]])
    {
        return;
    }
   
    _datasource = data;
    _title.text = [NSString stringWithFormat:@"Добавить участников (%ld/6)", (unsigned long)[data count]];
    [_collectioView reloadData];
    [_collectioView.collectionViewLayout invalidateLayout];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HotseatPlayerCollectionViewCell *cell = [_collectioView dequeueReusableCellWithReuseIdentifier:@"HotseatPlayerCollectionViewCell" forIndexPath:indexPath];
    
    [cell configureWithData:nil];
    if (indexPath.row<_datasource.count)
    {
        [cell configureWithData:_datasource[indexPath.row]];
    }
    
    
    return  cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return;
    }
    
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = TeamOfPlayersHotseatTableViewSecondPlayerAction;
        [self.output callBackObject:model];
    }
}



@end
