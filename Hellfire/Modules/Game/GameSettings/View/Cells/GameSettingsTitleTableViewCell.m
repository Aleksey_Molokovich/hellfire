//
//  TitleTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 13.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsTitleTableViewCell.h"

@interface GameSettingsTitleTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *label;
@end

@implementation GameSettingsTitleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithData:(NSString*)data{
    _label.text=data;
}

@end
