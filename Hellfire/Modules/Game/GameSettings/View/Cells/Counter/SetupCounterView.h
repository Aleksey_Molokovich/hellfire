//
//  SetupTimerCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CounterView.h"
#import "SetupCounterViewProtocol.h"

@interface SetupCounterView : UIView
@property (weak, nonatomic) id<SetupCounterViewProtocol> delegate;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet UIView *viewSplitLine;


@end
