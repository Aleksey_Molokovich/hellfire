//
//  SetupCounterTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "SetupCounterViewProtocol.h"
#import "CallBackActionProtocol.h"
@class SetupCounterView;
@interface SetupCounterTableViewCell : BaseTableViewCell<SetupCounterViewProtocol>
@property (weak, nonatomic) IBOutlet SetupCounterView *counterView;
@end
