//
//  SetupTimerCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SetupCounterView.h"

@interface SetupCounterView ()

@end

@implementation SetupCounterView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        
    }
    return self;
}

- (IBAction)increase:(id)sender {
    if([_delegate respondsToSelector:@selector(pressIncreaseBtn)])
        [_delegate pressIncreaseBtn];
}

- (IBAction)decrease:(id)sender {
    if([_delegate respondsToSelector:@selector(pressDecreaseBtn)])
        [_delegate pressDecreaseBtn];
}

@end
