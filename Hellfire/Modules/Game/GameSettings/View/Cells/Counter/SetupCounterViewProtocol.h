//
//  SetupCounterViewProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 10.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef SetupCounterViewProtocol_h
#define SetupCounterViewProtocol_h
@protocol SetupCounterViewProtocol<NSObject>
- (void)pressIncreaseBtn;
- (void)pressDecreaseBtn;
@end
#endif /* SetupCounterViewProtocol_h */
