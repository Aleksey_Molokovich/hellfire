//
//  SetupCounterTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SetupCounterTableViewCell.h"
#import "SetupCounterView.h"
#import "CounterCellModel.h"
#import "UIColor+HF.h"
#import "GameSettingsCellType.h"
#import "CallBackActionModel.h"

@implementation SetupCounterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithData:(id)data{
    if ([data isKindOfClass:[CounterCellModel class]]) {
        CounterCellModel *model=data;
        _counterView.counterView.shoudShowMinutes=model.shoudShowMinutes;
        _counterView.delegate = self;
        
        _counterView.lblTitle.text=model.title;
        _counterView.viewSplitLine.backgroundColor=[UIColor hfDarkGreen];
        
        [_counterView.counterView setTime: model.seconds];
 
        
    }
    
}



- (void)pressIncreaseBtn{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.actionType = GameSettingsCellActionIncrease;
        [self.output callBackObject:model];
    }
   
}

- (void)pressDecreaseBtn{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.actionType = GameSettingsCellActionDecrease;
        [self.output callBackObject:model];
    }
}
@end
