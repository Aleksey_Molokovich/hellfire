//
//  StartButtonTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "RoundedButtonView.h"

@interface ButtonNextTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet RoundedButtonView *view;

@end
