//
//  StartButtonTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ButtonNextTableViewCell.h"
#import "HFButtonData.h"
#import "ButtonViewProtocol.h"
#import "GameSettingsCellType.h"

@interface ButtonNextTableViewCell ()<ButtonViewProtocol>

@end


@implementation ButtonNextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithData:(HFButtonData*)data{
    [_view configureWithData:data];
    _view.delegate=self;
}

- (void)pressBtn{
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.actionType = GameSettingsCellActionNext;
        [self.output callBackObject:model];
    }
}

@end
