//
//  TitleTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 13.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface GameSettingsTitleTableViewCell : BaseTableViewCell

@end
