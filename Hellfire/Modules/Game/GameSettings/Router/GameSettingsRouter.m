//
//  GameSettingsRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsRouter.h"
#import "TimeBreakAssembly.h"
#import "TimeBreakModuleInput.h"
#import "HotseatPlayersListAssembly.h"
#import "HotseatPlayersListModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GameModuleDefenitions.h"

@interface GameSettingsRouter()
@property (nonatomic) TimeBreakAssembly *timeBreakAssembly;
@end


@implementation GameSettingsRouter

#pragma mark - Методы GameSettingsRouterInput
- (void)pushPrepare{
    _timeBreakAssembly=[[TimeBreakAssembly new] activated];
    self.factory=[_timeBreakAssembly factoryTimeBreak];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<TimeBreakModuleInput> moduleInput) {
                       [moduleInput configureModuleWithType:GameModuleInputTypePrepare];
                       return nil;
                   }];
}


@end
