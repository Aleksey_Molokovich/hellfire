//
//  GameSettingsRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol GameSettingsRouterInput <NSObject>

- (void)pushPrepare;
- (void)presentHotseatPlayerListWithPlayer:(CDPlayer*)player;
@end
