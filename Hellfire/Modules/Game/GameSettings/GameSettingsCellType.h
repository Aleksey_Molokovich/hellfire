//
//  GameSettingsCellType.h
//  Hellfire
//
//  Created by Алексей Молокович on 10.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef GameSettingsCellType_h
#define GameSettingsCellType_h

typedef NS_ENUM(NSInteger, GameSettingsCellType){
    GameSettingsCellTypeTitleOrPlayers,
    GameSettingsCellTypeCountRounds,
    GameSettingsCellTypeDurationRounds,
    GameSettingsCellTypeDurationBreak,
    GameSettingsCellTypeButtonStart
};

typedef NS_ENUM(NSInteger, GameSettingsCellActionType){
    GameSettingsCellActionIncrease,
    GameSettingsCellActionDecrease,
    GameSettingsCellActionNext
};
#endif /* GameSettingsCellType_h */
