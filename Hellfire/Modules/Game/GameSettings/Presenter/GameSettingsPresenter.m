//
//  GameSettingsPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsPresenter.h"
#import "GameSettingsViewInput.h"
#import "GameSettingsInteractorInput.h"
#import "GameSettingsRouterInput.h"
#import "GameSettingsCellType.h"
#import "CallBackActionModel.h"
#import "BluetoothService.h"
#import "HFNotification.h"

@implementation GameSettingsPresenter

#pragma mark - Методы GameSettingsModuleInput

- (void)configureModuleForSoloGame:(BOOL)isSolo
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [_interactor configure];
    [self.datasource prepareForSolo:isSolo];
    _isSolo = isSolo;
}

#pragma mark - Методы GameSettingsViewOutput

- (void)didTriggerViewReadyEvent {
    [_interactor setupSettings];
    if (_isSolo) {
        [self.view setupInitialStateForSolo];
    }
    else{

        [_interactor checkForcreateHotseatGame];
        [self.view setupInitialStateForHotseat];
    }
    
    [self updateSettings];
    [self.view updateBLEStatusView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBLEConnectionState:) name:kBluetoothServiceDidChangeConnection object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(acceptInviteFromPlayer:)
     name:HFPushNotificationInviteToHotseatAccept object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(rejectInviteFromPlayer:)
     name:HFPushNotificationInviteToHotseatReject object:nil];
}

- (void)dealloc
{
    [self.interactor outFromGame];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)acceptInviteFromPlayer:(NSNotification*)notif
{
    [_interactor acceptInviteFromPlayer:((HFNotification*)notif.object).data.Id];
}

- (void)rejectInviteFromPlayer:(NSNotification*)notif
{
    [_interactor rejectInviteFromPlayer:((HFNotification*)notif.object).data.Id];
}

- (void)changeBLEConnectionState:(NSNotification*)notif
{
    [self.view updateBLEStatusView];
}

- (void)updateSettings{
    self.datasource.settings= [_interactor getGameSettings];
    [self.datasource prepareForSolo:_isSolo];
    [self.view updateView];
}


- (void)viewWillAppear
{
    if (!_isSolo) {
        [self.datasource prepareForSolo:_isSolo];
        [_view updateCellAtIndex:0];
    }
}
#pragma mark - Методы GameSettingsInteractorOutput

- (void)didChangeData{
    
}

-(BOOL)isSoloConfig
{
    return self.isSolo;
}

- (void)successfulCreateHotseatGame
{
    [self.datasource prepareForSolo:_isSolo];
    [_view updateCellAtIndex:0];
}

- (void)successfulWithMessage:(NSString *)message
{
    [_view showAlertWithMessage:message];
}

- (void)failWithError:(NSString *)error
{
     [_view showAlertWithMessage:error];
}

- (void)updatePlayersList
{
    [self.datasource prepareForSolo:_isSolo];
    [_view updateCellAtIndex:0];
}



- (void)showProgress
{
    [_view showProgressWithColor:[UIColor whiteColor]];
}

- (void)hideProgress
{
    [_view hideProgress];
}

- (void)startGame
{
    if ([_interactor checkForHotseatGame]) {
        [_router pushPrepare];
    }
    else{
        [self failWithError:@"Ни один игрок не подтвердил игру"];
    }
    
}

- (void)showAlertBeforeStartGame
{
    [_view showAlertBeforeStartGame];
}

- (void)sendRequestToPlayers
{
    [_interactor sendInvitionForSelectedUsers];
}

- (void)outFromGame
{
    [self.interactor outFromGame];
}
#pragma mark - Методы SetupCounterViewProtocol

- (void)callBackObject:(CallBackActionModel *)object{
    
    switch (object.cellIndex.row) {
        case GameSettingsCellTypeTitleOrPlayers:
            [_router presentHotseatPlayerListWithPlayer:nil];
            break;
        case GameSettingsCellTypeCountRounds:
            if (object.actionType == GameSettingsCellActionIncrease) {
                [_interactor changeCountRound:1];
            }else{
                [_interactor changeCountRound:-1];
            }
            break;
        case GameSettingsCellTypeDurationRounds:
            if (object.actionType == GameSettingsCellActionIncrease) {
                [_interactor changeDurationRound:1];
            }else{
                [_interactor changeDurationRound:-1];
            }
            break;
        case GameSettingsCellTypeDurationBreak:
            if (object.actionType == GameSettingsCellActionIncrease) {
                [_interactor changeDurationBreak:1];
            }else{
                [_interactor changeDurationBreak:-1];
            }
            break;
        case GameSettingsCellTypeButtonStart:
            if (_isSolo) {
                [_router pushPrepare];
            }
            else
            {
                [_interactor checkPlayersToStartPlayHotseat];
            }
                
            break;
            
        default:
            break;
    }
    
    self.datasource.settings= [_interactor getGameSettings];
    [self.datasource prepareForSolo:_isSolo];
    [_view updateCellAtIndex:object.cellIndex.row];
}




@end
