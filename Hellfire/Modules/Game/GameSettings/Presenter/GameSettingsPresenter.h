//
//  GameSettingsPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 09/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsViewOutput.h"
#import "GameSettingsInteractorOutput.h"
#import "GameSettingsModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "GameSettingsDataSourceOutput.h"

@protocol BaseViewControllerProtocol;
@protocol GameSettingsViewInput;
@protocol GameSettingsInteractorInput;
@protocol GameSettingsRouterInput;


@interface GameSettingsPresenter : NSObject <GameSettingsModuleInput, GameSettingsViewOutput, GameSettingsInteractorOutput>

@property (nonatomic, weak) id<GameSettingsViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<GameSettingsInteractorInput> interactor;
@property (nonatomic, strong) id<GameSettingsRouterInput> router;
@property (nonatomic, strong) id<GameSettingsDataSourceOutput> datasource;
@property (nonatomic,assign) BOOL isSolo;

@end
