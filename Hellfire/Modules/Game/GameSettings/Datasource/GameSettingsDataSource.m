//
//  GameSettingsDataSource.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameSettingsDataSource.h"
#import "CounterCellModel.h"
#import "UIColor+HF.h"
#import "HFGameSettings.h"
#import "HFButtonData.h"
#import "UIColor+HF.h"
#import "CDHotseatPlayer+CoreDataClass.h"

@implementation GameSettingsDataSource
@synthesize settings;

- (void)prepareForSolo:(BOOL)isSolo{
    
    NSMutableArray *data=[NSMutableArray new];
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    
    if (isSolo)
    {
        item.cellId=CellIdSolo;
        item.height=72;
        item.data=@"Настройте игру, после чего вам будут даны 3 минуты для подготовки кальяна.";
    }
    else
    {
        item.cellId=CellIdHotseat;
        item.height=104;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld OR status == %ld OR status == %ld", PlayerHotseatStatusAccept,PlayerHotseatStatusInvite,PlayerHotseatStatusAcceptLock ];
        item.data = [CDHotseatPlayer MR_findAllSortedBy:@"index" ascending:YES withPredicate:predicate];
    }
    [data addObject:item];
    
    item=[ObjectTableViewCell new];
    CounterCellModel *itemData=[CounterCellModel new];
    itemData.title=@"Количество раундов в партии:";
    itemData.shoudShowMinutes=NO;
    itemData.seconds=self.settings.countRounds;
    itemData.colorSplitLine=[UIColor hfDarkGreen];
    
    item.cellId=CellIdCounter;
    item.height=104;
    item.data=itemData;
    
    [data addObject:item];
    
    item=[ObjectTableViewCell new];
    itemData=[CounterCellModel new];
    itemData.title=@"Длительность раунда:";
    
    itemData.shoudShowMinutes=YES;
    itemData.seconds=self.settings.durationRound;
    itemData.colorSplitLine=[UIColor hfDarkGreen];
    
    item.cellId=CellIdCounter;
    item.height=104;
    item.data=itemData;
    
    [data addObject:item];
    
    item=[ObjectTableViewCell new];
    itemData=[CounterCellModel new];
    itemData.title=@"Длительность перерыва:";

    itemData.colorSplitLine=[UIColor hfDarkGreen];
    itemData.shoudShowMinutes=YES;
    itemData.seconds=self.settings.durationBreak;
    item.cellId=CellIdCounter;
    item.height=104;
    item.data=itemData;
    
    [data addObject:item];
    
    item=[ObjectTableViewCell new];
    HFButtonData *button=[HFButtonData new];
    button.title=@"НАЧАТЬ!";
    if (isSolo) {
        button.bgColor = [UIColor hfLightGreen];
    }else
    {
        button.bgColor = [UIColor colorWithHex:@"#00B8D4"];
    }

    
    item.cellId=CellIdContinueButton;
    item.height=104;
    item.data=button;
    
    [data addObject:item];
    
    _datasource = data;
    
}

-(NSInteger)count{
    return [self datasource].count;
}

-(ObjectTableViewCell *)itemAtIndex:(NSInteger)index{
    return [self datasource][index];
}

-(NSArray *)cellIds
{
    return @[CellIdHotseat];
}

@end
