//
//  GameSettingsDataSource.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectTableViewCell.h"
#import "GameSettingsDataSourceInput.h"
#import "GameSettingsDataSourceOutput.h"

@class HFGameSettings;
static NSString *const CellIdHotseat = @"TeamOfPlayersHotseatTableViewCell";
static NSString *const CellIdSolo = @"GameSettingsTitleTableViewCell";
static NSString *const CellIdCounter = @"SetupCounterTableViewCell";
static NSString *const CellIdContinueButton = @"ButtonNextTableViewCell";


@interface GameSettingsDataSource : NSObject<GameSettingsDataSourceInput,GameSettingsDataSourceOutput>

@property (nonatomic) NSArray<ObjectTableViewCell *> *datasource;

@end
