//
//  GameSettingsDataSourceOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HFGameSettings;
@protocol GameSettingsDataSourceOutput <NSObject>

@property (nonatomic, strong) HFGameSettings *settings;

- (void) prepareForSolo:(BOOL)isSolo;

@end
