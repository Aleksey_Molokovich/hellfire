//
//  GameSettingsDataSourceInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ObjectTableViewCell;
@class HFGameSettings;

@protocol GameSettingsDataSourceInput <NSObject>

@property (nonatomic, assign, readonly) NSInteger count;
-(ObjectTableViewCell*) itemAtIndex:(NSInteger)index;
- (NSArray*)cellIds;
@end
