//
//  RulesModuleViewController.m
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModuleViewController.h"
#import "RulesModuleViewOutput.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"
#import "SoloRules.h"

@implementation RulesModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы RulesModuleViewInput

- (void)setupInitialStateAsSolo:(BOOL)isSolo {
	
    self.navigationItem.title =isSolo?@"Solo":@"Hotseat";
    [self hideBottomBar];

    if (isSolo) {
        self.solorules.delegate = self;
        self.hotseatrules.hidden = YES;
    }else
    {
        self.hotseatrules.delegate = self;
        self.solorules.hidden = YES;
    }
    
    
    
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];
    [self hideBackButton];
 

}



- (void)skipRules {
    [self.output skipRules];
}

- (void)skipAndNeverShowRules {
    [self.output skipAndNeverShowRules];
}
- (void)start {
    [self.output skipRules];
}

- (IBAction)close:(id)sender {
    [self.output close];
}


@end
