//
//  RulesModuleViewInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RulesModuleViewInput <NSObject>

/**
 @author Алексей Молокович

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateAsSolo:(BOOL)isSolo;

@end
