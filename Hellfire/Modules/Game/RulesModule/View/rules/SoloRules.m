//
//  SoloRules.m
//  Hellfire
//
//  Created by Алексей Молокович on 25.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "SoloRules.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"

@implementation SoloRules

@synthesize delegate;

-(instancetype)init{
    self = [super init];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (void)setup{
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.view];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    
    self.view.backgroundColor=[UIColor clearColor];
    
    self.startBtn.layer.cornerRadius = self.startBtn.frame.size.height/2;
    self.startBtn.clipsToBounds = YES;
    self.startBtn.backgroundColor = [UIColor hfYellow];
    NSAttributedString *str= [[NSAttributedString new]
                              initWithString:@"НАЧАТЬ"
                              attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                           NSKernAttributeName:@(2),
                                           NSForegroundColorAttributeName:[UIColor blackColor]}];
    [self.startBtn setAttributedTitle:str forState:UIControlStateNormal];
    

    self.smokeimg.image =[self.smokeimg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.smokeimg.tintColor = [UIColor hfYellow];
    self.speedimg.image =[self.speedimg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.speedimg.tintColor = [UIColor hfYellow];
    self.scoreimg.image =[self.scoreimg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.scoreimg.tintColor = [UIColor hfYellow];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (IBAction)skipRules:(id)sender {
    [self.delegate skipRules];
}

- (IBAction)skipAndNeverShowRules:(id)sender {
    [self.delegate skipAndNeverShowRules];
}
- (IBAction)start:(id)sender {
    [self.delegate start];
}

@end
