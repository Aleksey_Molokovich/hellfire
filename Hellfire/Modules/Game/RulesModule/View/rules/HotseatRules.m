//
//  HotseatRules.m
//  Hellfire
//
//  Created by Алексей Молокович on 25.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatRules.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"

@implementation HotseatRules

@synthesize delegate;

-(instancetype)init{
    self = [super init];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (void)setup{
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.view];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    
    self.view.backgroundColor=[UIColor clearColor];
    
    self.startBtn.layer.cornerRadius = self.startBtn.frame.size.height/2;
    self.startBtn.clipsToBounds = YES;
    self.startBtn.backgroundColor = [UIColor hfYellow];
    NSAttributedString *str= [[NSAttributedString new]
                              initWithString:@"НАЧАТЬ"
                              attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                           NSKernAttributeName:@(2),
                                           NSForegroundColorAttributeName:[UIColor blackColor]}];
    [self.startBtn setAttributedTitle:str forState:UIControlStateNormal];
    
    self.smokeimg.image =[self.smokeimg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.smokeimg.tintColor = [UIColor hfYellow];
    self.speedimg.image =[self.speedimg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.speedimg.tintColor = [UIColor hfYellow];
    self.scoreimage.image =[self.scoreimage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.scoreimage.tintColor = [UIColor hfYellow];
    [self.startBtn setAttributedTitle:str forState:UIControlStateNormal];
    
    self.smoke2img.image =[self.smoke2img.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.smoke2img.tintColor = [UIColor hfYellow];
    self.speed2img.image =[self.speed2img.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.speed2img.tintColor = [UIColor hfYellow];
    self.score2img.image =[self.score2img.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.score2img.tintColor = [UIColor hfYellow];
}

- (IBAction)skipRules:(id)sender {
    [self.delegate skipRules];
}

- (IBAction)skipAndNeverShowRules:(id)sender {
    [self.delegate skipAndNeverShowRules];
}
- (IBAction)start:(id)sender {
    [self.delegate start];
}

@end
