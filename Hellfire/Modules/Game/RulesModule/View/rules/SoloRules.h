//
//  SoloRules.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RulesViewDelegate.h"

@interface SoloRules : UIView<RulesViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *smokeimg;
@property (weak, nonatomic) IBOutlet UIImageView *speedimg;
@property (weak, nonatomic) IBOutlet UIImageView *scoreimg;

@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *skipBtn;
@property (weak, nonatomic) IBOutlet UIButton *skipAlwaysBtn;
@end
