//
//  RulesViewDelegate.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef RulesViewDelegate_h
#define RulesViewDelegate_h
#import "RulesModuleDelegate.h"

@protocol RulesViewDelegate<NSObject>

@property (nonatomic, weak) id<RulesModuleDelegate> delegate;

@end
#endif /* RulesViewDelegate_h */
