//
//  RulesModuleViewOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RulesModuleViewOutput <NSObject>

/**
 @author Алексей Молокович

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)skipRules;
- (void)skipAndNeverShowRules;
- (void)close;
@end
