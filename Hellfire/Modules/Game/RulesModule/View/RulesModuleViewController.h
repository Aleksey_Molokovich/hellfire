//
//  RulesModuleViewController.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RulesModuleViewInput.h"
#import "BaseViewController.h"
#import "RulesModuleDelegate.h"
#import "SoloRules.h"
#import "HotseatRules.h"

@protocol RulesModuleViewOutput;

@interface RulesModuleViewController : BaseViewController <RulesModuleViewInput, RulesModuleDelegate>

@property (nonatomic, strong) id<RulesModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet SoloRules *solorules;
@property (weak, nonatomic) IBOutlet HotseatRules *hotseatrules;

@end
