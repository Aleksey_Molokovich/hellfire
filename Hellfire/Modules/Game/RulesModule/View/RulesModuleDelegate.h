//
//  RulesModuleDelegate.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef RulesModuleDelegate_h
#define RulesModuleDelegate_h

@protocol RulesModuleDelegate<NSObject>

- (void)skipRules;
- (void)skipAndNeverShowRules;
- (void)start;
@end
#endif /* RulesModuleDelegate_h */
