//
//  RulesModuleAssembly.m
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModuleAssembly.h"

#import "RulesModuleViewController.h"
#import "RulesModuleInteractor.h"
#import "RulesModulePresenter.h"
#import "RulesModuleRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation RulesModuleAssembly

- (RulesModuleViewController *)viewRulesModule {
    return [TyphoonDefinition withClass:[RulesModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRulesModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterRulesModule]];
                          }];
}

- (RulesModuleInteractor *)interactorRulesModule {
    return [TyphoonDefinition withClass:[RulesModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRulesModule]];
                          }];
}

- (RulesModulePresenter *)presenterRulesModule{
    return [TyphoonDefinition withClass:[RulesModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewRulesModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorRulesModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerRulesModule]];
                          }];
}

- (RulesModuleRouter *)routerRulesModule{
    return [TyphoonDefinition withClass:[RulesModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewRulesModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryRulesModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardRulesModule]];
                                                  [initializer injectParameterWith:@"RulesModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardRulesModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
