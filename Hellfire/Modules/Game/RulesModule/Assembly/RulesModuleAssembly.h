//
//  RulesModuleAssembly.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author Алексей Молокович

 RulesModule module
 */
@interface RulesModuleAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryRulesModule;
@end
