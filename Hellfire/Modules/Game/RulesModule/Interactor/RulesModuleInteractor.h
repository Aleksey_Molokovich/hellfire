//
//  RulesModuleInteractor.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModuleInteractorInput.h"

@protocol RulesModuleInteractorOutput;

@interface RulesModuleInteractor : NSObject <RulesModuleInteractorInput>

@property (nonatomic, weak) id<RulesModuleInteractorOutput> output;

@end
