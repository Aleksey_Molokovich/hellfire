//
//  RulesModuleRouterInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RulesModuleRouterInput <NSObject>

- (void)closeCurrentModule;
- (void)pushSettingsForSolo:(BOOL)isSolo;
@end
