//
//  RulesModuleRouter.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModuleRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface RulesModuleRouter : BaseRouter <RulesModuleRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
