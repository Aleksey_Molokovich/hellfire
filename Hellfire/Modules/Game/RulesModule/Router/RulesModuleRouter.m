//
//  RulesModuleRouter.m
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModuleRouter.h"
#import "GameSettingsAssembly.h"
#import "GameSettingsModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface RulesModuleRouter()

@property (strong, nonatomic) GameSettingsAssembly *soloSettingsAssembly;
@end


@implementation RulesModuleRouter

#pragma mark - Методы RulesModuleRouterInput
- (void)pushSettingsForSolo:(BOOL)isSolo{
    _soloSettingsAssembly=[[GameSettingsAssembly new] activated];
    self.factory=[_soloSettingsAssembly factoryGameSettings];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<GameSettingsModuleInput> moduleInput) {
                       [moduleInput configureModuleForSoloGame:isSolo];
                       return nil;
                   }];
}





@end
