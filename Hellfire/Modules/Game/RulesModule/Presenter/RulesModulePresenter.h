//
//  RulesModulePresenter.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModuleViewOutput.h"
#import "RulesModuleInteractorOutput.h"
#import "RulesModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol RulesModuleViewInput;
@protocol RulesModuleInteractorInput;
@protocol RulesModuleRouterInput;

@interface RulesModulePresenter : NSObject <RulesModuleModuleInput, RulesModuleViewOutput, RulesModuleInteractorOutput>

@property (nonatomic, weak) id<RulesModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<RulesModuleInteractorInput> interactor;
@property (nonatomic, strong) id<RulesModuleRouterInput> router;

@property (nonatomic, assign) BOOL isSolo;

@end
