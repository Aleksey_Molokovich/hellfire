//
//  RulesModulePresenter.m
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RulesModulePresenter.h"

#import "RulesModuleViewInput.h"
#import "RulesModuleInteractorInput.h"
#import "RulesModuleRouterInput.h"
#import "Definitions.h"
#import "CDAccount+CoreDataClass.h"
@implementation RulesModulePresenter

#pragma mark - Методы RulesModuleModuleInput

- (void)configureModuleForSoloGame:(BOOL)isSolo {
    self.isSolo = isSolo;
}

#pragma mark - Методы RulesModuleViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialStateAsSolo:self.isSolo];
}

- (void)skipAndNeverShowRules
{
    if (self.isSolo) {
        [CDAccount MR_findFirst].soloRule = NO;
    }else{
       [CDAccount MR_findFirst].hotseatRule = NO;
    }
    
    
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

    [self.router pushSettingsForSolo:self.isSolo];
    
}

- (void)skipRules
{
    
    [self.router pushSettingsForSolo:self.isSolo];
}

- (void)close
{
    [self.router closeCurrentModule];
}
#pragma mark - Методы RulesModuleInteractorOutput

@end
