//
//  RulesModuleModuleInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 25/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol RulesModuleModuleInput <RamblerViperModuleInput>

/**
 @author Алексей Молокович

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleForSoloGame:(BOOL)isSolo;

@end
