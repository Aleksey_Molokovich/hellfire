//
//  GameModuleViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModuleViewController.h"
#import "GameModuleViewOutput.h"
#import "NewsTableViewCell.h"
#import "Advertising.h"
#import "UIColor+HF.h"
#import "AdPostResponse.h"
#import "GameModuleDefenitions.h"
#import "CellManager.h"
#import "BaseViewController+Game.h"
#import "BluetoothStatusView.h"

@interface GameModuleViewController()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>{
    NSArray *data;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopTableView;

@property (weak, nonatomic) IBOutlet BluetoothStatusView *viewHFPipeStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imageBG;

@property (nonatomic) NSDictionary *cellHeights;

@end


@implementation GameModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    _cellHeights = @{};
	[self.output didTriggerViewReadyEvent];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_viewHFPipeStatus connectcedToDevice:[_output connectcedToDevice]];
    [_output viewWillAppear];
}

#pragma mark - Методы GameModuleViewInput

- (void)setupInitialState {
    [CellManager registerCellsWithId:@[FirsrtCellId,NewsCellId]
                           tableView:_tableView];

    [self setBGImg:BackgroundColorBlue];
    [self prepareStatusBarWithHFBGColorType:HFBackgroundColorBlue];
    [self hideNaviagtionBar];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToBluetoothView)];
    [_viewHFPipeStatus addGestureRecognizer:tap];
}

- (void)updateTopBLEStatusView
{
    [_viewHFPipeStatus connectcedToDevice:[_output connectcedToDevice]];
}

- (void)updateWithData:(NSArray*)dataUpdate{
    data=dataUpdate;
    [_tableView reloadData];
}

#pragma mark - prepare Controller
- (void)setupRootGame{

    [self hideNaviagtionBar];
    [self showBottomBar];
}

- (void)tapToBluetoothView{
    [_output didTapToBluetooth];
}

- (void)hideBlueToothStatus{
    self.navigationController.navigationBar.hidden=NO;
    _viewHFPipeStatus.hidden=YES;
}
- (void)showBlueToothStatus{
    self.navigationController.navigationBar.hidden=YES;
    _viewHFPipeStatus.hidden=NO;
}

- (void)showAlertBLENotReady
{
    self.alert = [[LGAlertView alloc] initWithTitle:@"Внимание"
                                            message:@"Игра возможна только с подключенным устройством.\nПодключите HFPipe"
                                              style:LGAlertViewStyleAlert
                                       buttonTitles:@[@"Подключить"]
                                  cancelButtonTitle:@"Закрыть"
                             destructiveButtonTitle:nil] ;
    self.alert.delegate = self;
    
    self.alert.buttonsHeight = 44;
    [self.alert showAnimated];
}
- (void)updateCellWithIndexPath:(NSIndexPath *)indexPath height:(NSInteger)height
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSMutableDictionary *mCellHeight = [_cellHeights mutableCopy];
        
        if ([_cellHeights[indexPath] intValue]==height) {
            return ;
        }
        
//        NSLog(@"indexPath %@, height %ld",indexPath,(long)height);
        
        [mCellHeight setValue:@(height) forKey:indexPath];
        _cellHeights = [mCellHeight copy];
        
        
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    
    }];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellId;
    if (indexPath.row==0) {
        cellId=FirsrtCellId;
    }else{
        cellId=NewsCellId;
    }
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.output=_output;
    cell.cellIndex = indexPath;
    [cell configureWithData:data[indexPath.row]];
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (data.count == 1) {
        _tableView.scrollEnabled=NO;
        return self.tableView.frame.size.height;
    }
    _tableView.scrollEnabled=YES;
    if (indexPath.row == 0) return 224;
    
    if (_cellHeights[indexPath]) {
        return [_cellHeights[indexPath] intValue];
    }
    
    return UITableViewAutomaticDimension;
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output didTapToBluetooth];
}

#ifdef DEBUG

- (void)alertViewCancelled:(LGAlertView *)alertView
{
    [_output pushGameSettings];
}

#endif

@end
