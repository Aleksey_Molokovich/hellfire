//
//  GameModuleViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameModuleViewInput.h"
#import "BaseViewController.h"

@protocol GameModuleViewOutput;

@interface GameModuleViewController : BaseViewController <GameModuleViewInput>

@property (nonatomic, strong) id<GameModuleViewOutput> output;

@end
