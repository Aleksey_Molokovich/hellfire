//
//  GameModuleViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
@protocol GameModuleViewOutput <NSObject, CallBackActionProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didScollEnd;
- (void)didTapToBluetooth;
- (void)pushGameSettings;
- (void)viewWillAppear;
- (NSInteger)getType;
- (NSString*)connectcedToDevice;
@end
