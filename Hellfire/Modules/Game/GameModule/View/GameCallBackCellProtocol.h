//
//  GameCallBackCellProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef GameCallBackCellProtocol_h
#define GameCallBackCellProtocol_h

@protocol GameCallBackCellProtocol <NSObject>

- (void)clickButtonSolo;
- (void)clickButtonHotseat;


@end
#endif /* GameCallBackCellProtocol_h */
