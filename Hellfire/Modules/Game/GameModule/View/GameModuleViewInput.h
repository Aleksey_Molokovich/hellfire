//
//  GameModuleViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AdPostResponse;
@protocol GameModuleViewInput <NSObject>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)updateWithData:(NSArray<AdPostResponse*>*)dataUpdate;
- (void)hideBlueToothStatus;
- (void)showBlueToothStatus;
- (void)setupRootGame;
- (void)updateTopBLEStatusView;
- (void)showAlertBLENotReady;
- (void)updateCellWithIndexPath:(NSIndexPath *)indexPath height:(NSInteger)height;

@end
