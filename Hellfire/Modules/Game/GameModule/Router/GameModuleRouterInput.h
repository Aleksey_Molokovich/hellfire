//
//  GameModuleRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDPlayer;
@protocol GameModuleRouterInput <NSObject>
- (void)pushSettingsForSolo:(BOOL)isSolo;
- (void)pushRulesForSolo:(BOOL)isSolo;
- (void)pushBluetoothModule;
- (void)presentHotseatGame:(NSNumber*)game withMessage:(NSString*)message;
- (void)presentFriendRequestFromPlayer:(CDPlayer *)player withMessage:(NSString*)message;
- (void)presentHotseatRoundResult:(NSDictionary*)push;
- (void)presentHotseatFinalRoundResult:(NSDictionary*)push;
- (void)presentAgreement;
@end
