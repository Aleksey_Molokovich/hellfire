//
//  GameModuleRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModuleRouter.h"
#import "GameSettingsAssembly.h"
#import "GameSettingsModuleInput.h"
#import "BluetoothModuleAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "InvitionForHotseatAssembly.h"
#import "InvitionForHotseatModuleInput.h"
#import "FriendRequestAssembly.h"
#import "FriendRequestModuleInput.h"
#import "GameRoundResultAssembly.h"
#import "GameRoundResultModuleInput.h"
#import "GameRoundFinalResultAssembly.h"
#import "GameRoundFinalResultModuleInput.h"
#import "RulesModuleAssembly.h"
#import "RulesModuleModuleInput.h"
#import "AgreementModuleAssembly.h"
#import "AgreementModuleModuleInput.h"

@interface GameModuleRouter()

@property (strong, nonatomic) GameSettingsAssembly *soloSettingsAssembly;
@property (strong, nonatomic) BluetoothModuleAssembly *bluetoothModuleAssembly;
@property (nonatomic) InvitionForHotseatAssembly *invitionForHotseatAssembly;
@property (nonatomic) FriendRequestAssembly *friendRequestAssembly;
@property (nonatomic) GameRoundResultAssembly *gameRoundResultAssembly;
@property (nonatomic) GameRoundFinalResultAssembly *gameRoundFinalResultAssembly;
@property (nonatomic) RulesModuleAssembly *rulesModuleAssembly;
@property (nonatomic) AgreementModuleAssembly *agreementModuleAssembly;
@property (nonatomic) id<GameRoundResultModuleInput> gameRoundResultModule;
@property (nonatomic) id<GameRoundFinalResultModuleInput> gameRoundFinalResultModule;

@end


@implementation GameModuleRouter

#pragma mark - Методы GameModuleRouterInput

- (void)pushSettingsForSolo:(BOOL)isSolo{
    _soloSettingsAssembly=[[GameSettingsAssembly new] activated];
    self.factory=[_soloSettingsAssembly factoryGameSettings];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<GameSettingsModuleInput> moduleInput) {
                       [moduleInput configureModuleForSoloGame:isSolo];
                       return nil;
                   }];
}

- (void)pushRulesForSolo:(BOOL)isSolo{
    _rulesModuleAssembly=[[RulesModuleAssembly new] activated];
    self.factory=[_rulesModuleAssembly factoryRulesModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RulesModuleModuleInput> moduleInput) {
                       [moduleInput configureModuleForSoloGame:isSolo];
                       return nil;
                   }];
}


- (void)pushBluetoothModule{
    _bluetoothModuleAssembly=[[BluetoothModuleAssembly new] activated];
    self.factory=[_bluetoothModuleAssembly factoryBluetoothModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}


- (void)presentHotseatRoundResult:(NSDictionary*)push
{
    _gameRoundResultAssembly=[[GameRoundResultAssembly new] activated];
    self.factory=[_gameRoundResultAssembly factoryGameRoundResult];
    
    if (_gameRoundResultModule) {
        [_gameRoundResultModule close];
    }
    
    __weak typeof(self) weakSelf = self;
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<GameRoundResultModuleInput> moduleInput) {
                                                  __strong typeof(weakSelf) blockSelf = weakSelf;
                                                  [moduleInput configureModuleWith:push];
                                                  blockSelf.gameRoundResultModule = moduleInput;
                                                  return nil;
                                              }];
}

- (void)presentHotseatFinalRoundResult:(NSDictionary*)push
{
    _gameRoundFinalResultAssembly=[[GameRoundFinalResultAssembly new] activated];
    self.factory=[_gameRoundFinalResultAssembly factoryGameRoundFinalResult];
    
    if (_gameRoundResultModule) {
        [_gameRoundResultModule close];
    }
    
    if (_gameRoundFinalResultModule) {
        [_gameRoundFinalResultModule close];
    }
    
    __weak typeof(self) weakSelf = self;
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<GameRoundFinalResultModuleInput> moduleInput) {
                                                  __strong typeof(weakSelf) blockSelf = weakSelf;
                                                  [moduleInput configureModuleWith:push];
                                                  blockSelf.gameRoundFinalResultModule = moduleInput;
                                                  return nil;
                                              }];
}

- (void)presentHotseatGame:(NSNumber*)game withMessage:(NSString*)message
{
    _invitionForHotseatAssembly=[[InvitionForHotseatAssembly new] activated];
    self.factory=[_invitionForHotseatAssembly factoryInvitionForHotseat];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<InvitionForHotseatModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithGame:game andMessage:message];
                                                  return nil;
                                              }];
}

- (void)presentFriendRequestFromPlayer:(CDPlayer *)player withMessage:(NSString*)message
{
    _friendRequestAssembly=[[FriendRequestAssembly new] activated];
    self.factory=[_friendRequestAssembly factoryFriendRequest];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<FriendRequestModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithRequestFromPlayer:player andMessage:message];
                                                  return nil;
                                              }];
}

- (void)presentAgreement
{
    _agreementModuleAssembly=[[AgreementModuleAssembly new] activated];
    self.factory=[_agreementModuleAssembly factoryAgreementModule];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<AgreementModuleModuleInput> moduleInput) {
                                                  [moduleInput configureModuleFirstTime:YES];
                                                  return nil;
                                              }];
}
@end
