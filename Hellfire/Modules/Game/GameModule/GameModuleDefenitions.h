//
//  GameModuleDefenitions.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef GameModuleDefenitions_h
#define GameModuleDefenitions_h

typedef NS_ENUM(NSInteger, GameModuleInputType) {
    GameModuleInputTypeRootGame = 0,
    GameModuleInputTypeRootGameNonAuth,
    GameModuleInputTypePrepare,
    GameModuleInputTypeBreak,
};

typedef NS_ENUM(NSInteger, GameModuleHeaderCellActionType) {
    GameModuleHeaderCellActionTypePressSolo
};
#endif /* GameModuleDefenitions_h */
