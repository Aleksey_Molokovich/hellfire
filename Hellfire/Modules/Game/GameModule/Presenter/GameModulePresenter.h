//
//  GameModulePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModuleViewOutput.h"
#import "GameModuleInteractorOutput.h"
#import "GameModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol GameModuleViewInput;
@protocol GameModuleInteractorInput;
@protocol GameModuleRouterInput;

@interface GameModulePresenter : NSObject <GameModuleModuleInput, GameModuleViewOutput, GameModuleInteractorOutput>

@property (nonatomic, weak) id<GameModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<GameModuleInteractorInput> interactor;
@property (nonatomic, strong) id<GameModuleRouterInput> router;

@property (nonatomic) BOOL isGameStarted;
@end
