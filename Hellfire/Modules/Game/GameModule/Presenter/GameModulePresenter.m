//
//  GameModulePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModulePresenter.h"

#import "GameModuleViewInput.h"
#import "GameModuleInteractorInput.h"
#import "GameModuleRouterInput.h"
#import "SoloAndHotseatChooceActions.h"
#import "NewsTableViewCellAction.h"
#import "GameModuleDefenitions.h"
#import "FirstTableViewCellModel.h"
#import "BluetoothService.h"
#import "HFGameSettings.h"
#import "HFNotification.h"
#import "Definitions.h"
#import "CDAccount+CoreDataClass.h"

@interface GameModulePresenter(){
    GameModuleInputType typeModule;
}

@property (nonatomic, strong) NSString *action;

@end

@implementation GameModulePresenter

#pragma mark - Методы GameModuleModuleInput

- (void)configureModule{
}

#pragma mark - Методы GameModuleViewOutput

- (void)didTriggerViewReadyEvent {
    _isGameStarted = NO;
    typeModule=([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)?GameModuleInputTypeRootGameNonAuth:GameModuleInputTypeRootGame;
    [_view setupInitialState];
    [_view setupRootGame];
    [_interactor getNewAdPost];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBLEConnectionState:) name:kBluetoothServiceDidChangeConnection object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(presentHotseatAlert:)
     name:HFPushNotificationInviteToHotseat object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(presentFriendRequestAlert:)
     name:HFPushNotificationNewFriendRequest object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(presentHotseatRoundResult:)
     name:HFPushNotificationHotseatRoundResult object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(presentHotseatRoundFinalResult:)
     name:HFPushNotificationHotseatGameResult object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeAccountStatus)
     name:HFNotificationChangeAccountStatus object:nil];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsAgreement]){
        [self.router presentAgreement];
    }
    
}



- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeBLEConnectionState:(NSNotification*)notif
{
    [self.view updateTopBLEStatusView];
}

- (void)changeAccountStatus
{
    typeModule=([CDAccount MR_findFirst].state == AccountStateTypeNonRegister)?GameModuleInputTypeRootGameNonAuth:GameModuleInputTypeRootGame;
    [self successfulWithData:[self.interactor cachedPosts] ];
}

- (void)presentHotseatRoundResult:(NSNotification*)notification
{
    HFNotification * notif = notification.object;
    if (notif.data.Id) {
        [_router presentHotseatRoundResult:notif.data.Payload];
    }
}

- (void)presentHotseatRoundFinalResult:(NSNotification*)notification
{
    HFNotification * notif = notification.object;
    if (notif.data.Id) {
        [_router presentHotseatFinalRoundResult:notif.data.Payload];
    }
}

- (void)presentHotseatAlert:(NSNotification*)notification
{
    HFNotification * notif = notification.object;
    if (notif.data.Id) {
        [_router presentHotseatGame:notif.data.Id withMessage:notif.data.Message];
    }
}

- (void)presentFriendRequestAlert:(NSNotification*)notification
{
    if (_isGameStarted) {
        return;
    }
    HFNotification * notif = notification.object;
    if (notif.data.Id) {
        [_router presentFriendRequestFromPlayer:notif.data.player withMessage:notif.data.Message];
    }
}

-(NSInteger)getType{
    return typeModule;
}

- (void)viewWillAppear{
    _isGameStarted = NO;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
   [_view setupRootGame];
    [_view showBlueToothStatus];
    [_interactor getNewAdPost];
}

- (void)pushGameSettings
{
    CDAccount *account = [CDAccount MR_findFirst];
    
    if ([_action isEqualToString:SoloAndHotseatChooceActionsSolo]) {
        [_view hideBlueToothStatus];
        _isGameStarted = YES;
                
        if (account.soloRule) {
            [_router pushRulesForSolo:YES];
        }else
        {
            [self.router pushSettingsForSolo:YES];
        }
    }else if ([_action isEqualToString:SoloAndHotseatChooceActionsHotseat]) {
        [_view hideBlueToothStatus];
        _isGameStarted = YES;
        
        if (account.hotseatRule) {
            [_router pushRulesForSolo:NO];
        }else
        {
            [self.router pushSettingsForSolo:NO];
        }
    }
}

-(NSString*)connectcedToDevice
{
    return [[BluetoothService sharedInstance] connectedPeripheral].name;
}

- (void)didScollEnd{
    [_interactor getEarlyAdPost];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object{
    
    
    if ([object.action isEqualToString:NewsTableViewCellChangeCellHeight]) {
        [_view updateCellWithIndexPath:object.cellIndex height:[object.data intValue]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellShare]) {
        [_view showShareWithUrl:object.data[1] text:object.data[0]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellOpenSite]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:object.data]];
        return;
    }
    
    _action = object.action;
    
    if (![[BluetoothService sharedInstance] isReady]) {
        [_view showAlertBLENotReady];
        return;
    }

    
    [self pushGameSettings];
}



- (void)didTapToBluetooth{
    [_router pushBluetoothModule];
}
#pragma mark - Методы GameModuleInteractorOutput

- (void)successfulWithData:(NSArray *)data{
    
    FirstTableViewCellModel *firstCell=[FirstTableViewCellModel new];
    firstCell.type=typeModule;
    
    [_view updateWithData:[@[firstCell] arrayByAddingObjectsFromArray:data]];
}
@end
