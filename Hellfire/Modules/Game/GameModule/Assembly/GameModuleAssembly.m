//
//  GameModuleAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModuleAssembly.h"

#import "GameModuleViewController.h"
#import "GameModuleInteractor.h"
#import "GameModulePresenter.h"
#import "GameModuleRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation GameModuleAssembly

- (GameModuleViewController *)viewGameModule {
    return [TyphoonDefinition withClass:[GameModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterGameModule]];
                          }];
}

- (GameModuleInteractor *)interactorGameModule {
    return [TyphoonDefinition withClass:[GameModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameModule]];
                          }];
}

- (GameModulePresenter *)presenterGameModule{
    return [TyphoonDefinition withClass:[GameModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewGameModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorGameModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerGameModule]];
                          }];
}

- (GameModuleRouter *)routerGameModule{
    return [TyphoonDefinition withClass:[GameModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewGameModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryGameModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardGameModule]];
                                                  [initializer injectParameterWith:@"GameModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardGameModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
