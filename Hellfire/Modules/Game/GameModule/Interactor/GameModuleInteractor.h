//
//  GameModuleInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModuleInteractorInput.h"

@protocol GameModuleInteractorOutput;

@interface GameModuleInteractor : NSObject <GameModuleInteractorInput>

@property (nonatomic, weak) id<GameModuleInteractorOutput> output;

@end
