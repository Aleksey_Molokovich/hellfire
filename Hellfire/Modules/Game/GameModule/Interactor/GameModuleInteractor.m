//
//  GameModuleInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GameModuleInteractor.h"
#import "ClanRepository.h"
#import "GameModuleInteractorOutput.h"
#import "AdPostRequest.h"
#import "AdPostResponse.h"
#import "HFPost.h"
#import "HellfireDefines.h"


@interface GameModuleInteractor()
@property(assign, nonatomic) NSInteger page;
@property(assign, nonatomic) BOOL isExecuting;
@property(assign, nonatomic) BOOL isNeedUpdate;;
@property (nonatomic) NSMutableArray< HFPost* > *posts;
@end

@implementation GameModuleInteractor

#pragma mark - Методы GameModuleInteractorInput



- (void)loadPosts{
    _isExecuting=YES;
    _isNeedUpdate=NO;
    AdPostRequest *request=[AdPostRequest new];
    request.page=_page;
    if (!self.posts) {
        self.posts = [NSMutableArray new];
    }
    
    [_output successfulWithData:self.posts];
    
    __weak typeof(self) weakSelf = self;
    
    [ClanRepository posts:nil completion:^(NSArray<HFPost*> *receive) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if ([receive count]) {
            [blockSelf check:receive];
        }
        
        [blockSelf.output successfulWithData:blockSelf.posts];
        
    }];
}

- (void)check:(NSArray*)posts
{
    for (HFPost *post in posts) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id == %ld",post.id];
        
        if (![[self.posts filteredArrayUsingPredicate:predicate] firstObject]) {
            [self.posts addObject:post];
        }
    }
}

-(NSArray *)cachedPosts
{
    return self.posts;
}

- (void)getEarlyAdPost{
    _page=self.posts.count/numberItemsInPage;
    [self loadPosts];
}

- (void)getNewAdPost{
    _page=0;
    [self loadPosts];
}

@end
