//
//  GameModuleInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 30/08/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameModuleInteractorOutput <NSObject>
- (void)successfulWithData:(NSArray*)data;
@end
