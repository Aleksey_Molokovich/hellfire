//
//  InvitionForHotseatAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 InvitionForHotseat module
 */
@interface InvitionForHotseatAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryInvitionForHotseat;
@end
