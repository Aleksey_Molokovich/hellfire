//
//  InvitionForHotseatAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "InvitionForHotseatAssembly.h"

#import "InvitionForHotseatViewController.h"
#import "InvitionForHotseatInteractor.h"
#import "InvitionForHotseatPresenter.h"
#import "InvitionForHotseatRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation InvitionForHotseatAssembly

- (InvitionForHotseatViewController *)viewInvitionForHotseat {
    return [TyphoonDefinition withClass:[InvitionForHotseatViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterInvitionForHotseat]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterInvitionForHotseat]];
                          }];
}

- (InvitionForHotseatInteractor *)interactorInvitionForHotseat {
    return [TyphoonDefinition withClass:[InvitionForHotseatInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterInvitionForHotseat]];
                          }];
}

- (InvitionForHotseatPresenter *)presenterInvitionForHotseat{
    return [TyphoonDefinition withClass:[InvitionForHotseatPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewInvitionForHotseat]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorInvitionForHotseat]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerInvitionForHotseat]];
                          }];
}

- (InvitionForHotseatRouter *)routerInvitionForHotseat{
    return [TyphoonDefinition withClass:[InvitionForHotseatRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewInvitionForHotseat]];
                          }];
}

- (RamblerViperModuleFactory *)factoryInvitionForHotseat  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardInvitionForHotseat]];
                                                  [initializer injectParameterWith:@"InvitionForHotseatViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardInvitionForHotseat {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
