//
//  InvitionForHotseatPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "InvitionForHotseatPresenter.h"

#import "InvitionForHotseatViewInput.h"
#import "InvitionForHotseatInteractorInput.h"
#import "InvitionForHotseatRouterInput.h"

@implementation InvitionForHotseatPresenter

#pragma mark - Методы InvitionForHotseatModuleInput

- (void)configureModuleWithGame:(NSNumber*)game andMessage:(NSString *)message
{
    _message = message;
    [_interactor saveInviteGame:game];
}

#pragma mark - Методы InvitionForHotseatViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialStateWithPlayerName:_message];
}

- (void)didClickReject
{
    [_interactor rejectGame];
    [_router closeCurrentModule];
}

- (void)didClickAccept
{
    [_interactor acceptGame];
    [_router closeCurrentModule];
}

#pragma mark - Методы InvitionForHotseatInteractorOutput



@end
