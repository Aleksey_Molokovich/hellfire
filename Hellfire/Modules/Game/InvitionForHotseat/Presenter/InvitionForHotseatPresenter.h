//
//  InvitionForHotseatPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "InvitionForHotseatViewOutput.h"
#import "InvitionForHotseatInteractorOutput.h"
#import "InvitionForHotseatModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol InvitionForHotseatViewInput;
@protocol InvitionForHotseatInteractorInput;
@protocol InvitionForHotseatRouterInput;

@interface InvitionForHotseatPresenter : NSObject <InvitionForHotseatModuleInput, InvitionForHotseatViewOutput, InvitionForHotseatInteractorOutput>

@property (nonatomic, weak) id<InvitionForHotseatViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<InvitionForHotseatInteractorInput> interactor;
@property (nonatomic, strong) id<InvitionForHotseatRouterInput> router;
@property (nonatomic) NSString *message;
@end
