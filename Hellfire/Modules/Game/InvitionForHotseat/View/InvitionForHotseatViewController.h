//
//  InvitionForHotseatViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InvitionForHotseatViewInput.h"
#import "BaseViewController.h"
#import "RoundedButton.h"

@protocol InvitionForHotseatViewOutput;

@interface InvitionForHotseatViewController : BaseViewController <InvitionForHotseatViewInput>

@property (nonatomic, strong) id<InvitionForHotseatViewOutput> output;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet RoundedButton *btnReject;
@property (weak, nonatomic) IBOutlet RoundedButton *btnPlay;

@end
