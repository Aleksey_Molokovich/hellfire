//
//  InvitionForHotseatViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "InvitionForHotseatViewController.h"
#import "InvitionForHotseatViewOutput.h"
#import "UIColor+HF.h"

@implementation InvitionForHotseatViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы InvitionForHotseatViewInput

- (void)setupInitialStateWithPlayerName:(NSString*)text {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];
    
    _lblText.text = text;
    
    [_btnReject configureTitle:@"ОТКЛОНИТЬ"
                             bgColor:[UIColor hfBlue]];
    [_btnPlay configureTitle:@"ИГРАТЬ"
                       bgColor:[UIColor hfDeepPink]];
}

- (IBAction)clickAccept:(id)sender {
    [_output didClickAccept];
}

- (IBAction)clickReject:(id)sender {
    [_output didClickReject];
}

@end
