//
//  InvitionForHotseatInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "InvitionForHotseatInteractor.h"
#import "InvitionForHotseatInteractorOutput.h"
#import "GameRepository.h"

@implementation InvitionForHotseatInteractor

#pragma mark - Методы InvitionForHotseatInteractorInput
- (void)saveInviteGame:(NSNumber *)game
{
    _gameId = game;
}

- (void)acceptGame
{
    
    [GameRepository acceptInviteToGame:_gameId
                            completion:nil];
}

- (void)rejectGame
{
    [GameRepository rejectInviteToGame:_gameId
                            completion:nil];
}
@end
