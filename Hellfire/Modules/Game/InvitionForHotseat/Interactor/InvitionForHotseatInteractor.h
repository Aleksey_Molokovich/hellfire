//
//  InvitionForHotseatInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "InvitionForHotseatInteractorInput.h"

@protocol InvitionForHotseatInteractorOutput;

@interface InvitionForHotseatInteractor : NSObject <InvitionForHotseatInteractorInput>

@property (nonatomic, weak) id<InvitionForHotseatInteractorOutput> output;
@property (nonatomic, strong) NSNumber *gameId;
@end
