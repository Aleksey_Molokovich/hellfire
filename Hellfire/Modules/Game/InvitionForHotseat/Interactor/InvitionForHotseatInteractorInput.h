//
//  InvitionForHotseatInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 08/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol InvitionForHotseatInteractorInput <NSObject>
- (void)saveInviteGame:(NSNumber*)game;
- (void)acceptGame;
- (void)rejectGame;
@end
