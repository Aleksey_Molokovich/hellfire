//
//  MainGameScreeenAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenAssembly.h"

#import "MainGameScreeenViewController.h"
#import "MainGameScreeenInteractor.h"
#import "MainGameScreeenPresenter.h"
#import "MainGameScreeenRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation MainGameScreeenAssembly

- (MainGameScreeenViewController *)viewMainGameScreeen {
    return [TyphoonDefinition withClass:[MainGameScreeenViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMainGameScreeen]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterMainGameScreeen]];
                          }];
}

- (MainGameScreeenInteractor *)interactorMainGameScreeen {
    return [TyphoonDefinition withClass:[MainGameScreeenInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMainGameScreeen]];
                          }];
}

- (MainGameScreeenPresenter *)presenterMainGameScreeen{
    return [TyphoonDefinition withClass:[MainGameScreeenPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewMainGameScreeen]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorMainGameScreeen]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerMainGameScreeen]];
                          }];
}

- (MainGameScreeenRouter *)routerMainGameScreeen{
    return [TyphoonDefinition withClass:[MainGameScreeenRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewMainGameScreeen]];
                          }];
}

- (RamblerViperModuleFactory *)factoryMainGameScreeen  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardMainGameScreeen]];
                                                  [initializer injectParameterWith:@"MainGameScreeenViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardMainGameScreeen {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
