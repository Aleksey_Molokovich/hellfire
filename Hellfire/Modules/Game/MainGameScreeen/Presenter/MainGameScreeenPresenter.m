//
//  MainGameScreeenPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenPresenter.h"
#import "MainGameScreeenViewInput.h"
#import "MainGameScreeenInteractorInput.h"
#import "MainGameScreeenRouterInput.h"
#import "BluetoothService.h"
#import "GameService.h"
#import "HFGameSettings.h"
#import "CDHotseatPlayer+CoreDataClass.h"

@interface MainGameScreeenPresenter()<GameServiceProtocol>
@property (nonatomic) GameService *gameService;
@property (nonatomic,assign) BOOL isRedyNext;

@end

@implementation MainGameScreeenPresenter

#pragma mark - Методы MainGameScreeenModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы MainGameScreeenViewOutput

- (void)didTriggerViewReadyEvent
{
    [_interactor configure];
	[self.view setupInitialState];
    NSInteger count;
    _isDisconnected = NO;
    _isRedyNext = YES;
    count=[_interactor durationRound];
    _gameService =[GameService new];
    _gameService.delegate = self;
    [_gameService startWithDuration:count];
    
    [_view changeCurrentRound:[_interactor getCurrentRound]
                       amount:[_interactor getCountRounds]];
    [_view changeTimer:count];
    [self.view updateBLEStatusView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBLEConnectionState:) name:kBluetoothServiceDidChangeConnection object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBLEConnectionState:) name:HFPushNotificationCancelPeripheralConnection object:nil];
    
    
    if (![self isSoloGame]) {
        
        
        
        [_view showCurrentPlayerAvatar:[_gameService currentHotseatPlayer].avatarPath
                      nextPlayerAvatar:[self isFinishedHotseatGameRound]?nil:[_gameService nextHotseatPlayer].avatarPath];
    }
}

-(BOOL)isSoloGame
{
    return [_interactor isSoloGame];
}

- (BOOL)isFinishedHotseatGameRound
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %ld", PlayerHotseatStatusAccept];
    CDHotseatPlayer *lastPlayer = [[CDHotseatPlayer MR_findAllSortedBy:@"index" ascending:YES withPredicate:predicate] lastObject];
    
    CDHotseatPlayer *currentPlayer = [_gameService currentHotseatPlayer];
    
    if (lastPlayer.id == currentPlayer.id) {
        return YES;
    }
    
    return NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeBLEConnectionState:(NSNotification*)notif
{
    if ([[BluetoothService sharedInstance] connectedPeripheral].state == CBPeripheralStateDisconnected)
    {
        [self.view showAlertAfterDisconnected];
        _isDisconnected = YES;
        [self stopTimer];
    }
    [self.view updateBLEStatusView];
}

- (void)waitUserAction
{
    _isRedyNext = NO;
}

- (void)continueGame
{
    _isRedyNext = YES;
    if (!_gameService.delegate) {
        [self goNext];
    }
}

- (void)stopTimer
{
    _gameService.delegate = nil;
    [_gameService stop];
}

- (void)goNext
{
    if (!_isRedyNext)
        return;
        
    [_interactor stopGame];
    
    if ([self isSoloGame]) {
        if ([_interactor isGameOver])
        {
            [_router pushResults];
        }
        else{
            [_router pushSoloBreak];
        }
    }
    else
    {
        if ([self isFinishedHotseatGameRound]) {
            if ([_interactor isGameOver])
            {
                [_router pushResults];
                
            }
            else{
                [_router pushSoloBreak];
            }
        }else
        {
            [_gameService changeCurrentHotseatPlayer];
            [_router pushSoloUserPrepare];
        }
    }
    
    
}


- (void)changeProgressQualitySmoke:(NSInteger)value
{
    [_view changeProgressCurrentQualitySmoke:value];
}

- (void)changeProgressSpeedSmoke:(NSInteger)value
{
    [_view changeProgressCurrentSpeedSmoke:value];
}

- (void)changeRoundPoints:(NSInteger)max
{
    [_view changeRoundPoints:max];
}
- (void)changeMaxQualitySmoke:(NSInteger)value
{
    [_view changeMaxCurrentQualitySmoke:value];
}

- (void)changeMaxSpeedSmoke:(NSInteger)value
{
    [_view changeMaxCurrentSpeedSmoke:value];
}

- (void)changeAmountPoints:(NSInteger)points
{
    [_view changeAmountPoints:points];
}

#pragma mark - Методы MainGameScreeenInteractorOutput


#pragma mark - Методы GameServiceProtocol

- (void)updateTimerWithSeconds:(NSInteger)seconds{
   [_view changeTimer:seconds];
    NSLog(@"BLE %@",@([[BluetoothService sharedInstance] isReady]));
    if (seconds<=0) {
        _gameService.delegate=nil;
        [self goNext];
        
    }else{
        if (_isDisconnected)
        {
            return;
        }
    }
}
@end
