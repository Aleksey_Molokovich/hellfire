//
//  MainGameScreeenPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenViewOutput.h"
#import "MainGameScreeenInteractorOutput.h"
#import "MainGameScreeenModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol MainGameScreeenViewInput;
@protocol MainGameScreeenInteractorInput;
@protocol MainGameScreeenRouterInput;

@interface MainGameScreeenPresenter : NSObject <MainGameScreeenModuleInput, MainGameScreeenViewOutput, MainGameScreeenInteractorOutput>

@property (nonatomic, weak) id<MainGameScreeenViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<MainGameScreeenInteractorInput> interactor;
@property (nonatomic, strong) id<MainGameScreeenRouterInput> router;
@property (nonatomic, assign) BOOL isDisconnected;
@end
