//
//  MainGameScreeenRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenRouter.h"
#import "TimeBreakAssembly.h"
#import "TimeBreakModuleInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GameModuleDefenitions.h"
#import "ResultsGameModuleAssembly.h"
#import "ResultsGameModuleModuleInput.h"
#import "GamePrepareAssembly.h"
@interface MainGameScreeenRouter()

@property (nonatomic) TimeBreakAssembly *timeBreakAssembly;
@property (strong, nonatomic) ResultsGameModuleAssembly *resultsGameModuleAssembly;
@property (strong, nonatomic) GamePrepareAssembly *soloUserPrepareToPlayAssembly;

@end


@implementation MainGameScreeenRouter

#pragma mark - Методы MainGameScreeenRouterInput
- (void)pushSoloBreak{
    _timeBreakAssembly=[[TimeBreakAssembly new] activated];
    self.factory=[_timeBreakAssembly factoryTimeBreak];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<TimeBreakModuleInput> moduleInput) {
                       [moduleInput configureModuleWithType:GameModuleInputTypeBreak];
                       return nil;
                   }];
}

- (void)pushResults{
    _resultsGameModuleAssembly=[[ResultsGameModuleAssembly new] activated];
    self.factory=[_resultsGameModuleAssembly factoryResultsGameModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<ResultsGameModuleModuleInput> moduleInput) {
                       [moduleInput configureModule];
                       return nil;
                   }];
}

- (void)pushSoloUserPrepare{
    _soloUserPrepareToPlayAssembly=[[GamePrepareAssembly new] activated];
    self.factory=[_soloUserPrepareToPlayAssembly factoryGamePrepare];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
