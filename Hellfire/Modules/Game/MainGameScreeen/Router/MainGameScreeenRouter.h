//
//  MainGameScreeenRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenRouterInput.h"
#import "BaseRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface MainGameScreeenRouter : BaseRouter <MainGameScreeenRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
