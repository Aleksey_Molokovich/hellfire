//
//  MainGameScreeenRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MainGameScreeenRouterInput <NSObject>

- (void)pushSoloBreak;
- (void)pushResults;
- (void)pushSoloUserPrepare;
@end
