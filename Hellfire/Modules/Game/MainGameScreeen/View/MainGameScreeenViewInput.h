//
//  MainGameScreeenViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerGameInput.h"
@protocol MainGameScreeenViewInput <BaseViewControllerGameInput>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)changeProgressCurrentQualitySmoke:(NSInteger)progress;
- (void)changeProgressCurrentSpeedSmoke:(NSInteger)progress;
- (void)changeTimer:(NSInteger)number;
- (void)changeMaxCurrentSpeedSmoke:(NSInteger)progress;
- (void)changeMaxCurrentQualitySmoke:(NSInteger)progress;
- (void)changeCurrentRound:(NSInteger)current amount:(NSInteger)amount;
- (void)showPoints:(NSInteger)points;
- (void)changeAmountPoints:(NSInteger)points;
- (void)showCurrentPlayerAvatar:(NSString*)url nextPlayerAvatar:(NSString*)nextUrl;
- (void)changeRoundPoints:(NSInteger)max;
@end
