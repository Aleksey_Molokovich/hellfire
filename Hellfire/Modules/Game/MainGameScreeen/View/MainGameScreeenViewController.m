//
//  MainGameScreeenViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenViewController.h"
#import "MainGameScreeenViewOutput.h"
#import "CounterView.h"
#import "CircleProgressView.h"
#import "UIColor+HF.h"
#import <LGAlertView/LGAlertView.h>
#import "UIFont+HF.h"
#import "BaseViewController+Game.h"
#import "UIImageViewRounded.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MainGameScreeenViewController(){
    CGFloat oldProgress1;
    CGFloat oldProgress2;
}
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet CircleProgressView *currentQualitySmokeProgress;
@property (weak, nonatomic) IBOutlet CircleProgressView *currentSpeedSmokeProgress;

@property (weak, nonatomic) IBOutlet UILabel *lblMaxQualitySmoke;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxSpeedSmoke;
@property (weak, nonatomic) IBOutlet UILabel *lblCountRounds;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountRoundsPoints;


@property (weak, nonatomic) IBOutlet UIImageViewRounded *currentPlayerAvtar;
@property (weak, nonatomic) IBOutlet UIImageViewRounded *nextPlayerAvatar;
@property (weak, nonatomic) IBOutlet UIView *currentPlayerView;

@end


@implementation MainGameScreeenViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (IBAction)exit:(id)sender {
    [_output waitUserAction];
   [self goOut];
}

- (void)continueGame
{
    [super continueGame];
    [_output continueGame];
}

- (void)gameOver
{
    [_output stopTimer];
    [super gameOver];
}

#pragma mark - Методы MainGameScreeenViewInput

- (void)setupInitialState {
    [self changeProgressCurrentSpeedSmoke:0];
    [self changeProgressCurrentQualitySmoke:0];
    [self.currentQualitySmokeProgress drawCircleWithPercent:100
                                                   duration:1
                                                  lineWidth:8
                                                  clockwise:NO
                                                    lineCap:kCALineCapRound
                                                  fillColor:[UIColor hfYellow]
                                                strokeColor:[UIColor whiteColor]
                                             animatedColors:nil];
    [self.currentSpeedSmokeProgress drawCircleWithPercent:100
                                                 duration:1
                                                lineWidth:8
                                                clockwise:NO
                                                  lineCap:kCALineCapRound
                                                fillColor:[UIColor hfYellow]
                                              strokeColor:[UIColor whiteColor]
                                           animatedColors:nil];
    _counterView.shoudShowMinutes=YES;
    self.navigationItem.hidesBackButton = YES;
    if ([_output isSoloGame]) {
        _currentPlayerView.hidden = YES;
        [self prepareStyleWithHFBGColorType:HFBackgroundColorOrange];
    }
    else
    {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorOrangeHotseat];
    }

    [self changeMaxCurrentSpeedSmoke:0];
    [self changeMaxCurrentQualitySmoke:0];
}

- (void)showCurrentPlayerAvatar:(NSString*)url nextPlayerAvatar:(NSString*)nextUrl
{
    [self.currentPlayerAvtar sd_setImageWithURL:[NSURL  URLWithString:url]
                         placeholderImage:nil
                                  options:SDWebImageRetryFailed
                                completed:nil];
    
    [self.nextPlayerAvatar sd_setImageWithURL:[NSURL  URLWithString:nextUrl]
                               placeholderImage:nil
                                        options:SDWebImageRetryFailed
                                      completed:nil];
}

- (void)changeCurrentRound:(NSInteger)current amount:(NSInteger)amount{
    NSAttributedString *text= [[NSAttributedString new]
                                initWithString:[NSString stringWithFormat:@"%ld/%ld",(long)current,(long)amount]
                                attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                             NSKernAttributeName:@(2),
                                             NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _lblCountRounds.attributedText=text;
}

- (void)changeProgressCurrentQualitySmoke:(NSInteger)progress{
    
    [self.currentQualitySmokeProgress animateFrom:oldProgress1 to:progress/100.0];
    oldProgress1= progress/100.0;
}

- (void)changeProgressCurrentSpeedSmoke:(NSInteger)progress{
    
    [self.currentSpeedSmokeProgress animateFrom:oldProgress2 to: progress/100.0];
    oldProgress2= progress/100.0;
}

- (void)changeMaxCurrentSpeedSmoke:(NSInteger)max{
    _lblMaxSpeedSmoke.text=[NSString stringWithFormat:@"Рекорд: %ld", (long)max];
}
- (void)changeMaxCurrentQualitySmoke:(NSInteger)max{
    _lblMaxQualitySmoke.text=[NSString stringWithFormat:@"Рекорд: %ld", (long)max];
}

- (void)changeRoundPoints:(NSInteger)max{
    _lblAmountRoundsPoints.font = [UIFont hfMediumItalicSize:12];
    _lblAmountRoundsPoints.text=[NSString stringWithFormat:@"Общий счёт партии: %ld", (long)max];
}

- (void)changeAmountPoints:(NSInteger)points
{
    _lblAmountPoints.text = [@(points) stringValue];
}

- (void)changeTimer:(NSInteger)number{
    
    [_counterView setTime:number];
}
@end
