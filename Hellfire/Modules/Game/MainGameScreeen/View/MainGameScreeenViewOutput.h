//
//  MainGameScreeenViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MainGameScreeenViewOutput <NSObject>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (BOOL)isSoloGame;
- (void)stopTimer;
- (void)waitUserAction;
- (void)continueGame;
@end
