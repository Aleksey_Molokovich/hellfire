//
//  MainGameScreeenViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainGameScreeenViewInput.h"
#import "BaseViewController.h"

@protocol MainGameScreeenViewOutput;

@interface MainGameScreeenViewController : BaseViewController <MainGameScreeenViewInput>

@property (nonatomic, strong) id<MainGameScreeenViewOutput> output;
- (void)animateFrom:(CGFloat)from to:(CGFloat)to;
@end
