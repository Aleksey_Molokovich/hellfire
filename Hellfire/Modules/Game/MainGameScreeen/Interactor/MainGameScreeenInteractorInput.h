//
//  MainGameScreeenInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MainGameScreeenInteractorInput <NSObject>
- (void)configure;
-(NSInteger)durationRound;
-(BOOL)isGameOver;
-(NSInteger)getCurrentRound;
-(NSInteger)getCountRounds;
- (void)stopGame;
-(BOOL)isSoloGame;
@end
