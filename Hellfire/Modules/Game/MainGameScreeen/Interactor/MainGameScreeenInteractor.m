
//
//  MainGameScreeenInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenInteractor.h"
#import "HFGameSettings.h"
#import "MainGameScreeenInteractorOutput.h"
#import "CDGameResults+CoreDataClass.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "math.h"

@implementation MainGameScreeenInteractor

#pragma mark - Методы MainGameScreeenInteractorInput
- (void)configure
{
    [BluetoothService sharedInstance].delegate = self;
    
    _gameService = [GameService new];
    
    _allPoints = [_gameService calculateAllPointsForCurrentPlayer];
    
    _speed = 0;
    _quality = 0;
    _points = 0;
    _tiksCount = 0;
    
    self.metrics = [NSMutableArray new];
    
    if (![BluetoothService sharedInstance].isReady){
        _gameService.delegate = self;
        [_gameService startWithDuration:[self durationRound]];
    }
}

-(NSInteger)durationRound{
    return [HFGameSettings load].durationRound;
}

-(NSInteger)getCurrentRound{
    return [HFGameSettings load].currentRound;
}

-(NSInteger)getCountRounds{
    return [HFGameSettings load].countRounds;
}

-(BOOL)isGameOver{
    return [_gameService isGameOver];
}

-(BOOL)isSoloGame
{
    return [HFGameSettings load].gameId>0?NO:YES;
}

#pragma mark BluetoothService


- (void)serialDidChangeState {
    
}

- (void)serialDidDisconnect:(CBPeripheral *)peripheral error:(NSError *)error {
    
}


- (void)stopGame
{
    _isGameOver = YES;
    CDGameResults *result = [_gameService currentGameResults];
    result.tiksCount = _tiksCount;
    result.maxSpeed = _maxSpeed;
    result.speed = _speed;
    result.maxQuality = _maxQuality;
    result.quality = _quality;
    result.maxPoints = _maxPoints;
    result.points = (int)_points;
    result.volume = [self calculateVolume];
    result.maxVolume = result.volume;
    if ([BluetoothService sharedInstance].isSendMetrics){
      result.metrics = [NSJSONSerialization dataWithJSONObject:[self.metrics copy] options:0 error:nil];
    }
    
    [_gameService calculateAllPointsForCurrentPlayer];
    NSLog(@"maxVolume = %hd",result.maxVolume);
}

-(NSInteger) calculateVolume{
    
    double pascalToKGsm2 = 101.9716;
    double airConstant = 1.318;
    double ratio = 2.59896; //0.013*4*1.02*7*7
    double toPascal = 255 * 4;
    
    double averageSpeed = _speed/_tiksCount;
    
    double pressure = (averageSpeed / toPascal) * pascalToKGsm2;
    double consumption = ratio * sqrt(pressure / airConstant);
    double volume = ((consumption * 1000) / 3600) * (double)[self durationRound];
    return (int) round(volume);
}



- (void)serialDidReceiveQualityData:(NSInteger)quality speedData:(NSInteger)speed
{
//#if DEBUG
////    NSLog(@"quality:%ld speed:%ld",(long)quality,(long)speed);
//    quality = arc4random() % 120;
//    speed = arc4random() % 100;
//#endif
    if (_isGameOver) {
        return;
    }
    _tiksCount++;
    _quality+=quality;
    _speed+=speed;
    if (_maxSpeed<speed) {
        _maxSpeed = speed;
        [_output changeMaxSpeedSmoke:_maxSpeed];
    }
    
    if (_maxQuality<quality) {
        _maxQuality = quality;
        [_output changeMaxQualitySmoke:_maxQuality];
    }
    
    _points +=(speed*quality)/1020.0;
    
    if (_maxPoints<_points) {
        _maxPoints = (int)_points;
    }
    
    [self.metrics addObject:[NSString stringWithFormat:@"%ld-%ld",(long)speed,(long)quality]];
    
    [_output changeRoundPoints:(_allPoints + (int)_points)];
    [_output changeAmountPoints:(int)_points];
    [_output changeProgressSpeedSmoke:speed/2.55];
    [_output changeProgressQualitySmoke:quality/2.55];
}

#pragma mark - Методы GameServiceProtocol

- (void)updateTimerWithSeconds:(NSInteger)seconds{
   
    if (_isGameOver) {
        _gameService.delegate = nil;
        [_gameService stop];
        return;
    }
    
    int quality = arc4random() % 120;
    int speed = arc4random() % 100;
    
    [self.metrics addObject:[NSString stringWithFormat:@"%ld-%ld",(long)speed,(long)quality]];

    
    _tiksCount++;
    _quality+=quality;
    _speed+=speed;
    if (_maxSpeed<speed) {
        _maxSpeed = speed;
        [_output changeMaxSpeedSmoke:_maxSpeed];
    }
    
    if (_maxQuality<quality) {
        _maxQuality = quality;
        [_output changeMaxQualitySmoke:_maxQuality];
    }
    
    _points +=(speed*quality)/1020.0;
    
    if (_maxPoints<_points) {
        _maxPoints = _points;
    }
    
    [_output changeRoundPoints:(_allPoints + (int)_points)];
    [_output changeAmountPoints:(int)_points];
    [_output changeProgressSpeedSmoke:speed/2.55];
    [_output changeProgressQualitySmoke:quality/2.55];
}

@end
