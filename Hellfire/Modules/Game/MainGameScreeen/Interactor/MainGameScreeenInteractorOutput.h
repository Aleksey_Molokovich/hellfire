//
//  MainGameScreeenInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MainGameScreeenInteractorOutput <NSObject>
- (void)changeProgressQualitySmoke:(NSInteger)value;
- (void)changeProgressSpeedSmoke:(NSInteger)value;
- (void)changeMaxQualitySmoke:(NSInteger)value;
- (void)changeMaxSpeedSmoke:(NSInteger)value;
- (void)changeAmountPoints:(NSInteger)points;
- (void)changeRoundPoints:(NSInteger)max;
@end
