//
//  MainGameScreeenInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MainGameScreeenInteractorInput.h"
#import "BluetoothService.h"

@protocol MainGameScreeenInteractorOutput;

#import "GameService.h"

@interface MainGameScreeenInteractor : NSObject <MainGameScreeenInteractorInput, BluetoothServiceDelegate,  GameServiceProtocol>

@property (nonatomic, weak) id<MainGameScreeenInteractorOutput> output;

@property (nonatomic, assign) NSInteger maxQuality;

@property (nonatomic, assign) NSInteger maxSpeed;

@property (nonatomic, assign) NSInteger maxPoints;

@property (nonatomic, assign) NSInteger allPoints;


@property (nonatomic, assign) CGFloat points;

@property (nonatomic, assign) CGFloat quality;

@property (nonatomic, assign) CGFloat speed;


@property (nonatomic, assign) BOOL isGameOver;

@property (nonatomic, assign) NSInteger tiksCount;

@property (nonatomic) GameService *gameService;

@property (nonatomic) NSMutableArray *metrics;

@end
