//
//  GameRoundFinalResultDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultDatasourceInput.h"

@protocol GameRoundFinalResultDatasourceOutput;

@interface GameRoundFinalResultDatasource : NSObject <GameRoundFinalResultDatasourceInput, GameRoundFinalResultDatasourceOutput>


@end
