//
//  GameRoundFinalResultDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultDatasource.h"
#import "GameRoundFinalResultDatasourceOutput.h"

#import "ObjectTableViewCell.h"
#import "SeparateLineTableViewCellModel.h"
#import "HFHotseatPlayer.h"
#import "HFHotseatGameResult.h"
#import "HFPushFinalResultHotseatModel.h"
#import "CDUser+CoreDataClass.h"
#import "UIColor+HF.h"
#import "HFButtonData.h"

static NSString *const kResultsSoloGameRegistredTableViewCell = @"ResultsSoloGameRegistredTableViewCell";
static NSString *const kResultsSoloGameTableViewCell = @"ResultsSoloGameTableViewCell";
static NSString *const kSeparateLineTableViewCell = @"SeparateLineTableViewCell";
static NSString *const kPLayerResultsHotseatGameTableViewCell = @"PLayerResultsHotseatGameTableViewCell";
static NSString *const kShareButtonTableViewCell = @"ShareButtonTableViewCell";


@interface GameRoundFinalResultDatasource()

@property (nonatomic, strong) NSArray<NSArray<ObjectTableViewCell*>*> *data;

@end

@implementation GameRoundFinalResultDatasource
@synthesize indicators,hotseatSummary, bestPointResults, bestSpeedResults, bestQualityResults, pushHotseatSummary, players;

#pragma mark - Методы GameRoundFinalResultDatasourceInput
- (void)prepareDatasource
{
    NSMutableArray *mData=[NSMutableArray new];

    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=kResultsSoloGameRegistredTableViewCell;
    item.data=self.hotseatSummary;
    [mData addObject:item];
    
    [mData addObject:[self addSeparateCell]];
    
    item=[ObjectTableViewCell new];
    item.cellId=kResultsSoloGameTableViewCell;
    item.data=self.indicators;
    [mData addObject:item];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"self.indicators.totalScore" ascending:NO];
    self.players = [self.players sortedArrayUsingDescriptors:@[sort]];
    
    for (HFHotseatPlayer *player in self.players) {
        [mData addObject:[self addSeparateCell]];
        
        item=[ObjectTableViewCell new];
        item.cellId=kPLayerResultsHotseatGameTableViewCell;
        
        item.data=player;
        [mData addObject:item];
    }
    

    [mData addObject:[self addSeparateCell]];
    
    
    item=[ObjectTableViewCell new];
    item.cellId=kShareButtonTableViewCell;
    item.height=72;
    HFButtonData *buttonData = [HFButtonData new];
    buttonData.title = @"ПОДЕЛИТЬСЯ";
    buttonData.bgColor = [UIColor colorWithHex:@"#00B8D4"];
    item.data=buttonData;
    [mData addObject:item];
    
    _data = @[mData];
}

-(ObjectTableViewCell*)addSeparateCell
{
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=kSeparateLineTableViewCell;
    SeparateLineTableViewCellModel *separateLineData = [SeparateLineTableViewCellModel new];
    separateLineData.color1 =[UIColor colorWithHex:@"#0097A7"];
    separateLineData.color2 =[UIColor colorWithHex:@"#0097A7"];
    item.data=separateLineData;
    return item;
}

-(NSInteger)countItemsInSection:(NSInteger)section
{
    return _data[section].count;
}
-(NSInteger)count{
    return _data.count;
}


-(NSString *)cellIdAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].cellId;
}

-(id)dataAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].data;
}
-(NSArray*)cellsIdentifire{
    return @[kResultsSoloGameRegistredTableViewCell,
             kResultsSoloGameTableViewCell,
             kSeparateLineTableViewCell,
             kPLayerResultsHotseatGameTableViewCell, kShareButtonTableViewCell];
}
@end
