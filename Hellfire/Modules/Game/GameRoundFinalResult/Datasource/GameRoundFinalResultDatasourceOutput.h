//
//  GameRoundFinalResultDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HFHotseatGameResult;
@class HFPlayerIndicators;
@class HFHotseatPlayer;
@class HFPushFinalResultHotseatModel;

@protocol GameRoundFinalResultDatasourceOutput <NSObject>

@property (nonatomic, strong) HFPlayerIndicators *indicators;
@property (nonatomic, strong) HFHotseatGameResult *hotseatSummary;
@property (nonatomic) HFPushFinalResultHotseatModel *pushHotseatSummary;
@property (nonatomic) HFHotseatPlayer *bestQualityResults;
@property (nonatomic) HFHotseatPlayer *bestSpeedResults;
@property (nonatomic) HFHotseatPlayer *bestPointResults;
@property (nonatomic) NSArray<HFHotseatPlayer*> *players;

- (void)prepareDatasource;
@end


