//
//  GameRoundFinalResultDatasourceInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameRoundFinalResultDatasourceInput <NSObject>

-(NSInteger)countItemsInSection:(NSInteger)section;
-(NSString*)cellIdAtIndex:(NSIndexPath*)index;
-(id) dataAtIndex:(NSIndexPath*)index;
-(NSArray*)cellsIdentifire;

@end
