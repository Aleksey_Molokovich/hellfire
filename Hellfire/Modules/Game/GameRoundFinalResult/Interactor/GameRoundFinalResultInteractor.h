//
//  GameRoundFinalResultInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultInteractorInput.h"
@class GameService;
@class HFHotseatGameResult;

@protocol GameRoundFinalResultInteractorOutput;

@interface GameRoundFinalResultInteractor : NSObject <GameRoundFinalResultInteractorInput>

@property (nonatomic, weak) id<GameRoundFinalResultInteractorOutput> output;

@property (nonatomic, strong) GameService *gameService;

@property (nonatomic) HFHotseatGameResult *hotseatGameResult;

@end
