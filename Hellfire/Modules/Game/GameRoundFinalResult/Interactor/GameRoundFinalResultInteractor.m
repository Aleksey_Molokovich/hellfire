//
//  GameRoundFinalResultInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultInteractor.h"
#import "GameRoundFinalResultInteractorOutput.h"

#import "HFAverageAndMaxResultModel.h"
#import "HFHotseatGameResult.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "GameRepository.h"
#import "GameService.h"

@implementation GameRoundFinalResultInteractor

#pragma mark - Методы GameRoundFinalResultInteractorInput

-(HFAverageAndMaxResultModel *)getResults
{
    _gameService = [GameService new];
    
    HFAverageAndMaxResultModel *result=[HFAverageAndMaxResultModel new];
     
    return result;
    
}

@end
