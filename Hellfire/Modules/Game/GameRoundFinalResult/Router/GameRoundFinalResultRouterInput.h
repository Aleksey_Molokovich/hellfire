//
//  GameRoundFinalResultRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameRoundFinalResultRouterInput <NSObject>
- (void)closeCurrentModule;
- (void)presentShareGameId:(NSNumber*)gameId solo:(BOOL)isSolo;

@end
