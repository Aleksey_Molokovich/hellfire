//
//  GameRoundFinalResultRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ShareModuleAssembly.h"
#import "ShareModuleModuleInput.h"

@interface GameRoundFinalResultRouter()

@property (nonatomic) ShareModuleAssembly *shareModuleAssembly;

@end


@implementation GameRoundFinalResultRouter

#pragma mark - Методы GameRoundFinalResultRouterInput

- (void)presentShareGameId:(NSNumber*)gameId solo:(BOOL)isSolo
{
    _shareModuleAssembly=[[ShareModuleAssembly new] activated];
    self.factory=[_shareModuleAssembly factoryShareModule];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<ShareModuleModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithGameId:gameId solo:isSolo];
                                                  return nil;
                                              }];
}

@end
