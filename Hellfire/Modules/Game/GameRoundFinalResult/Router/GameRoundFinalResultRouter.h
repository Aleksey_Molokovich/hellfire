//
//  GameRoundFinalResultRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface GameRoundFinalResultRouter : BaseRouter <GameRoundFinalResultRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
