//
//  GameRoundFinalResultAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultAssembly.h"

#import "GameRoundFinalResultViewController.h"
#import "GameRoundFinalResultInteractor.h"
#import "GameRoundFinalResultPresenter.h"
#import "GameRoundFinalResultRouter.h"
#import "GameRoundFinalResultDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation GameRoundFinalResultAssembly

- (GameRoundFinalResultViewController *)viewGameRoundFinalResult {
    return [TyphoonDefinition withClass:[GameRoundFinalResultViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameRoundFinalResult]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterGameRoundFinalResult]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceGameRoundFinalResult]];
                          }];
}

- (GameRoundFinalResultInteractor *)interactorGameRoundFinalResult {
    return [TyphoonDefinition withClass:[GameRoundFinalResultInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameRoundFinalResult]];
                          }];
}

- (GameRoundFinalResultPresenter *)presenterGameRoundFinalResult{
    return [TyphoonDefinition withClass:[GameRoundFinalResultPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewGameRoundFinalResult]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorGameRoundFinalResult]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerGameRoundFinalResult]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceGameRoundFinalResult]];
                          }];
}

- (GameRoundFinalResultDatasource *)datasourceGameRoundFinalResult{
    return [TyphoonDefinition withClass:[GameRoundFinalResultDatasource class]];
}

- (GameRoundFinalResultRouter *)routerGameRoundFinalResult{
    return [TyphoonDefinition withClass:[GameRoundFinalResultRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewGameRoundFinalResult]];
                          }];
}

- (RamblerViperModuleFactory *)factoryGameRoundFinalResult  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardGameRoundFinalResult]];
                                                  [initializer injectParameterWith:@"GameRoundFinalResultViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardGameRoundFinalResult {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
