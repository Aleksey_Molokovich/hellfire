//
//  GameRoundFinalResultViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameRoundFinalResultViewInput.h"
#import "GameRoundFinalResultDatasourceInput.h"
#import "BaseViewController.h"

@protocol GameRoundFinalResultViewOutput;
@protocol GameRoundFinalResultDatasourceInput;

@interface GameRoundFinalResultViewController : BaseViewController <GameRoundFinalResultViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<GameRoundFinalResultViewOutput> output;
@property (nonatomic, strong) id<GameRoundFinalResultDatasourceInput> datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
