//
//  GameRoundFinalResultPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultViewOutput.h"
#import "GameRoundFinalResultInteractorOutput.h"
#import "GameRoundFinalResultModuleInput.h"
#import "GameRoundFinalResultDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol GameRoundFinalResultViewInput;
@protocol GameRoundFinalResultInteractorInput;
@protocol GameRoundFinalResultRouterInput;
@protocol GameRoundFinalResultDatasourceOutput;

@interface GameRoundFinalResultPresenter : NSObject <GameRoundFinalResultModuleInput, GameRoundFinalResultViewOutput, GameRoundFinalResultInteractorOutput>

@property (nonatomic, weak) id<GameRoundFinalResultViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<GameRoundFinalResultInteractorInput> interactor;
@property (nonatomic, strong) id<GameRoundFinalResultRouterInput> router;
@property (nonatomic, strong) id<GameRoundFinalResultDatasourceOutput> datasource;

@property (nonatomic, strong) NSNumber *gameId;

@end
