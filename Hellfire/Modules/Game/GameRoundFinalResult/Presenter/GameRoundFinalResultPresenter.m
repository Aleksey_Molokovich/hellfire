//
//  GameRoundFinalResultPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 04/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundFinalResultPresenter.h"

#import "GameRoundFinalResultViewInput.h"
#import "GameRoundFinalResultInteractorInput.h"
#import "GameRoundFinalResultRouterInput.h"
#import "HFHotseatGameResult.h"
#import "HFAverageAndMaxResultModel.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "HFPushFinalResultHotseatModel.h"
#import "ShareButtonTableViewCellAction.h"
#import "CallBackActionModel.h"
#import "AccountService.h"
@implementation GameRoundFinalResultPresenter

#pragma mark - Методы GameRoundFinalResultModuleInput

- (void)configureModuleWith:(NSDictionary *)push {
   
    
    self.gameId = push[@"GameId"];
    
    NSMutableArray<HFHotseatPlayer*> *players =[NSMutableArray new];
    NSMutableArray<HFPushHotseatPlayer*> *pushPlayers =[NSMutableArray new];
    
    for (NSDictionary *dic in push[@"Players"]) {
        NSError *error;
        HFPushHotseatPlayer *pushPlayer = [[HFPushHotseatPlayer alloc] initWithDictionary:dic error:&error];
        HFHotseatPlayer *player = [HFHotseatPlayer new];
        
        player.userName = pushPlayer.UserName;
        player.avatarPath = pushPlayer.AvatarPath;
        player.isWinner = pushPlayer.IsWinner;
        player.hotseatPoints = pushPlayer.HotseatPoints;
        player.id = pushPlayer.Id;
        player.isWinner = pushPlayer.IsWinner;
        player.points = pushPlayer.Points;
        player.indicators.totalScore = pushPlayer.Indicators.TotalScore;
        player.indicators.points.v1 = [pushPlayer.Indicators.Points.V1 intValue];
        player.indicators.points.v2 = [pushPlayer.Indicators.Points.V2 intValue];

        player.indicators.speed.v1 = [pushPlayer.Indicators.Speed.V1 intValue];
        player.indicators.speed.v2 = [pushPlayer.Indicators.Speed.V2 intValue];

        player.indicators.quality.v1 = [pushPlayer.Indicators.Quality.V1 intValue];
        player.indicators.quality.v2 = [pushPlayer.Indicators.Quality.V2 intValue];

        player.indicators.volume.v1 = [pushPlayer.Indicators.Volume.V1 intValue];
        player.indicators.volume.v2 = [pushPlayer.Indicators.Volume.V2 intValue];

        [players addObject:player];
        [pushPlayers addObject:pushPlayer];
    }
    NSSortDescriptor *sortQualityPoints = [NSSortDescriptor sortDescriptorWithKey:@"self.indicators.quality.v2" ascending:NO];
    [players sortUsingDescriptors:@[sortQualityPoints]];
    NSInteger maxQuality = [players firstObject].indicators.quality.v2;
    int index = 0;
    NSArray *sortPlayers = [players copy];
    for (HFHotseatPlayer *player in sortPlayers) {
        if (player.indicators.quality.v2 == maxQuality) {
            player.maxQuality = @(maxQuality);
            [players replaceObjectAtIndex:index withObject:player];
        }
        index++;
    }
    
    NSSortDescriptor *sortSpeedPoints = [NSSortDescriptor sortDescriptorWithKey:@"self.indicators.speed.v2" ascending:NO];
    [players sortUsingDescriptors:@[sortSpeedPoints]];
    NSInteger maxSpeed = [players firstObject].indicators.speed.v2;
    index = 0;
    sortPlayers = [players copy];
    for (HFHotseatPlayer *player in sortPlayers) {
        if (player.indicators.speed.v2 == maxSpeed) {
            player.maxSpeed = @(maxSpeed);
            [players replaceObjectAtIndex:index withObject:player];
        }
        index++;
    }

    
    NSSortDescriptor *sortTotalPoints = [NSSortDescriptor sortDescriptorWithKey:@"self.indicators.totalScore" ascending:NO];
    [players sortUsingDescriptors:@[sortTotalPoints]];
    NSInteger maxTotalScore = [players firstObject].indicators.totalScore;
    index = 0;
    sortPlayers = [players copy];
    for (HFHotseatPlayer *player in sortPlayers) {
        if (player.indicators.speed.v2 == maxTotalScore) {
            player.maxPoints = @(maxTotalScore);
            [players replaceObjectAtIndex:index withObject:player];
        }
        index++;
    }
    
    NSSortDescriptor *sortVolumePoints = [NSSortDescriptor sortDescriptorWithKey:@"self.indicators.volume.v2" ascending:NO];
    [players sortUsingDescriptors:@[sortVolumePoints]];
    NSInteger maxVolume = [players firstObject].indicators.volume.v2;
    index = 0;
    sortPlayers = [players copy];
    for (HFHotseatPlayer *player in sortPlayers) {
        if (player.indicators.volume.v2 == maxVolume) {
            player.maxVolume = @(maxVolume);
            [players replaceObjectAtIndex:index withObject:player];
        }
        index++;
    }
    
    
    HFHotseatGameResult *hotseatGameResult = [HFHotseatGameResult new];
    
    HFPushPlaceModel *place = [[HFPushPlaceModel alloc] initWithDictionary:push[@"Place"] error:nil];
    hotseatGameResult.place = [HFPlaceModel new];
    hotseatGameResult.place.id = place.Id;
    hotseatGameResult.place.name = place.Name;
    hotseatGameResult.place.address = [HFAddressModel new];
    hotseatGameResult.place.address.formattedAddress = place.Address.FormattedAddress;
    
    HFPushClanModel *clan = [[HFPushClanModel alloc] initWithDictionary:push[@"Clan"] error:nil];
    hotseatGameResult.clan = [HFClanModel new];
    hotseatGameResult.clan.name = clan.Name;
    hotseatGameResult.clan.avatar = clan.Avatar;
    hotseatGameResult.clan.points = clan.Points;

    hotseatGameResult.players = [players copy];
    
    _datasource.hotseatSummary = hotseatGameResult;
    HFHotseatPlayer *winner = [[players filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" self.isWinner == 1"]] firstObject];
    _datasource.indicators = winner.indicators;
    _datasource.players = [players filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" self.isWinner != 1"]];
    
    [_datasource prepareDatasource];
    
}

- (void)close
{
    [_router closeCurrentModule];
}
#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    
    if ([object.action isEqualToString:kShareButtonTableViewCellAction] )
    {
        if ([AccountService state] == AccountStateTypeSuccessfulRegister)
        {
            [_router presentShareGameId:self.gameId solo:NO];
        }
        else
        {
            [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
        }
        
    }
}

#pragma mark - Методы GameRoundFinalResultViewOutput

- (void)didTriggerViewReadyEvent {
    [self.view setupInitialState];

    [_view update];
}

#pragma mark - Методы GameRoundFinalResultInteractorOutput

@end
