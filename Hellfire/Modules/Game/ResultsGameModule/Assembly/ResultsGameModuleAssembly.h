//
//  ResultsGameModuleAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 ResultsGameModule module
 */
@interface ResultsGameModuleAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryResultsGameModule;
@end
