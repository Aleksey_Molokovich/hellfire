//
//  ResultsGameModuleAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleAssembly.h"

#import "ResultsGameModuleViewController.h"
#import "ResultsGameModuleInteractor.h"
#import "ResultsGameModulePresenter.h"
#import "ResultsGameModuleRouter.h"
#import "ResultsGameModuleDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ResultsGameModuleAssembly

- (ResultsGameModuleViewController *)viewResultsGameModule {
    return [TyphoonDefinition withClass:[ResultsGameModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterResultsGameModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterResultsGameModule]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceResultsGameModule]];
                          }];
}

-(ResultsGameModuleDatasource *)datasourceResultsGameModule{
    return [TyphoonDefinition withClass:[ResultsGameModuleDatasource class]];
}

- (ResultsGameModuleInteractor *)interactorResultsGameModule {
    return [TyphoonDefinition withClass:[ResultsGameModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterResultsGameModule]];
                          }];
}

- (ResultsGameModulePresenter *)presenterResultsGameModule{
    return [TyphoonDefinition withClass:[ResultsGameModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewResultsGameModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorResultsGameModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerResultsGameModule]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceResultsGameModule]];
                          }];
}

- (ResultsGameModuleRouter *)routerResultsGameModule{
    return [TyphoonDefinition withClass:[ResultsGameModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewResultsGameModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryResultsGameModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardResultsGameModule]];
                                                  [initializer injectParameterWith:@"ResultsGameModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardResultsGameModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
