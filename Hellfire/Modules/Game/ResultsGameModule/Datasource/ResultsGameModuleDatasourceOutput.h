//
//  ResultsGameModuleDatasourceOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef ResultsGameModuleDatasourceOutput_h
#define ResultsGameModuleDatasourceOutput_h
@class HFSoloGameResult;
@class HFHotseatGameResult;
@class HFPlayerIndicators;
@class HFHotseatPlayer;

@protocol ResultsGameModuleDatasourceOutput<NSObject>

@property (nonatomic, assign) BOOL isRegistred;
@property (nonatomic, assign) BOOL isSolo;
@property (nonatomic, assign) BOOL isSendData;
@property (nonatomic, strong) HFPlayerIndicators *indicators;
@property (nonatomic, strong) HFSoloGameResult *soloSummary;
@property (nonatomic, strong) HFHotseatGameResult *hotseatSummary;
@property (nonatomic) HFHotseatPlayer *bestQualityResults;
@property (nonatomic) HFHotseatPlayer *bestSpeedResults;
@property (nonatomic) HFHotseatPlayer *bestPointResults;
- (void)prepareDatasource;
@end

#endif /* ResultsGameModuleDatasourceOutput_h */
