//
//  ResultsGameModuleDatasourceInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef ResultsGameModuleDatasourceInput_h
#define ResultsGameModuleDatasourceInput_h
@class ObjectTableViewCell;
@protocol ResultsGameModuleDatasourceInput<NSObject>

@property (nonatomic, assign, readonly) NSInteger count;
-(ObjectTableViewCell*) itemAtIndex:(NSInteger)index;
-(NSArray*)cellIds;
@end

#endif /* ResultsGameModuleDatasourceInput_h */
