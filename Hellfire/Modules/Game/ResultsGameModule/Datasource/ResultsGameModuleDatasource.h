//
//  ResultsGameModuleDatasource.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFAverageAndMaxResultModel.h"
#import "ObjectTableViewCell.h"
#import "ResultsGameModuleDatasourceOutput.h"
#import "ResultsGameModuleDatasourceInput.h"





@interface ResultsGameModuleDatasource : NSObject<ResultsGameModuleDatasourceOutput,ResultsGameModuleDatasourceInput>

@property (nonatomic, strong) NSArray *datasource;

@end
