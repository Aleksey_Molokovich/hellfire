//
//  ResultsGameModuleDatasource.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleDatasource.h"
#import "HFButtonData.h"
#import "UIColor+HF.h"
#import "HFSoloGameResult.h"
#import "MainAchievementModel.h"
#import "CDGameResults+CoreDataClass.h"
#import "SeparateLineTableViewCellModel.h"
#import "HFHotseatPlayer.h"
#import "HFHotseatGameResult.h"

static NSString *const CellIdHeader = @"ResultsSoloGameHeaderTableViewCell";
static NSString *const CellIdRegistredHeader = @"ResultsSoloGameRegistredTableViewCell";

static NSString *const CellIdResults = @"ResultsSoloGameTableViewCell";
static NSString *const CellIdButton = @"ShareButtonTableViewCell";
static NSString *const kUserAchievementTableViewCell = @"UserAchievementTableViewCell";
static NSString *const kSeparateLineTableViewCell = @"SeparateLineTableViewCell";
static NSString *const kPLayerResultsHotseatGameTableViewCell = @"PLayerResultsHotseatGameTableViewCell";

@implementation ResultsGameModuleDatasource
@synthesize soloSummary,indicators, isRegistred,hotseatSummary, bestPointResults, bestSpeedResults, bestQualityResults, isSolo, isSendData;

- (void)prepareDatasource
{
    NSMutableArray *mData=[NSMutableArray new];
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    if (isRegistred)
    {
        item.cellId=CellIdRegistredHeader;

        if (self.isSolo)
        {
            item.data=self.soloSummary;
        }
        else
        {
            item.data=self.hotseatSummary;
        }
        
        [mData addObject:item];
    }
    else
    {
        item.cellId=CellIdHeader;
        item.height=120;
        item.data=@"";
        [mData addObject:item];
    }
    
    [mData addObject:[self separateLine]];
    
    item=[ObjectTableViewCell new];
    item.cellId=CellIdResults;
    item.data=self.indicators;
    [mData addObject:item];

    
    
    
    if (self.hotseatSummary) {
        
        [mData addObject:[self separateLine]];
        
        for (HFHotseatPlayer *player in self.hotseatSummary.players) {
            if (player.isWinner) {
                continue;
            }
            
            item=[ObjectTableViewCell new];
            item.cellId=kPLayerResultsHotseatGameTableViewCell;
            
            if (bestQualityResults && [player.id isEqualToNumber:bestQualityResults.id]) {
                player.maxQuality = bestQualityResults.maxQuality;
            }
            
            if (bestSpeedResults && [player.id isEqualToNumber:bestSpeedResults.id]) {
                player.maxSpeed = bestSpeedResults.maxSpeed;
            }
            
            if (bestPointResults && [player.id isEqualToNumber:bestPointResults.id]) {
                player.maxPoints = bestPointResults.maxPoints;
            }
            
            item.data=player;
            [mData addObject:item];
        }
    }

    [mData addObject:[self separateLine]];
    
    item=[ObjectTableViewCell new];
    item.cellId=CellIdButton;
    item.height=72;
    HFButtonData *buttonData = [HFButtonData new];
    buttonData.disable = !self.isSendData;
    buttonData.title = @"ПОДЕЛИТЬСЯ";
    if (self.isSolo) {
       buttonData.bgColor = [UIColor hfLightGreen];
    }else
    {
        buttonData.bgColor = [UIColor colorWithHex:@"#00B8D4"];
    }
    item.data=buttonData;
    [mData addObject:item];
    
    _datasource = [mData copy];

}

-(ObjectTableViewCell*)separateLine
{
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=kSeparateLineTableViewCell;
    SeparateLineTableViewCellModel *separateLineData = [SeparateLineTableViewCellModel new];
    separateLineData.color1 =[UIColor colorWithHex:self.isSolo?@"#008B7B":@"#0097A7"];
    separateLineData.color2 =[UIColor colorWithHex:self.isSolo?@"#008B7B":@"#0097A7"];
    item.data=separateLineData;
    return item;
}

-(NSInteger)count{
    return _datasource.count;
}

-(ObjectTableViewCell *)itemAtIndex:(NSInteger)index{
    return _datasource[index];
}

-(NSArray*)cellIds
{
    return @[CellIdHeader,CellIdResults,CellIdButton,CellIdRegistredHeader, kUserAchievementTableViewCell, kSeparateLineTableViewCell, kPLayerResultsHotseatGameTableViewCell];
}



@end
