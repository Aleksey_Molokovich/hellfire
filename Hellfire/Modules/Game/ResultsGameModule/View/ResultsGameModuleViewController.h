//
//  ResultsGameModuleViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultsGameModuleViewInput.h"
#import "BaseViewController.h"
#import "ResultsGameModuleDatasourceInput.h"

@protocol ResultsGameModuleViewOutput;

@interface ResultsGameModuleViewController : BaseViewController <ResultsGameModuleViewInput>

@property (nonatomic, strong) id<ResultsGameModuleViewOutput> output;
@property (nonatomic, strong) id<ResultsGameModuleDatasourceInput> datasource;
@end
