//
//  ResultsGameModuleViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleViewController.h"
#import "BaseTableViewCell.h"
#import "ResultsGameModuleViewOutput.h"
#import "CellManager.h"
#import "BaseViewController+Game.h"
#import "ObjectTableViewCell.h"

@interface ResultsGameModuleViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveExitBtn;

@end

@implementation ResultsGameModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    [CellManager registerCellsWithId:[_datasource cellIds]
                           tableView:_tableView];
	[self.output didTriggerViewReadyEvent];
}
- (IBAction)goOut:(id)sender {
    [self goOut];
}

- (void)goBack
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.output.isLoadingData){
        [self showProgressWithColor:[UIColor whiteColor]];
    }else{
        [self hideProgress];
    }
    
}
#pragma mark - Методы ResultsGameModuleViewInput

- (void)setupInitialState {
    
    if ([_output isSoloGame]) {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorGreen];
    }
    else
    {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorGreenHotseat];
    }
    [self hideBackButton];
    [self hideBottomBar];
}

- (void)didSaveResult
{
    _saveExitBtn.image = [UIImage imageNamed:@"check"];
    _saveExitBtn.action = @selector(goBack);
}

- (void)reloadData
{
    [_tableView reloadData];
}

#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource itemAtIndex:indexPath.row].cellId forIndexPath:indexPath];
    [cell configureWithData:[_datasource itemAtIndex:indexPath.row].data];
    cell.output = _output;
    cell.cellIndex=indexPath;
    
    return cell;
}

-(void)alertFailSendGameData
{
    self.alert = [[LGAlertView alloc] initWithTitle:nil
                                        message:@"При отправке данных произошла ошибка. Повторить?"
                                          style:LGAlertViewStyleActionSheet
                                   buttonTitles:@[@"Да"]
                              cancelButtonTitle:@"Отмена"
                         destructiveButtonTitle:nil] ;
    self.alert.tag = 2;
    self.alert.delegate = self;
    [self.alert showAnimated];
}

#pragma mark - Методы LGAlertViewDelegate
- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    [_output alertWithTag:alertView.tag clickedButtonAtIndex:index];
}

@end
