//
//  ResultsGameModuleViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerProtocol.h"

@protocol ResultsGameModuleViewInput <BaseViewControllerProtocol>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)reloadData;
- (void)didSaveResult;
- (void)alertFailSendGameData;
@end
