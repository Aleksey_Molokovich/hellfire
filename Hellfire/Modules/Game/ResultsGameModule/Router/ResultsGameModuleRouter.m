//
//  ResultsGameModuleRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ShareModuleAssembly.h"
#import "ShareModuleModuleInput.h"
@interface ResultsGameModuleRouter()

@property (nonatomic) ShareModuleAssembly *shareModuleAssembly;
@end


@implementation ResultsGameModuleRouter

#pragma mark - Методы ResultsGameModuleRouterInput


- (void)presentShareGameId:(NSNumber*)gameId solo:(BOOL)isSolo
{
    _shareModuleAssembly=[[ShareModuleAssembly new] activated];
    self.factory=[_shareModuleAssembly factoryShareModule];
    
    [self presentModuleWithNavigationControllerUsingFactory:self.factory
                                              withLinkBlock:^id<RamblerViperModuleOutput>(id<ShareModuleModuleInput> moduleInput) {
                                                  [moduleInput configureModuleWithGameId:gameId solo:isSolo];
                                                  return nil;
                                              }];
}
@end
