//
//  ResultsGameModuleRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ResultsGameModuleRouterInput <NSObject>
- (void)presentRegistration;
- (void)presentShareGameId:(NSNumber*)gameId solo:(BOOL)isSolo;
@end
