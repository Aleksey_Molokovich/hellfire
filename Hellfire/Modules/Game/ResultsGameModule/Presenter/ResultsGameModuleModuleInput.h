//
//  ResultsGameModuleModuleInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol ResultsGameModuleModuleInput <RamblerViperModuleInput>

/**
 @author AlekseyMolokovich

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
