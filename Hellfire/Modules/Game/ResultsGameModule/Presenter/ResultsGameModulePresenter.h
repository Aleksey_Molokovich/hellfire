//
//  ResultsGameModulePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleViewOutput.h"
#import "ResultsGameModuleInteractorOutput.h"
#import "ResultsGameModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"
#import "ResultsGameModuleDatasourceOutput.h"
#import "GameService.h"
@protocol BaseViewControllerProtocol;
@protocol ResultsGameModuleViewInput;
@protocol ResultsGameModuleInteractorInput;
@protocol ResultsGameModuleRouterInput;

@interface ResultsGameModulePresenter : NSObject <ResultsGameModuleModuleInput, ResultsGameModuleViewOutput, ResultsGameModuleInteractorOutput>

@property (nonatomic, weak) id<ResultsGameModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<ResultsGameModuleInteractorInput> interactor;
@property (nonatomic, strong) id<ResultsGameModuleRouterInput> router;
@property (nonatomic, strong) id<ResultsGameModuleDatasourceOutput> datasource;

@property (nonatomic, strong) GameService *gameService;
@property (nonatomic) NSNumber *gameId;
@end
