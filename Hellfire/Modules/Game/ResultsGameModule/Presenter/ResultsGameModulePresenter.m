//
//  ResultsGameModulePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModulePresenter.h"

#import "ResultsGameModuleViewInput.h"
#import "ResultsGameModuleInteractorInput.h"
#import "ResultsGameModuleRouterInput.h"
#import "HFAverageAndMaxResultModel.h"
#import "AccountService.h"
#import "HFGameSettings.h"
#import "ShareButtonTableViewCellAction.h"
#import "HFSoloGameResult.h"
#import "HFHotseatGameResult.h"
#import "CDUser+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
@implementation ResultsGameModulePresenter
@synthesize isLoadingData = _isLoadingData;
#pragma mark - Методы ResultsGameModuleModuleInput

- (void)configureModule
{
    
}

#pragma mark - Методы ResultsGameModuleViewOutput

- (void)didTriggerViewReadyEvent {
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showRegistredUser)
     name:HFNotificationChangeAccountStatus object:nil];
    _datasource.isRegistred = NO;
    _datasource.isSolo = [self isSoloGame];
    _datasource.isSendData = NO;
    if ([AccountService state] == AccountStateTypeSuccessfulRegister)
    {
        _datasource.isRegistred = YES;
        self.isLoadingData = YES;
        [_interactor sendGameResult];
    }
    
    if ([self isSoloGame]) {
        self.datasource.soloSummary = [HFSoloGameResult new];
        self.datasource.soloSummary.user.avatarPath = [AccountService account].avatarPath;
        self.datasource.soloSummary.user.userName = [AccountService account].user.nickname;
    }
    
    _datasource.indicators = [_interactor getResults];
    [_datasource prepareDatasource];

	[self.view setupInitialState];
}

-(BOOL)isLoadingData{
    return _isLoadingData;
}

- (void)setIsLoadingData:(BOOL)isLoadingData
{
    _isLoadingData = isLoadingData;
    if (isLoadingData) {
        [_view showProgressWithColor:[UIColor whiteColor]];
    }else{
        [_view hideProgress];
    }
}

-(BOOL)isSoloGame
{
    return [HFGameSettings load].gameId>0?NO:YES;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showRegistredUser
{
    if ([AccountService state] == AccountStateTypeSuccessfulRegister)
    {
        _datasource.isRegistred = YES;
        self.isLoadingData = YES;
        [_interactor sendGameResult];
    }
}

- (void)alertWithTag:(NSInteger)tag clickedButtonAtIndex:(NSInteger)index
{
    switch (tag) {
        case 0:
            if (index == 0)
            {
                [_router presentRegistration];
            }
            break;
        case 2:
            if (index == 0)
            {
                [self.interactor sendGameResult];
            }
            break;
            
        default:
            break;
    }
    
    
}
#pragma mark - Методы ResultsGameModuleInteractorOutput

- (void)successfulWithSoloGameResult:(HFSoloGameResult *)data
{
    self.gameId = data.id;
    self.isLoadingData = NO;
    _datasource.isSendData = YES;
    _datasource.soloSummary = data;
    [_datasource prepareDatasource];
    [_view didSaveResult];
    [_view reloadData];
}

- (void)successfulWithHotseatGameResult:(HFHotseatGameResult *)data
{
    self.gameId = [_interactor currentGameId];
    self.isLoadingData = NO;
    _datasource.isSendData = YES;
    _datasource.hotseatSummary = data;
    _datasource.bestQualityResults = [_interactor totalBestQualityResults];
    _datasource.bestSpeedResults = [_interactor totalBestSpeedResults];
    _datasource.bestPointResults = [_interactor totalBestPointResults];
    _datasource.indicators = [_interactor getResults];
    [_datasource prepareDatasource];
    [_view didSaveResult];
    [_view reloadData];
}

- (void)fail
{
    self.isLoadingData = NO;
    [_datasource prepareDatasource];
    [_view reloadData];
    [_view alertFailSendGameData];
}
#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{
    if (object.cellIndex.row == 0 )
    {
        [_router presentRegistration];
    }
    
    if ([object.action isEqualToString:kShareButtonTableViewCellAction] )
    {
        if ([AccountService state] == AccountStateTypeSuccessfulRegister)
        {
            if (self.gameId) {
                [_router presentShareGameId:self.gameId solo:[self isSoloGame]];
            }
        }
        else
        {
            [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
        }
        
    }
}




@end
