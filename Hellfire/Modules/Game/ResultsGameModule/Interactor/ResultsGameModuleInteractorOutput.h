//
//  ResultsGameModuleInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HFSoloGameResult;
@class HFHotseatGameResult;
@protocol ResultsGameModuleInteractorOutput <NSObject>
- (void)successfulWithSoloGameResult:(HFSoloGameResult*)data;
- (void)successfulWithHotseatGameResult:(HFHotseatGameResult*)data;
- (void)fail;
- (void)showProgess;
- (void)hideProgess;
@end
