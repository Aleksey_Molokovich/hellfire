//
//  ResultsGameModuleInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleInteractorInput.h"
@class GameService;
@class HFHotseatGameResult;
@protocol ResultsGameModuleInteractorOutput;

@interface ResultsGameModuleInteractor : NSObject <ResultsGameModuleInteractorInput>

@property (nonatomic, weak) id<ResultsGameModuleInteractorOutput> output;

@property (nonatomic, strong) GameService *gameService;

@property (nonatomic) HFHotseatGameResult *hotseatGameResult;

@property (nonatomic) NSNumber *soloGameId;
@end
