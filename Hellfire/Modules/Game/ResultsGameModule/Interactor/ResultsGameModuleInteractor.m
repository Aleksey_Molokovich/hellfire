//
//  ResultsGameModuleInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsGameModuleInteractor.h"
#import "HFPlayerIndicators.h"
#import "ResultsGameModuleInteractorOutput.h"
#import "CDGameResults+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
#import "HFGameSettings.h"

#import "GameRepository.h"
#import "GameRequest.h"
#import "BluetoothService.h"
#import "GameService.h"
#import "HFHotseatGameResult.h"
#import "HFSoloGameResult.h"

@implementation ResultsGameModuleInteractor

#pragma mark - Методы ResultsGameModuleInteractorInput

-(HFPlayerIndicators *)getResults
{
    _gameService = [GameService new];
    
    HFPlayerIndicators *indicators=[HFPlayerIndicators new];
    
    if ([_gameService isSoloGame]) {
        HFGameSettings *settings = [HFGameSettings load];
        
        for (int i=1 ; i<=settings.countRounds; i++) {

            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentRound = %ld", i];
            CDGameResults *cdResult = [CDGameResults MR_findFirstWithPredicate:predicate];
            
            
            if (indicators.quality.v2<cdResult.maxQuality)
            {
                indicators.quality.v2 = cdResult.maxQuality;
            }
            if (indicators.speed.v2<cdResult.maxSpeed)
            {
                indicators.speed.v2 = cdResult.maxSpeed;
            }

            if (indicators.points.v2<cdResult.maxPoints)
            {
                indicators.points.v2 = cdResult.maxPoints;
            }

            indicators.totalScore += cdResult.points;
            indicators.quality.v1 += cdResult.quality;
            indicators.speed.v1 += cdResult.speed;
            indicators.countTiks += cdResult.tiksCount;
            indicators.volume.v1 += cdResult.volume;
            indicators.volume.v2 += cdResult.volume;
        }
        indicators.countRounds = settings.countRounds;
        indicators.speed.v1 = (int)(indicators.speed.v1/indicators.countTiks);
        indicators.quality.v1 = (int)(indicators.quality.v1/indicators.countTiks);
        indicators.points.v1 = (int)(indicators.totalScore/indicators.countRounds);
    }
    else
    {
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"self.isWinner == 1"];
        HFHotseatPlayer *winner = [[_hotseatGameResult.players filteredArrayUsingPredicate:filter] firstObject] ;
        indicators = winner.indicators;
        
    }
    return indicators;
}

- (NSNumber*)currentGameId
{
    HFGameSettings *settings = [HFGameSettings load];
    return @(settings.gameId);
}

- (CDAccount*)account
{
    return [CDAccount MR_findFirst];
}

- (void)sendGameResult
{
    if (!_gameService) {
        _gameService = [GameService new];
    }
    
    __weak typeof(self) weakSelf = self;

    if ([_gameService isSoloGame]) {
        [_gameService sendSoloGameResultsWithCompletion:^(HFSoloGameResult *receive) {
            __strong typeof(weakSelf) blockSelf = weakSelf;
            if (!blockSelf) return;
            if (receive)
            {
                blockSelf.soloGameId = receive.id;
                [blockSelf.output successfulWithSoloGameResult:receive];
            }
            else
            {
                [blockSelf.output fail];
            }
        }];
    }
    else
    {
        [_gameService sendCurrentHotseatRoundResultsWithCompletion:^(HFHotseatGameResult *result) {
            __strong typeof(weakSelf) blockSelf = weakSelf;
            if (!blockSelf) return;
            if (result)
            {
                blockSelf.hotseatGameResult = result;
                [blockSelf.output successfulWithHotseatGameResult:result];
            }
            else
            {
                [blockSelf.output fail];
            }
        }];
    }
    
}

- (HFHotseatPlayer*)totalBestSpeedResults
{
    HFHotseatPlayer *bestPlayer = [_hotseatGameResult.players firstObject];
    for (HFHotseatPlayer *player in _hotseatGameResult.players) {
        if (bestPlayer.indicators.speed.v2<player.indicators.speed.v2) {
            bestPlayer = player;
        }
    }
    bestPlayer.maxSpeed = @(bestPlayer.indicators.speed.v2);
    return bestPlayer.isWinner?nil:bestPlayer;
}
- (HFHotseatPlayer*)totalBestPointResults
{
    HFHotseatPlayer *bestPlayer = [_hotseatGameResult.players firstObject];
    for (HFHotseatPlayer *player in _hotseatGameResult.players) {
        if (bestPlayer.indicators.points.v2<player.indicators.points.v2) {
            bestPlayer = player;
        }
    }
    bestPlayer.maxPoints = @(bestPlayer.indicators.points.v2);
    return bestPlayer.isWinner?nil:bestPlayer;
}
- (HFHotseatPlayer*)totalBestQualityResults
{
    HFHotseatPlayer *bestPlayer = [_hotseatGameResult.players firstObject];
    for (HFHotseatPlayer *player in _hotseatGameResult.players) {
        if (bestPlayer.indicators.quality.v2<player.indicators.quality.v2) {
            bestPlayer = player;
        }
    }
    bestPlayer.maxQuality = @(bestPlayer.indicators.quality.v2);
    return bestPlayer.isWinner?nil:bestPlayer;
}

- (void)shareWithMessage:(NSString*)message
{
    NSError *error;
//    NSData *data = [NSData dataWithContentsOfURL:<#(nonnull NSURL *)#> 
    [_gameService shareSoloGame:_soloGameId completion:^(HFSoloGameResult *data) {
        
    }];
}

@end
