//
//  ResultsGameModuleInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 03/10/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HFPlayerIndicators;
@class CDAccount;
@class HFHotseatPlayer;
@protocol ResultsGameModuleInteractorInput <NSObject>

- (HFPlayerIndicators*)getResults;
- (CDAccount*)account;
- (void)sendGameResult;
- (void)shareWithMessage:(NSString*)message;
- (NSNumber*)currentGameId;

- (HFHotseatPlayer*)totalBestSpeedResults;
- (HFHotseatPlayer*)totalBestPointResults;
- (HFHotseatPlayer*)totalBestQualityResults;
@end
