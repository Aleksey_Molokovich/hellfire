//
//  ShareModuleViewInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShareModuleViewInput <NSObject>

/**
 @author Алексей Молокович

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)showImage:(UIImage *)image imageUrl:(NSString*)imageUrl;
@end
