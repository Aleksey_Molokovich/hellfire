//
//  ShareModuleViewController.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareModuleViewInput.h"
#import "BaseViewController.h"

@protocol ShareModuleViewOutput;

@interface ShareModuleViewController : BaseViewController <ShareModuleViewInput, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) id<ShareModuleViewOutput> output;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightImage;

@property (nonatomic) NSString *text;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic, assign) BOOL isNeedAlert;
@property (nonatomic, assign) BOOL isNeedProgress;
@end
