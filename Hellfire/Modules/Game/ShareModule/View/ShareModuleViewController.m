//
//  ShareModuleViewController.m
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModuleViewController.h"
#import "ShareModuleViewOutput.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <VK_ios_sdk/VKSdk.h>

typedef NS_ENUM(NSInteger, ShareDialogType) {
    ShareDialogTypeNone,
    ShareDialogTypeVK,
    ShareDialogTypeFB
    
};

@interface ShareModuleViewController()<  FBSDKSharingDelegate,VKSdkDelegate,VKSdkUIDelegate>

@property (nonatomic, assign) ShareDialogType shareDialogType;

@end

@implementation ShareModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTextAlert)];
    [self.view addGestureRecognizer:tap];
    self.isNeedAlert = YES;
	[self.output didTriggerViewReadyEvent];
    
//     [VKSdk forceLogout];
    [[VKSdk initializeWithAppId:@"6132082"] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    
   
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    switch (self.shareDialogType) {
        case ShareDialogTypeVK:
            [self openShareVKDialog];
            self.shareDialogType= ShareDialogTypeNone;
            break;
        case ShareDialogTypeFB:
            self.shareDialogType= ShareDialogTypeNone;
            break;
            
        default:
            break;
    }
    
}
#pragma mark - Методы ShareModuleViewInput

- (void)setupInitialState {
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGray];
}


- (void)showImage:(UIImage *)image imageUrl:(NSString*)imageUrl
{
    if (image) {
        [self hideProgress];
        if (_isNeedAlert) {
            [self showTextAlert];
        }
        _isNeedAlert = NO;
        _imageUrl = imageUrl;
        
        
        self.imageView.image = image;
        CGFloat aspect = self.view.frame.size.width/image.size.width;
        self.constraintHeightImage.constant = self.imageView.image.size.height*aspect;
        [self.imageView updateConstraints];
    }
   
}
- (IBAction)close:(id)sender {
    [self.output close];
}

- (IBAction)share:(id)sender {
    [self shareAlert];
    
    
}

- (void)showTextAlert
{
    self.alert = [[LGAlertView alloc] initWithTextFieldsAndTitle:@"Опубликовать результаты"
                                                                     message:@"Введите сообщение для публикации"
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index) {

                                                          textField.tag = index;
                                                          textField.delegate = self;
                                                          textField.enablesReturnKeyAutomatically = YES;
                                                          textField.autocapitalizationType = NO;
                                                          textField.autocorrectionType = NO;
                                                      }
                                                                buttonTitles:@[@"ОК"]
                                                           cancelButtonTitle:@"Отмена"
                                                      destructiveButtonTitle:nil
                                                                    delegate:self];
    self.alert.tag =0;

    [self.alert showAnimated:YES completionHandler:nil];
}


- (void)shareVK
{
    __weak typeof(self) weakSelf = self;
    
    NSArray *scope = @[VK_PER_PHOTOS, VK_PER_FRIENDS, VK_PER_EMAIL, VK_PER_MESSAGES, VK_PER_WALL];
    [VKSdk wakeUpSession:scope completeBlock:^(VKAuthorizationState state, NSError *err) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (state == VKAuthorizationAuthorized) {
            [blockSelf openShareVKDialog];
        } else {
            [VKSdk authorize:scope];
        }
    }];
}

- (void)openShareVKDialog
{
    VKShareDialogController * shareDialog = [VKShareDialogController new];
    shareDialog.dismissAutomatically = YES;
    shareDialog.uploadImages = @[ [VKUploadImage uploadImageWithImage:self.imageView.image andParams:[VKImageParameters jpegImageWithQuality:0.5] ] ];
    shareDialog.completionHandler = ^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        if (result == VKShareDialogControllerResultDone) {
            [self showAlertWithMessage:@"Пост опубликован"];
        }else if (result == VKShareDialogControllerResultCancelled){
            [self showAlertWithMessage:@"Пост не опубликован"];
        }else{
            [self showAlertWithMessage:@"ошибка при публикации"];
        }
        
    } ;
    
    [self presentViewController:shareDialog animated:YES completion:nil];
}

- (void)shareFB
{

    BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbshareextension://"]];
    
    if (isInstalled) {
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = _imageView.image;
        photo.userGenerated = YES;
        FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
        content.photos = @[photo];
        
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.fromViewController = self;
        dialog.shareContent = content;
        dialog.delegate = self;
        dialog.mode = FBSDKLoginBehaviorNative;
        [dialog show];
    }else{
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"]
                               fromViewController:self
                                          handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                              FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                              content.contentURL = [NSURL URLWithString:self.imageUrl];
                                              [FBSDKShareDialog showFromViewController:self
                                                                           withContent:content
                                                                              delegate:self];
                                          }];
    }

    
}


- (void)shareAlert
{
    self.alert = [[LGAlertView alloc] initWithTitle:@"Поделиться"
                                            message:nil
                                              style:LGAlertViewStyleAlert
                                       buttonTitles:@[@"Вконтакте",@"Facebook",@"Отправить сообщением"]
                                  cancelButtonTitle:nil
                             destructiveButtonTitle:nil
                                           delegate:self];
    self.alert.tag =1;
    self.alert.delegate = self;
    [self.alert showAnimated:YES completionHandler:nil];
}

- (void)hideProgress
{
    [super hideProgress];
    _isNeedProgress = NO;
}

- (void)showProgressWithColor:(UIColor *)color
{
    if (_isNeedProgress) {
        [super showProgressWithColor:color];
    }
}

- (void)alertViewDidDismiss:(LGAlertView *)alertView
{
    
    [self showProgressWithColor:[UIColor whiteColor]];
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    if (alertView.tag) {
        switch (index) {
            case 0:
                [self shareVK];
                break;
            case 1:
                [self shareFB];
                break;
            case 2:
                [self showShareWithUrl:_imageUrl  text:_text image:_imageView.image];
                break;
                
            default:
                break;
        }
    }else{
        self.isNeedProgress = YES;
      [self.output updateImageWithText:self.text];
    }

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return YES;
}

#pragma mark - Методы FBSDKLoginButton



- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [VKSdk authorize:@[VK_PER_PHOTOS, VK_PER_FRIENDS, VK_PER_EMAIL, VK_PER_MESSAGES, VK_PER_WALL]];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token) {
        self.shareDialogType = ShareDialogTypeVK;
    } else if (result.error) {
        [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"Access denied\n%@", result.error] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller{
    //    https://github.com/VKCOM/vk-ios-sdk/issues/284#issuecomment-171597916
    [self presentViewController:controller animated:YES completion:nil];
}



- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    [self showAlertWithMessage:@"Пост опубликован"];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    if (error) {
        [self showAlertWithMessage:@"ошибка при публикации"];
    }else{
       [self showAlertWithMessage:@"Пост не опубликован"];
    }
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    if (!sharer.shouldFailOnDataError) {
        [self showAlertWithMessage:@"ошибка при публикации"];
    }else{
        [self showAlertWithMessage:@"Пост опубликован"];
    }
}
@end
