//
//  ShareModuleViewOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShareModuleViewOutput <NSObject>

/**
 @author Алексей Молокович

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)close;
- (void)updateImageWithText:(NSString *)text;
@end
