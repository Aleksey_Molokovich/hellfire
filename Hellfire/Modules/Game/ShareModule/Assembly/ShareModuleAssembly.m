//
//  ShareModuleAssembly.m
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModuleAssembly.h"

#import "ShareModuleViewController.h"
#import "ShareModuleInteractor.h"
#import "ShareModulePresenter.h"
#import "ShareModuleRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ShareModuleAssembly

- (ShareModuleViewController *)viewShareModule {
    return [TyphoonDefinition withClass:[ShareModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterShareModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterShareModule]];
                          }];
}

- (ShareModuleInteractor *)interactorShareModule {
    return [TyphoonDefinition withClass:[ShareModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterShareModule]];
                          }];
}

- (ShareModulePresenter *)presenterShareModule{
    return [TyphoonDefinition withClass:[ShareModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewShareModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorShareModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerShareModule]];
                          }];
}

- (ShareModuleRouter *)routerShareModule{
    return [TyphoonDefinition withClass:[ShareModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewShareModule]];
                          }];
}

- (RamblerViperModuleFactory *)factoryShareModule  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardShareModule]];
                                                  [initializer injectParameterWith:@"ShareModuleViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardShareModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
