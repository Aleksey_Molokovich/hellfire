//
//  ShareModuleModuleInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol ShareModuleModuleInput <RamblerViperModuleInput>

/**
 @author Алексей Молокович

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithGameId:(NSNumber*)gameId solo:(BOOL)isSolo;

@end
