//
//  ShareModulePresenter.m
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModulePresenter.h"

#import "ShareModuleViewInput.h"
#import "ShareModuleInteractorInput.h"
#import "ShareModuleRouterInput.h"

@implementation ShareModulePresenter

#pragma mark - Методы ShareModuleModuleInput

- (void)configureModuleWithGameId:(NSNumber *)gameId solo:(BOOL)isSolo {

    _isSolo = isSolo;
    _gameId = gameId;
    [self.interactor loadShareImageForGame:_gameId solo:_isSolo text:@""];
    
}

#pragma mark - Методы ShareModuleViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.view showProgressWithColor:[UIColor whiteColor]];
}

- (void)updateImageWithText:(NSString *)text
{
    if (_isClose)
        return;
    
//    [self.view showProgressWithColor:[UIColor whiteColor]];
    [self.interactor loadShareImageForGame:_gameId solo:_isSolo text:text];
}
- (void)close
{
    self.isClose = YES;
    [self.view hideProgress];
    [self.router closeCurrentModule];
}

- (void)succesfulLoadImage:(UIImage *)image imageUrl:(NSString*)imageUrl
{
    if (_isClose)
        return;
    
    [self.view showImage:image imageUrl:imageUrl];
}
#pragma mark - Методы ShareModuleInteractorOutput

@end
