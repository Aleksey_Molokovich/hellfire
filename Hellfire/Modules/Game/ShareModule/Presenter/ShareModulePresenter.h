//
//  ShareModulePresenter.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModuleViewOutput.h"
#import "ShareModuleInteractorOutput.h"
#import "ShareModuleModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol ShareModuleViewInput;
@protocol ShareModuleInteractorInput;
@protocol ShareModuleRouterInput;

@interface ShareModulePresenter : NSObject <ShareModuleModuleInput, ShareModuleViewOutput, ShareModuleInteractorOutput>

@property (nonatomic, weak) id<ShareModuleViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<ShareModuleInteractorInput> interactor;
@property (nonatomic, strong) id<ShareModuleRouterInput> router;

@property (nonatomic) NSNumber *gameId;
@property (nonatomic) BOOL isSolo;
@property (nonatomic) BOOL isClose;
@end
