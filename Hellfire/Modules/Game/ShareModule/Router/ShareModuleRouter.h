//
//  ShareModuleRouter.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModuleRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface ShareModuleRouter : BaseRouter <ShareModuleRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
