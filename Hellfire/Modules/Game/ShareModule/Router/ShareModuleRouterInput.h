//
//  ShareModuleRouterInput.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShareModuleRouterInput <NSObject>
- (void)closeCurrentModule;
@end
