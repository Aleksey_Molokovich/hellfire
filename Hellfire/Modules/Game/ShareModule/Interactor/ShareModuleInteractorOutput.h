//
//  ShareModuleInteractorOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShareModuleInteractorOutput <NSObject>

- (void)succesfulLoadImage:(UIImage *)image imageUrl:(NSString*)imageUrl;

@end
