//
//  ShareModuleInteractor.m
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModuleInteractor.h"
#import "ShareModuleInteractorOutput.h"
#import "EncryptService.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "HTTPService.h"
@implementation ShareModuleInteractor

#pragma mark - Методы ShareModuleInteractorInput



- (void)loadShareImageForGame:(NSNumber *)gameId solo:(BOOL)isSolo text:(NSString*)text
{
    
    CDUser *user = [CDAccount MR_findFirst].user;
    
    NSData *nsdata = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSCharacterSet *allowedCharacters = [NSCharacterSet URLFragmentAllowedCharacterSet];
    NSString *percentEncodedString = [base64Encoded stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    
    NSDictionary *params = @{@"gameId":gameId,
                             @"userId":@(user.id),
                             @"message":base64Encoded
                             };
    
    NSString *signIn = [EncryptService encryptFromDictionary:params];
    
    NSString *url;
    if (isSolo) {
        url = [NSString stringWithFormat:@"%@share/solo?gameId=%@&message=%@&sign=%@&userId=%@",[HTTPService baseURL],gameId,base64Encoded,signIn,@(user.id)];
    }else
    {
        url = [NSString stringWithFormat:@"%@share/hotseat?gameId=%@&message=%@&sign=%@&userId=%@",[HTTPService baseURL],gameId,base64Encoded,signIn,@(user.id)];
    }

    __weak typeof(self) weakSelf = self;
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            __strong typeof(weakSelf) blockSelf = weakSelf;
            [blockSelf.output succesfulLoadImage:[UIImage imageWithData:data] imageUrl:url];
            ;
        }];
    }];
    
    

}

@end
