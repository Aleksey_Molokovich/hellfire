//
//  ShareModuleInteractor.h
//  Hellfire
//
//  Created by Алексей Молокович on 27/03/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "ShareModuleInteractorInput.h"

@protocol ShareModuleInteractorOutput;

@interface ShareModuleInteractor : NSObject <ShareModuleInteractorInput>

@property (nonatomic, weak) id<ShareModuleInteractorOutput> output;

@end
