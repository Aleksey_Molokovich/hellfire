//
//  GameRoundResultAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultAssembly.h"

#import "GameRoundResultViewController.h"
#import "GameRoundResultInteractor.h"
#import "GameRoundResultPresenter.h"
#import "GameRoundResultRouter.h"
#import "GameRoundResultDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation GameRoundResultAssembly

- (GameRoundResultViewController *)viewGameRoundResult {
    return [TyphoonDefinition withClass:[GameRoundResultViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameRoundResult]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterGameRoundResult]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceGameRoundResult]];
                          }];
}

- (GameRoundResultInteractor *)interactorGameRoundResult {
    return [TyphoonDefinition withClass:[GameRoundResultInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGameRoundResult]];
                          }];
}

- (GameRoundResultPresenter *)presenterGameRoundResult{
    return [TyphoonDefinition withClass:[GameRoundResultPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewGameRoundResult]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorGameRoundResult]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerGameRoundResult]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceGameRoundResult]];
                          }];
}

- (GameRoundResultDatasource *)datasourceGameRoundResult{
    return [TyphoonDefinition withClass:[GameRoundResultDatasource class]];
}

- (GameRoundResultRouter *)routerGameRoundResult{
    return [TyphoonDefinition withClass:[GameRoundResultRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewGameRoundResult]];
                          }];
}

- (RamblerViperModuleFactory *)factoryGameRoundResult  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardGameRoundResult]];
                                                  [initializer injectParameterWith:@"GameRoundResultViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardGameRoundResult {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
