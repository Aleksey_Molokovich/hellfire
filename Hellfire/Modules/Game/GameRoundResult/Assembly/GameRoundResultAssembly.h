//
//  GameRoundResultAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 GameRoundResult module
 */
@interface GameRoundResultAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryGameRoundResult;
@end
