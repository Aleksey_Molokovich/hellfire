//
//  GameRoundResultInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultInteractorInput.h"

@protocol GameRoundResultInteractorOutput;

@interface GameRoundResultInteractor : NSObject <GameRoundResultInteractorInput>

@property (nonatomic, weak) id<GameRoundResultInteractorOutput> output;

@end
