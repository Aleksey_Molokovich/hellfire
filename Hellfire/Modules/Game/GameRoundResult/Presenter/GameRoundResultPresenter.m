//
//  GameRoundResultPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultPresenter.h"

#import "GameRoundResultViewInput.h"
#import "GameRoundResultInteractorInput.h"
#import "GameRoundResultRouterInput.h"
#import "GameRoundResultModel.h"

#import "HFPlayerIndicators.h"
#import "HFHotseatPlayer.h"

#import "AccountService.h"
#import "CDUser+CoreDataClass.h"
#import "CDAccount+CoreDataClass.h"
#import "ShareButtonTableViewCellAction.h"
#import "CallBackActionModel.h"

@implementation GameRoundResultPresenter

#pragma mark - Методы GameRoundResultModuleInput

- (void)configureModuleWith:(NSDictionary *)push {
    NSError *err;
    _data = [[GameRoundResultModel alloc] initWithDictionary:push error:&err];
    
    _datasource.indicators = [HFPlayerIndicators new];
    _datasource.indicators.totalScore = [_data.Indicators.TotalPoints intValue];
    _datasource.indicators.points.v1 = [_data.Indicators.Points.V1 intValue];
    _datasource.indicators.speed.v2 = [_data.Indicators.Speed.V2 intValue];
    _datasource.indicators.speed.v1 = [_data.Indicators.Speed.V1 intValue];
    _datasource.indicators.quality.v2 = [_data.Indicators.Quality.V2 intValue];
    _datasource.indicators.quality.v1 = [_data.Indicators.Quality.V1 intValue];
    _datasource.indicators.volume.v1 = [_data.Indicators.Volume.V1 intValue];

    _datasource.player = [HFHotseatPlayer new];
    _datasource.player.avatarPath = [CDAccount MR_findFirst].avatarPath;
    _datasource.player.userName = [CDAccount MR_findFirst].user.nickname;
    _datasource.player.points = _data.CurrentRound;
    _datasource.player.maxPoints = _data.RoundCount;
    
    if (_data.RoundRecords.MaxPoints.UserId) {
        _datasource.bestPointResults = [HFHotseatPlayer new];
        _datasource.bestPointResults.avatarPath =  _data.RoundRecords.MaxPoints.Avatar;
        _datasource.bestPointResults.maxPoints = _data.RoundRecords.MaxPoints.Value;
    }
    if (_data.RoundRecords.MaxSpeed.UserId) {
        _datasource.bestSpeedResults = [HFHotseatPlayer new];
        _datasource.bestSpeedResults.avatarPath =  _data.RoundRecords.MaxSpeed.Avatar;
        _datasource.bestSpeedResults.maxSpeed = _data.RoundRecords.MaxSpeed.Value;
    }
    if (_data.RoundRecords.MaxQuality.UserId) {
        _datasource.bestQualityResults = [HFHotseatPlayer new];
        _datasource.bestQualityResults.avatarPath =  _data.RoundRecords.MaxQuality.Avatar;
        _datasource.bestQualityResults.maxQuality = _data.RoundRecords.MaxQuality.Value;
    }
    
    [_datasource prepare];
    
}

- (void)close
{
    [_router closeCurrentModule];
}

#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object
{

    if ([object.action isEqualToString:kShareButtonTableViewCellAction] )
    {
        if ([AccountService state] == AccountStateTypeSuccessfulRegister)
        {
            [self.view showShareWithUrl:nil text:nil];
        }
        else
        {
            [_view showAlert2ButtonsWithMessage:@"Необходимо пройти регистрацию" tag:0];
        }
    }
}

#pragma mark - Методы GameRoundResultViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [_view update];
}

#pragma mark - Методы GameRoundResultInteractorOutput

@end
