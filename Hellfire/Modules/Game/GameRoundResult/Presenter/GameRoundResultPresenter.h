//
//  GameRoundResultPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultViewOutput.h"
#import "GameRoundResultInteractorOutput.h"
#import "GameRoundResultModuleInput.h"
#import "GameRoundResultDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol GameRoundResultViewInput;
@protocol GameRoundResultInteractorInput;
@protocol GameRoundResultRouterInput;
@protocol GameRoundResultDatasourceOutput;

@class GameRoundResultModel;

@interface GameRoundResultPresenter : NSObject <GameRoundResultModuleInput, GameRoundResultViewOutput, GameRoundResultInteractorOutput>

@property (nonatomic, weak) id<GameRoundResultViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<GameRoundResultInteractorInput> interactor;
@property (nonatomic, strong) id<GameRoundResultRouterInput> router;
@property (nonatomic, strong) id<GameRoundResultDatasourceOutput> datasource;

@property (nonatomic) GameRoundResultModel *data;

@end
