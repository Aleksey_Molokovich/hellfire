//
//  GameRoundResultViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameRoundResultViewInput.h"
#import "GameRoundResultDatasourceInput.h"
#import "BaseViewController.h"

@protocol GameRoundResultViewOutput;
@protocol GameRoundResultDatasourceInput;

@interface GameRoundResultViewController : BaseViewController <GameRoundResultViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<GameRoundResultViewOutput> output;
@property (nonatomic, strong) id<GameRoundResultDatasourceInput> datasource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
