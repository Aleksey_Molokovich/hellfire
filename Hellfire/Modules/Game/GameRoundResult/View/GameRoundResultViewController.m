//
//  GameRoundResultViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultViewController.h"
#import "GameRoundResultViewOutput.h"
#import "BaseTableViewCell.h"
#import "CellManager.h"
#import "BaseViewController+Game.h"

@implementation GameRoundResultViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}
- (IBAction)back:(id)sender {
    [_output close];
}

#pragma mark - Методы GameRoundResultViewInput

- (void)setupInitialState
{
    [self prepareStyleWithHFBGColorType:HFBackgroundColorGreenHotseat];
    [CellManager registerCellsWithId:[_datasource cellsIdentifire] tableView:_tableView];
    [self hideBackButton];
    [self hideBottomBar];
}

- (void)update
{
    [_tableView reloadData];
}
#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_datasource countItemsInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellId = [_datasource cellIdAtIndex:indexPath];
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.output = self.output;
    [cell configureWithData:[_datasource dataAtIndex:indexPath]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
@end
