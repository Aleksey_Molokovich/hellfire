//
//  GameRoundResultModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 22.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface UserRoundPoints : BaseModel
@property (nonatomic) NSNumber *UserId;
@property (nonatomic) NSNumber *Value;
@property (nonatomic) NSString *Avatar;
@end

@interface BestRoundPoints : BaseModel
@property (nonatomic) UserRoundPoints *MaxPoints;
@property (nonatomic) UserRoundPoints *MaxQuality;
@property (nonatomic) UserRoundPoints *MaxSpeed;
@end

@interface GameRoundCategory : BaseModel
@property (nonatomic) NSNumber *V1;
@property (nonatomic) NSNumber *V2;
@end

@interface GameRoundIndicators : BaseModel
@property (nonatomic) NSNumber *TotalPoints;
@property (nonatomic) GameRoundCategory *Points;
@property (nonatomic) GameRoundCategory *Quality;
@property (nonatomic) GameRoundCategory *Speed;
@property (nonatomic) GameRoundCategory *Volume;
@end

@interface GameRoundResultModel : BaseModel
@property (nonatomic) GameRoundIndicators *Indicators;
@property (nonatomic) BestRoundPoints *RoundRecords;
@property (nonatomic) NSNumber *RoundCount;
@property (nonatomic) NSNumber *CurrentRound;

@end
