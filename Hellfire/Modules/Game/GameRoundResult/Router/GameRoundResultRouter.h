//
//  GameRoundResultRouter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultRouterInput.h"
#import "BaseRouter.h"
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface GameRoundResultRouter : BaseRouter <GameRoundResultRouterInput>

//@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;
@end
