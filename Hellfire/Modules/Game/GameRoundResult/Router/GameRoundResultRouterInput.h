//
//  GameRoundResultRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameRoundResultRouterInput <NSObject>
- (void)closeCurrentModule;

@end
