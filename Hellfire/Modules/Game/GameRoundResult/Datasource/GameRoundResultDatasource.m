//
//  GameRoundResultDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultDatasource.h"
#import "GameRoundResultDatasourceOutput.h"
#import "ObjectTableViewCell.h"
#import "UIColor+HF.h"
#import "MainAchievementModel.h"
#import "CDGameResults+CoreDataClass.h"
#import "SeparateLineTableViewCellModel.h"
#import "HFHotseatPlayer.h"
#import "HFHotseatGameResult.h"
#import "AccountService.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "GameRoundResultModel.h"
#import "HFButtonData.h"

static NSString *const kHotseatPlayerInfoTableViewCell = @"HotseatPlayerInfoTableViewCell";
static NSString *const kShareButtonTableViewCell = @"ShareButtonTableViewCell";

static NSString *const CellIdResults = @"ResultsSoloGameTableViewCell";
static NSString *const kUserAchievementTableViewCell = @"UserAchievementTableViewCell";
static NSString *const kSeparateLineTableViewCell = @"SeparateLineTableViewCell";

@interface GameRoundResultDatasource()

@property (nonatomic, strong) NSArray<NSArray<ObjectTableViewCell*>*> *data;

@end

@implementation GameRoundResultDatasource

@synthesize  indicators, player, bestPointResults, bestSpeedResults, bestQualityResults;

#pragma mark - Методы GameRoundResultDatasourceInput
- (void)awakeFromNib
{
    [super awakeFromNib];
}


- (void)prepare
{
    NSMutableArray *mData=[NSMutableArray new];
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=kHotseatPlayerInfoTableViewCell;
    item.data=self.player;
    [mData addObject:item];
    

    [mData addObject:[self addSeparateCell]];
    
    
    item=[ObjectTableViewCell new];
    item.cellId=CellIdResults;
    item.data=self.indicators;
    [mData addObject:item];
    
    [mData addObject:[self addSeparateCell]];
    
    CDAccount *account = [CDAccount MR_findFirst];
    
    if (bestQualityResults) {
        item=[ObjectTableViewCell new];
        item.cellId=kUserAchievementTableViewCell;
        MainAchievementModel *data = [MainAchievementModel new];
        data.urlAvatar =bestQualityResults.avatarPath;
        data.v2 = bestQualityResults.maxQuality;
        data.type = AverageAndMaxResultTypeQualitySmoke;
        item.data=data;
        [mData addObject:item];
    }
    
    if (bestSpeedResults) {
        item=[ObjectTableViewCell new];
        item.cellId=kUserAchievementTableViewCell;
        MainAchievementModel *data = [MainAchievementModel new];
        data.urlAvatar =bestSpeedResults.avatarPath;
        data.v2 = bestSpeedResults.maxSpeed;
        data.type = AverageAndMaxResultTypeSpeedSmoke;
        item.data=data;
        [mData addObject:item];
    }
    
    if (bestPointResults) {
        item=[ObjectTableViewCell new];
        item.cellId=kUserAchievementTableViewCell;
        MainAchievementModel *data = [MainAchievementModel new];
        data.urlAvatar =bestPointResults.avatarPath;
        data.v2 = bestPointResults.maxPoints;
        data.type = AverageAndMaxResultTypeRoundPoints;
        item.data=data;
        [mData addObject:item];
    }
    
    
    
    
    
//    [mData addObject:[self addSeparateCell]];
//    
//    item=[ObjectTableViewCell new];
//    item.cellId=kShareButtonTableViewCell;
//    HFButtonData *buttonData = [HFButtonData new];
//    buttonData.title = @"ПОДЕЛИТЬСЯ";
//    buttonData.bgColor = [UIColor colorWithHex:@"#00B8D4"];
//    item.data=buttonData;
//    [mData addObject:item];
    
    _data = @[[mData copy]];
}

-(ObjectTableViewCell*)addSeparateCell
{
    ObjectTableViewCell *item=[ObjectTableViewCell new];
    item.cellId=kSeparateLineTableViewCell;
    SeparateLineTableViewCellModel *separateLineData = [SeparateLineTableViewCellModel new];
    separateLineData.color1 =[UIColor colorWithHex:@"#0097A7"];
    separateLineData.color2 =[UIColor colorWithHex:@"#0097A7"];
    item.data=separateLineData;
    return item;
}

-(NSInteger)countItemsInSection:(NSInteger)section
{
    return _data[section].count;
}
-(NSInteger)count{
    return _data.count;
}


-(NSString *)cellIdAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].cellId;
}

-(id)dataAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].data;
}
-(NSArray*)cellsIdentifire{
    return @[kHotseatPlayerInfoTableViewCell, CellIdResults, kUserAchievementTableViewCell, kSeparateLineTableViewCell, kShareButtonTableViewCell];
}
@end
