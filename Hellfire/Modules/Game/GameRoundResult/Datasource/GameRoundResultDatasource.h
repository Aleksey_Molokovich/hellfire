//
//  GameRoundResultDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "GameRoundResultDatasourceInput.h"

@protocol GameRoundResultDatasourceOutput;

@interface GameRoundResultDatasource : NSObject <GameRoundResultDatasourceInput, GameRoundResultDatasourceOutput>


@end
