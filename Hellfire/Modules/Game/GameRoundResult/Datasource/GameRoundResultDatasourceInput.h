//
//  GameRoundResultDatasourceInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ObjectTableViewCell;

@protocol GameRoundResultDatasourceInput <NSObject>

-(NSInteger)countItemsInSection:(NSInteger)section;
-(NSString*)cellIdAtIndex:(NSIndexPath*)index;
-(id) dataAtIndex:(NSIndexPath*)index;
-(NSArray*)cellsIdentifire;

@end
