//
//  GameRoundResultDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 22/02/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HFHotseatGameResult;
@class HFPlayerIndicators;
@class HFHotseatPlayer;

@protocol GameRoundResultDatasourceOutput <NSObject>
- (void)prepare;
@property (nonatomic, strong) HFPlayerIndicators *indicators;
@property (nonatomic) HFHotseatPlayer *player;
@property (nonatomic) HFHotseatPlayer *bestQualityResults;
@property (nonatomic) HFHotseatPlayer *bestSpeedResults;
@property (nonatomic) HFHotseatPlayer *bestPointResults;
@end
