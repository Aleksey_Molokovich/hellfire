//
//  GamePrepareInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GamePrepareInteractorInput <NSObject>
-(NSInteger)getSoloDurationUserPreparation;
@end
