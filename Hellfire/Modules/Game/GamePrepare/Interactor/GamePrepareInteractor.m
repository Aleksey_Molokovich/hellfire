//
//  GamePrepareInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePrepareInteractor.h"
#import "HFGameSettings.h"
#import "GamePrepareInteractorOutput.h"

@implementation GamePrepareInteractor

#pragma mark - Методы GamePrepareInteractorInput
-(NSInteger)getSoloDurationUserPreparation{
    
    HFGameSettings *soloSettings = [HFGameSettings load];
    if (!soloSettings){
        return 0;
    }
    
    return soloSettings.durationUserPreparation;
    
}
@end
