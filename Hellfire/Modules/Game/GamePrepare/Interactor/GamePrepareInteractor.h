//
//  GamePrepareInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePrepareInteractorInput.h"

@protocol GamePrepareInteractorOutput;

@interface GamePrepareInteractor : NSObject <GamePrepareInteractorInput>

@property (nonatomic, weak) id<GamePrepareInteractorOutput> output;

@end
