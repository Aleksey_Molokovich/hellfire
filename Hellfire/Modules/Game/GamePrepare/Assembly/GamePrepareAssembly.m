//
//  GamePrepareAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePrepareAssembly.h"

#import "GamePrepareViewController.h"
#import "GamePrepareInteractor.h"
#import "GamePreparePresenter.h"
#import "GamePrepareRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation GamePrepareAssembly

- (GamePrepareViewController *)viewGamePrepare {
    return [TyphoonDefinition withClass:[GamePrepareViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGamePrepare]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterGamePrepare]];
                          }];
}

- (GamePrepareInteractor *)interactorGamePrepare {
    return [TyphoonDefinition withClass:[GamePrepareInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterGamePrepare]];
                          }];
}

- (GamePreparePresenter *)presenterGamePrepare{
    return [TyphoonDefinition withClass:[GamePreparePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewGamePrepare]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorGamePrepare]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerGamePrepare]];
                          }];
}

- (GamePrepareRouter *)routerGamePrepare{
    return [TyphoonDefinition withClass:[GamePrepareRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewGamePrepare]];
                          }];
}

- (RamblerViperModuleFactory *)factoryGamePrepare  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardGamePrepare]];
                                                  [initializer injectParameterWith:@"GamePrepareViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardGamePrepare {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
