//
//  GamePrepareRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePrepareRouter.h"
#import "MainGameScreeenAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface GamePrepareRouter()

@property (strong, nonatomic) MainGameScreeenAssembly *soloUserGameScreeenAssembly;

@end


@implementation GamePrepareRouter

#pragma mark - Методы GamePrepareRouterInput
- (void)pushSoloUserGame{
    _soloUserGameScreeenAssembly=[[MainGameScreeenAssembly new] activated];
    self.factory=[_soloUserGameScreeenAssembly factoryMainGameScreeen];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
