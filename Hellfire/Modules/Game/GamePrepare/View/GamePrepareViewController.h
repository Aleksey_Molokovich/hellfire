//
//  GamePrepareViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GamePrepareViewInput.h"
#import "BaseViewController.h"

@protocol GamePrepareViewOutput;

@interface GamePrepareViewController : BaseViewController <GamePrepareViewInput>

@property (nonatomic, strong) id<GamePrepareViewOutput> output;

@end
