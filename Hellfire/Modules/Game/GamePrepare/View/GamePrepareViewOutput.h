//
//  GamePrepareViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ButtonViewProtocol.h"
#import "CallBackActionProtocol.h"

@protocol GamePrepareViewOutput <ButtonViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)stopTimer;
- (BOOL)isSoloGame;
- (void)waitUserAction;
- (void)continueGame;
@end
