//
//  GamePrepareViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerGameInput.h"
@class HFButtonData;

@protocol GamePrepareViewInput <BaseViewControllerGameInput>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)changeTimer:(NSInteger)number;
- (void)configureStartBtn:(HFButtonData*)button;
- (void)showCurrentHotseatPlayer:(NSString*)url;
@end
