//
//  GamePrepareViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePrepareViewController.h"
#import "GamePrepareViewOutput.h"
#import "CounterView.h"
#import "ButtonYellowStart.h"
#import <LGAlertView/LGAlertView.h>
#import "BaseViewController+Game.h"
#import "UIImageViewRounded.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface GamePrepareViewController()
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet ButtonYellowStart *startBtn;
@property (weak, nonatomic) IBOutlet UIImageViewRounded *avatarPlayer;

@end

@implementation GamePrepareViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.output didTriggerViewReadyEvent];
}

- (IBAction)exit:(id)sender {
    [_output waitUserAction];
    [self goOut];
}



- (void)continueGame
{
    [super continueGame];
    [_output continueGame];
}

- (void)gameOver
{
    [_output stopTimer];
    [super gameOver];
}

#pragma mark - Методы GamePrepareViewInput

- (void)setupInitialState {
    if ([_output isSoloGame]) {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorOrange];
    }
    else
    {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorOrangeHotseat];
    }
    
    self.navigationItem.hidesBackButton = YES;
    _counterView.shoudShowMinutes=YES;
    _startBtn.delegate=_output;
    [_startBtn fullRoundSide];
}

- (void)showCurrentHotseatPlayer:(NSString*)url
{
    [self.avatarPlayer sd_setImageWithURL:[NSURL  URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"avatar"]
                               options:SDWebImageRetryFailed
                             completed:nil];
}

- (void)changeTimer:(NSInteger)number{
    [_counterView setTime:number];
}


@end
