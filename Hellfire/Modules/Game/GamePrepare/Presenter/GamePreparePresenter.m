//
//  GamePreparePresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePreparePresenter.h"
#import "GameService.h"
#import "GamePrepareViewInput.h"
#import "GamePrepareInteractorInput.h"
#import "GamePrepareRouterInput.h"
#import "HFButtonData.h"
#import "BluetoothService.h"
#import "HFGameSettings.h"
#import "CDHotseatPlayer+CoreDataClass.h"

@interface GamePreparePresenter()<GameServiceProtocol>
@property (nonatomic,assign) BOOL isRedyNext;
@property (nonatomic) GameService *gameService;

@end

@implementation GamePreparePresenter

#pragma mark - Методы GamePrepareModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы GamePrepareViewOutput

- (void)didTriggerViewReadyEvent {
    [self.view setupInitialState];
    NSInteger count=10;
    _isRedyNext = YES;
//    count=[_interactor getSoloDurationUserPreparation];
    [_view changeTimer:count];
	_gameService =[GameService new];
    _gameService.delegate = self;
    [_gameService startWithDuration:count];

    [self.view updateBLEStatusView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBLEConnectionState:) name:kBluetoothServiceDidChangeConnection object:nil];
    
    if (![self isSoloGame]) {
        [_view showCurrentHotseatPlayer:[_gameService currentHotseatPlayer].avatarPath];
    }
}

- (void)stopTimer
{
     _gameService.delegate = nil;
    [_gameService stop];
}

- (void)waitUserAction
{
    _isRedyNext = NO;
}

- (void)continueGame
{
    _isRedyNext = YES;
    if (!_gameService.delegate) {
        [self goNext];
    }
}

-(BOOL)isSoloGame
{
    return [HFGameSettings load].gameId>0?NO:YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeBLEConnectionState:(NSNotification*)notif
{
    if ([[BluetoothService sharedInstance] connectedPeripheral].state == CBPeripheralStateDisconnected)
    {
        [self.view showAlertAfterDisconnected];
        [_gameService stop];
    }
    [self.view updateBLEStatusView];
}

#pragma mark - Методы ButtonViewProtocol.h


- (void)pressBtn
{
     [_gameService stop];
}

- (void)goNext
{
    if (_isRedyNext)
        [_router pushSoloUserGame];
}



#pragma mark - Методы GamePrepareInteractorOutput
#pragma mark - Методы GameServiceProtocol

- (void)updateTimerWithSeconds:(NSInteger)seconds{
    [_view changeTimer:seconds];
    if (seconds<=0) {
        _gameService.delegate = nil;
        [self goNext];
    }
}
@end
