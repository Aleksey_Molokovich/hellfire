//
//  GamePreparePresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 17/09/2017.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "GamePrepareViewOutput.h"
#import "GamePrepareInteractorOutput.h"
#import "GamePrepareModuleInput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol GamePrepareViewInput;
@protocol GamePrepareInteractorInput;
@protocol GamePrepareRouterInput;

@interface GamePreparePresenter : NSObject <GamePrepareModuleInput, GamePrepareViewOutput, GamePrepareInteractorOutput>

@property (nonatomic, weak) id<GamePrepareViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<GamePrepareInteractorInput> interactor;
@property (nonatomic, strong) id<GamePrepareRouterInput> router;

@end
