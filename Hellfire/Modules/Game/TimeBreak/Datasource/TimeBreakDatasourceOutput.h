//
//  TimeBreakDatasourceOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameModuleDefenitions.h"


@class CDGameResults;
@protocol TimeBreakDatasourceOutput <NSObject>

@property (nonatomic, assign) NSInteger seconds;
@property (nonatomic, assign) GameModuleInputType type;
@property (nonatomic, assign) BOOL isSolo;
@property (nonatomic, strong) NSArray *adds;
@property (nonatomic)  NSString *avatarCurrent;
@property (nonatomic)  NSString *avatarNext;
@property (nonatomic) CDGameResults *gameResults;
@property (nonatomic) CDGameResults *bestQualityResults;
@property (nonatomic) CDGameResults *bestSpeedResults;
@property (nonatomic) CDGameResults *bestPointResults;
- (void)prepare;

@end
