//
//  TimeBreakDatasource.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakDatasource.h"
#import "TimeBreakDatasourceOutput.h"
#import "ObjectTableViewCell.h"
#import "TimerModel.h"
#import "MainAchievementModel.h"
#import "HFGameSettings.h"
#import "CDGameResults+CoreDataClass.h"

static NSString *const kTimerTableViewCell = @"TimerTableViewCell";
static NSString *const kNewsTableViewCell = @"NewsTableViewCell";
static NSString *const kPrepareWithTimerTableViewCell = @"PrepareWithTimerTableViewCell";
static NSString *const kMainAchievementTableViewCell = @"MainAchievementTableViewCell";
static NSString *const kUserAchievementTableViewCell = @"UserAchievementTableViewCell";


@interface TimeBreakDatasource()

@property (nonatomic, strong) NSArray<NSArray<ObjectTableViewCell*>*> *data;

@end

@implementation TimeBreakDatasource
@synthesize seconds, adds, type, isSolo, avatarNext, avatarCurrent, gameResults, bestPointResults, bestSpeedResults, bestQualityResults;

#pragma mark - Методы TimeBreakDatasourceInput
- (void)prepare
{
	NSMutableArray *mData=[NSMutableArray new];

    ObjectTableViewCell *item=[ObjectTableViewCell new];

    if (type == GameModuleInputTypeBreak) {
        item.cellId=kTimerTableViewCell;
    }
    else{
        item.cellId=kPrepareWithTimerTableViewCell;
    }
    
    TimerModel *timer = [TimerModel new];
    timer.seconds = seconds;
    timer.avatarCurrentUrl = avatarCurrent;
    timer.avatarNextUrl = avatarNext;
    item.data=timer;
    [mData addObject:item];
    

    if (type == GameModuleInputTypeBreak && isSolo && gameResults.tiksCount) {
        
        item=[ObjectTableViewCell new];
        item.cellId=kMainAchievementTableViewCell;
        MainAchievementModel *data = [MainAchievementModel new];
        data.isDarkThem = NO;
        data.type = AverageAndMaxResultTypeQualitySmoke;
        data.v1 = @((int)gameResults.quality/gameResults.tiksCount);
        data.v2 = @(gameResults.maxQuality);
        item.data=data;
        [mData addObject:item];
        
        item=[ObjectTableViewCell new];
        item.cellId=kMainAchievementTableViewCell;
        data = [MainAchievementModel new];
        data.isDarkThem = NO;
        data.type = AverageAndMaxResultTypeSpeedSmoke;
        data.v1 = @((int)gameResults.speed/gameResults.tiksCount);
        data.v2 = @(gameResults.maxSpeed);
        item.data=data;
        [mData addObject:item];
        
        item=[ObjectTableViewCell new];
        item.cellId=kMainAchievementTableViewCell;
        data = [MainAchievementModel new];
        data.isDarkThem = NO;
        data.type = AverageAndMaxResultTypePoints;
        data.v1 = @((int)gameResults.points);
        data.v2 = @(gameResults.allGamePoints);
        item.data=data;
        [mData addObject:item];
    }
    else if (type == GameModuleInputTypeBreak && !isSolo && bestQualityResults.tiksCount)
    {
        
        item=[ObjectTableViewCell new];
        item.cellId=kUserAchievementTableViewCell;
        MainAchievementModel *data = [MainAchievementModel new];
        data.isDarkThem = NO;
        data.type = AverageAndMaxResultTypeQualitySmoke;
        data.v1 = @((int)bestQualityResults.quality/bestQualityResults.tiksCount);
        data.v2 = @(bestQualityResults.maxQuality);
        data.urlAvatar = bestQualityResults.playerAvatar;
        item.data=data;
        [mData addObject:item];
        

        item=[ObjectTableViewCell new];
        item.cellId=kUserAchievementTableViewCell;
        data = [MainAchievementModel new];
        data.isDarkThem = NO;
        data.type = AverageAndMaxResultTypeSpeedSmoke;
        data.v1 = @((int)bestSpeedResults.speed/bestSpeedResults.tiksCount);
        data.v2 = @(bestSpeedResults.maxSpeed);
        data.urlAvatar = bestSpeedResults.playerAvatar;
        item.data=data;
        [mData addObject:item];
        
        item=[ObjectTableViewCell new];
        item.cellId=kUserAchievementTableViewCell;
        data = [MainAchievementModel new];
        data.isDarkThem = NO;
        data.type = AverageAndMaxResultTypePoints;
        data.v1 = @((int)bestPointResults.points);
        data.urlAvatar = bestPointResults.playerAvatar;
        data.v2 = @(bestPointResults.allGamePoints);
        item.data=data;
        [mData addObject:item];
    }
    
    for (id data in adds) {
        item=[ObjectTableViewCell new];
        item.cellId=kNewsTableViewCell;
        item.data=data;
        [mData addObject:item];
    }
    
	_data =@[mData];
}

-(NSInteger)countItemsInSection:(NSInteger)section
{
    if (section>=[_data count]) {
        return 0;
    }
    return _data[section].count;
}
-(NSInteger)countSection{
    return _data.count;
}

-(CGFloat)heightAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].height;
}

-(NSString *)cellIdAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].cellId;
}

-(id)dataAtIndex:(NSIndexPath*)index{
    return _data[index.section][index.row].data;
}
-(NSArray*)cellsIdentifire{
    return @[kTimerTableViewCell,kPrepareWithTimerTableViewCell,kNewsTableViewCell,kMainAchievementTableViewCell,kUserAchievementTableViewCell];
}
@end
