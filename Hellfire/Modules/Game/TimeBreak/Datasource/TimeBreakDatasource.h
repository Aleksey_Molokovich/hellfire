//
//  TimeBreakDatasource.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakDatasourceInput.h"

@protocol TimeBreakDatasourceOutput;

@interface TimeBreakDatasource : NSObject <TimeBreakDatasourceInput, TimeBreakDatasourceOutput>


@end
