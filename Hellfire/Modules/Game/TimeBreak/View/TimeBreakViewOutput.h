//
//  TimeBreakViewOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CallBackActionProtocol.h"
#import "ButtonViewProtocol.h"

@protocol TimeBreakViewOutput <CallBackActionProtocol,ButtonViewProtocol>

/**
 @author AlekseyMolokovich

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didScollEnd;
- (void)stopTimer;
- (void)waitUserAction;
- (void)continueGame;
- (BOOL)isSoloGame;
- (BOOL)isHasPosts;
@end
