//
//  TimeBreakViewInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewControllerGameInput.h"

@protocol TimeBreakViewInput <BaseViewControllerGameInput>

/**
 @author AlekseyMolokovich

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)configureForBreak;
- (void)configureForPrepare;
- (void)shouldShowTimerWithButton:(BOOL)flag;
- (void)changeTimer:(NSInteger)number;
- (void)update;
- (void)setTitle:(NSString*)title;
- (void)updateCellWithIndexPath:(NSIndexPath *)indexPath height:(NSInteger)height;

@end
