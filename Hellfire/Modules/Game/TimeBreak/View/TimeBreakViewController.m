//
//  TimeBreakViewController.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakViewController.h"
#import "TimeBreakViewOutput.h"
#import "BaseTableViewCell.h"
#import "MiniTimerWithButtonView.h"
#import "MiniTimerView.h"
#import "CellManager.h"
#import "BaseViewController+Game.h"

@interface TimeBreakViewController()<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopMiniHeaderVew;
@property (weak, nonatomic) IBOutlet MiniTimerWithButtonView *miniTimerWithButtonView;
@property (weak, nonatomic) IBOutlet MiniTimerView *miniTimerView;
@property (nonatomic) NSDictionary *cellHeights;

@end

@implementation TimeBreakViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    _cellHeights = @{};
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы Action
- (IBAction)goOut:(id)sender {
    [_output waitUserAction];
    [self goOut];
}

- (void)continueGame
{
    [super continueGame];
    [_output continueGame];
}

- (void)gameOver
{
    [_output stopTimer];
    [super gameOver];
}

#pragma mark - Методы TimerAndAddsModuleViewInput

- (void)setupInitialState {
    [CellManager registerCellsWithId:[_datasource cellsIdentifire]
                           tableView:_tableView];

    
    [self hideBackButton];
    [self hideBottomBar];
    self.navigationItem.hidesBackButton = YES;
    _constraintTopMiniHeaderVew.constant=-80;
    
    _miniTimerWithButtonView.button.delegate=_output;
    _miniTimerView.counterView.shoudShowMinutes=YES;
    _miniTimerWithButtonView.counterView.shoudShowMinutes=YES;
}

- (void)setTitle:(NSString *)title{
    self.navigationItem.title=title;
}

- (void)configureForPrepare{
    if ([_output isSoloGame]) {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorOrange];
    }else{
        [self prepareStyleWithHFBGColorType:HFBackgroundColorOrangeHotseat];
    }
    
    if ([_output isSoloGame]) {
        _miniTimerView.bgImageView.image=[UIImage imageNamed:@"bg_orange"];
        _miniTimerWithButtonView.bgImageView.image=[UIImage imageNamed:@"bg_orange"];
    }else{
        _miniTimerView.bgImageView.image=[UIImage imageNamed:@"bg_orange_hotseat"];
        _miniTimerWithButtonView.bgImageView.image=[UIImage imageNamed:@"bg_orange_hotseat"];
    }
    
}

- (void)configureForBreak{
    if ([_output isSoloGame]) {
        [self prepareStyleWithHFBGColorType:HFBackgroundColorGreen];
    }else{
        [self prepareStyleWithHFBGColorType:HFBackgroundColorGreenHotseat];
    }
    
    if ([_output isSoloGame]) {
        _miniTimerView.bgImageView.image=[UIImage imageNamed:@"bg_green"];
        _miniTimerWithButtonView.bgImageView.image=[UIImage imageNamed:@"bg_green"];
    }else{
        _miniTimerView.bgImageView.image=[UIImage imageNamed:@"bg_green_hotseat"];
        _miniTimerWithButtonView.bgImageView.image=[UIImage imageNamed:@"bg_green_hotseat"];
    }
}

- (void)shouldShowTimerWithButton:(BOOL)flag
{
    if (flag) {
        _miniTimerView.hidden=YES;
        _miniTimerWithButtonView.hidden=NO;
    }else{
        _miniTimerView.hidden=NO;
        _miniTimerWithButtonView.hidden=YES;
    }
}


- (void)update{
    [_tableView reloadData];
    if ([self.output isHasPosts]) {
        _tableView.scrollEnabled = YES;
    }else{
        _tableView.scrollEnabled = NO;
    }
}

- (void)changeTimer:(NSInteger)number{
    [UIView setAnimationsEnabled:NO];
    [_tableView beginUpdates];
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_tableView endUpdates];
    [UIView setAnimationsEnabled:YES];
    [_miniTimerWithButtonView.counterView setTime:number];
    [_miniTimerView.counterView setTime:number];
    
}

- (void)updateCellWithIndexPath:(NSIndexPath *)indexPath height:(NSInteger)height
{
    if ([self.output isHasPosts]) {
        _tableView.scrollEnabled = YES;
    }else{
        _tableView.scrollEnabled = NO;
    }
    
    NSMutableDictionary *mCellHeight = [_cellHeights mutableCopy];
    
    if ([_cellHeights[indexPath] intValue] == height) {
        return ;
    }
    
    [mCellHeight setValue:@(height) forKey:indexPath];
    _cellHeights = [mCellHeight copy];
    
    
    
    if (indexPath.row == 0) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

#pragma mark - Методы TableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_datasource countItemsInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[_datasource cellIdAtIndex:indexPath]];
    cell.output = _output;
    cell.cellIndex = indexPath;
    [cell configureWithData:[_datasource dataAtIndex:indexPath]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (_cellHeights[indexPath]) {
        return [_cellHeights[indexPath] intValue];
    }
    
    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 33)];
    view.backgroundColor = [UIColor whiteColor];
    if ([_datasource countSection] == 1) {
        view.backgroundColor = [UIColor clearColor];
    }
    return view;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UITableView *)scrollView{
    
    if (scrollView.contentOffset.y == 0 )
        return;
    
    if(scrollView.contentOffset.y>72 && scrollView.contentOffset.y<152){
        _constraintTopMiniHeaderVew.constant=scrollView.contentOffset.y-152;
    }else if (scrollView.contentOffset.y<=72){
        
        _constraintTopMiniHeaderVew.constant=-80;
    }
    else{
        _constraintTopMiniHeaderVew.constant=0;
    }

}
@end
