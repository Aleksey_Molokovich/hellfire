//
//  TimeBreakViewController.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeBreakViewInput.h"
#import "TimeBreakDatasourceInput.h"
#import "BaseViewController.h"

@protocol TimeBreakViewOutput;
@protocol TimeBreakDatasourceInput;

@interface TimeBreakViewController : BaseViewController <TimeBreakViewInput, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) id<TimeBreakViewOutput> output;
@property (nonatomic, strong) id<TimeBreakDatasourceInput> datasource;

@end
