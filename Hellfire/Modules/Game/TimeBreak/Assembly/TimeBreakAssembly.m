//
//  TimeBreakAssembly.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakAssembly.h"

#import "TimeBreakViewController.h"
#import "TimeBreakInteractor.h"
#import "TimeBreakPresenter.h"
#import "TimeBreakRouter.h"
#import "TimeBreakDatasource.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation TimeBreakAssembly

- (TimeBreakViewController *)viewTimeBreak {
    return [TyphoonDefinition withClass:[TimeBreakViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterTimeBreak]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterTimeBreak]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceTimeBreak]];
                          }];
}

- (TimeBreakInteractor *)interactorTimeBreak {
    return [TyphoonDefinition withClass:[TimeBreakInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterTimeBreak]];
                          }];
}

- (TimeBreakPresenter *)presenterTimeBreak{
    return [TyphoonDefinition withClass:[TimeBreakPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewTimeBreak]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorTimeBreak]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerTimeBreak]];
                              [definition injectProperty:@selector(datasource)
                                                    with:[self datasourceTimeBreak]];
                          }];
}

- (TimeBreakDatasource *)datasourceTimeBreak{
    return [TyphoonDefinition withClass:[TimeBreakDatasource class]];
}

- (TimeBreakRouter *)routerTimeBreak{
    return [TyphoonDefinition withClass:[TimeBreakRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewTimeBreak]];
                          }];
}

- (RamblerViperModuleFactory *)factoryTimeBreak  {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardTimeBreak]];
                                                  [initializer injectParameterWith:@"TimeBreakViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardTimeBreak {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Game"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}
@end
