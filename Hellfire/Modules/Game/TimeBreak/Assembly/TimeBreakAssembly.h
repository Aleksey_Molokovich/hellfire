//
//  TimeBreakAssembly.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
/**
 @author AlekseyMolokovich

 TimeBreak module
 */
@interface TimeBreakAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (RamblerViperModuleFactory *)factoryTimeBreak;
@end
