//
//  TimeBreakRouterInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TimeBreakRouterInput <NSObject>
- (void)pushSoloUserPrepare;
- (void)pushResults;
@end
