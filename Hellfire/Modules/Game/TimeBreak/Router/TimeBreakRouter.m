//
//  TimeBreakRouter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GamePrepareAssembly.h"
#import "ResultsGameModuleAssembly.h"

@interface TimeBreakRouter()

@property (strong, nonatomic) GamePrepareAssembly *soloUserPrepareToPlayAssembly;
@property (strong, nonatomic) ResultsGameModuleAssembly *resultsGameModuleAssembly;
@end


@implementation TimeBreakRouter

#pragma mark - Методы TimeBreakRouterInput
- (void)pushSoloUserPrepare{
    _soloUserPrepareToPlayAssembly=[[GamePrepareAssembly new] activated];
    self.factory=[_soloUserPrepareToPlayAssembly factoryGamePrepare];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}

- (void)pushResults{
    _resultsGameModuleAssembly=[[ResultsGameModuleAssembly new] activated];
    self.factory=[_resultsGameModuleAssembly factoryResultsGameModule];
    
    [self pushModuleUsingFactory:self.factory
                   withLinkBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                       return nil;
                   }];
}
@end
