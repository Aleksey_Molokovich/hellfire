//
//  TimeBreakInteractorOutput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TimeBreakInteractorOutput <NSObject>
- (void)successfulWithData:(NSArray*)data;
@end
