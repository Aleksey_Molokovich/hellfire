//
//  TimeBreakInteractor.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakInteractor.h"
#import "TimeBreakInteractorOutput.h"
#import "AdPostRequest.h"
#import "AdPostResponse.h"
#import "HFPost.h"
#import "HellfireDefines.h"
#import "ClanRepository.h"
#import "HFGameSettings.h"
#import "CDHotseatPlayer+CoreDataClass.h"
#import "GameService.h"

@interface TimeBreakInteractor()
@property(assign, nonatomic) NSInteger page;
@property(assign, nonatomic) BOOL isExecuting;
@property(assign, nonatomic) BOOL isNeedUpdate;;
@property (nonatomic) GameService *gameService;
@property (nonatomic) NSMutableArray< HFPost* > *posts;
@end

@implementation TimeBreakInteractor

#pragma mark - Методы TimeBreakInteractorInput

- (void)configure
{
    _gameService = [GameService new];
    self.posts = [NSMutableArray new];

}

- (void)sendHotseatGameResult
{
    [_gameService sendCurrentHotseatRoundResultsWithCompletion:^(HFHotseatGameResult *result) {
        
    }];
}

-(NSInteger)getSoloDurationPreparation{
    return [HFGameSettings load].durationBreak;
}

-(NSInteger)getSoloDurationBreak{
    return [HFGameSettings load].durationBreak;
}

-(BOOL)isSoloGame
{
    return [_gameService isSoloGame];
}

- (void)nextRound
{
    [_gameService changeCurrentHotseatPlayer];
    [_gameService nextRound];
}

- (void)loadPosts{
    _isExecuting=YES;
    _isNeedUpdate=NO;
    AdPostRequest *request=[AdPostRequest new];
    request.page=_page;
    
    [_output successfulWithData:_posts];
    
    __weak typeof(self) weakSelf = self;
    
    [ClanRepository posts:nil completion:^(NSArray<HFPost*> *receive) {
    __strong typeof(weakSelf) blockSelf = weakSelf;
        if ([receive count]) {
            [blockSelf check:receive];
        }
        [blockSelf.output successfulWithData:blockSelf.posts];
        
    }];
}

- (void)check:(NSArray*)posts
{
    for (HFPost *post in posts) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id == %ld",post.id];
        
        if (![[self.posts filteredArrayUsingPredicate:predicate] firstObject]) {
            [self.posts addObject:post];
        }
    }
}

- (void)getEarlyAdPost{
    _page=self.posts.count/numberItemsInPage;
    [self loadPosts];
}

- (void)getNewAdPost{
    _page=0;
    [self loadPosts];
}

-(NSString*)currentHotseatPlayerAvatarUrl
{
    return [_gameService currentHotseatPlayer].avatarPath;
}

-(NSString*)nextHotseatPlayerAvatarUrl
{
    return [_gameService nextHotseatPlayer].avatarPath;
}
@end
