//
//  TimeBreakInteractor.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakInteractorInput.h"

@protocol TimeBreakInteractorOutput;

@interface TimeBreakInteractor : NSObject <TimeBreakInteractorInput>

@property (nonatomic, weak) id<TimeBreakInteractorOutput> output;



@end
