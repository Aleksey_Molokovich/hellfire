//
//  TimeBreakInteractorInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TimeBreakInteractorInput <NSObject>
- (void)configure;
- (void)getEarlyAdPost;
- (void)getNewAdPost;
- (NSInteger)getSoloDurationPreparation;
- (NSInteger)getSoloDurationBreak;
- (BOOL)isSoloGame;
- (void)nextRound;
-(NSString*)nextHotseatPlayerAvatarUrl;
-(NSString*)currentHotseatPlayerAvatarUrl;
- (void)sendHotseatGameResult;
@end
