//
//  TimeBreakModuleInput.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "GameModuleDefenitions.h"

@protocol TimeBreakModuleInput <RamblerViperModuleInput>

/**
 @author AlekseyMolokovich

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithType:(GameModuleInputType)type;

@end
