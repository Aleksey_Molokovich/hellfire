//
//  TimeBreakPresenter.m
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakPresenter.h"

#import "TimeBreakViewInput.h"
#import "TimeBreakInteractorInput.h"
#import "TimeBreakRouterInput.h"
#import "GameService.h"
#import "FirstTableViewCellModel.h"
#import "TimerModel.h"
#import "HFButtonData.h"
#import "BluetoothService.h"
#import "HFGameSettings.h"
#import "NewsTableViewCellAction.h"

@interface TimeBreakPresenter()<GameServiceProtocol>
{
    GameModuleInputType typeModule;
}
@property (nonatomic,assign) BOOL isRedyNext;
@property (nonatomic) GameService *gameService;
@end


@implementation TimeBreakPresenter

#pragma mark - Методы TimeBreakModuleInput

- (void)configureModuleWithType:(GameModuleInputType)type {
    typeModule=type;
    [_interactor configure];
    [_interactor getNewAdPost];
}

#pragma mark - Методы TimeBreakViewOutput

- (void)didTriggerViewReadyEvent {
    _isRedyNext = YES;
    _gameService =[GameService new];
    _gameService.delegate = self;
    _datasource.type = typeModule;
    _datasource.gameResults = [_gameService currentGameResults];
    if (![_interactor isSoloGame]) {
        _datasource.avatarCurrent = [_interactor currentHotseatPlayerAvatarUrl];
        _datasource.avatarNext = [_interactor nextHotseatPlayerAvatarUrl];
        
        _datasource.bestQualityResults = [_gameService bestQualityResults];
        _datasource.bestSpeedResults = [_gameService bestSpeedResults];
        _datasource.bestPointResults = [_gameService bestPointResults];
    }
    
    _datasource.isSolo = [_interactor isSoloGame];
    NSInteger count=0;
    switch (typeModule) {
        case GameModuleInputTypePrepare:
            [_view configureForPrepare];
            count=180;
            [_view setTitle:@"Готовьте кальян!"];
            [_view shouldShowTimerWithButton:YES];
            break;
        case GameModuleInputTypeBreak:
            count=[_interactor getSoloDurationBreak];
            [_view configureForBreak];
            [_view setTitle:@"Перерыв!"];
            [_view shouldShowTimerWithButton:NO];
            if (![_interactor isSoloGame]) {
                [_interactor sendHotseatGameResult];
            }
            
            break;
            
        default:
            break;
    }
    [_gameService startWithDuration:count];
    [self.view setupInitialState];
    [self.view updateBLEStatusView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBLEConnectionState:) name:kBluetoothServiceDidChangeConnection object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeBLEConnectionState:(NSNotification*)notif
{
    if ([[BluetoothService sharedInstance] connectedPeripheral].state == CBPeripheralStateDisconnected)
    {
        [self.view showAlertAfterDisconnected];
        [self stopTimer];
    }
    [self.view updateBLEStatusView];
}

- (void)didScollEnd{
    
}

- (void)waitUserAction
{
    _isRedyNext = NO;
}

- (void)continueGame
{
     _isRedyNext = YES;
    if (!_gameService.delegate) {
        [self goNext];
    }
}

- (void)stopTimer
{
    _gameService.delegate = nil;
    [_gameService stop];
}
#pragma mark - Методы ButtonViewProtocol.h

- (void)pressBtn{
    [_gameService stop];
}


- (void)goNext
{
    if (_isRedyNext) {
        if (typeModule ==GameModuleInputTypeBreak) {
            [_interactor nextRound];
        }
        
        [_router pushSoloUserPrepare];
    }
}
#pragma mark - Методы CallBackActionProtocol

- (void)callBackObject:(CallBackActionModel *)object{
    
    if ([object.action isEqualToString:NewsTableViewCellChangeCellHeight]) {
        [_view updateCellWithIndexPath:object.cellIndex height:[object.data intValue]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellShare]) {
        [_view showShareWithUrl:object.data[1] text:object.data[0]];
        return;
    }
    
    if ([object.action isEqualToString:NewsTableViewCellOpenSite]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:object.data]];
        return;
    }
    
    [_gameService stop];
}

-(BOOL)isSoloGame
{
    return [HFGameSettings load].gameId>0?NO:YES;
}

-(BOOL)isHasPosts
{
    return [_datasource.adds count]?YES:NO;
}

#pragma mark - Методы TimerAndAddsModuleInteractorOutput

- (void)successfulWithData:(NSArray *)data{
    _datasource.adds =data;
    [_datasource prepare];
    [_view update];
}

#pragma mark - Методы GameServiceProtocol

- (void)updateTimerWithSeconds:(NSInteger)seconds{
    _datasource.seconds =seconds;
    [_datasource prepare];
    [_view changeTimer:seconds];
    if (seconds<=0) {
        _gameService.delegate = nil;
        [self goNext];
    }
}
@end
