//
//  TimeBreakPresenter.h
//  Hellfire
//
//  Created by AlekseyMolokovich on 21/01/2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimeBreakViewOutput.h"
#import "TimeBreakInteractorOutput.h"
#import "TimeBreakModuleInput.h"
#import "TimeBreakDatasourceOutput.h"
#import "BaseViewControllerProtocol.h"

@protocol BaseViewControllerProtocol;
@protocol TimeBreakViewInput;
@protocol TimeBreakInteractorInput;
@protocol TimeBreakRouterInput;
@protocol TimeBreakDatasourceOutput;

@interface TimeBreakPresenter : NSObject <TimeBreakModuleInput, TimeBreakViewOutput, TimeBreakInteractorOutput>

@property (nonatomic, weak) id<TimeBreakViewInput, BaseViewControllerProtocol> view;
@property (nonatomic, strong) id<TimeBreakInteractorInput> interactor;
@property (nonatomic, strong) id<TimeBreakRouterInput> router;
@property (nonatomic, strong) id<TimeBreakDatasourceOutput> datasource;


@end
