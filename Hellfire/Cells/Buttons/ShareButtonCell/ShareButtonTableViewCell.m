//
//  ShareButtonTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ShareButtonTableViewCell.h"
#import "HFButtonData.h"
#import "ShareButtonTableViewCellAction.h"
@implementation ShareButtonTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[HFButtonData class]])
        return;
    
    HFButtonData *config = data;
    
    [_button configureTitle:config.title bgColor:config.bgColor];
    _button.enabled = !config.disable;
    self.shareimg.alpha = config.disable?0.2:1;
}

- (IBAction)pressBTn:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kShareButtonTableViewCellAction;
        [self.output callBackObject:model];
    }
    
}
@end
