//
//  ShareButtonTableViewCellAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 18.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef ShareButtonTableViewCellAction_h
#define ShareButtonTableViewCellAction_h

static NSString *const kShareButtonTableViewCellAction = @"ShareButtonTableViewCellAction";
#endif /* ShareButtonTableViewCellAction_h */
