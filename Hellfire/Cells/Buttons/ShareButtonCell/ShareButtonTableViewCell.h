//
//  ShareButtonTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RoundedButton.h"

@interface ShareButtonTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet RoundedButton *button;
@property (weak, nonatomic) IBOutlet UIImageView *shareimg;

@end
