//
//  ButtonTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RoundedButton.h"

@interface ButtonTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet RoundedButton *button;

@end
