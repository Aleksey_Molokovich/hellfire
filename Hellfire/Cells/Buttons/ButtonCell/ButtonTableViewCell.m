//
//  ButtonTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ButtonTableViewCell.h"
#import "HFButtonData.h"
#import "ButtonTableViewAction.h"

@implementation ButtonTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[HFButtonData class]])
        return;
    
    HFButtonData *config = data;
    
    [_button configureTitle:config.title bgColor:config.bgColor];
    
}

- (IBAction)pressBTn:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = ButtonTableViewActionPress;
        [self.output callBackObject:model];
    }
    
}


@end
