//
//  ResultsSoloGameTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 03.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsSoloGameTableViewCell.h"
#import "AverageAndMaxFinalResultView.h"
#import "HFPlayerIndicators.h"

@interface ResultsSoloGameTableViewCell ()
@property (weak, nonatomic) IBOutlet AverageAndMaxFinalResultView *viewPoints;
@property (weak, nonatomic) IBOutlet AverageAndMaxFinalResultView *viewQuality;
@property (weak, nonatomic) IBOutlet AverageAndMaxFinalResultView *viewVolume;
@property (weak, nonatomic) IBOutlet AverageAndMaxFinalResultView *viewSpeed;

@end
@implementation ResultsSoloGameTableViewCell



- (void)configureWithData:(HFPlayerIndicators*)data
{
    
    
    [_viewSpeed configureWithType:AverageAndMaxResultTypeSpeedSmoke
                    averageAmount:data.speed.v1
                        maxAmount:data.speed.v2 ];
    [_viewQuality configureWithType:AverageAndMaxResultTypeQualitySmoke
                    averageAmount:data.quality.v1
                          maxAmount:data.quality.v2];
    [_viewVolume configureWithType:AverageAndMaxResultTypeVolume
                    averageAmount:data.volume.v1
                         maxAmount:data.totalScore?data.volume.v2:data.volume.v1];

    [_viewPoints configureWithType:data.totalScore?AverageAndMaxResultTypePoints:AverageAndMaxResultTypeRoundPoints
                     averageAmount:data.points.v1
                         maxAmount:data.totalScore];

    
}

@end
