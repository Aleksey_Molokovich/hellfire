//
//  PLayerResultsHotseatGameTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 07.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "PLayerResultsHotseatGameTableViewCell.h"
#import "HFHotseatPlayer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIFont+HF.h"

@implementation PLayerResultsHotseatGameTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[HFHotseatPlayer class]]) {
        return;
    }
    
    HFHotseatPlayer *player = data;
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: player.avatarPath]
                   placeholderImage:nil
                            options:SDWebImageRetryFailed
                          completed:nil];
    
    _lblName.text = player.userName;
    
    _title1.text = @"Очки за партию:";
    _value1.text = [@(player.indicators.totalScore) stringValue];
    _value1.font = [UIFont hfBoldSize:14];
    
    _title2.text = @"Рейтинг Hotseat";
    if ([player.points intValue]<0) {
        _value2.text = [NSString stringWithFormat:@"%@ (%@)",player.hotseatPoints,player.points];
    }else
    {
        _value2.text = [NSString stringWithFormat:@"%@ (+%@)",player.hotseatPoints,player.points];
    }
    
    _value2.font = [UIFont hfBoldSize:14];
    
    _constraint3.constant=player.maxPoints?8:0;
    _title3.text = player.maxPoints?@"Макс. счет в раунде":nil;
    _value3.text = player.maxPoints?[@(player.indicators.totalScore) stringValue]:nil;
    _value3.font = [UIFont hfBoldSize:14];
    
    _constraint4.constant=player.maxQuality?8:0;
    _title4.text = player.maxQuality?@"Макс. качество дыма":nil;
    _value4.text = player.maxQuality?[player.maxQuality stringValue]:nil;
    _value4.font = [UIFont hfBoldSize:14];
    
    _constraint5.constant=player.maxSpeed?8:0;
    _title5.text = player.maxSpeed?@"Макс. скорость дыма":nil;
    _value5.text = player.maxSpeed?[player.maxSpeed stringValue]:nil;
    _value5.font = [UIFont hfBoldSize:14];
}

@end
