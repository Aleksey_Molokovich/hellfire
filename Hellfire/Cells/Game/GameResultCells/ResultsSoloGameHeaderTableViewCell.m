//
//  ResultsGameHeaderTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 03.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsSoloGameHeaderTableViewCell.h"
#import "UIFont+HF.h"

@implementation ResultsSoloGameHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor clearColor];
    self.backgroundColor=[UIColor clearColor];
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString new] initWithString:_lblMessage.text
                                                                                       attributes:@{NSFontAttributeName:[UIFont hfRegularSize:12],
                                                                                                    NSForegroundColorAttributeName:[UIColor whiteColor]}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 4;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, _lblMessage.text.length)];
    
    _lblMessage.attributedText=attributedString;
    
    _lblBottom.attributedText=[[NSAttributedString new] initWithString:_lblBottom.text
                                                            attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                                                         NSKernAttributeName:@(2),
                                                                         NSForegroundColorAttributeName:[UIColor whiteColor]}];;
    self.backgroundColor=[UIColor clearColor];
}

- (IBAction)clickRegBtn:(id)sender
{
    CallBackActionModel *object=[CallBackActionModel new];
    object.cellIndex = self.cellIndex;
    
    [self.output callBackObject:object];
}




@end
