//
//  ResultsSoloGameRegistredTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "UIImageViewRounded.h"

@interface ResultsSoloGameRegistredTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageViewRounded *avatar;
@property (weak, nonatomic) IBOutlet UIImageViewRounded *logoClan;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *soloPoints;
@property (weak, nonatomic) IBOutlet UILabel *clanPoints;
@property (weak, nonatomic) IBOutlet UILabel *clanName;
@property (weak, nonatomic) IBOutlet UILabel *labelTypePoints;

@end
