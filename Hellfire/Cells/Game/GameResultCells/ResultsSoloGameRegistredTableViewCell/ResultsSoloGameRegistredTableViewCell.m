//
//  ResultsSoloGameRegistredTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsSoloGameRegistredTableViewCell.h"
#import "HFSoloGameResult.h"
#import "HFHotseatGameResult.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIFont+HF.h"
#import "HFPushFinalResultHotseatModel.h"

@implementation ResultsSoloGameRegistredTableViewCell

- (void)configureWithData:(id)cellData
{
    
    if ([cellData isKindOfClass:[HFSoloGameResult class]])
    {
        HFSoloGameResult *data = cellData;
        
        [self.avatar sd_setImageWithURL:[NSURL URLWithString: data.user.avatarPath]
                       placeholderImage:nil
                                options:SDWebImageRetryFailed
                              completed:nil];
        
        [self.logoClan sd_setImageWithURL:[NSURL URLWithString: data.clan.avatar]
                         placeholderImage:nil
                                  options:SDWebImageRetryFailed
                                completed:nil];
        _labelTypePoints.text = @"Рейтинг Solo";
        _userName.text = data.user.userName;
        _soloPoints.text = data.user.soloPoints?data.user.soloPoints:@"-";
        _soloPoints.font = [UIFont hfBoldSize:14];
        _clanPoints.text = data.clan.points?[NSString stringWithFormat:@"+%@",data.clan.points]:@"-";
        _clanPoints.font = [UIFont hfBoldSize:14];
        _clanName.text = data.clan.name;
    }
    else if ([cellData isKindOfClass:[HFHotseatGameResult class]])
    {
        HFHotseatGameResult *data = cellData;
        
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"self.isWinner == 1"];
        HFHotseatPlayer *winner = [[data.players filteredArrayUsingPredicate:filter] firstObject] ;
        
        _labelTypePoints.text = @"Рейтинг Hotseat";
        
        [self.avatar sd_setImageWithURL:[NSURL URLWithString: winner.avatarPath]
                       placeholderImage:nil
                                options:SDWebImageRetryFailed
                              completed:nil];

        [self.logoClan sd_setImageWithURL:[NSURL URLWithString: data.clan.avatar]
                         placeholderImage:nil
                                  options:SDWebImageRetryFailed
                                completed:nil];

        _userName.text = winner.userName;
        if ([winner.points intValue]<0) {
            _soloPoints.text =[NSString stringWithFormat:@"%@ (%@)",winner.hotseatPoints, winner.points] ;
        }else
        {
            _soloPoints.text =[NSString stringWithFormat:@"%@ (+%@)",winner.hotseatPoints, winner.points] ;
        }
        
        _soloPoints.font = [UIFont hfBoldSize:14];
        
        if ([data.clan.points intValue]<0) {
            _clanPoints.text = [NSString stringWithFormat:@"%@",data.clan.points];
        }else
        {
            _clanPoints.text = [NSString stringWithFormat:@"+%@",data.clan.points];
        }
        
        _clanPoints.font = [UIFont hfBoldSize:14];
        _clanName.text = data.clan.name;
    }
    else if([cellData isKindOfClass:[HFPushFinalResultHotseatModel class]])
    {
        HFPushFinalResultHotseatModel *data = cellData;
        
        HFPushHotseatPlayer *player = [data.Players firstObject] ;
        
        _labelTypePoints.text = @"Рейтинг Hotseat";
        
        [self.avatar sd_setImageWithURL:[NSURL URLWithString: player.AvatarPath]
                       placeholderImage:nil
                                options:SDWebImageRetryFailed
                              completed:nil];
        
        [self.logoClan sd_setImageWithURL:[NSURL URLWithString: data.Clan.Avatar]
                         placeholderImage:nil
                                  options:SDWebImageRetryFailed
                                completed:nil];
        
        _userName.text = player.UserName;
        if ([player.Points intValue]<0) {
            _soloPoints.text =[NSString stringWithFormat:@"%@ (%@)",player.HotseatPoints, player.Points] ;
        }else
        {
            _soloPoints.text =[NSString stringWithFormat:@"%@ (+%@)",player.HotseatPoints, player.Points] ;
        }
        
        _soloPoints.font = [UIFont hfBoldSize:14];
        
        if ([data.Clan.Points intValue]<0) {
            _clanPoints.text = [NSString stringWithFormat:@"%@",data.Clan.Points];
        }else
        {
            _clanPoints.text = [NSString stringWithFormat:@"+%@",data.Clan.Points];
        }
        
        _clanPoints.font = [UIFont hfBoldSize:14];
        _clanName.text = data.Clan.Name;
    }

    
    
}

@end
