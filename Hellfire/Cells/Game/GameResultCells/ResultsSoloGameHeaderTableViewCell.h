//
//  ResultsGameHeaderTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 03.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ResultsSoloGameHeaderTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBottom;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@end
