//
//  ResultsSoloGameRegistredTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResultsSoloGameRegistredTableViewCell.h"
#import "CDPlayer+CoreDataClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ResultsSoloGameRegistredData.h"

@implementation ResultsSoloGameRegistredTableViewCell

-(void)configureWithData:(id)cellData
{
    if (![cellData isEqual:[ResultsSoloGameRegistredData class]])
    {
        return;
    }
    
    ResultsSoloGameRegistredData *data = cellData;
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: data.urlAvatar]
                        placeholderImage:nil
                                 options:SDWebImageRetryFailed
                               completed:nil];
    
    [self.logoClan sd_setImageWithURL:[NSURL URLWithString: data.urlClan]
                   placeholderImage:nil
                            options:SDWebImageRetryFailed
                          completed:nil];
    
    _soloPoints.text = data.soloPoints;
    
    if (data.clanPoints)
    {
        _clanPoints.text = data.clanPoints;
        _clanName.text = [NSString stringWithFormat:@"Клан %@", data.clanName];
    }
    else
    {
        _clanName.hidden = YES;
        _clanPoints.hidden = YES;
    }
    
    
}

@end
