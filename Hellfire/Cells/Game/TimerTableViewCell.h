//
//  TimerTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "CounterView.h"
#import "UIImageViewRounded.h"

@interface TimerTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet UIImageViewRounded *avatarCurrent;
@property (weak, nonatomic) IBOutlet UIImageViewRounded *avatarNext;
@property (weak, nonatomic) IBOutlet UIView *nextPlayerView;

@end
