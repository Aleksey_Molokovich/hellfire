//
//  TimerTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 21.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TimerTableViewCell.h"
#import "TimerModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation TimerTableViewCell


- (void)configureWithData:(id)data
{
    
    if (![data isKindOfClass:[TimerModel class]]) {
        return;
    }
    
    TimerModel *model = data;
    
    _counterView.shoudShowMinutes=YES;
    [_counterView setTime:model.seconds];
    
    if (!model.avatarCurrentUrl && !model.avatarNextUrl) {
        _nextPlayerView.hidden = YES;
        return;
    }
    
    [self.avatarCurrent sd_setImageWithURL:[NSURL  URLWithString:model.avatarCurrentUrl]
                               placeholderImage:nil
                                        options:SDWebImageRetryFailed
                                      completed:nil];
    
    [self.avatarNext sd_setImageWithURL:[NSURL  URLWithString:model.avatarNextUrl]
                             placeholderImage:nil
                                      options:SDWebImageRetryFailed
                                    completed:nil];
    

}

@end
