//
//  PrepareWithTimerTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 21.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "PrepareWithTimerTableViewCell.h"
#import "TimerModel.h"
#import "PrepareWithTimerTableViewCellAction.h"
#import "CallBackActionModel.h"

@implementation PrepareWithTimerTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [_startBtn roundCorner];
    _startBtn.delegate = self;
}


- (void)configureWithData:(id)data
{
    
    if (![data isKindOfClass:[TimerModel class]]) {
        return;
    }
    
    TimerModel *model = data;
    
    _counterView.shoudShowMinutes=YES;
    [_counterView setTime:model.seconds];
}

- (void)pressBtn
{
    CallBackActionModel *object=[CallBackActionModel new];
    object.cellIndex = self.cellIndex;
    object.action = kPrepareWithTimerTableViewCellActionPressBtn;
    [self.output callBackObject:object];
}
@end
