//
//  PrepareWithTimerTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "CounterView.h"
#import "ButtonYellowStart.h"
#import "ButtonViewProtocol.h"

@interface PrepareWithTimerTableViewCell : BaseTableViewCell<ButtonViewProtocol>
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet ButtonYellowStart *startBtn;
@end
