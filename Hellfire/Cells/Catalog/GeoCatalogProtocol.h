//
//  GeoCatalogProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef GeoCatalogProtocol_h
#define GeoCatalogProtocol_h

@protocol GeoCatalogProtocol<NSObject>

- (BOOL)isDarkStyle;

@end

#endif /* GeoCatalogProtocol_h */
