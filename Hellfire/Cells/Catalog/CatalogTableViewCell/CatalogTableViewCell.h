//
//  CatalogDarkStyleTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 12.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface CatalogTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *lblRegion;

@end
