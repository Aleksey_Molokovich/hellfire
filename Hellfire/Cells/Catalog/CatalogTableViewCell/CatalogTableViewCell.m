//
//  CatalogDarkStyleTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 12.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CatalogTableViewCell.h"
#import "CatalogModel.h"
#import "GeoCatalogProtocol.h"
#import "UIColor+HF.h"
@interface CatalogTableViewCell()
@property (weak, nonatomic) id<GeoCatalogProtocol> output;

@end

@implementation CatalogTableViewCell
@dynamic output;



- (void)configureWithData:(CatalogModel*)data
{
    _label.text=data.name;
    if (data.region)
    {
        _lblRegion.text = data.region;
    }
    if ([self.output respondsToSelector:@selector(isDarkStyle)])
    {
        if ([self.output isDarkStyle])
        {
            _label.textColor = [UIColor whiteColor];
            _lblRegion.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        }
        else
        {
            _label.textColor = [UIColor hfBlue];
            _lblRegion.textColor = [[UIColor hfBlue] colorWithAlphaComponent:0.5];
        }
    }
}

@end
