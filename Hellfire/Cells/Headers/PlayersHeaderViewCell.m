//
//  PlayersHeaderViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersHeaderViewCell.h"
#import "PlayersHeaderViewCellProtocol.h"
#import "UIFont+HF.h"

@implementation PlayersHeaderViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.lblTitle.font = [UIFont hfRegularSize:14];
}


- (void)setHiddenFilter:(BOOL)hiddenFilter
{
    _imgFilter.hidden = hiddenFilter;
    _btnFilter.hidden = hiddenFilter;
}

- (IBAction)pressFilter:(id)sender
{
    if ([_delegate respondsToSelector:@selector(openFilterAtIndex:)])
    {
        [_delegate openFilterAtIndex:self.section];
    }
}

- (IBAction)pressExpand:(id)sender
{
    if ([_delegate respondsToSelector:@selector(pressExpandAtIndex:)]) {
        [_delegate pressExpandAtIndex:self.section];
    }
}

- (void)expandList:(BOOL)expand
{
    if (expand)
    {
        _bottomSpliteLineThin.hidden = NO;
        _bottomSpliteLineRegular.hidden = YES;
        _imgViewExpand.image = [UIImage imageNamed:@"chevron-up"];
    }
    else
    {
        _bottomSpliteLineThin.hidden = YES;
        _bottomSpliteLineRegular.hidden = NO;

        _imgViewExpand.image = [UIImage imageNamed:@"chevron-down"];
    }
}
@end
