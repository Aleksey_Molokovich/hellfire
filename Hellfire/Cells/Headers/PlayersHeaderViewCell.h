//
//  PlayersHeaderViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayersHeaderViewCellProtocol.h"

@interface PlayersHeaderViewCell : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UIImageView *imgViewExpand;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnExpand;
@property (weak, nonatomic) IBOutlet UIView *bottomSpliteLineThin;
@property (weak, nonatomic) IBOutlet UIView *bottomSpliteLineRegular;


@property (assign, nonatomic) NSInteger section;
@property (weak, nonatomic) id<PlayersHeaderViewCellProtocol> delegate;
- (void) expandList:(BOOL)expand;
@property (nonatomic, assign) BOOL hiddenFilter;
@end
