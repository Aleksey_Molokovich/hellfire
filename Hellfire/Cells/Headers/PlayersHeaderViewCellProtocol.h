//
//  RequestPlayersHeaderProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 26.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef RequestPlayersHeaderProtocol_h
#define RequestPlayersHeaderProtocol_h
@protocol PlayersHeaderViewCellProtocol <NSObject>

- (void)openFilterAtIndex:(NSInteger)index;

- (void)pressExpandAtIndex:(NSInteger)index;

@end

#endif /* RequestPlayersHeaderProtocol_h */
