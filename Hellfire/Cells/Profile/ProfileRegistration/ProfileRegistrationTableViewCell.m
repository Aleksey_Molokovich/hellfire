//
//  ProfileRegistrationTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 11.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ProfileRegistrationTableViewCell.h"
#import "ButtonSignInOrOn.h"

@interface ProfileRegistrationTableViewCell()
@property (weak, nonatomic) IBOutlet ButtonSignInOrOn *buttonView;

@end
@implementation ProfileRegistrationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setOutput:(id<CallBackActionProtocol>)output
{
    _buttonView.output = output;
}
@end
