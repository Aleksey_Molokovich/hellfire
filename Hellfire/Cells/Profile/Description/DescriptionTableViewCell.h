//
//  DescriptionTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DescriptionTableViewCell:BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;


@end
