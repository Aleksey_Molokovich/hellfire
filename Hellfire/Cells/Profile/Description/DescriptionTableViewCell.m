//
//  DescriptionTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "DescriptionTableViewCell.h"
#import "UIFont+HF.h"

@implementation DescriptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}



- (void)configureWithData:(id)data
{
    
    if (![data isKindOfClass:[NSString class]])
    {
        return;
    }
    NSString *string = data;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString new] initWithString:string
                                                                                       attributes:@{NSFontAttributeName:[UIFont hfMediumSize:12],
                                                                                                    NSForegroundColorAttributeName:[UIColor blackColor]}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 8;
    [paragraphStyle setAlignment:NSTextAlignmentNatural];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, string.length)];
    
    _lblInfo.attributedText=attributedString;
    
}

@end
