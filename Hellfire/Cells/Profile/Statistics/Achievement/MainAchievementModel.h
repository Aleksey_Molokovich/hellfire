//
//  MainAchievementModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AverageAndMaxResultDefines.h"

@interface MainAchievementModel : NSObject
@property (nonatomic) NSNumber *v1;
@property (nonatomic) NSNumber *v2;
@property (nonatomic,assign) AverageAndMaxResultType type;
@property (nonatomic,assign) BOOL isDarkThem;
@property (nonatomic) NSString *urlAvatar;
@end
