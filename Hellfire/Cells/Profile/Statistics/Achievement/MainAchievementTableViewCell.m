//
//  MainAchievementTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "MainAchievementTableViewCell.h"
#import "MainAchievementModel.h"
#import "AverageAndMaxResultDefines.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"
@implementation MainAchievementTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[MainAchievementModel class]]) {
        return;
    }
    
    MainAchievementModel *model = data;
    
    _labelNumbers.text=[NSString stringWithFormat:@"%@ / %@",model.v1,model.v2];
//    if ([model.v1 intValue] == 0 || [model.v2 intValue] ==0 ) {
//        _labelNumbers.text=@"- / -";
//    }
    _labelNumbers.font = [UIFont hfBoldSize:20];
    UIImage *img;
    switch (model.type) {
        case AverageAndMaxResultTypeQualitySmoke:
            img=[UIImage imageNamed:@"game-smoke-quality"];
            _labelTitle.text=@"Сред. / макс. качество дыма";
            break;
        case AverageAndMaxResultTypeSpeedSmoke:
            img=[UIImage imageNamed:@"game-smoke-speed"];
            _labelTitle.text=@"Сред. / макс. скорость дыма";
            break;
        case AverageAndMaxResultTypePoints:
            img=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Очки за раунд / общий счёт партии";
            break;
        case AverageAndMaxResultTypeNumberOfWins:
            img=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Побед / поражений";
            break;
        case AverageAndMaxResultTypeVolume:
            img=[UIImage imageNamed:@"game-volume-result"];
            _labelTitle.text=@"Общий объем дыма в литрах";
            _labelNumbers.text=[NSString stringWithFormat:@"%@",model.v2];
//            if ([model.v2 intValue] <= 0) {
//                _labelNumbers.text=@"-";
//            }
            break;
        case AverageAndMaxResultTypeSoloScore:
            img=[UIImage imageNamed:@"solo_stat"];
            _labelTitle.text=@"Всего партий / рейтинг Solo";
            break;
            
        case AverageAndMaxResultTypeHotseatScore:
            img=[UIImage imageNamed:@"hotseat_stat"];
            _labelTitle.text=@"Всего партий / рейтинг Hotseat";
            break;
        default:
            break;
    }
    _image.image = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    _image.tintColor =model.isDarkThem?[UIColor blackColor]:[UIColor whiteColor];
    _labelTitle.textColor = model.isDarkThem?[UIColor hfGray]:[UIColor whiteColor];
    _labelNumbers.textColor = model.isDarkThem?[UIColor blackColor]:[UIColor whiteColor];
    
}

@end
