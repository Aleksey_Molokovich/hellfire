//
//  MainAchievementTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MainAchievementTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *labelNumbers;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end
