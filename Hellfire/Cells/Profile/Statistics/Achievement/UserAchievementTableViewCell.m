//
//  UserAchievementTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 25.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "UserAchievementTableViewCell.h"
#import "MainAchievementModel.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"
#import "AverageAndMaxResultDefines.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UserAchievementTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[MainAchievementModel class]]) {
        return;
    }
    
    MainAchievementModel *model = data;
    
    _labelNumbers.text=[NSString stringWithFormat:@"%@", model.v2];
    _labelNumbers.font = [UIFont hfBoldSize:20];
    UIImage *img;
    switch (model.type) {
        case AverageAndMaxResultTypeQualitySmoke:
            img=[UIImage imageNamed:@"game-smoke-quality"];
            _labelTitle.text=@"Макс. качество дыма";
            break;
        case AverageAndMaxResultTypeSpeedSmoke:
            img=[UIImage imageNamed:@"game-smoke-speed"];
            _labelTitle.text=@"Макс. скорость дыма";
            break;
        case AverageAndMaxResultTypePoints:
            img=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Макс. счёт партии";
            break;
        case AverageAndMaxResultTypeRoundPoints:
            img=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Макс. счёт в раунде";
            break;
        case AverageAndMaxResultTypeNumberOfWins:
            img=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Побед / поражений";
            break;
        case AverageAndMaxResultTypeVolume:
            img=[UIImage imageNamed:@"game-volume-result"];
            _labelTitle.text=@"Макс. объём дыма";
            break;
        case AverageAndMaxResultTypeSoloScore:
            img=[UIImage imageNamed:@"solo_stat"];
            _labelTitle.text=@"Всего партий / рейтинг Solo";
            break;
            
        case AverageAndMaxResultTypeHotseatScore:
            img=[UIImage imageNamed:@"hotseat_stat"];
            _labelTitle.text=@"Всего партий / рейтинг Hotseat";
            break;
        default:
            break;
    }
    _image.image = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    _image.tintColor =model.isDarkThem?[UIColor blackColor]:[UIColor whiteColor];
    _labelTitle.textColor = model.isDarkThem?[UIColor hfGray]:[UIColor whiteColor];
    _labelNumbers.textColor = model.isDarkThem?[UIColor blackColor]:[UIColor whiteColor];
    
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: model.urlAvatar]
                   placeholderImage:[UIImage imageNamed:@"avatar"]
                            options:SDWebImageRetryFailed
                          completed:nil];
}

@end
