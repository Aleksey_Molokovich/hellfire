//
//  DateRangeFilterModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateRangeFilterModel : NSObject
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *dateFrom;
@property (nonatomic) NSString *dateTo;
@end
