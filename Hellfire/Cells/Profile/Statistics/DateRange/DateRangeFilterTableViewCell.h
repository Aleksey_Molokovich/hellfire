//
//  DateRangeFilterTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 14.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DateRangeFilterTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

@end
