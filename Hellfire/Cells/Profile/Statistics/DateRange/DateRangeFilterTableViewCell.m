//
//  DateRangeFilterTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 14.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "DateRangeFilterTableViewCell.h"
#import "DateRangeFilterTableViewCellAction.h"
#import "DateRangeFilterModel.h"

@interface DateRangeFilterTableViewCell()

@property (nonatomic) DateRangeFilterModel *model;

@end

@implementation DateRangeFilterTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[DateRangeFilterModel class]]) {
        return;
    }
    
    self.model = data;
    
    _lblTo.text =[NSString stringWithFormat:@"по %@", self.model.dateTo];
    _title.text = self.model.title;
    _lblFrom.text =[NSString stringWithFormat:@"с %@", self.model.dateFrom];
}

- (IBAction)fromDate:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kDateRangeFilterFromDate;
        model.data = self.model.dateFrom;
        [self.output callBackObject:model];
    }
}

- (IBAction)toDate:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kDateRangeFilterToDate;
        model.data = self.model.dateTo;
        [self.output callBackObject:model];
    }
}

@end
