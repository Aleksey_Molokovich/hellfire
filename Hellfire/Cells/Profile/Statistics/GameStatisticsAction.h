//
//  GameStatisticsAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 22.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef GameStatisticsAction_h
#define GameStatisticsAction_h

static NSString *const kShareButtonHotseatStatisticAction = @"ShareButtonHotseatStatisticAction";
static NSString *const kShareButtonSoloStatisticAction = @"ShareButtonSoloStatisticAction";
static NSString *const kPopUpButtonHotseatStatisticAction = @"PopUpButtonHotseatStatisticAction";
static NSString *const kPopUpButtonSoloStatisticAction = @"PopUpButtonSoloStatisticAction";

#endif /* GameStatisticsAction_h */
