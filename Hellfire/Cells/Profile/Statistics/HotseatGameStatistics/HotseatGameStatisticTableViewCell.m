//
//  HotseatGameStatisticTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatGameStatisticTableViewCell.h"
#import "UsersStatisticsModel.h"
#import "UIFont+HF.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GameStatisticsAction.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
@interface HotseatGameStatisticTableViewCell()

@property (nonatomic) UserInfo *user;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;
@property (weak, nonatomic) IBOutlet UIImageView *actionImage;
@end

@implementation HotseatGameStatisticTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[UsersStatisticsGame class]]) {
        return;
    }
    
    UsersStatisticsGame *model = data;
    
    self.user = model.user;
    
    if ([CDAccount MR_findFirst].user.id == [model.user.id intValue]) {
        _actionBtn.enabled = NO;
        _actionImage.hidden = YES;
    }
    else{
        _actionBtn.enabled = YES;
        _actionImage.hidden = NO;
    }
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: model.user.avatarPath]
                   placeholderImage:[UIImage imageNamed:@"avatar"]
                            options:SDWebImageRetryFailed
                          completed:nil];
    
    _lblNickname.text = model.user.userName;
    _lblNickname.font = [UIFont hfRegularSize:14];
    _lblPoints.text = [NSString stringWithFormat:@"Solo %@ / %@ Hotseat", model.user.soloPoints,model.user.hotseatPoints];
    
    _lblAvePoints.font = [UIFont hfMediumSize:14];
    _lblMaxPoints.font = [UIFont hfMediumSize:14];
    _lblAveSpeed.font = [UIFont hfMediumSize:14];
    _lblMaxSpeed.font = [UIFont hfMediumSize:14];
    _lblAveQuality.font = [UIFont hfMediumSize:14];
    _lblMaxQuality.font = [UIFont hfMediumSize:14];
    _lblVolume.font = [UIFont hfMediumSize:14];
    _lblGamePoints.font = [UIFont hfMediumSize:14];
    _lblRoundPoints.font = [UIFont hfMediumSize:14];
    
    _lblGamePointsTitle.font = [UIFont hfMediumItalicSize:12];
    if ([model.points intValue]>0) {
        _lblGamePointsTitle.text = [NSString stringWithFormat:@"Победа. Изменение рейтинга:"];
    }
    else
    {
        _lblGamePointsTitle.text = [NSString stringWithFormat:@"Поражение. Изменение рейтинга:"];
    }
    
    
    if ([model.points intValue]>0) {
        _lblGamePoints.text =[NSString stringWithFormat:@"+%@", [model.points stringValue]];
    }else{
        _lblGamePoints.text =[NSString stringWithFormat:@"%@", [model.points stringValue]];

    }
    
    _lblRoundPoints.text = [model.score stringValue];
    
    
    _lblAvePoints.text = [@(model.indicators.points.v1) stringValue];
    _lblMaxPoints.text = [@(model.indicators.points.v2) stringValue];
    
    _lblAveSpeed.text = [@(model.indicators.speed.v1) stringValue];
    _lblMaxSpeed.text = [@(model.indicators.speed.v2) stringValue];
    
    _lblAveQuality.text = [@(model.indicators.quality.v1) stringValue];
    _lblMaxQuality.text = [@(model.indicators.quality.v2) stringValue];
    
    _lblVolume.text = [@(model.indicators.volume.v2) stringValue];
}

- (IBAction)popUp:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kPopUpButtonHotseatStatisticAction;
        model.data = self.user;
        [self.output callBackObject:model];
    }
}


@end
