//
//  HotseatGameStatisticTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 20.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface HotseatGameStatisticTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *lblNickname;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;



@property (weak, nonatomic) IBOutlet UILabel *lblGamePointsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGamePoints;

@property (weak, nonatomic) IBOutlet UILabel *lblRoundPoints;


@property (weak, nonatomic) IBOutlet UILabel *lblAvePoints;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblAveQuality;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxQuality;
@property (weak, nonatomic) IBOutlet UILabel *lblAveSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@end
