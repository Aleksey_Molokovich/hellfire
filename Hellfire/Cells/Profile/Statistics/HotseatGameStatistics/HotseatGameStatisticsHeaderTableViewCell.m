//
//  HotseatGameStatisticsHeaderTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatGameStatisticsHeaderTableViewCell.h"
#import "UsersStatisticsModel.h"
#import "UIFont+HF.h"
#import "GameStatisticsAction.h"

@implementation HotseatGameStatisticsHeaderTableViewCell

- (void)configureWithData:(id)data
{
    
    if (![data isKindOfClass:[UsersStatisticsHotseatHistoryModel class]]) {
        return;
    }
    
    UsersStatisticsHotseatHistoryModel *model = data;
    self.gameId = model.id;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    
    NSTimeInterval unixTimeStamp = [model.date integerValue]/1000.0;
    _lblDate.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:unixTimeStamp]];
    _lblDate.font = [UIFont hfBoldSize:14];
    _members.font = [UIFont hfMediumSize:14];
    _lblRoundsxTime.font = [UIFont hfMediumSize:14];
    
    _members.text = [@([model.diaryItems count]) stringValue];
    _lblRoundsxTime.text = [NSString stringWithFormat:@"%@ x %@",model.roundCount, model.duration];
}

- (IBAction)share:(id)sender {
    
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kShareButtonHotseatStatisticAction;
        model.data = self.gameId;
        [self.output callBackObject:model];
    }
}


@end
