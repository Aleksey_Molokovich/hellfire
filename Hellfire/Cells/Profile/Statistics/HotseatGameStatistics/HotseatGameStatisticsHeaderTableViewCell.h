//
//  HotseatGameStatisticsHeaderTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 20.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface HotseatGameStatisticsHeaderTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblRoundsxTime;
@property (weak, nonatomic) IBOutlet UILabel *members;

@property (nonatomic) NSNumber *gameId;

@end
