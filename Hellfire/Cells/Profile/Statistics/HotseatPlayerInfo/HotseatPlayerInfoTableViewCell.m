//
//  HotseatPlayerInfoTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatPlayerInfoTableViewCell.h"
#import "HFHotseatGameResult.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIFont+HF.h"

@interface HotseatPlayerInfoTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblRounds;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@end


@implementation HotseatPlayerInfoTableViewCell



- (void)configureWithData:(id)cellData
{
    if ([cellData isKindOfClass:[HFHotseatPlayer class]])
    {
        HFHotseatPlayer *data = cellData;
        
        _lblName.text = data.userName;
        
        _lblRounds.text = [NSString stringWithFormat:@"%@/%@",data.points,data.maxPoints];
        
        [self.avatar sd_setImageWithURL:[NSURL URLWithString: data.avatarPath]
                       placeholderImage:nil
                                options:SDWebImageRetryFailed
                              completed:nil];
    }
}

@end
