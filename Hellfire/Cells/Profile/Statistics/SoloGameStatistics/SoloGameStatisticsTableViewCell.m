//
//  SoloGameStatisticsTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 15.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "SoloGameStatisticsTableViewCell.h"
#import "UsersStatisticsModel.h"
#import "UIFont+HF.h"
#import "GameStatisticsAction.h"

@implementation SoloGameStatisticsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[UsersStatisticsSoloHistoryModel class]])
        return;
    
    UsersStatisticsSoloHistoryModel *model = data;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    
    NSTimeInterval unixTimeStamp = [model.date integerValue]/1000.0;
    _lblDate.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:unixTimeStamp]];
    _lblDate.font = [UIFont hfBoldSize:14];
    
    _lblAvePoints.font = [UIFont hfMediumSize:14];
    _lblMaxPoints.font = [UIFont hfMediumSize:14];
    _lblAveSpeed.font = [UIFont hfMediumSize:14];
    _lblMaxSpeed.font = [UIFont hfMediumSize:14];
    _lblAveQuality.font = [UIFont hfMediumSize:14];
    _lblMaxQuality.font = [UIFont hfMediumSize:14];
    _lblVolume.font = [UIFont hfMediumSize:14];
    _lblGamePoints.font = [UIFont hfMediumSize:14];
    _lblGameRoundsXTime.font = [UIFont hfMediumSize:14];

    
    _lblGamePoints.text = [model.score stringValue];
    _lblGameRoundsXTime.text = [NSString stringWithFormat:@"%@ x %@",model.roundCount, model.duration];
    
    _lblAvePoints.text = [@(model.indicators.points.v1) stringValue];
    _lblMaxPoints.text = [@(model.indicators.points.v2) stringValue];
    
    _lblAveSpeed.text = [@(model.indicators.speed.v1) stringValue];
    _lblMaxSpeed.text = [@(model.indicators.speed.v2) stringValue];
    
    _lblAveQuality.text = [@(model.indicators.quality.v1) stringValue];
    _lblMaxQuality.text = [@(model.indicators.quality.v2) stringValue];
    
    _lblVolume.text = [@(model.indicators.volume.v2) stringValue];
    
    self.gameId = model.id;
}

- (IBAction)share:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kShareButtonSoloStatisticAction;
        model.data = self.gameId;
        [self.output callBackObject:model];
    }
}


@end
