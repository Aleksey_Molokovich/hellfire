//
//  SoloGameStatisticsTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 15.02.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface SoloGameStatisticsTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgShare;

@property (weak, nonatomic) IBOutlet UILabel *lblGamePoints;
@property (weak, nonatomic) IBOutlet UILabel *lblGameRoundsXTime;

@property (weak, nonatomic) IBOutlet UILabel *lblAvePoints;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblAveQuality;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxQuality;
@property (weak, nonatomic) IBOutlet UILabel *lblAveSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;

@property (nonatomic) NSNumber *gameId;
@end
