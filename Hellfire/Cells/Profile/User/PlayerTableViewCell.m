//
//  PlayerTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 24.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerTableViewCell.h"
#import "CDPlayer+CoreDataClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PlayerTableViewAction.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"

@implementation PlayerTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[CDPlayer class]])
        return ;
    CDPlayer *person = (CDPlayer*)data;
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString: person.avatarPath]
                   placeholderImage:[UIImage imageNamed:@"avatar"]
                            options:SDWebImageRetryFailed
                          completed:nil];
    
    _lblNickname.text = person.userName;
    _lblPoints.text = [NSString stringWithFormat:@"Solo %hd / %hd Hotseat", person.soloPoints,person.hotseatPoints];
    
    if ([CDAccount MR_findFirst].user.id == person.id) {
        _actionBtn.enabled = NO;
        _actionImage.hidden = YES;
    }
    else{
        _actionBtn.enabled = YES;
        _actionImage.hidden = NO;
    }
}

- (IBAction)pressAdditionalActionBtn:(id)sender {
    CallBackActionModel *object=[CallBackActionModel new];
    object.cellIndex = self.cellIndex;
    object.action = HFActionPlayerTableView;
    
    [self.output callBackObject:object];
    
}

@end
