//
//  HomePageTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HomePageTableViewCell.h"
#import "HomePageTableViewCellAction.h"
#import "CDClan+CoreDataClass.h"

@implementation HomePageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[CDClan class]])
    {
        return;
    }
    
    CDClan *clan = data;
    
    _urlSite = clan.siteUrl;
    _textForSharing = clan.netName;
    _lblUrl.text = clan.siteUrl;
}

- (IBAction)openSite:(id)sender {
    CallBackActionModel *model=[CallBackActionModel new];
    model.cellIndex = self.cellIndex;
    model.action = kHomePageTableViewCellOpenSite;
    model.data = self.urlSite;
    [self.output callBackObject:model];
}


- (IBAction)share:(id)sender {
    CallBackActionModel *model=[CallBackActionModel new];
    model.cellIndex = self.cellIndex;
    model.action = kHomePageTableViewCellShare;
    model.data = @[self.urlSite?self.urlSite:@"", self.textForSharing?self.textForSharing:@""];
    [self.output callBackObject:model];
}

@end
