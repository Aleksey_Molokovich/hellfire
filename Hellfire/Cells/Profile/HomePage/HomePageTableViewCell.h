//
//  HomePageTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface HomePageTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUrl;

@property (nonatomic) NSString *textForSharing;
@property (nonatomic) NSString *urlSite;
@end
