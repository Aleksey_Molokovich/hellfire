//
//  HomePageTableViewCellAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

static NSString *const kHomePageTableViewCellOpenSite = @"HomePageTableViewCellOpenSite";

static NSString *const kHomePageTableViewCellShare = @"HomePageTableViewCellShare";
