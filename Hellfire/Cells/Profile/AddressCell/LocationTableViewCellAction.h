//
//  LocationTableViewCellAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef LocationTableViewCellAction_h
#define LocationTableViewCellAction_h

static NSString *const ButtonLocationTableViewCellActionPress = @"ButtonLocationTableViewCellActionPress";

#endif /* LocationTableViewCellAction_h */
