//
//  LocationTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
@class CDPlace;
@interface LocationTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (nonatomic) CDPlace *place;
@end
