//
//  LocationTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "LocationTableViewCell.h"
#import "UIFont+HF.h"
#import "CDPlace+CoreDataClass.h"
#import "CDAddress+CoreDataClass.h"
#import "LocationTableViewCellAction.h"
@implementation LocationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _lblName.font = [UIFont hfMediumSize:14];
    _lblAddress.font = [UIFont hfRegularSize:12];
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[CDPlace class]])
    {
        return;
    }
    _place = data;
    
    _lblName.text = ((CDPlace*)data).name;
    _lblAddress.text = ((CDPlace*)data).address.formattedAddress;
    
}


- (IBAction)pressLocation:(id)sender {
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = ButtonLocationTableViewCellActionPress;
        model.data =_place;
        [self.output callBackObject:model];
    }
}

@end
