//
//  PlayerAndClanProfileTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 29.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "UILabelForNumber.h"

@interface PlayerAndClanProfileTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblGeoposition;
@property (weak, nonatomic) IBOutlet UILabelForNumber *lblSoloPoints;
@property (weak, nonatomic) IBOutlet UILabelForNumber *lblHotseatPoints;

@property (weak, nonatomic) IBOutlet UIImageView *soloImg;
@property (weak, nonatomic) IBOutlet UIImageView *hotseatImg;
@end
