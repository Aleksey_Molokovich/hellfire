//
//  PlayerAndClanProfileCellAction.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef PlayerAndClanProfileCellAction_h
#define PlayerAndClanProfileCellAction_h

static NSString *const kPlayerAndClanProfileCellShowMembersOfClan = @"PlayerAndClanProfileCellShowMembersOfClan";

#endif /* PlayerAndClanProfileCellAction_h */
