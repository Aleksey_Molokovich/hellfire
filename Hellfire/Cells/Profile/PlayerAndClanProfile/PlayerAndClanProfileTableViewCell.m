//
//  PlayerAndClanProfileTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 29.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayerAndClanProfileTableViewCell.h"
#import "CDPlayer+CoreDataClass.h"
#import "CDClan+CoreDataClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIColor+HF.h"
#import "PlayerAndClanProfileCellAction.h"

@implementation PlayerAndClanProfileTableViewCell


- (void)configureWithData:(id)data
{
    if ([data isKindOfClass:[CDPlayer class]])
        [self configureWithPlayer:data];
    
    if ([data isKindOfClass:[CDClan class]])
        [self configureWithClan:data];
    
    self.soloImg.image = [[UIImage imageNamed:@"solo_stat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.soloImg.tintColor = [UIColor blackColor];
    self.hotseatImg.image = [self.hotseatImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.hotseatImg.tintColor = [UIColor blackColor];
}

- (void)configureWithPlayer:(CDPlayer*)player
{
    [self.imgAvatar sd_setImageWithURL:[NSURL URLWithString: player.avatarPath]
                      placeholderImage:[UIImage imageNamed:@"avatar"]
                               options:SDWebImageRetryFailed
                             completed:nil];
    
    _lblName.text = player.userName;
    _lblGeoposition.text =[NSString stringWithFormat:@"%@, %@", player.city, player.country];
    
    _lblSoloPoints.text = [@(player.soloPoints) stringValue];
    _lblHotseatPoints.text = [@(player.hotseatPoints) stringValue];
}

- (void)configureWithClan:(CDClan*)clan
{
    [self.imgAvatar sd_setImageWithURL:[NSURL URLWithString: clan.logoUrl]
                      placeholderImage:[UIImage imageNamed:@"avatar"]
                               options:SDWebImageRetryFailed
                             completed:nil];
    
    _lblName.text = clan.name;
    _lblGeoposition.text = [NSString stringWithFormat:@"Открыть список участников (%@)", @(clan.memberCount)];
    
    _lblSoloPoints.text = [@(clan.soloPoints) stringValue];
    _lblHotseatPoints.text = [@(clan.hotseatPoints) stringValue];
}

- (IBAction)members:(id)sender {
    
    if ([self.output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.cellIndex = self.cellIndex;
        model.action = kPlayerAndClanProfileCellShowMembersOfClan;
        [self.output callBackObject:model];
    }
    
}

@end
