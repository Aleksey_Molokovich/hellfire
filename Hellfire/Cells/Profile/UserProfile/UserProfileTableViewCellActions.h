//
//  UserProfileTableViewCellActions.h
//  Hellfire
//
//  Created by Алексей Молокович on 22.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef UserProfileTableViewCellActions_h
#define UserProfileTableViewCellActions_h

static NSString *const HFActionEditUserNickname = @"HFActionEditUserNickname";
static NSString *const HFActionEditUserLocation = @"HFActionEditUserLocation";
static NSString *const HFActionEditUserPassword = @"HFActionEditUserPassword";
static NSString *const HFActionShowSoloStatistics = @"HFActionShowSoloStatistics";
static NSString *const HFActionShowHotseatStatistics = @"HFActionShowHotseatStatistics";

static NSString *const kHFActionShowGameRule = @"HFActionShowGameRule";
static NSString *const kHFActionShowAgreement = @"HFActionShowAgreement";

#endif /* UserProfileTableViewCellActions_h */
