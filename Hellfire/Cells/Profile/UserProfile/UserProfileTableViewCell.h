//
//  UserProfileTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "UILabelForNumber.h"

@interface UserProfileTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblNickname;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblRule;

@property (weak, nonatomic) IBOutlet UIImageView *soloImg;
@property (weak, nonatomic) IBOutlet UIImageView *hotseatImg;
@property (weak, nonatomic) IBOutlet UILabelForNumber *soloPoints;
@property (weak, nonatomic) IBOutlet UILabelForNumber *hotseatPoints;

@end
