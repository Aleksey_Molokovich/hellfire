//
//  UserProfileTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 21.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserProfileTableViewCell.h"

#import "CDAccount+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import <LGHelper/UIColor+LGHelper.h>

#import "UserProfileTableViewCellActions.h"

@implementation UserProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.soloImg.image = [[UIImage imageNamed:@"solo_stat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.soloImg.tintColor = [UIColor blackColor];
    self.hotseatImg.image = [[UIImage imageNamed:@"hotseat_stat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.hotseatImg.tintColor = [UIColor blackColor];
    
    CDAccount *account = [CDAccount MR_findFirst];
    self.backgroundColor = [UIColor colorWithHEX:(unsigned)account.colorAvatar];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithData:(id)data
{
    CDAccount *account = [CDAccount MR_findFirst];
    _lblCity.text = [NSString stringWithFormat:@"%@, %@", account.city.name, account.country.name];
    _lblNickname.text = account.user.nickname;
    _lblEmail.text = account.email;
    _soloPoints.text = [@(account.soloPoints) stringValue];
    _hotseatPoints.text = [@(account.hotseatPoints) stringValue];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"avatar_big.png"];
    
    UIImage *avatar=[UIImage imageWithContentsOfFile:filePath];
    
    [self.imageAvatar sd_setImageWithURL:[NSURL URLWithString: account.avatarPath]
                      placeholderImage:[avatar imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                               options:SDWebImageRetryFailed
                               completed:nil];
    self.soloImg.image = [[UIImage imageNamed:@"solo_stat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    

    if (account.soloRule && account.hotseatRule) {
        self.lblRule.alpha = 0.5;
    }else
    {
        self.lblRule.alpha = 1;
    }
    
}

+(CGFloat)estimateHeight
{
    return 730;
}

- (IBAction)clickEditNickname:(id)sender {
    [self callBackWithAction:HFActionEditUserNickname];
}
- (IBAction)clickEditLocation:(id)sender {
    [self callBackWithAction:HFActionEditUserLocation];
}

- (IBAction)clickEditPassword:(id)sender {
    [self callBackWithAction:HFActionEditUserPassword];
}

- (IBAction)clickSoloStatistic:(id)sender {
    [self callBackWithAction:HFActionShowSoloStatistics];
}

- (IBAction)clickHotseatStatistic:(id)sender {
    [self callBackWithAction:HFActionShowHotseatStatistics];
}

- (IBAction)showRules:(id)sender {
    [self callBackWithAction:kHFActionShowGameRule];
}

- (IBAction)showAgreement:(id)sender {
    [self callBackWithAction:kHFActionShowAgreement];
}

- (void)callBackWithAction:(NSString*)action
{
    CallBackActionModel *object=[CallBackActionModel new];
    object.cellIndex = self.cellIndex;
    object.action = action;
    
    [self.output callBackObject:object];
}



@end
