//
//  FirstTableViewCellModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 01.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameModuleDefenitions.h"

@interface FirstTableViewCellModel : NSObject
@property (assign, nonatomic) GameModuleInputType type;
@property (strong, nonatomic) id data;
@end
