//
//  FirstTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 28.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "FirstTableViewCell.h"
#import "GameModuleDefenitions.h"
#import "FirstTableViewCellModel.h"
#import "HFButtonData.h"
#import "SoloAndHotseatChooceActions.h"

@implementation FirstTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configureWithData:(FirstTableViewCellModel*)data{
    if (![data isKindOfClass:[FirstTableViewCellModel class]]) return;
    
    switch (data.type) {
        case GameModuleInputTypeRootGame:
            _hotseatBtn.enabled = YES;
            _hotseatBtn.alpha = 1;
            break;
        case GameModuleInputTypeRootGameNonAuth:
            _hotseatBtn.enabled = NO;
            _hotseatBtn.alpha = 0.3;
            break;
            
        default:
            break;
    }

}

- (IBAction)clickSolo:(id)sender {
    CallBackActionModel *object=[CallBackActionModel new];
    object.action=SoloAndHotseatChooceActionsSolo;
    [self.output callBackObject:object];
}

- (IBAction)clickHotseat:(id)sender {
    CallBackActionModel *object=[CallBackActionModel new];
    object.action=SoloAndHotseatChooceActionsHotseat;
    [self.output callBackObject:object];
}

@end
