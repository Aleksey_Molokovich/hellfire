//
//  FirstTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "ButtonViewProtocol.h"

@interface FirstTableViewCell : BaseTableViewCell<ButtonViewProtocol>
@property (weak, nonatomic) IBOutlet UIButton *hotseatBtn;

@end
