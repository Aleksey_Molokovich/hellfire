//
//  SoloAndHotseatChooceCellProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef SoloAndHotseatChooceActions_h
#define SoloAndHotseatChooceActions_h


//typedef NS_ENUM(NSInteger,SoloAndHotseatChooceActions){
//    SoloAndHotseatChooceActionsSolo
//};

static NSString *const SoloAndHotseatChooceActionsSolo= @"SoloAndHotseatChooceActionsSolo";
static NSString *const SoloAndHotseatChooceActionsHotseat= @"SoloAndHotseatChooceActionsHotseat";
#endif /* SoloAndHotseatChooceCellProtocol_h */
