//
//  ClanBrifTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 28.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ClanBrifTableViewCell.h"
#import "CDClan+CoreDataClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ClanBrifTableViewAction.h"



@implementation ClanBrifTableViewCell

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[CDClan class]])
        return ;
    CDClan *clan = (CDClan*)data;
    
    [self.logo sd_setImageWithURL:[NSURL URLWithString: clan.logoUrl]
                   placeholderImage:[UIImage imageNamed:@"avatar"]
                            options:SDWebImageRetryFailed
                          completed:nil];
    
    _lblName.text = clan.name;
    _lblNetName.text = clan.netName;
    _lblPoints.text = [NSString stringWithFormat:@"Solo %d / %d Hotseat", clan.soloPoints,clan.hotseatPoints];
    _lblMembers.text = [NSString stringWithFormat:@"Участников %d", clan.memberCount];
}

- (IBAction)pressAdditionalActionBtn:(id)sender {
    CallBackActionModel *object=[CallBackActionModel new];
    object.cellIndex = self.cellIndex;
    object.action = HFActionClanTableView;
    
    [self.output callBackObject:object];
    
}

@end
