//
//  CellManager.m
//  Hellfire
//
//  Created by Алексей Молокович on 30.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CellManager.h"

@implementation CellManager
+(void)registerCellsWithId:(NSArray *)cellsId tableView:(UITableView *)tableView{
    for (NSString *cellId in cellsId) {
        [tableView registerNib:[UINib nibWithNibName:cellId
                                              bundle:nil]
        forCellReuseIdentifier:cellId];
    }
}

+(void)registerCellsWithId:(NSArray *)cellsId
                   headers:(NSArray*)headersId
                 tableView:(UITableView *)tableView
{
    for (NSString *cellId in cellsId) {
        [tableView registerNib:[UINib nibWithNibName:cellId
                                              bundle:nil]
        forCellReuseIdentifier:cellId];
    }
    
    for (NSString *cellId in headersId) {
        [tableView registerNib:[UINib nibWithNibName:cellId
                                              bundle:nil]
forHeaderFooterViewReuseIdentifier:cellId];
    }
}
@end
