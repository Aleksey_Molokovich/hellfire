//
//  CustomViewTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CustomViewTableViewCell.h"
#import "ButtonSignInOrOn.h"
#import "BaseView.h"
#import "CustomViewTableViewCellModel.h"

@implementation CustomViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)prepareForReuse{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithData:(CustomViewTableViewCellModel*)data{
    
    BaseView *view = [[NSClassFromString(data.viewClass) alloc] init];
    
    if (![view isKindOfClass:[UIView class]]) return;
    
    
    [self.contentView addSubview:view];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":view}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":view}]];
}

@end
