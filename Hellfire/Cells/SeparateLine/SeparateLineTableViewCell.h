//
//  SeparateLineTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "SeparateLineTableViewCellModel.h"
@interface SeparateLineTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIView *gradientView;
@property (nonatomic) SeparateLineTableViewCellModel *model;
@end
