//
//  SeparateLineTableViewCellModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SeparateLineTableViewCellModel : NSObject
@property (nonatomic) UIColor *color1;
@property (nonatomic) UIColor *color2;
@end
