//
//  SeparateLineTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 28.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "SeparateLineTableViewCell.h"

@implementation SeparateLineTableViewCell

- (void)configureWithData:(id)data
{
   if(![data isKindOfClass:[SeparateLineTableViewCellModel class]])
   {
       return;
   }
    
    _model= data;
    
    
}

- (void)drawRect:(CGRect)rect
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _gradientView.bounds;
    gradient.colors = @[(id)_model.color1.CGColor, (id)_model.color2.CGColor];
    
    [_gradientView.layer insertSublayer:gradient atIndex:0];
}

@end
