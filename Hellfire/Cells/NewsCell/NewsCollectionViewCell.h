//
//  NewsCollectionViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 08.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
