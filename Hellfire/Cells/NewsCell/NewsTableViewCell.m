//
//  NewsTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 08.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

@import WebKit;

#import "NewsTableViewCell.h"
#import "HFPost.h"
#import "NewsCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIFont+HF.h"
#import "NewsCollectionViewFlowLayout.h"
#import "NewsTableViewCellAction.h"

#define HTML_BODY @"<html><head><style> body {font-family:'Raleway-v4020-Medium'; font-size:12} img{max-width:100%%;height:auto !important;width:auto !important;};</style></head><body style='margin:0; padding:0;'>%@</body></html>"

@interface NewsTableViewCell()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIWebViewDelegate>{
    NSArray *images;
    NSInteger selectedImage;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webText;

@property (weak, nonatomic) IBOutlet UILabel *lblLink;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic) NSString *textForSharing;
@property (nonatomic) NSString *urlForSharing;
@property (nonatomic) NSString *urlSite;


@end

@implementation NewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_collectionView registerNib:[UINib nibWithNibName:@"NewsCollectionViewCell"
                                                bundle:nil]
      forCellWithReuseIdentifier:@"NewsCollectionViewCell"];
    _collectionView.collectionViewLayout=[NewsCollectionViewFlowLayout new];
    selectedImage=0;
    _webText.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)openSite:(id)sender {
    CallBackActionModel *model=[CallBackActionModel new];
    model.cellIndex = self.cellIndex;
    model.action = NewsTableViewCellOpenSite;
    model.data = _urlSite;
    [self.output callBackObject:model];
}

- (void)configureWithData:(HFPost *)data{
    images=data.imageUrls;
    _lblTitle.text=data.title;
 
    
    
    NSString *strTemplateHTML = [NSString stringWithFormat:HTML_BODY, data.article];
    
    if (strTemplateHTML) {
        [_webText loadHTMLString:strTemplateHTML baseURL:nil];
    }
    
    _lblLink.text=data.siteUrl;
    _urlSite = data.siteUrl;
    _textForSharing = data.shareUrl;
    _urlForSharing = data.title;
    [_collectionView reloadData];
}

- (IBAction)clickNextImage:(id)sender {
    selectedImage++;
    if (selectedImage>=images.count) {
        selectedImage=images.count-1;
    }
    [self scrollCollectionView:_collectionView toCenterOfItemAtIndex:selectedImage];
    [self scrollToCalculatorContentViewAtIndex:selectedImage];
}

- (IBAction)clickPrevImage:(id)sender {
    selectedImage--;
    if (selectedImage<0) {
        selectedImage=0;
    }
    [self scrollCollectionView:_collectionView toCenterOfItemAtIndex:selectedImage];
    [self scrollToCalculatorContentViewAtIndex:selectedImage];
}

- (IBAction)clickShare:(id)sender {
    CallBackActionModel *model=[CallBackActionModel new];
    model.cellIndex = self.cellIndex;
    model.action = NewsTableViewCellShare;
    model.data = @[self.urlForSharing?self.urlForSharing:@"", self.textForSharing?self.textForSharing:@""];
    [self.output callBackObject:model];
}


#pragma mark CollectionView

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return images.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return _collectionView.frame.size;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NewsCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewsCollectionViewCell" forIndexPath:indexPath];
    [cell.image sd_setImageWithURL:[NSURL URLWithString:images[indexPath.item]]
                 placeholderImage:nil];

    return cell;
}

- (void)scrollCollectionView:(UICollectionView *)collectionView toCenterOfItemAtIndex:(NSInteger)index {
    CGFloat neededOffsetX = [self collectionView:collectionView xOffsetForItemAtIndex:index];
    [collectionView setContentOffset:CGPointMake(neededOffsetX, 0) animated:YES];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView xOffsetForItemAtIndex:(NSInteger)index {
    UICollectionViewLayoutAttributes *attributes = [collectionView layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    CGRect frame = attributes.frame;
    
    CGFloat neededOffsetX = CGRectGetMinX(frame) + (CGRectGetWidth(frame) / 2) - (CGRectGetWidth(collectionView.bounds) / 2);
    return neededOffsetX;
}

- (void)scrollToCalculatorContentViewAtIndex:(NSInteger)index {
    CGFloat offsetX = CGRectGetWidth(_collectionView.bounds) * index;
    [_collectionView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}

#pragma - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if([self.output respondsToSelector:@selector(callBackObject:)] ){
        if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
            webView.scrollView.scrollEnabled=NO;
            CGRect frame = webView.frame;
            frame.size.height = 1;        // Set the height to a small one.
            
            webView.frame = frame;
            NSInteger height=webView.scrollView.contentSize.height;
            
            if(height<35){
                height=35;
            }
            
            CallBackActionModel *model=[CallBackActionModel new];
            model.cellIndex = self.cellIndex;
            model.action = NewsTableViewCellChangeCellHeight;
            model.data = @(height+370);
            [self.output callBackObject:model];
            
            frame.size.height = height ;
            webView.frame = frame;
            [webView.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        }
        
    }
}

@end
