//
//  NewsTableViewCellOutput.h
//  Hellfire
//
//  Created by Алексей Молокович on 06.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

static NSString *const NewsTableViewCellChangeCellHeight = @"NewsTableViewCellChangeCellHeight";
static NSString *const NewsTableViewCellShare = @"NewsTableViewCellShare";

static NSString *const NewsTableViewCellOpenSite = @"NewsTableViewCellOpenSite";
