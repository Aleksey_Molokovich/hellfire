//
//  CellManager.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const FirsrtCellId=@"FirstTableViewCell";
static NSString *const NewsCellId=@"NewsTableViewCell";
static NSString *const ResultsSoloGameCellId=@"ResultsSoloGameTableViewCell";
static NSString *const ResultsSoloGameHeaderCellId=@"ResultsSoloGameHeaderTableViewCell";
static NSString *const CustomViewTableViewCellId=@"CustomViewTableViewCell";


@interface CellManager : NSObject
+(void)registerCellsWithId:(NSArray*)cellsId tableView:(UITableView*)tableView;
+(void)registerCellsWithId:(NSArray *)cellsId
                   headers:(NSArray*)headersId
                 tableView:(UITableView *)tableView;
@end
