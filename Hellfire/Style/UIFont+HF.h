//
//  UIFonts+HF.h
//  Hellfire
//
//  Created by Алексей Молокович on 01.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (UIFont_HF)
+(UIFont *)hfRegularSize:(CGFloat)size;
+(UIFont *)hfMediumSize:(CGFloat)size;
+(UIFont *)hfMediumItalicSize:(CGFloat)size;
+(UIFont *)hfBoldSize:(CGFloat)size;
@end
