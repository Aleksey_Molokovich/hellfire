//
//  UIColor+HF.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIColor+HF.h"

@implementation UIColor (UIColor_HF)

+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:((CGFloat)red/255.0f) green:((CGFloat)green/255.0f) blue:((CGFloat)blue/255.0f) alpha:alpha];
}

+ (UIColor *) colorWithHex:(NSString *)hex
{
    const char* hexStr = [hex UTF8String];
    if( hexStr == NULL || *hexStr != '#' )
    {
        return nil;
    }
    
    int r = 0, g = 0, b = 0;
    if( sscanf(hexStr, "#%02x%02x%02x", &r, &g, &b) != 3 )
    {
        return nil;
    }
    return [UIColor colorWith8BitRed:r green:g blue:b alpha:1];
}

+(UIColor*)hfGray{
    return [UIColor colorWithRed:144/255.0
                           green:164/255.0
                            blue:174/255.0
                           alpha:1];
}

+(UIColor*)hflightGray{
    return [UIColor colorWithRed:236/255.0
                           green:239/255.0
                            blue:241/255.0
                           alpha:1];
}

+(UIColor*)hfDeepPink{
    return [UIColor colorWithRed:240/255.0
                           green:98/255.0
                            blue:146/255.0
                           alpha:1];
}

+(UIColor *)hfRadicalRed{
    return [UIColor colorWithRed:255/255.0
                           green:24/255.0
                            blue:101/255.0
                           alpha:1];
}

+(UIColor *)hfLightChery{
    return [UIColor colorWithRed:200/255.0
                           green:35/255.0
                            blue:123/255.0
                           alpha:1];
}

+(UIColor*)hfRed{
    return [UIColor colorWithRed:255/255.0
                           green:25/255.0
                            blue:100/255.0
                           alpha:1];
}

+(UIColor *)hfDarkGrey{
    return [UIColor colorWithRed:55/255.0
                           green:72/255.0
                            blue:80/255.0
                           alpha:1];
}

+(UIColor *)hfDarkGreyStatus{
    return [UIColor colorWithRed:49/255.0
                           green:65/255.0
                            blue:73/255.0
                           alpha:1];
}

+(UIColor *)hfDarkGreen{
    return [UIColor colorWithRed:0
                           green:137/255.0
                            blue:123/255.0
                           alpha:1];
}

+(UIColor *)hfLightGreen{
    return [UIColor colorWithRed:29/255.0
                           green:233/255.0
                            blue:182/255.0
                           alpha:1];
}

+(UIColor *)hfYellow{
    return [UIColor colorWithRed:255/255.0
                           green:193/255.0
                            blue:19/255.0
                           alpha:1];
}
+(UIColor *)hfBottomMenu{
    return [UIColor colorWithRed:38/255.0
                           green:50/255.0
                            blue:56/255.0
                           alpha:.1];
}

+(UIColor *)hfOrange{
    return [UIColor colorWithRed:255/255.0
                           green:87/255.0
                            blue:34/255.0
                           alpha:1];
}

+(UIColor *)hfBlue{
    return [UIColor colorWithRed:33/255.0
                           green:150/255.0
                            blue:243/255.0
                           alpha:1];
}

+(UIColor *)hfVeryDarkGrey{
    return [UIColor colorWithRed:53/255.0
                           green:49/255.0
                            blue:55/255.0
                           alpha:1];
}


@end
