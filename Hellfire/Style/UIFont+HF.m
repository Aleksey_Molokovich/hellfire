//
//  UIFonts+HF.m
//  Hellfire
//
//  Created by Алексей Молокович on 01.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIFont+HF.h"
#import <CoreText/SFNTLayoutTypes.h>

@implementation UIFont (UIFont_HF)

+(UIFont *)hfRegularSize:(CGFloat)size{

    return [UIFont fontWithDescriptor:[self fontDescriptorWithFontName:@"Raleway-v4020-Regular"] size:size];
}

+(UIFont *)hfMediumSize:(CGFloat)size{
    
    return [UIFont fontWithDescriptor:[self fontDescriptorWithFontName:@"Raleway-v4020-Medium"] size:size];
}

+(UIFont *)hfMediumItalicSize:(CGFloat)size{
    
    return [UIFont fontWithDescriptor:[self fontDescriptorWithFontName:@"Raleway-v4020-MediumItalic"] size:size];
}

+(UIFont *)hfBoldSize:(CGFloat)size{
    
    return [UIFont fontWithDescriptor:[self fontDescriptorWithFontName:@"Raleway-v4020-Bold"] size:size];
}

+(UIFontDescriptor*)fontDescriptorWithFontName:(NSString*)name{
    NSDictionary *lowercaseNumbers = @{  UIFontFeatureTypeIdentifierKey:@(kNumberCaseType),
                                         UIFontFeatureSelectorIdentifierKey: @(kUpperCaseNumbersSelector)};
    
    return [[UIFontDescriptor alloc] initWithFontAttributes:
                                    @{UIFontDescriptorNameAttribute: name,
                                      UIFontDescriptorFeatureSettingsAttribute:@[ lowercaseNumbers ]}];
}

@end
