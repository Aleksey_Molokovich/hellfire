//
//  UIColor+HF.h
//  Hellfire
//
//  Created by Алексей Молокович on 20.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (UIColor_HF)
+ (UIColor *) colorWithHex:(NSString *)hex;

+(UIColor*)hfGray;
+(UIColor*)hflightGray;
+(UIColor*)hfDeepPink;
+(UIColor*)hfLightChery;
+(UIColor *)hfRadicalRed;
+(UIColor*)hfDarkGrey;
+(UIColor*)hfDarkGreen;
+(UIColor*)hfLightGreen;
+(UIColor*)hfBottomMenu;
+(UIColor*)hfYellow;
+(UIColor*)hfOrange;
+(UIColor*)hfBlue;
+(UIColor *)hfVeryDarkGrey;
+(UIColor*)hfDarkGreyStatus;

@end
