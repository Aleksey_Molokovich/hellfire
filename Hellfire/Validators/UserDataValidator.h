//
//  UserDataValidator.h
//  Hellfire
//
//  Created by Алексей Молокович on 29.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@class StatusModel;
@class AccountNew;

@interface UserDataValidator : NSObject
+(StatusModel*)isValidEmail:(NSString *)email;
+(StatusModel*)isValidPassword:(NSString *)password confirm:(NSString*)confirm;


+(StatusModel*)validateUserData:(AccountNew*)account;
+(StatusModel*)softValidateUserData:(AccountNew *)account;
@end
