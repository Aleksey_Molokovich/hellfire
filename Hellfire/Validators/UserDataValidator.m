//
//  UserDataValidator.m
//  Hellfire
//
//  Created by Алексей Молокович on 29.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserDataValidator.h"
#import "StatusModel.h"
#import "AccountNew.h"

@implementation UserDataValidator
+(StatusModel*)isValidEmail:(NSString *)email{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    StatusModel *response=[[StatusModel alloc] initByDefault];
    
    if(![emailTest evaluateWithObject:email]){
        response.status=NO;
        [response.errors addObject:@"проверьте почтовый адрес"];
    }
    
    return response;
}

+(StatusModel *)isValidPassword:(NSString *)password confirm:(NSString *)confirm{
    
    StatusModel *response=[[StatusModel alloc] initByDefault];
    if(password.length<6){
        response.status=NO;
        [response.errors addObject:@"минимальня длина пароля 6 символов"];
    }
    
    if(![password isEqualToString:confirm]){
        response.status=NO;
        [response.errors addObject:@"пароли не совпадают"];
    }
    
    
    
    return response;
}

+(StatusModel *)softValidateUserData:(AccountNew *)account{
    StatusModel *status=[StatusModel new];

    
    if(account.email.length>0){
        status=[UserDataValidator isValidEmail:account.email];
    }
    
    if(account.password.length>0){
        status=[UserDataValidator isValidPassword:account.password
                                          confirm:account.passwordConfirm];
    }
    
    return status;
}

+(StatusModel *)validateUserData:(AccountNew *)account{
    StatusModel *status=[StatusModel new];
    status.status=YES;
    
    if(![UserDataValidator isValidEmail:account.email].status){
        status=[UserDataValidator isValidEmail:account.email];
    }
    
    if(![UserDataValidator isValidPassword:account.password
                                   confirm:account.passwordConfirm].status){
        status=[UserDataValidator isValidPassword:account.password
                                          confirm:account.passwordConfirm];
    }
    
    if(account.nickname.length==0){
        status.status=NO;
        [status.errors addObject:@"не указан псевдоним"];
    }

    return status;
}
@end
