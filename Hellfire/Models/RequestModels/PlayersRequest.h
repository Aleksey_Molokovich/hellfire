//
//  PlayersRequest.h
//  Hellfire
//
//  Created by Алексей Молокович on 26.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

static NSString *const sortTypeSolo = @"solo";
static NSString *const sortTypeHotseat = @"hotseat";
static NSString *const sortTypeName = @"name";

@interface PlayersRequest : BaseModel
@property (nonatomic) NSString *query;
@property (nonatomic) NSNumber *page;
@property (nonatomic) NSNumber *take;
@property (nonatomic) NSNumber *cityId;
@property (nonatomic) NSNumber *countryId;
@property (nonatomic) NSString *includeMyFriends;
@property (nonatomic) NSString *sortType;
@property (nonatomic) NSString *isSortAsc;

@end
