//
//  AdPostRequest.h
//  Hellfire
//
//  Created by Алексей Молокович on 20.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface AdPostRequest : BaseModel
@property (nonatomic) NSInteger type;
@property (nonatomic) NSInteger page;
@property (nonatomic) NSInteger take;
@property (nonatomic) NSInteger clanId;
@end
