//
//  PlayersRequest.m
//  Hellfire
//
//  Created by Алексей Молокович on 26.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PlayersRequest.h"

@implementation PlayersRequest
- (instancetype)init
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    
    self.take = @(25);
    
    return self;
}
@end
