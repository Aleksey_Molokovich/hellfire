//
//  AccountNew.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "CatalogModel.h"
#import "CDAccount+CoreDataClass.h"

@interface AccountNew : JSONModel
@property (nonatomic) NSString *nickname;
@property (nonatomic) NSString *password;
@property (nonatomic) NSString *passwordConfirm;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *accessToken;
@property (nonatomic) NSString *token;
@property (nonatomic) NSString *social;
@property (nonatomic) NSString *pushToken;
@property (nonatomic) NSString *deviceId;
@property (nonatomic) NSNumber *cityId;
@property (nonatomic) CatalogModel *city;
@property (nonatomic) CatalogModel *country;

- (instancetype)initWithSocialToken:(NSString*)token;

+ (instancetype)fromCoreData:(CDAccount*)data;
- (CDAccount*)toCoreData;

@end
