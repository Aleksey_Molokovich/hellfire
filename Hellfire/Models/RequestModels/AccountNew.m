//
//  AccountNew.m
//  Hellfire
//
//  Created by Алексей Молокович on 23.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AccountNew.h"
#import "CDUser+CoreDataClass.h"

@implementation AccountNew

- (instancetype)initWithSocialToken:(NSString *)token{
    if(self=[super init]){
        self.accessToken=token;
    }
    return self;
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

+(instancetype)fromCoreData:(CDAccount *)data
{
    AccountNew *model = [AccountNew new];
    model.email = data.email;
    model.city = [CatalogModel new];
    model.city.id = data.city.id;
    model.cityId =  @(data.city.id);
    model.city.name = data.city.name;
    model.country = [CatalogModel new];
    model.country.id = data.country.id;
    model.country.name = data.country.name;
    model.accessToken = data.accessToken;
    model.password = data.password;
    model.passwordConfirm = data.confirmPassword;
    model.nickname = data.user.nickname;
    return model;
}


@end
