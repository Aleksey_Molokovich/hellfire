//
//  ClansRequest.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface ClansRequest : BaseModel
@property (nonatomic) NSString *query;
@property (nonatomic) NSNumber *page;
@property (nonatomic) NSNumber *take;
@property (nonatomic) NSNumber *clanId;
@property (nonatomic) NSNumber *cityId;
@property (nonatomic) NSNumber *countryId;
@property (nonatomic) NSString *includeMyClans;
@property (nonatomic) NSString *isSortAsc;
@property (nonatomic) NSString *sortType;
@end
