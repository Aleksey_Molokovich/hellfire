//
//  Profile.m
//  Hellfire
//
//  Created by Алексей Молокович on 23.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UserProfile.h"
#import "CDAccount+CoreDataClass.h"
#import "CDUser+CoreDataClass.h"
#import "CatalogModel.h"

@implementation UserProfile

+(instancetype)fromCDAccount:(CDAccount *)account
{
    UserProfile *profile = [UserProfile new];
    profile.userName = account.user.nickname;
    profile.cityId = @([CatalogModel fromCoreData:account.city].id);
    
    return profile;
}


@end
