//
//  Profile.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
@class CatalogModel;
@class CDAccount;
@interface UserProfile : BaseModel

@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *avatarId;
@property (nonatomic, strong) NSNumber *cityId;

+ (instancetype) fromCDAccount:(CDAccount*)account;

@end
