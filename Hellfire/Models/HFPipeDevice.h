//
//  HFPipeDevice.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
typedef NS_ENUM(NSInteger, HFPipeDeviceState){
    HFPipeDeviceStateConnected,
    HFPipeDeviceStateOnline
};


@interface HFPipeDevice : BaseModel
@property (nonatomic, strong) NSString *serialNumber;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) HFPipeDeviceState *state;
@end
