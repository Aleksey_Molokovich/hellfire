//
//  Advertising.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface Advertising : BaseModel
@property (nonatomic) NSString * title;
@property (nonatomic) NSArray * images;
@property (nonatomic) NSString *text;
@end
