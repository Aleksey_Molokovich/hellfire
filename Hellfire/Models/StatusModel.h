//
//  StatusModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface StatusModel : JSONModel
@property (nonatomic, assign) BOOL status;
@property (nonatomic, strong) NSMutableArray *errors;

-(instancetype)initByDefault;
@end
