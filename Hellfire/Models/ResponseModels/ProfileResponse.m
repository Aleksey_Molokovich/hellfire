//
//  ProfileResponse.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ProfileResponse.h"
#import "CDUser+CoreDataClass.h"
#import "CDCatalog+CoreDataClass.h"
@implementation ProfileResponse

-(CDAccount *)toCoreData
{
    
    CDAccount *account = [CDAccount MR_findFirst];
    
    if (!account)
    {
        account = [CDAccount MR_createEntity];
    }
    
    if (!account.user)
    {
        account.user = [CDUser MR_createEntity];
    }
    
    account.avatarPath = self.avatar.path;
    account.city = [self.city toCoreData];
    account.country = [self.country toCoreData];
    account.user.fullName = self.userName;
    account.user.nickname = self.userName;
    account.user.id = self.id;
    account.email = self.email;
    account.soloPoints = self.soloPoints;
    account.hotseatPoints = self.hotseatPoints;
    
    return account;
}

@end
