//
//  AvatarResonse.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface AvatarResonse : BaseModel
@property (nonatomic) NSInteger id;
@property (nonatomic) NSString *path;
@end
