//
//  AgreementResponse.h
//  Hellfire
//
//  Created by Алексей Молокович on 24.03.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface AgreementResponse : BaseModel
@property (nonatomic) NSString *message;
@property (nonatomic) NSString *rules;
@property (nonatomic) NSString *terms;
@end
