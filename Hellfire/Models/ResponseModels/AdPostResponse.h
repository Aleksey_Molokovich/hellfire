//
//  AdPostResponse.h
//  Hellfire
//
//  Created by Алексей Молокович on 20.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResponseModel.h"
@class HFPost;
@interface AdPostResponse : ResponseModel

@property (nonatomic) NSInteger id;
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger clanId;
@property (nonatomic) NSArray<NSString*> *imageUrls;
@property (nonatomic) NSString *article;
@property (nonatomic) NSString *shareUrl;
@property (nonatomic) NSInteger type;
@property (nonatomic) NSString *siteUrl;

-(HFPost *)toCoreDataModel:(HFPost*)model;
+(instancetype)fromCoreDataModel:(HFPost*)model;
@end
