//
//  CatalogModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "CDCatalog+CoreDataClass.h"

@interface CatalogModel : BaseModel
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *region;

@property (nonatomic) NSInteger id;

-(CDCatalog*)toCoreData;
+(instancetype)fromCoreData:(CDCatalog*)data;
@end
