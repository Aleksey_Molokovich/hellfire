//
//  AdPostResponse.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AdPostResponse.h"
#import "HFPost.h"
@implementation AdPostResponse

-(HFPost *)toCoreDataModel:(HFPost*)model{
    
    model.id = self.id;
    model.title = self.title;
    model.clanId = self.clanId;
    model.imageUrls = self.imageUrls;
    model.article  = self.article;
    model.shareUrl = self.shareUrl;
    model.type = self.type;
    model.siteUrl = self.siteUrl;
    
    return model;
}

+(instancetype)fromCoreDataModel:(HFPost *)data{
    AdPostResponse *model=[AdPostResponse new];
    model.id = data.id;
    model.title = data.title;
    model.clanId = data.clanId;
    model.imageUrls = data.imageUrls;
    model.article  = data.article;
    model.shareUrl = data.shareUrl;
    model.type = data.type;
    model.siteUrl = data.siteUrl;
    
    return model;
}

@end
