//
//  CatalogModel.m
//  Hellfire
//
//  Created by Алексей Молокович on 25.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CatalogModel.h"

@implementation CatalogModel

-(CDCatalog *)toCoreData
{
    CDCatalog *data=[CDCatalog MR_createEntity];
    data.id = self.id;
    data.name = self.name;
    return data;
}

+(instancetype)fromCoreData:(CDCatalog *)data
{
    CatalogModel *model = [CatalogModel new];
    model.id = data.id;
    model.name = data.name;
    
    return model;
}
@end
