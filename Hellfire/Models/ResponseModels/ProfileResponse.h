//
//  ProfileResponse.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "AvatarResonse.h"
#import "CatalogModel.h"
#import "CDAccount+CoreDataClass.h"

@interface ProfileResponse : BaseModel
@property (nonatomic) AvatarResonse *avatar;
@property (nonatomic) CatalogModel *city;
@property (nonatomic) CatalogModel *country;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSInteger hotseatPoints;
@property (nonatomic) NSInteger soloPoints;
@property (nonatomic) NSInteger id;
@property (nonatomic) BOOL isSendMetrics;

-(CDAccount*)toCoreData;
@end
