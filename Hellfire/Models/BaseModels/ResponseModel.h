//
//  BaseModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ResponseModel.h"
#import "BaseModel.h"

@interface Meta :BaseModel
@property (nonatomic) NSInteger pages;
@property (nonatomic) NSInteger count;

@end

@interface Error :BaseModel
@property (nonatomic) NSInteger code;
@property (nonatomic) NSString *message;
@end

@interface ResponseModel : BaseModel
@property (nonatomic) NSDictionary *data;
@property (nonatomic) NSInteger status;
@property (nonatomic) Meta *meta;
@property (nonatomic) Error *error;
@end
