//
//  BaseModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface BaseModel : JSONModel

@end
