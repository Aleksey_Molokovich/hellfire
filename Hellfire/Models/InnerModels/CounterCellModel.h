//
//  CounterCellModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "TimerModel.h"
#import <UIKit/UIKit.h>

@interface CounterCellModel : TimerModel
@property (nonatomic, strong) UIColor *colorSplitLine;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL shoudShowMinutes;
@end
