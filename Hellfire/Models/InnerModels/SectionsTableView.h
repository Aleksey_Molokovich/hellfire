//
//  SectionsTableView.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SectionsTableView : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSInteger countItems;
@end
