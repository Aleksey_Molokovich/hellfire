//
//  TimerModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 01.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimerModel : NSObject
@property (nonatomic, assign) NSInteger seconds;
@property (nonatomic) NSString *avatarCurrentUrl;
@property (nonatomic) NSString *avatarNextUrl;
@end
