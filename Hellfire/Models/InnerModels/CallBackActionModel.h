//
//  CallBackActionModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 10.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
#import "HellfireDefines.h"
@interface CallBackActionModel : NSObject

@property (strong, nonatomic) NSIndexPath *cellIndex;
@property (assign, nonatomic) ActionType actionType;
@property (strong, nonatomic) NSString *action;
@property (nonatomic) id data;



@end
