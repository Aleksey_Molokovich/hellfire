//
//  HFSoloSettings.m
//  Hellfire
//
//  Created by Алексей Молокович on 13.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HFGameSettings.h"
static NSString *const kGameSettings =@"game_settings";

@implementation HFGameSettings

+(instancetype)load
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kGameSettings]) {
        [[NSUserDefaults standardUserDefaults] setObject:[[HFGameSettings new] toDictionary] forKey:kGameSettings];
    }
    
    
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:kGameSettings];
    

    return [[HFGameSettings alloc] initWithDictionary:dic error:nil];
}

- (void)save
{
    [[NSUserDefaults standardUserDefaults] setObject:[self toDictionary] forKey:kGameSettings];
}


- (void)setCountRounds:(NSInteger)countRounds
{
    _countRounds = countRounds;
    [self save];
}

- (void)setCurrentRound:(NSInteger)currentRound
{
    _currentRound = currentRound;
    [self save];
}

- (void)setDurationBreak:(NSInteger)durationBreak
{
    _durationBreak = durationBreak;
    [self save];
}

- (void)setDurationPreparation:(NSInteger)durationPreparation
{
    _durationPreparation = durationPreparation;
    [self save];
}

- (void)setDurationRound:(NSInteger)durationRound
{
    _durationRound = durationRound;
    [self save];
}

- (void)setDurationUserPreparation:(NSInteger)durationUserPreparation
{
    _durationUserPreparation = _durationUserPreparation;
    [self save];
}

- (void)setGameId:(NSInteger)gameId
{
    _gameId = gameId;
    [self save];
}

@end
