//
//  HFSoloSettings.h
//  Hellfire
//
//  Created by Алексей Молокович on 13.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
@class HFGameSettings;

@interface HFGameSettings : BaseModel
@property (nonatomic, assign) NSInteger countRounds;
@property (nonatomic, assign) NSInteger currentRound;
@property (nonatomic, assign) NSInteger durationBreak;
@property (nonatomic, assign) NSInteger durationPreparation;
@property (nonatomic, assign) NSInteger durationRound;
@property (nonatomic, assign) NSInteger durationUserPreparation;
@property (nonatomic, assign) NSInteger gameId;

+(instancetype)load;
- (void)save;
@end
