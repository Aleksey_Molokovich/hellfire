//
//  HFAverageAndMaxResultModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface HFAverageAndMaxResultModel : NSObject
@property (assign, nonatomic) NSInteger averagePoints;
@property (assign, nonatomic) NSInteger maxPoints;
@property (assign, nonatomic) double roundPoints;
@property (assign, nonatomic) NSInteger averageVolume;
@property (assign, nonatomic) NSInteger maxVolume;
@property (assign, nonatomic) double volume;
@property (assign, nonatomic) NSInteger averageSpeed;
@property (assign, nonatomic) NSInteger maxSpeed;
@property (assign, nonatomic) double speed;
@property (assign, nonatomic) NSInteger averageQuality;
@property (assign, nonatomic) NSInteger maxQuality;
@property (assign, nonatomic) double quality;
@property (assign, nonatomic) NSInteger countTiks;
@property (assign, nonatomic) NSInteger countRounds;


@end
