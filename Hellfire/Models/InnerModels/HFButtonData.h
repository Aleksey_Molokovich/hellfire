//
//  HFButtonData.h
//  Hellfire
//
//  Created by Алексей Молокович on 13.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface HFButtonData : NSObject
@property (strong, nonatomic) UIColor *bgColor;
@property (assign, nonatomic) BOOL disable;
@property (strong, nonatomic) NSString *title;
@end
