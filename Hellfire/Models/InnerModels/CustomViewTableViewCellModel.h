//
//  CustomViewTableViewCellModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 10.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomViewTableViewCellModel : NSObject
@property (nonatomic, strong) NSString *viewClass;
@property (nonatomic, strong) id data;
@end
