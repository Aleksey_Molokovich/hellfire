//
//  HFNotification.h
//  Hellfire
//
//  Created by Алексей Молокович on 02.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "CDPlayer+CoreDataClass.h"

@interface HFNotificationAlert : BaseModel
@property (nonatomic) NSString *body;
@property (nonatomic) NSString *title;
@end

@interface HFNotificationData : BaseModel
@property (nonatomic) NSDictionary *Payload;
@property (nonatomic) NSNumber *Id;
@property (nonatomic) NSString *Message;
@property (nonatomic) NSString *Type;
@property (nonatomic) CDPlayer *player;
@end

@interface HFNotification : BaseModel

@property (nonatomic) HFNotificationData *data;
@property (nonatomic) HFNotificationAlert *alert;
@end
