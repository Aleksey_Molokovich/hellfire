//
//  ObjectTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomViewTableViewCellModel.h"

@interface ObjectTableViewCell : NSObject

@property (nonatomic, strong) NSString *cellId;
@property (nonatomic, strong) id data;
@property (nonatomic, strong) CustomViewTableViewCellModel *viewData;
@property (nonatomic, assign) NSInteger height;

@end
