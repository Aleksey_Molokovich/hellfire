//
//  ObjectTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ObjectTableViewCell.h"

@implementation ObjectTableViewCell

-(instancetype)init
{
    if (self=[super init]) {
        self.viewData=[CustomViewTableViewCellModel new];
    }
    return self;
}

@end
