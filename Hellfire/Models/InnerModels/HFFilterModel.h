//
//  HFFilterModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.04.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface HFFilterModel : BaseModel
@property (nonatomic) NSInteger countryId;
@property (nonatomic) NSString *countryName;
@property (nonatomic) NSInteger cityId;
@property (nonatomic) NSString *cityName;
@end
