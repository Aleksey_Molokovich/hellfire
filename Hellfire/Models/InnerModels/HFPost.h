//
//  HFPost.h
//  Hellfire
//
//  Created by Алексей Молокович on 18.04.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface HFPost : BaseModel
@property (nonatomic) NSString *article;
@property (nonatomic) NSInteger clanId;
@property (nonatomic) NSInteger id;
@property (nonatomic) NSArray *imageUrls;
@property (nonatomic) NSString *shareUrl;
@property (nonatomic) NSString *siteUrl;
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger type;

@end
