//
//  CBPeripheralModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 25.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

@import Foundation;
@import CoreBluetooth;

@interface CBPeripheralModel : NSObject
@property(strong, nonatomic) CBPeripheral *peripheral;
@property(assign, nonatomic) CGFloat RSSI;

@end
