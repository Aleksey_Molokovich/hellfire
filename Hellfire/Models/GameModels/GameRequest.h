//
//  GameRequest.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
@protocol GameRound;
@interface GameRound : BaseModel
@property (nonatomic,strong) NSNumber *duration;
@property (nonatomic,strong) NSNumber *maxQuality;
@property (nonatomic,strong) NSNumber *averageQuality;
@property (nonatomic,strong) NSNumber *averageSpeed;
@property (nonatomic,strong) NSNumber *maxSpeed;
@property (nonatomic,strong) NSNumber *points;
@property (nonatomic,strong) NSNumber *volume;
@property (nonatomic,strong) NSNumber *frequency;
@property (nonatomic,strong) NSArray *metrics;

@end

@interface GameRequest : BaseModel
@property (nonatomic) NSString *hfPipeSerialNumber;
@property (nonatomic) NSString *gameDate;
@property (nonatomic) NSArray<GameRound> *rounds;
@end

@protocol HotseatGamePLayer;
@interface HotseatGamePLayer : BaseModel
@property (nonatomic) NSNumber *userId;
@property (nonatomic) NSNumber *gameId;

@property (nonatomic) GameRound *round;
@end

@interface HotseatGameRoundRequest : BaseModel
@property (nonatomic) NSNumber *gameId;
@property (nonatomic) NSString *lastRound;
@property (nonatomic) NSNumber *currentRound;
@property (nonatomic) NSArray<HotseatGamePLayer> *players;
@end
