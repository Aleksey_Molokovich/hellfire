//
//  HFPlayerIndicators.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "UsersStatisticsCategory.h"

@protocol HFPlayerIndicators;
@interface HFPlayerIndicators : BaseModel
@property (nonatomic) UsersStatisticsCategory *points;
@property (nonatomic) UsersStatisticsCategory *quality;
@property (nonatomic) UsersStatisticsCategory *speed;
@property (nonatomic) UsersStatisticsCategory *volume;
@property (nonatomic) NSInteger totalScore;
@property (nonatomic) NSInteger countTiks;
@property (nonatomic) NSInteger countRounds;
@end
