//
//  HFSoloGameResult.m
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "HFSoloGameResult.h"

@implementation SoloSummaryPlace

@end
@implementation SoloSummaryUser

@end
@implementation HFSoloGameResult

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.user = [SoloSummaryUser new];
        self.place = [SoloSummaryPlace new];
    }
    return self;
}

@end
