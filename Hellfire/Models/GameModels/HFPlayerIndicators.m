//
//  HFPlayerIndicators.m
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HFPlayerIndicators.h"

@implementation HFPlayerIndicators

-(instancetype)init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    
    self.points = [UsersStatisticsCategory new];
    self.speed = [UsersStatisticsCategory new];
    self.quality = [UsersStatisticsCategory new];
    self.volume = [UsersStatisticsCategory new];
    
    return self;
}

@end
