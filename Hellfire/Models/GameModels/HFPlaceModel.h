//
//  HFPlaceModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "HFAddressModel.h"

@interface HFPlaceModel : BaseModel
@property (nonatomic) NSNumber *id;
@property (nonatomic) NSString *name;
@property (nonatomic) HFAddressModel *address;
@end
