//
//  HotseatGameResult.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "HFClanModel.h"
#import "HFPlaceModel.h"
#import "HFHotseatPlayer.h"

@interface HFHotseatGameResult : BaseModel
@property (nonatomic) HFClanModel *clan;
@property (nonatomic) HFPlaceModel *place;
@property (nonatomic) NSArray<HFHotseatPlayer> *players;
@end
