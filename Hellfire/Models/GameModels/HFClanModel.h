//
//  HFClanModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface HFClanModel : BaseModel
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *avatar;
@property (nonatomic) NSString *points;

@end
