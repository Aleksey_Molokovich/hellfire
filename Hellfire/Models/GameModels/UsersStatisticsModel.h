    //
//  UsersStatistics.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "UsersStatisticsCategory.h"
#import "HFPlayerIndicators.h"

@interface UsersStatisticsSolo : BaseModel
@property (nonatomic) UsersStatisticsCategory *parts;
@property (nonatomic) UsersStatisticsCategory *points;
@property (nonatomic) UsersStatisticsCategory *quality;
@property (nonatomic) UsersStatisticsCategory *speed;
@property (nonatomic) UsersStatisticsCategory *volume;
@end

@interface UsersStatisticsHotseat : BaseModel
@property (nonatomic) UsersStatisticsCategory *parts;
@property (nonatomic) UsersStatisticsCategory *partResult;
@end

@interface UsersStatisticsModel : BaseModel
@property (nonatomic) UsersStatisticsSolo *solo;
@property (nonatomic) UsersStatisticsHotseat *hotseat;
@end

@interface UserInfo : BaseModel
@property (nonatomic) NSNumber *id;
@property (nonatomic) NSString *avatarPath;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSNumber *soloPoints;
@property (nonatomic) NSNumber *hotseatPoints;
@property (nonatomic) NSNumber *status;
@end

@protocol UsersStatisticsGame;
@interface UsersStatisticsGame : BaseModel
@property (nonatomic) UserInfo *user;
@property (nonatomic) BOOL isWinner;
@property (nonatomic) NSNumber *points;
@property (nonatomic) NSNumber *score;
@property (nonatomic) HFPlayerIndicators *indicators;
@end

@interface UsersStatisticsSoloHistoryModel : BaseModel
@property (nonatomic) NSNumber *id;
@property (nonatomic) HFPlayerIndicators *indicators;
@property (nonatomic) NSNumber *score;
@property (nonatomic) NSNumber *date;
@property (nonatomic) NSNumber *roundCount;
@property (nonatomic) NSNumber *duration;
@end

@interface UsersStatisticsHotseatHistoryModel : BaseModel
@property (nonatomic) NSNumber *id;
@property (nonatomic) NSArray<UsersStatisticsGame> *diaryItems;
@property (nonatomic) NSNumber *date;
@property (nonatomic) NSNumber *roundCount;
@property (nonatomic) NSNumber *duration;
@end
