//
//  HFAddressModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface HFAddressModel : BaseModel
@property (nonatomic) NSString *formattedAddress;
@property (nonatomic) NSNumber *lon;
@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *address;
@property (nonatomic) NSNumber *cityId;
@end
