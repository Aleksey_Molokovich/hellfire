//
//  HFSoloGameResult.h
//  Hellfire
//
//  Created by Алексей Молокович on 06.12.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "HFClanModel.h"


@interface SoloSummaryPlace : BaseModel
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *phone;
@end

@interface SoloSummaryUser : BaseModel
@property (nonatomic) NSString *avatarPath;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *soloPoints;
@end

@interface HFSoloGameResult : BaseModel
@property (nonatomic) NSNumber *id;
@property (nonatomic) NSString *date;
@property (nonatomic) HFClanModel *clan;
@property (nonatomic) SoloSummaryPlace *place;
@property (nonatomic) SoloSummaryUser *user;
@end


