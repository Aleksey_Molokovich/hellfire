//
//  HFHotseatPlayer.m
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HFHotseatPlayer.h"

@implementation HFHotseatPlayer
-(instancetype)init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    
    self.indicators = [HFPlayerIndicators new];
    
    return self;
}

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"points": @"newPoints"
                                                                  }];
}
@end
