//
//  HFHotseatPlayer.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"
#import "HFPlayerIndicators.h"

@protocol HFHotseatPlayer;
@interface HFHotseatPlayer : BaseModel
@property (nonatomic) NSNumber *id;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *avatarPath;
@property (nonatomic) NSString *hotseatPoints;
@property (nonatomic) NSNumber *points;//new
@property (nonatomic) NSNumber *maxPoints;
@property (nonatomic) NSNumber *maxQuality;
@property (nonatomic) NSNumber *maxSpeed;
@property (nonatomic) NSNumber *maxVolume;
@property (nonatomic) BOOL isWinner;
@property (nonatomic) HFPlayerIndicators *indicators;
@end
