//
//  HFPushFinalResultHotseatModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface HFPushGameValue : BaseModel
@property (nonatomic) NSNumber *V1;
@property (nonatomic) NSNumber *V2;
@end

@interface HFPushPlayerIndicators : BaseModel
@property (nonatomic) HFPushGameValue *Points;
@property (nonatomic) HFPushGameValue *Quality;
@property (nonatomic) HFPushGameValue *Speed;
@property (nonatomic) HFPushGameValue *Volume;
@property (nonatomic) NSInteger TotalScore;
@end

@interface HFPushAddressModel : BaseModel
@property (nonatomic) NSString *FormattedAddress;
@property (nonatomic) NSNumber *Lon;
@property (nonatomic) NSNumber *Lat;
@property (nonatomic) NSNumber *Address;
@property (nonatomic) NSNumber *CityId;
@end

@interface HFPushPlaceModel : BaseModel
@property (nonatomic) NSNumber *Id;
@property (nonatomic) NSString *Name;
@property (nonatomic) HFPushAddressModel *Address;
@end

@interface HFPushClanModel : BaseModel
@property (nonatomic) NSString *Name;
@property (nonatomic) NSString *Avatar;
@property (nonatomic) NSString *Points;

@end

@protocol HFPushHotseatPlayer;
@interface HFPushHotseatPlayer : BaseModel
@property (nonatomic) NSNumber *Id;
@property (nonatomic) NSString *UserName;
@property (nonatomic) NSString *AvatarPath;
@property (nonatomic) NSString *HotseatPoints;
@property (nonatomic) NSNumber *Points;//new
@property (nonatomic) NSNumber *maxPoints;
@property (nonatomic) NSNumber *maxQuality;
@property (nonatomic) NSNumber *maxSpeed;
@property (nonatomic) NSNumber *maxVolume;
@property (nonatomic) BOOL IsWinner;
@property (nonatomic) HFPushPlayerIndicators *Indicators;
@end

@interface HFPushFinalResultHotseatModel : BaseModel
@property (nonatomic) HFPushClanModel *Clan;
@property (nonatomic) HFPushPlaceModel *Place;
@property (nonatomic) NSArray<HFPushHotseatPlayer> *Players;
@end
