//
//  HFPushFinalResultHotseatModel.m
//  Hellfire
//
//  Created by Алексей Молокович on 07.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HFPushFinalResultHotseatModel.h"

@implementation HFPushGameValue

@end
@implementation HFPushPlayerIndicators

@end
@implementation HFPushAddressModel

@end
@implementation HFPushPlaceModel

@end
@implementation HFPushClanModel
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"Points": @"NewPoints"
                                                                  }];
}
@end
@implementation HFPushHotseatPlayer
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"Points": @"NewPoints"
                                                                  }];
}
@end
@implementation HFPushFinalResultHotseatModel

@end

