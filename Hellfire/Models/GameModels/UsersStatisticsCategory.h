//
//  UsersStatisticsCategory.h
//  Hellfire
//
//  Created by Алексей Молокович on 27.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseModel.h"

@interface UsersStatisticsCategory : BaseModel
@property (nonatomic) NSInteger v1;
@property (nonatomic) NSInteger v2;
@end
