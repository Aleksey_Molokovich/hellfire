//
//  StatusModel.m
//  Hellfire
//
//  Created by Алексей Молокович on 30.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "StatusModel.h"

@implementation StatusModel
-(instancetype)initByDefault{
    if (self = [super init]) {
        self.status=YES;
        self.errors=[NSMutableArray new];
    }
    return self;
}
@end
