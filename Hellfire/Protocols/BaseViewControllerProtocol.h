//
//  BaseViewControllerProtocol.h
//  DiaryCulture
//
//  Created by Алексей Молокович on 24.07.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef BaseViewControllerProtocol_h
#define BaseViewControllerProtocol_h

@protocol BaseViewControllerProtocol <NSObject>
- (void)showProgressWithColor:(UIColor*)color;
- (void)hideProgress;
- (void)hideNaviagtionBar;
- (void)showNavigationBar;
- (void)hideBottomBar;
- (void)showBottomBar;
- (void)showUploadProgressWithColor:(UIColor*)color;
- (void)updateUploadProgress:(float)progress;

- (void)showAlertWithMessage:(NSString*)message;
- (void)showAlertWithMessage:(NSString*)message tag:(NSInteger)tag;

- (void)showAlert2ButtonsWithMessage:(NSString *)message tag:(NSInteger)tag;

- (void)showShareWithUrl:(NSString*)url text:(NSString*)text;
@end
#endif /* BaseViewControllerProtocol_h */
