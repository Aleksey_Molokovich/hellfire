//
//  BaseViewProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef BaseViewProtocol_h
#define BaseViewProtocol_h

@protocol BaseViewProtocol <NSObject>

- (NSInteger) expectedHeight;

@end
#endif /* BaseViewProtocol_h */
