//
//  ButtonViewProtocol.h.h
//  Hellfire
//
//  Created by Алексей Молокович on 14.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef ButtonViewProtocol_h
#define ButtonViewProtocol_h

@protocol ButtonViewProtocol<NSObject>
- (void)pressBtn;
@end
#endif /* ButtonViewProtocol.h_h */
