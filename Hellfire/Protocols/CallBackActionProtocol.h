//
//  CallBackActionProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef CallBackActionProtocol_h
#define CallBackActionProtocol_h

#import "CallBackActionModel.h"

@protocol CallBackActionProtocol <NSObject>

- (void)callBackObject:(CallBackActionModel*)object;

@end

#endif /* CallBackActionProtocol_h */
