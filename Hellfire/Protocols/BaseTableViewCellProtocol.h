//
//  BaseTableViewCellProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef BaseTableViewCellProtocol_h
#define BaseTableViewCellProtocol_h

@protocol BaseTableViewCellProtocol <NSObject>

- (void)configureWithData:(id)data;
- (void)setOutputModule:(id)output;

@end


#endif /* BaseTableViewCellProtocol_h */
