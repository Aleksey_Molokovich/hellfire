//
//  CallBackViewProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef CallBackViewProtocol_h
#define CallBackViewProtocol_h
#import "CallBackActionProtocol.h"
@protocol CallBackViewProtocol <NSObject>

- (void)setOutputModule:(id<CallBackActionProtocol>)output;

@end
#endif /* CallBackViewProtocol_h */
