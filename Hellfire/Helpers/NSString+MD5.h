//
//  NSString+MD5.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.03.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(NSString_MD5)
- (NSString*)MD5;
@end
