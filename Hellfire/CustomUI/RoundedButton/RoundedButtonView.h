//
//  RoundedButtonView.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButtonRounded.h"
#import "ButtonViewProtocol.h"

@protocol ButtonViewProtocol;

@class HFButtonData;
@interface RoundedButtonView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) id<ButtonViewProtocol> delegate;
- (void)configureWithData:(HFButtonData*)data;
@end
