//
//  RoundedButtonView.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RoundedButtonView.h"
#import "HFButtonData.h"

@interface RoundedButtonView()
@property (weak, nonatomic) IBOutlet UIButtonRounded *button;

@end

@implementation RoundedButtonView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        
    }
    return self;
}



- (void)configureWithData:(HFButtonData *)data{

    _button.layer.borderColor=data.bgColor.CGColor;
    
    NSAttributedString *string  = [[NSAttributedString new] initWithString:data.title
                                                                attributes:@{NSKernAttributeName:@(2),
                                                                             NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [_button setAttributedTitle:string forState:UIControlStateNormal];
    
    [_button setBackgroundColor:data.bgColor];
    _button.enabled=!data.disable;
    if (data.disable) {
        NSAttributedString *string  = [[NSAttributedString new] initWithString:data.title
                                                                    attributes:@{NSKernAttributeName:@(2),
                                                                                 NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _button.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        [_button setAttributedTitle:string forState:UIControlStateNormal];
    }
}

- (IBAction)click:(id)sender {
    if ([_delegate respondsToSelector:@selector(pressBtn)]) {
        [_delegate pressBtn];
    }
}


@end
