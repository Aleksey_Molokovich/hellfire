//
//  HotseatProgressView.h
//  Hellfire
//
//  Created by Алексей Молокович on 07.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotseatProgressView : UIView
@property (nonatomic, strong) CAShapeLayer *indefiniteAnimatedLayer;
@property (nonatomic, assign) CGFloat strokeThickness;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic) UIView *innerView;
@property (nonatomic) UIImageView *imageView;
- (void)stopWithSuccess;
- (void)stopWithFail;
- (void)run;
@end
