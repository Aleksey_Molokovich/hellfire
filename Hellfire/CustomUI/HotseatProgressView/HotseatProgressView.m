//
//  HotseatProgressView.m
//  Hellfire
//
//  Created by Алексей Молокович on 07.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "HotseatProgressView.h"
#import "UIColor+HF.h"

@implementation HotseatProgressView
-(instancetype)init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ( (self = [super initWithCoder:aDecoder]) == nil )
    {
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (void)commonInit
{
    self.radius = self.frame.size.height/2-3;
    self.strokeThickness = 4;
    self.strokeColor = [UIColor hfVeryDarkGrey];
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = self.frame.size.height/2;
    self.clipsToBounds = YES;
    self.innerView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, self.frame.size.width-2, self.frame.size.height-2)];
    self.innerView.backgroundColor = [UIColor hfOrange];
    self.innerView.layer.cornerRadius = self.innerView.frame.size.height/2;
    [self addSubview:self.innerView];
}

- (void)willMoveToSuperview:(UIView*)newSuperview {
    if (newSuperview) {
        [self layoutAnimatedLayer];
    } else {
        [_indefiniteAnimatedLayer removeFromSuperlayer];
        _indefiniteAnimatedLayer = nil;
    }
}

- (void)layoutAnimatedLayer {
    CALayer *layer = self.indefiniteAnimatedLayer;
    [self.layer addSublayer:layer];
    
    CGFloat widthDiff = CGRectGetWidth(self.bounds) - CGRectGetWidth(layer.bounds);
    CGFloat heightDiff = CGRectGetHeight(self.bounds) - CGRectGetHeight(layer.bounds);
    layer.position = CGPointMake(CGRectGetWidth(self.bounds) - CGRectGetWidth(layer.bounds) / 2 - widthDiff / 2, CGRectGetHeight(self.bounds) - CGRectGetHeight(layer.bounds) / 2 - heightDiff / 2);
}


- (CAShapeLayer*)indefiniteAnimatedLayer {
    if(!_indefiniteAnimatedLayer) {
        
        CGPoint arcCenter = CGPointMake(self.radius+self.strokeThickness/2+5, self.radius+self.strokeThickness/2+5);
        UIBezierPath* smoothedPath = [UIBezierPath bezierPathWithArcCenter:arcCenter radius:self.radius startAngle:0 endAngle:(CGFloat) (M_PI*4) clockwise:YES];
        
        _indefiniteAnimatedLayer = [CAShapeLayer layer];
        _indefiniteAnimatedLayer.contentsScale = [[UIScreen mainScreen] scale];
        _indefiniteAnimatedLayer.frame = CGRectMake(0.0f, 0.0f, arcCenter.x*2, arcCenter.y*2);
        _indefiniteAnimatedLayer.fillColor = [UIColor clearColor].CGColor;
        _indefiniteAnimatedLayer.strokeColor = self.strokeColor.CGColor;
        _indefiniteAnimatedLayer.lineWidth = self.strokeThickness;
        _indefiniteAnimatedLayer.lineCap = kCALineCapSquare;
        _indefiniteAnimatedLayer.lineJoin = kCALineJoinBevel;
        _indefiniteAnimatedLayer.path = smoothedPath.CGPath;
        
        NSTimeInterval animationDuration = 1;
        CAMediaTimingFunction *linearCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        animation.fromValue = (id) 0;
        animation.toValue = @(M_PI*2);
        animation.duration = animationDuration;
        animation.timingFunction = linearCurve;
        animation.removedOnCompletion = NO;
        animation.repeatCount = INFINITY;
        animation.fillMode = kCAFillModeForwards;
        animation.autoreverses = NO;
        [_indefiniteAnimatedLayer.mask addAnimation:animation forKey:@"rotate"];
        
        CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
        animationGroup.duration = animationDuration;
        animationGroup.repeatCount = INFINITY;
        animationGroup.removedOnCompletion = NO;
        animationGroup.timingFunction = linearCurve;
        
        CABasicAnimation *strokeStartAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
        strokeStartAnimation.fromValue = @0;
        strokeStartAnimation.toValue = @0.5;
        
        CABasicAnimation *strokeEndAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        strokeEndAnimation.fromValue = @0.1;
        strokeEndAnimation.toValue = @0.6;
        
        animationGroup.animations = @[strokeStartAnimation, strokeEndAnimation];
        [_indefiniteAnimatedLayer addAnimation:animationGroup forKey:@"progress"];
        
    }
    return _indefiniteAnimatedLayer;
}

- (void)setFrame:(CGRect)frame {
    if(!CGRectEqualToRect(frame, super.frame)) {
        [super setFrame:frame];
        
        if(self.superview) {
            [self layoutAnimatedLayer];
        }
    }
    
}

- (void)setRadius:(CGFloat)radius {
    if(radius != _radius) {
        _radius = radius;
        
        [_indefiniteAnimatedLayer removeFromSuperlayer];
        _indefiniteAnimatedLayer = nil;
        
        if(self.superview) {
            [self layoutAnimatedLayer];
        }
    }
}

- (void)setStrokeColor:(UIColor*)strokeColor {
    _strokeColor = strokeColor;
    _indefiniteAnimatedLayer.strokeColor = strokeColor.CGColor;
}

- (void)setStrokeThickness:(CGFloat)strokeThickness {
    _strokeThickness = strokeThickness;
    _indefiniteAnimatedLayer.lineWidth = _strokeThickness;
}

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake((self.radius+self.strokeThickness/2+5)*2, (self.radius+self.strokeThickness/2+5)*2);
}

- (void)run
{
    if (_imageView) {
        _imageView = nil;
        _indefiniteAnimatedLayer = nil;
        [self.innerView removeFromSuperview];
        [self commonInit];
        [self layoutAnimatedLayer];
    }
}

- (void)stopWithSuccess
{
    _imageView= [UIImageView new];
    _imageView.image = [UIImage imageNamed:@"check"];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.innerView addSubview:_imageView];
    _imageView.frame = self.innerView.frame;
    _imageView.center = self.innerView.center;
    _imageView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    __weak typeof(self) weakSelf = self;

    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         __strong typeof(weakSelf) blockSelf = weakSelf;
                        blockSelf.innerView.backgroundColor = [UIColor hfVeryDarkGrey];
                        blockSelf.backgroundColor = [UIColor hfVeryDarkGrey];
                         [blockSelf.indefiniteAnimatedLayer removeFromSuperlayer];
                         blockSelf.imageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
                     } completion:nil];
}

- (void)stopWithFail
{
    _imageView= [UIImageView new];
    _imageView.image = [UIImage imageNamed:@"close"];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.innerView addSubview:_imageView];
    _imageView.frame = self.innerView.frame;
    _imageView.center = self.innerView.center;
    _imageView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    __weak typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         __strong typeof(weakSelf) blockSelf = weakSelf;
                         blockSelf.innerView.backgroundColor = [UIColor hfVeryDarkGrey];
                         blockSelf.backgroundColor = [UIColor hfVeryDarkGrey];
                         [blockSelf.indefiniteAnimatedLayer removeFromSuperlayer];
                         blockSelf.imageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
                     } completion:nil];
}
@end
