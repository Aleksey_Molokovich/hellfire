//
//  BluetoothStatusView.m
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BluetoothStatusView.h"
#import <LGHelper/UIImage+LGHelper.h>
#import "UIColor+HF.h"

@interface BluetoothStatusView()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end

@implementation BluetoothStatusView



- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {

        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = YES;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];


    }
    return self;
}

- (void)connectcedToDevice:(NSString*)device
{
    if (device)
    {
        _label.text = [NSString stringWithFormat:@"%@: подключено",device];
        self.bgImageView.image = [UIImage imageNamed:@"bg_blue_nb"];
        self.image.image = [UIImage imageNamed:@"radiobox-check-white"];
    }
    else
    {
        _label.text = @"Нажмите, чтобы подключить HFPipe";
        self.image.image = [UIImage imageNamed:@"bluetooth"];
        self.bgImageView.image = [UIImage image1x1WithColor:[UIColor hfRadicalRed]];
    }
}

- (void)drawRect:(CGRect)rect
{
    self.view.frame = rect;
//    [self.view updateConstraints];
    

}

@end
