//
//  BluetoothStatusView.h
//  Hellfire
//
//  Created by Алексей Молокович on 23.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BluetoothStatusView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;

- (void)connectcedToDevice:(NSString*)device;
@end
