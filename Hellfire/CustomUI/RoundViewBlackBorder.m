//
//  RoundViewBlackBorder.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.04.2018.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "RoundViewBlackBorder.h"

@implementation RoundViewBlackBorder

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.layer.cornerRadius = self.bounds.size.height / 2.0;
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.clipsToBounds = YES;
    }
    return self;
}

@end
