//
//  MiniTimerWithButtonView.m
//  Hellfire
//
//  Created by Алексей Молокович on 16.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "MiniTimerWithButtonView.h"

@implementation MiniTimerWithButtonView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self.button roundCorner];
    }
    return self;
}


@end
