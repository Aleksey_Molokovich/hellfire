//
//  MiniTimerWithButtonView.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CounterView.h"
#import "ButtonViewProtocol.h"
#import "ButtonYellowStart.h"

@interface MiniTimerWithButtonView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet ButtonYellowStart *button;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end
