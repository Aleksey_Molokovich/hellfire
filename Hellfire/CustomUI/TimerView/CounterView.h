//
//  CounterView.h
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CounterView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *imgDots;
@property (weak, nonatomic) IBOutlet UIImageView *imgMinute;
@property (weak, nonatomic) IBOutlet UILabel *labelMinute;
@property (weak, nonatomic) IBOutlet UILabel *labelSecFirsteDigit;
@property (weak, nonatomic) IBOutlet UILabel *labelSecSecondDigit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTralingView;

@property (assign, nonatomic) BOOL shoudShowMinutes;
- (void)setTime:(NSInteger)seconds;

@end
