//
//  CounterView.m
//  Hellfire
//
//  Created by Алексей Молокович on 09.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "CounterView.h"
#import "UIFont+HF.h"

@interface CounterView()


@end

@implementation CounterView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        self.view.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setTime:(NSInteger)data{
    _labelMinute.font=[UIFont hfRegularSize:36];
    _labelSecSecondDigit.font=[UIFont hfRegularSize:36];
    _labelSecFirsteDigit.font=[UIFont hfRegularSize:36];
    
    NSString *minutes=[@(data/60) stringValue];
    NSString *seconds=[@(data%60) stringValue];
    
    if(_shoudShowMinutes){
        _labelMinute.hidden=NO;
        _imgMinute.hidden=NO;
        _imgDots.hidden=NO;
        _labelMinute.text=[minutes substringWithRange:NSMakeRange(0, 1)];
        _constraintTralingView.constant=0;
        
    }else{
        _imgDots.hidden=YES;
        _labelMinute.hidden=YES;
        _imgMinute.hidden=YES;
        _constraintTralingView.constant=24;
    }
    
        
    if (seconds.length>1) {
        _labelSecSecondDigit.text=[seconds substringWithRange:NSMakeRange(0, 1)];
        _labelSecFirsteDigit.text=[seconds substringWithRange:NSMakeRange(1, 1)];
    }else{
        _labelSecSecondDigit.text=@"0";
        _labelSecFirsteDigit.text=[seconds substringWithRange:NSMakeRange(0, 1)];
        
    }
}

@end
