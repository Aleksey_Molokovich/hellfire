//
//  UIButtonEye.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButtonEye : UIButton

@property (assign, nonatomic) BOOL white;
@property (assign, nonatomic) BOOL hide;


@end
