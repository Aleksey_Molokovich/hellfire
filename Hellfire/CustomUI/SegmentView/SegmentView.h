//
//  SegmentView.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "SegmentViewProtocol.h"

@interface SegmentView : BaseView
@property (weak, nonatomic) IBOutlet UIButton *buttonSolo;
@property (weak, nonatomic) IBOutlet UIButton *buttonHotseat;
@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tralingConstraintSlider;
@property (nonatomic, weak) id<SegmentViewProtocol> delegate;

- (void)selectSolo:(BOOL)isSolo;
@end
