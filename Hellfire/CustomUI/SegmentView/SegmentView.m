//
//  SegmentView.m
//  Hellfire
//
//  Created by Алексей Молокович on 16.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "SegmentView.h"

@implementation SegmentView



- (void)selectSolo:(BOOL)isSolo
{
    if (isSolo) {
        _tralingConstraintSlider.constant = self.frame.size.width/2;
    }
    else
    {
        _tralingConstraintSlider.constant = 0;
    }
}

- (IBAction)clickHotseat:(id)sender {
    if (_tralingConstraintSlider.constant == 0) {
        return;
    }
    
    if ([_delegate respondsToSelector:@selector(pressHotseat)]) {
        [_delegate pressHotseat];
    }
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _tralingConstraintSlider.constant = 0;
                     } completion:nil];
}

- (IBAction)clickSolo:(id)sender {
    if (_tralingConstraintSlider.constant == self.frame.size.width/2) {
        return;
    }
    
    if ([_delegate respondsToSelector:@selector(pressSolo)]) {
        [_delegate pressSolo];
    }
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _tralingConstraintSlider.constant = self.frame.size.width/2;
                     } completion:nil];
}

@end
