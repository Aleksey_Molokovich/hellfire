//
//  SegmentViewProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 17.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#ifndef SegmentViewProtocol_h
#define SegmentViewProtocol_h

@protocol SegmentViewProtocol<NSObject>

- (void)pressSolo;
- (void)pressHotseat;
@end
#endif /* SegmentViewProtocol_h */
