//
//  UIImageViewRounded.m
//  Hellfire
//
//  Created by Алексей Молокович on 26.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIImageViewRounded.h"

@implementation UIImageViewRounded

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.layer.cornerRadius = self.frame.size.height/2;
        self.clipsToBounds = YES;
    }
    return self;
}

@end
