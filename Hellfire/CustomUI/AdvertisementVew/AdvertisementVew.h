//
//  AdvertisementVew.h
//  Hellfire
//
//  Created by Алексей Молокович on 15.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Advertising;

@interface AdvertisementVew : UIView


- (void)configureWithData:(Advertising*)data;
@end
