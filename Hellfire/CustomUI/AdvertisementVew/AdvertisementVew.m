//
//  AdvertisementVew.m
//  Hellfire
//
//  Created by Алексей Молокович on 15.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AdvertisementVew.h"
#import "Advertising.h"
#import "AdvertisementCollectionViewCell.h"

@interface AdvertisementVew()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    NSArray *images;
}
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblLink;

@end

@implementation AdvertisementVew

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        
    }
    
    [_collectionView registerNib:[UINib nibWithNibName:@"AdvertisementCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AdvertisementCollectionViewCell"];
    return self;
}

- (void)configureWithData:(Advertising *)data{
    images=data.images;
    [_collectionView reloadData];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return images.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return _collectionView.frame.size;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AdvertisementCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"AdvertisementCollectionViewCell" forIndexPath:indexPath];
    
    cell.imageView.image=images[indexPath.item];
    
    return cell;
}
@end
