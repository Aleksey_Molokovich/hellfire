//
//  AdvertisementCollectionViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 15.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvertisementCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
