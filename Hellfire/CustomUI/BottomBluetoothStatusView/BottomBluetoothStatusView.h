//
//  BottomBluetoothStatusView.h
//  Hellfire
//
//  Created by Алексей Молокович on 30.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomBluetoothStatusView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *image;

- (void)connectcedToDevice:(NSString*)device;

@end
