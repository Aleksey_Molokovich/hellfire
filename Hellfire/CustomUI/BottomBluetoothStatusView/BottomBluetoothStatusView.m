//
//  BottomBluetoothStatusView.m
//  Hellfire
//
//  Created by Алексей Молокович on 30.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BottomBluetoothStatusView.h"

@implementation BottomBluetoothStatusView

-(instancetype)init{
    self = [super init];
    if (self) {
        
        [self setup];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self setup];
        
    }
    return self;
}


- (void)setup{
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.view];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    self.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.1];
    
    
}


- (void)connectcedToDevice:(NSString *)device
{
    if (device)
    {
        _label.text = @"устройство подключено";
        _image.hidden = NO;
    }
    else
    {
        _label.text = @"устройство не подключено";
        _image.hidden = YES;
    }
}
@end
