//
//  ButtonYellowStart.h
//  Hellfire
//
//  Created by Алексей Молокович on 02.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonViewProtocol.h"

@interface ButtonYellowStart : UIButton
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) id<ButtonViewProtocol> delegate;
- (void)fullRoundSide;
- (void)roundCorner;
@end
