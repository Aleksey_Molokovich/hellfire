//
//  ButtonYellowStart.m
//  Hellfire
//
//  Created by Алексей Молокович on 02.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ButtonYellowStart.h"

@implementation ButtonYellowStart

-(instancetype)init
{
    if ((self = [super init]) ==nil ) return nil;
    [self setup];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder]) ==nil ) return nil;
    [self setup];
    return self;
}



- (void)setup
{
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.view];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    
    NSAttributedString *string  = [[NSAttributedString new] initWithString:@"НАЧАТЬ!"
                                                                attributes:@{NSKernAttributeName:@(2),
                                                                             NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    [_button setAttributedTitle:string forState:UIControlStateNormal];
}



- (void)roundCorner{
    self.button.layer.borderWidth=0;
    self.button.layer.cornerRadius = 8;
    self.button.clipsToBounds = YES;
}

- (void)fullRoundSide{
    self.button.layer.borderWidth=0;
    self.button.layer.cornerRadius = self.bounds.size.height / 2.0;
    self.button.clipsToBounds = YES;
}

- (IBAction)click:(id)sender {
    if ([self.delegate respondsToSelector:@selector(pressBtn)]) {
        [self.delegate pressBtn];
    }
}
@end
