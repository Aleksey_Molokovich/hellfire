//
//  RoundedButton.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundedButton : UIButton
- (void)configureTitle:(NSString *)title bgColor:(UIColor*)color;
@end
