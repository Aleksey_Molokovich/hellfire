//
//  RoundedButton.m
//  Hellfire
//
//  Created by Алексей Молокович on 19.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "RoundedButton.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"

@implementation RoundedButton

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]) == nil)
        return nil;
    
    self.layer.cornerRadius = self.bounds.size.height / 2.0;
    self.clipsToBounds = YES;
    
    return self;
}

- (void)configureTitle:(NSString *)title bgColor:(UIColor*)color
{
    self.backgroundColor = color;
    
    NSAttributedString *str= [[NSAttributedString new]
                              initWithString:title
                              attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                           NSKernAttributeName:@(2),
                                           NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self setAttributedTitle:str forState:UIControlStateNormal];
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    
    if (enabled) {
        self.alpha = 1;
    }else{
        self.alpha = 0.3;
    }
}

@end
