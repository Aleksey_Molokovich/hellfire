//
//  ButtonShareView.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ButtonShareView.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"

@interface ButtonShareView ()
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;

@end
@implementation ButtonShareView
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {

        self.backgroundColor=[UIColor clearColor];
        self.view.backgroundColor=[UIColor clearColor];
        
        _buttonShare.layer.cornerRadius = _buttonShare.bounds.size.height / 2.0;
        _buttonShare.clipsToBounds = YES;
        _buttonShare.backgroundColor = [UIColor hfLightGreen];
        NSAttributedString *title= [[NSAttributedString new] initWithString:@"ПОДЕЛИТЬСЯ"
                                                                 attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                                                              NSKernAttributeName:@(2),
                                                                              NSForegroundColorAttributeName:[UIColor whiteColor]}];
        
        [_buttonShare setAttributedTitle:title forState:UIControlStateNormal];
        
        
    }
    return self;
}


@end
