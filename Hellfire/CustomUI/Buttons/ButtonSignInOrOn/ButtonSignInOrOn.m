//
//  ButtonSignInOrOn.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ButtonSignInOrOn.h"
#import "UIColor+HF.h"
#import "UIFont+HF.h"
#import "CallBackActionModel.h"

@implementation ButtonSignInOrOn

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self.backgroundColor=[UIColor clearColor];
        self.view.backgroundColor=[UIColor clearColor];
        
        _button.layer.cornerRadius = _button.bounds.size.height / 2.0;
        _button.clipsToBounds = YES;
        _button.backgroundColor = [UIColor hfDeepPink];
        NSAttributedString *title= [[NSAttributedString new] initWithString:@"ВХОД / РЕГИСТРАЦИЯ"
                                                                 attributes:@{NSFontAttributeName:[UIFont hfBoldSize:12],
                                                                              NSKernAttributeName:@(2),
                                                                              NSForegroundColorAttributeName:[UIColor whiteColor]}];
        
        [_button setAttributedTitle:title forState:UIControlStateNormal];
        
        
    }
    return self;
}
- (IBAction)pressButton:(id)sender {
    
    if ([_output respondsToSelector:@selector(callBackObject:)]) {
        CallBackActionModel *model=[CallBackActionModel new];
        model.actionType = ActionButtonPress;
        [_output callBackObject:model];
    }
}

@end
