//
//  ButtonSignInOrOn.h
//  Hellfire
//
//  Created by Алексей Молокович on 05.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "CallBackActionProtocol.h"
@interface ButtonSignInOrOn : BaseView
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) id<CallBackActionProtocol> output;
@end
