//
//  UIButtonPink.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIButtonPink.h"
#import "UIColor+HF.h"

@interface UIButtonPink ()
@property (nonatomic) BOOL isEnabled;

@end


@implementation UIButtonPink

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
            self.layer.cornerRadius = self.bounds.size.height / 2.0;
            self.clipsToBounds = YES;
        [self setNormalState];
    }
    return self;
}

- (void)setNormalState {
    self.alpha=1;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.tintColor = [UIColor whiteColor];
    self.tintColor= [UIColor whiteColor];
    [self setBackgroundColor:[UIColor hfDeepPink]];
}

- (void)setDisabledState {
    self.alpha=0.65;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.tintColor = [UIColor whiteColor];
    self.tintColor= [UIColor whiteColor];
    [self setBackgroundColor:[UIColor hfDeepPink]];
    
}

- (void)setHighlightedColors {
    dispatch_async(dispatch_get_main_queue(), ^{
       self.backgroundColor = [UIColor hfLightChery];
    });
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state {
    [super setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:state];
}

- (void)setHighlighted:(BOOL)highlighted {
    if (highlighted && _isEnabled) {
        [self setHighlightedColors];
    }
    else if (_isEnabled) {
        [self setNormalState];
    }
    else {
        [self setDisabledState];
    }
}

- (void)setEnabled:(BOOL)enabled {
    _isEnabled = enabled;
    if (_isEnabled) {
        [self setNormalState];
    }
    else {
        [self setDisabledState];
    }
}

@end
