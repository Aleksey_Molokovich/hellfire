//
//  SearchView.h
//  Hellfire
//
//  Created by Алексей Молокович on 11.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "SearchViewProtocol.h"

@interface SearchView : BaseView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *roundView;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) id<SearchViewProtocol> delegate;

- (instancetype)initWithLightThem;

@end
