//
//  SearchViewProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 11.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef SearchViewProtocol_h
#define SearchViewProtocol_h
@protocol SearchViewProtocol <NSObject>

- (void) didChangeSearchString:(NSString*)searchString;

@end

#endif /* SearchViewProtocol_h */
