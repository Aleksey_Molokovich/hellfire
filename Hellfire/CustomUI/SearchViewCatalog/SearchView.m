//
//  SearchView.m
//  Hellfire
//
//  Created by Алексей Молокович on 11.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView

- (instancetype)initWithLightThem
{
    if ((self = [super init]) == nil)
        return nil;
    
    [self setup];
    [self lightThem];
    return self;
}

- (void)lightThem
{
    self.roundView.layer.borderColor = [UIColor blackColor].CGColor;
}

- (void)setup
{
    [super setup];
    
    _roundView.layer.cornerRadius = _roundView.bounds.size.height / 2.0;
    _roundView.clipsToBounds = YES;
}


- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([_delegate respondsToSelector:@selector(didChangeSearchString:)]) {
        NSString *substring = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [_delegate didChangeSearchString:substring];
    }
    return YES;
}


@end
