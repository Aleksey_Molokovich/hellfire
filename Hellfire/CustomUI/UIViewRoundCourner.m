//
//  UIViewRoundCourner.m
//  Hellfire
//
//  Created by Алексей Молокович on 28.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIViewRoundCourner.h"

@implementation UIViewRoundCourner

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.layer.cornerRadius = 5;
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.clipsToBounds = YES;
    }
    return self;
}
@end
