//
//  SearchViewPlayersAndClans.m
//  Hellfire
//
//  Created by Алексей Молокович on 28.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "SearchViewPlayersAndClans.h"
#import <LGHelper/UIColor+LGHelper.h>

@implementation SearchViewPlayersAndClans

- (void)setup
{
    [super setup];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clean)];
    _cleanView.hidden = YES;
    [_cleanView addGestureRecognizer:tap];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([_delegate respondsToSelector:@selector(didChangeSearchString:)]) {
        [_delegate didChangeSearchString:substring];
    }
    
    if (substring.length)
    {
        _cleanView.hidden = NO;
    }
    else
    {
        _cleanView.hidden = YES;
    }
    
    return YES;
}

- (void)placeholder:(NSString *)placeholder
{
    UIColor *color = [UIColor colorWithRed:144/255.0 green:202/255.0 blue:249/255.0 alpha:1];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:placeholder attributes:@{ NSForegroundColorAttributeName : color }];

    _tfSearch.attributedPlaceholder = str;
}

- (void)clean{
    _tfSearch.text = @"";
    [_delegate didChangeSearchString:@""];
    _cleanView.hidden = YES;
}

@end
