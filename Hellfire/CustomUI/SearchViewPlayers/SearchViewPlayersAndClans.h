//
//  SearchViewPlayersAndClans.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "SearchViewProtocol.h"

@interface SearchViewPlayersAndClans : BaseView<UITextFieldDelegate>
@property (weak, nonatomic) id<SearchViewProtocol> delegate;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UIView *cleanView;
- (void)placeholder:(NSString*)placeholder;
@end
