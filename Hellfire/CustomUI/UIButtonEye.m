//
//  UIButtonEye.m
//  Hellfire
//
//  Created by Алексей Молокович on 05.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIButtonEye.h"

@implementation UIButtonEye

//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super initWithCoder:aDecoder];
//    if (self)
//    {
//        [self setImage:[UIImage imageNamed:@"eye-off-outline-black"] forState:UIControlStateNormal];
//    }
//    return self;
//}
- (void)setWhite:(BOOL)white{
    _white=white;
    [self change];
}



- (void)setHide:(BOOL)hide{
    _hide=hide;
    [self change];
}


- (void)change{
    if(_white){
        if(_hide){
            [self setImage:[UIImage imageNamed:@"eye-off-outline-white"] forState:UIControlStateNormal];
        }else{
            [self setImage:[UIImage imageNamed:@"eye-outline-white"] forState:UIControlStateNormal];
        }
    }else{
        if(_hide){
            [self setImage:[UIImage imageNamed:@"eye-off-outline-black"] forState:UIControlStateNormal];
        }else{
            [self setImage:[UIImage imageNamed:@"eye-outline-black"] forState:UIControlStateNormal];
        }
 
    }
}
@end
