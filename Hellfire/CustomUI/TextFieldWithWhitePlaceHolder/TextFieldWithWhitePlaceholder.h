//
//  TextFieldWithWhitePlaceholder.h
//  Hellfire
//
//  Created by Алексей Молокович on 16.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldWithWhitePlaceholder : UITextField
- (void)configurePlaceholder:(NSString *)placeholder;
@end
