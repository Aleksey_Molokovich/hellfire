//
//  TextFieldWithWhitePlaceholder.m
//  Hellfire
//
//  Created by Алексей Молокович on 16.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "TextFieldWithWhitePlaceholder.h"

@implementation TextFieldWithWhitePlaceholder





- (void)configurePlaceholder:(NSString *)placeholder
{
    if ([self respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
    }
}

@end
