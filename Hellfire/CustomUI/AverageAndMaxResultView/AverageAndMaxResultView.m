//
//  AverageAndMaxResultView.m
//  Hellfire
//
//  Created by Алексей Молокович on 28.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AverageAndMaxResultView.h"
#import "UIColor+HF.h"

@interface AverageAndMaxResultView()
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *labelNumbers;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end

@implementation AverageAndMaxResultView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        self.backgroundColor=[UIColor clearColor];
        self.view.backgroundColor=[UIColor clearColor];
        self.isDarkThem = NO;

    }
    return self;
}

- (void)configureWithType:(AverageAndMaxResultType)type
           averageAmount:(NSInteger)averageAmount
               maxAmount:(NSInteger)maxAmount
{
    
}

@end
