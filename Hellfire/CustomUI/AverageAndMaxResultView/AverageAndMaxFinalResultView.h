//
//  AverageAndMaxFinalResultView.h
//  Hellfire
//
//  Created by Алексей Молокович on 03.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AverageAndMaxResultDefines.h"

@interface AverageAndMaxFinalResultView : UIView

- (void)configureWithType:(AverageAndMaxResultType)type
            averageAmount:(NSInteger)averageAmount
                maxAmount:(NSInteger)maxAmount;
@end
