//
//  AverageAndMaxResultDefines.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef AverageAndMaxResultDefines_h
#define AverageAndMaxResultDefines_h
typedef NS_ENUM(NSInteger, AverageAndMaxResultType){
    AverageAndMaxResultTypeQualitySmoke,
    AverageAndMaxResultTypeSpeedSmoke,
    AverageAndMaxResultTypePoints,
    AverageAndMaxResultTypeNumberOfWins,
    AverageAndMaxResultTypeVolume,
    AverageAndMaxResultTypeSoloScore,
    AverageAndMaxResultTypeHotseatScore,
    AverageAndMaxResultTypeRoundPoints
};


#endif /* AverageAndMaxResultDefines_h */
