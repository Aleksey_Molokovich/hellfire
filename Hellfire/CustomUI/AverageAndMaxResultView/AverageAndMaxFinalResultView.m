//
//  AverageAndMaxFinalResultView.m
//  Hellfire
//
//  Created by Алексей Молокович on 03.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "AverageAndMaxFinalResultView.h"
#import "UIFont+HF.h"

@interface AverageAndMaxFinalResultView()
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIView *view;


@end


@implementation AverageAndMaxFinalResultView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        self.backgroundColor=[UIColor clearColor];
        self.view.backgroundColor=[UIColor clearColor];
        
    }
    return self;
}

- (void)configureWithType:(AverageAndMaxResultType)type
           averageAmount:(NSInteger)averageAmount
               maxAmount:(NSInteger)maxAmount
{
    _lblScore.text=[NSString stringWithFormat:@"%ld/%ld",(long)averageAmount,(long)maxAmount];
//    if (averageAmount == 0 || maxAmount ==0 ) {
//     _lblScore.text=@"-/-";
//    }
    
    _lblScore.font = [UIFont hfBoldSize:20];

    switch (type) {
        case AverageAndMaxResultTypeQualitySmoke:
            _imageView.image=[UIImage imageNamed:@"game-smoke-quality"];
            _labelTitle.text=@"Сред. / макс.\nкачество дыма";
            break;
            
        case AverageAndMaxResultTypeSpeedSmoke:
            _imageView.image=[UIImage imageNamed:@"game-smoke-speed"];
            _labelTitle.text=@"Сред. / макс.\nскорость дыма";
            break;
            
        case AverageAndMaxResultTypeRoundPoints:
            _imageView.image=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Счет раунда";
            _lblScore.text=[NSString stringWithFormat:@"%ld",(long)averageAmount];

            break;
        
        case AverageAndMaxResultTypePoints:
            _imageView.image=[UIImage imageNamed:@"game-score"];
            _labelTitle.text=@"Сред. / общий счёт\nэтой партии";
            break;
            
        case AverageAndMaxResultTypeVolume:
            _lblScore.text=[NSString stringWithFormat:@"%ld",(long)maxAmount];
//            if ( maxAmount ==0 ) {
//                _lblScore.text=@"-";
//            }
            _imageView.image=[UIImage imageNamed:@"game-volume-result"];
            _labelTitle.text=@"Общий объем дыма в литрах";
            break;
            
        default:
            break;
    }
}

@end
