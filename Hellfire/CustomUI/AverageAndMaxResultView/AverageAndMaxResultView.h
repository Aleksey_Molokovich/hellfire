//
//  AverageAndMaxResultView.h
//  Hellfire
//
//  Created by Алексей Молокович on 28.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AverageAndMaxResultDefines.h"

@interface AverageAndMaxResultView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;

@property (nonatomic) BOOL isDarkThem;

- (void)configureWithType:(AverageAndMaxResultType)type
                 averageAmount:(NSInteger)averageAmount
                     maxAmount:(NSInteger)maxAmount;
@end
