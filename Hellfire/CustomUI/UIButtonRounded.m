//
//  UIButtonRounded.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIButtonRounded.h"

@implementation UIButtonRounded

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {   self.layer.borderColor=[UIColor whiteColor].CGColor;
        self.layer.borderWidth=1;
        self.layer.cornerRadius = self.bounds.size.height / 2.0;
        self.clipsToBounds = YES;
    }
    return self;
}

@end
