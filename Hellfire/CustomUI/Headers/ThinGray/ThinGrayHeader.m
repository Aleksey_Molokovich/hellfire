//
//  ThinGrayHeader.m
//  Hellfire
//
//  Created by Алексей Молокович on 04.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "ThinGrayHeader.h"
#import "UIFont+HF.h"
#import "UIColor+HF.h"

@implementation ThinGrayHeader

- (void)setup
{
    [super setup];
    [self.label setFont:[UIFont hfMediumItalicSize:12]];
    [self.view setBackgroundColor:[UIColor hflightGray]];
}

@end
