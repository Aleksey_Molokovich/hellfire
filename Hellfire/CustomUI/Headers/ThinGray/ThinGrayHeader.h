//
//  ThinGrayHeader.h
//  Hellfire
//
//  Created by Алексей Молокович on 04.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "UILabelForNumber.h"

@interface ThinGrayHeader : BaseView
@property (weak, nonatomic) IBOutlet UILabelForNumber *label;

@end
