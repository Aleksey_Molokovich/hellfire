//
//  UIView_Round.m
//  Hellfire
//
//  Created by Алексей Молокович on 20.08.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UIView_Round.h"

@implementation UIView_Round

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.layer.cornerRadius = self.bounds.size.height / 2.0;
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.clipsToBounds = YES;
    }
    return self;
}

@end
