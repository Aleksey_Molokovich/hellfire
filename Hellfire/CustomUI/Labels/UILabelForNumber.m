//
//  UITextFieldForNumber.m
//  Hellfire
//
//  Created by Алексей Молокович on 21.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "UILabelForNumber.h"
#import "UIFont+HF.h"

@implementation UILabelForNumber

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]) == nil)
        return nil;
    
    self.font = [UIFont hfBoldSize:self.font.pointSize];
    
    
    return self;
}

@end
