//
//  PopUpCellModel.m
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PopUpCellModel.h"

@implementation PopUpCellModel

-(instancetype)initWithAction:(PopUpCellDataAction)action
{
    
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    
    self.action = action;
    switch (action)
    {
        case PopUpCellDataClanRequestSend:
            self.title = @"Вступить";
            break;
        case PopUpCellDataFriendRequestSend:
            self.title = @"Добавить";
            break;
        case PopUpCellDataFriendRequestAccept:
            self.title = @"Принять";
            break;
        case PopUpCellDataFriendRequestReject:
            self.title = @"Отклонить";
            break;
        case PopUpCellDataInviteFriendRequestToHotseat:
            self.title = @"Пригласить в Hotseat";
            break;
        case PopUpCellDataClanRemove:
            self.title = @"Выйти из клана";
            break;
        case PopUpCellDataFriendRemove:
            self.title = @"Удалить";
            break;
        case PopUpCellDataOpenClanInfo:
        case PopUpCellDataOpenPlayerInfo:
            self.title = @"Профиль";
            break;
            
            
        default:
            break;
    }
    
    return self;
}

@end
