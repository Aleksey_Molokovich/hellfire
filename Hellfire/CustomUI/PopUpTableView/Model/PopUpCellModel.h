//
//  PopUpCellModel.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HellfireDefines.h"


@interface PopUpCellModel : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) PopUpCellDataAction action;
- (instancetype)initWithAction:(PopUpCellDataAction)action;
@end
