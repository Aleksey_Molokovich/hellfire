//
//  PopUpTableViewCell.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface PopUpTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
