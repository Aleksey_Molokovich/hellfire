//
//  PopUpTableViewCell.m
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PopUpTableViewCell.h"
#import "PopUpCellModel.h"
#import "UIFont+HF.h"
@implementation PopUpTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _label.font = [UIFont hfRegularSize:14];
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[PopUpCellModel class]])
    {
        return;
    }
    
    PopUpCellModel *datasource = data;
    
    _label.text = datasource.title;
}

@end
