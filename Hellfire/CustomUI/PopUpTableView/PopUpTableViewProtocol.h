//
//  PopUpTableViewProtocol.h
//  Hellfire
//
//  Created by Алексей Молокович on 19.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef PopUpTableViewProtocol_h
#define PopUpTableViewProtocol_h
#import "HellfireDefines.h"

@protocol PopUpTableViewProtocol<NSObject>

- (void)popUpAction:(PopUpCellDataAction)action;

@end
#endif /* PopUpTableViewProtocol_h */
