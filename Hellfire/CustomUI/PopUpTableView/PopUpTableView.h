//
//  PopUpTableView.h
//  Hellfire
//
//  Created by Алексей Молокович on 18.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "PopUpTableViewProtocol.h"

@interface PopUpTableView : BaseView
@property (strong, nonatomic) id<PopUpTableViewProtocol> output;
@end
