//
//  PopUpTableView.m
//  Hellfire
//
//  Created by Алексей Молокович on 18.11.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "PopUpTableView.h"
#import "CellManager.h"
#import "BaseTableViewCell.h"
#import "PopUpCellModel.h"
#import "UIColor+HF.h"
#import <QuartzCore/QuartzCore.h>

static NSString *const kPopUpTableViewCell = @"PopUpTableViewCell";

@interface PopUpTableView() <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<PopUpCellModel*> *datasource;

@end

@implementation PopUpTableView

- (void)setup
{
    [super setup];
    [CellManager registerCellsWithId:@[kPopUpTableViewCell] tableView:self.tableView];
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
//    self.view.layer.borderColor = [UIColor hfGray].CGColor;
//    self.view.layer.borderWidth = 1;
    
}

- (void)configureWithData:(id)data
{
    if (![data isKindOfClass:[NSArray class]])
    {
        return;
    }
    
    self.datasource = data;
    
    [_tableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPopUpTableViewCell forIndexPath:indexPath];
    
    [cell configureWithData:self.datasource[indexPath.row]];
    
    return cell;
}
    
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_datasource count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return  self.frame.size.height/_datasource.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if ([_output respondsToSelector:@selector(popUpAction:)])
    {
         [_output popUpAction:_datasource[indexPath.row].action];
    }
   
}

@end
