//
//  MiniTimerView.h
//  Hellfire
//
//  Created by Алексей Молокович on 03.10.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#import "BaseView.h"
#import "CounterView.h"

@interface MiniTimerView : BaseView
@property (weak, nonatomic) IBOutlet CounterView *counterView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end
