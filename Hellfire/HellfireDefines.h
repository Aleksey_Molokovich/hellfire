//
//  HellfireDefines.h
//  Hellfire
//
//  Created by Алексей Молокович on 21.09.17.
//  Copyright © 2017 Aleksey Molokovich. All rights reserved.
//

#ifndef HellfireDefines_h
#define HellfireDefines_h
#import <Foundation/Foundation.h>

#pragma UserDefults Keys
static NSString *const SessionToken =@"session_token";
static NSString *const DevicePushToken =@"device_token";
static NSString *const DeviceID =@"device_id";

static NSString *const AgreementMessage =@"agreement_message";
static NSString *const AgreementFullText =@"agreement_full_text";

static NSString *const FilterPlayer =@"filter_player";
static NSString *const FilterClan =@"filter_clan";


#pragma NOtifications Keys
static NSString *const HFNotificationChangeAvatar = @"change_avatar";
static NSString *const HFNotificationChangeAccountStatus = @"change_account_status";
static NSString *const HFNotificationUpdateApp = @"update_application";
static NSString *const HFNotificationUnvalidToken = @"unvalid_token";
static NSString *const HFNotificationHasNewFriendRequest = @"new_friend_request";
static NSString *const HFPushNotification = @"push_notificationNone";
static NSString *const HFPushNotificationCancelPeripheralConnection = @"push_notification_cancel_peripheral_connection";

static NSString *const HFPushNotificationNewFriendRequest = @"push_notification_new_friend_request";
static NSString *const HFPushNotificationInviteToHotseat = @"push_notification_invite";
static NSString *const HFPushNotificationHotseatRoundResult = @"push_notification_round_result";
static NSString *const HFPushNotificationHotseatGameResult = @"push_notification_game_result";
static NSString *const HFPushNotificationInviteToHotseatAccept = @"push_notification_invite_accept";
static NSString *const HFPushNotificationInviteToHotseatReject = @"push_notification_invite_reject";
static NSString *const HFUpdatePlayersList = @"update_players_list";
static NSString *const HFUpdateClansList = @"update_clans_list";

static NSString *const HFLaunchAppByPushNotification = @"launch_app_by_push_notificationNone";

static NSString *const HFShowUserProfile = @"showUserProfile";



static NSInteger const numberItemsInPage=25;

typedef NS_ENUM(NSInteger,AccountStateType){
    AccountStateTypeNonRegister,
    AccountStateTypeSuccessfulRegister
};


typedef NS_ENUM(NSInteger,PlayerStatusType){
    PlayerStatusTypeRequest = 0,
    PlayerStatusTypeFriend = 1,
    PlayerStatusTypeOther = 2,
    PlayerStatusTypeI = 3
};

typedef NS_ENUM(NSInteger,PlayerHotseatStatusType){
    PlayerHotseatStatusNone,
    PlayerHotseatStatusMarked,
    PlayerHotseatStatusInvite,
    PlayerHotseatStatusAccept,
    PlayerHotseatStatusReject,
    PlayerHotseatStatusAcceptLock,
    PlayerHotseatStatusLock

};

typedef NS_ENUM(NSInteger,ActionType){
    ActionButtonPress
};

typedef NS_ENUM(NSInteger, CatalogType)
{
    CatalogCountry,
    CatalogCity,
    CatalogCountryEditUserProfile
};


typedef NS_ENUM(NSInteger, CatalogStyleType)
{
    CatalogStyleDark,
    CatalogStyleLight
};

typedef NS_ENUM(NSInteger, CatalogActionType)
{
    CatalogActionRegistration,
    CatalogActionEditUserLocation,
    CatalogActionFilter
};




typedef NS_ENUM(NSInteger,PushNotificationType)
{
    PushNotificationNone,
    PushNotificationFriendRequest,
    PushNotificationFriendAccept,
    PushNotificationNewFriendRequestReject,
    PushNotificationInviteToHotseat,
    PushNotificationAcceptInviteHotseat,
    PushNotificationRejectInviteHotseat
};

typedef NS_ENUM(NSInteger,PopUpCellDataAction){
    PopUpCellDataOpenPlayerInfo,
    PopUpCellDataFriendRequestAccept,
    PopUpCellDataInviteFriendRequestToHotseat,
    PopUpCellDataFriendRequestReject,
    PopUpCellDataFriendRequestSend,
    PopUpCellDataFriendRemove,
    
    PopUpCellDataOpenClanInfo,
    PopUpCellDataClanRequestSend,
    PopUpCellDataClanRemove,
};
#endif /* HellfireDefines_h */
